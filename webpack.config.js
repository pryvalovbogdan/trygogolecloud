const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: {
        authorization: __dirname + "/src/script/authorizationValidation.js",
        index: __dirname + "/src/script/script.js",
        registration: __dirname + "/src/script/registrationValidation.js",
        accountSettings: __dirname + "/src/script/accountSettingValidation.js",
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: '[name].js',
        publicPath: ''
    },
    mode: "development",
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader",
                options: { presets: ["@babel/env"] }
            },
            {
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',

                    }, {
                        loader: 'less-loader',

                    }]

            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    resolve: { extensions: ["*", ".js", ".jsx"] },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: __dirname + "/public/index.html",
            inject: 'body',
            chunks: [ "authorization"]
        }),
        new HtmlWebpackPlugin({
            filename: "table.html",
            template: __dirname + "/public/table.html",
            inject: 'body',
            chunks: ["index"]
        }),
        new HtmlWebpackPlugin({
            filename: "registration.html",
            template: __dirname + "/public/registration.html",
            inject: 'body',
            chunks: ["registration"]
        }),
        new HtmlWebpackPlugin({
            filename: "accountSettings.html",
            template: __dirname + "/public/accountSettings.html",
            inject: 'body',
            chunks: [ "accountSettings"]
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
    ],
    devtool: 'inline-source-map',
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9010,
        index: 'index.html'
    }
};