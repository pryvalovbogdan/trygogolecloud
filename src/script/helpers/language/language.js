import languageBoxIndex from "./indexLanguage";
import autorPage from "./autorizationLanguage";
import accSetPage from "./accountSettingsLanguage";
import regPage from "./registrationLanguage"
export {changeLanguage};
function changeLanguage(page) {
    if (page === "author"){
        var pageCol=autorPage;
    } else if (page === "accSet"){
        var pageCol=accSetPage;
    } else if(page === "index"){
        var pageCol=languageBoxIndex;
    } else if(page==="reg"){
        var pageCol=regPage;
    }
    var language = selectElementLanguage.value;
    if (language==="ar"){
        document.getElementById("direction").setAttribute("dir","rtl");
        sessionStorage.setItem('language', language);
        language="en";
    } else {
        document.getElementById("direction").setAttribute("dir","ltl");
        sessionStorage.setItem('language', language);
    }
    changeText(pageCol[language].text);
    changePlaceHolder(pageCol[language].placeholder)
}
function changeText(obj) {
    for (const key in obj) {
        if(document.getElementById(key)!==null) {
            document.getElementById(key).innerHTML = obj[key];
        }
    }
};
function changePlaceHolder(obj) {
    for (const key in obj) {
        if(document.getElementById(key)!==null) {
            document.getElementById(key).placeholder = obj[key];
        }
    }
};
