export default {
    en:{ text:{
            close: "Return to login",
            regLabel: "Registration",
            loginLabel: "Login*",
            passLabel: "Password*",
            pass2Label: "Confirm password*",
            emailLabel: "E-mail*",
            phoneLabel: "Phone number*",
            keywordLabel: "Keyword",
            keywordNotification:"The keyword needed for password recovery",
            registrationBtn:"Sign up",
        },
        placeholder:{
            login:"Login",
            password1:"Password",
            password2:"Confirm Password",
            email:"E-mail",
            phone:"Phone number",
            keyword: "Keyword",
        }

    },
    ru:{ text: {
            close: "Вернутся к авторизации",
            regLabel: "Регистрация",
            loginLabel: "Логин*",
            passLabel: "Пароль*",
            pass2Label: "Подтвердите пароль*",
            emailLabel: "Почта*",
            phoneLabel: "Телефонный номер*",
            keywordLabel: "Секретное слово",
            keywordNotification:"Секретное слово для восстановления пароля",
            registrationBtn:"Зарегистрироваться",
        },
        placeholder:{
            login:"Введите логин",
            password1:"Введите пароль",
            password2:"Повторите пароль",
            email:"Введите почту",
            phone:"Введите телефонный номер",
            keyword: "Введите секретное слово",
        }
    }
};