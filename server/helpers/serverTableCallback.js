function insertDataIntoStudentsTable(request, response, client) {
  var user = {
    userName: request.body.username,
    age: request.body.age,
    lastName: request.body.lastname,
    city: request.body.city,
    groups_id: request.body.groups_id
  };
  var newUser = `INSERT INTO students( firstname, lastname, age, city, groups_id) VALUES
    ('${user.userName}', '${user.lastName}', '${user.age}', '${user.city}', ${user.groups_id}) RETURNING user_id`;
  client.query(newUser, [],
    function (err, result) {
      if (err) {
        response.status(400).send("error")
      }
      response.json(result.rows)
    })

}

function updateDataIntoStudentsTable(request, response, client) { // TODO check update value in client
  var userID = {
    id: request.body.id
  };
  var queryColomn = [
    "firstname",
    "lastname",
    "age",
    "city"
  ];

  var queryComand = "";

  var valueCounter = 0;
  var counterLink = 1;

  var user = {
    username: request.body.username,
    lastname: request.body.lastname,
    age: request.body.age,
    city: request.body.city
  };

  var upgradeSQL = [];

  Object.keys(user).forEach(function (key) {
    if (!(this[key].length === 0)) {
      upgradeSQL.push(`${this[key]}`);
      // if ()
      queryComand += queryColomn[valueCounter] + "= $" + counterLink + ",";

      counterLink++
    }
    valueCounter++;
  }, user);

  queryComand = queryComand.substring(0, queryComand.length - 1);
  console.log(upgradeSQL, queryComand);

  client.query(`UPDATE students SET ${queryComand} WHERE user_id = ${userID.id}`,
    upgradeSQL,
    function (err, result) {
      if (err) {
        response.status(401).send("error")
      } else {
        response.status(200).send("ok")
      }
    });
}

function  deleteFromStudentTable(request, response, client) {
  var id = request.body.id;
  client.query(`DELETE FROM students WHERE user_id = ${id}`, [], function (err, result) {
    if (err) {
      response.status(401).send("error")
    } else {
      response.status(200).send("ok")
    }
  });
}

function getGroupOfStudents (request, response, client)  {
  var userId = +request.body.name;
  client.query(`SELECT * FROM students WHERE groups_id = ${userId} ORDER BY user_id;`, [], function (err, result) {
    response.json(result.rows);
  });
}

function getAllGroups(request, response, client) {
  client.query(`SELECT * FROM groups WHERE teacher_id = ${request.body.teachers_id};`, [], function (err, result) {
    response.json(result.rows);
  });
}

function deleteGroup(request, response, client) {
  var id = request.body.groupId;
  client.query(`DELETE FROM students WHERE groups_id = ${id}`, [], function (err, result) {
    if (err) {
      console.log("error")
    } else {
      console.log("ok")
    }
  });
  client.query(`DELETE FROM groups WHERE groups_id = ${id}`, [], function (err, result) {
    if (err) {
      response.status(401).send("error")
    } else {
      response.status(200).send("ok")
    }
  });
}

function deleteAllStudentFromGroup(request, response, client) {
  var id = request.body.groupId;
  client.query(`DELETE FROM students WHERE groups_id = ${id}`, [], function (err, result) {
    if (err) {
      response.status(401).send("error")
    } else {
      response.status(200).send("ok")
    }
  });
}

function updateGroupName(request, response, client) {
  client.query(`UPDATE groups SET groupname = '${request.body.name}' WHERE groups_id = ${request.body.id};`, [], (err, res) => {
    if (err) {
      response.status(502).send("SERVER ERROR");
    } else {
      response.status(200).send("ok");
    }
  });
}

function resetSettings(request, response, client) {
  client.query(`DELETE FROM students WHERE user_id >= 1`, [], function (err, result) {
    if (err) {
      console.log("error") // TODO delete console log
    } else {
      console.log("ok")
    }
  });
  client.query(`DELETE FROM groups WHERE groups_id >=1`, [], function (err, result) {
    if (err) {
      response.status(401).send("error")
    } else {
      response.status(200).send("ok")
    }
  });
}

module.exports = {
  insertDataIntoStudentsTable,
  updateDataIntoStudentsTable,
  deleteFromStudentTable,
  getGroupOfStudents,
  getAllGroups,
  deleteGroup,
  deleteAllStudentFromGroup,
  updateGroupName,
  resetSettings,
};