var express = require('express');
var app = express();
var path = require('path');
var fs = require('fs');
var bodyParser = require('body-parser');
var constants = require('./helpers/constants');
var serverCallback = require('./helpers/serverCallback');
var serverTableCallback = require('./helpers/serverTableCallback');
// var fileUpload = require('express-fileupload');
var authorized;
var teacherId;

app.use(express.static(path.join(__dirname, 'public')));///public
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json({limit: '10mb', extended: true}));

const {Client} = require('pg');
const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'postgres',
    password: 'postgres',
    port: 5432,
    // connectionString: process.env.DATABASE_URL,
    // ssl: true TODO ssl sacrifice
});

client.connect(function (err) {
    console.log("Connected!");
});

app.listen(constants.port, function () {
    console.log("port: " + constants.port)
});
app.post(constants.routing.authorization, (request, response) => serverCallback.authorization(request, response, client));
app.post(constants.routing.registration, (request, response) =>  serverCallback.registration(request, response, client));
app.post(constants.routing.forgottenPassword, (request, response) => serverCallback.checkForgottenPassword(request, response, client));
app.post(constants.routing.groupStudent, (request, response) => serverTableCallback.getGroupOfStudents(request, response, client)); // TODO structure fix
app.post(constants.routing.getAllGroups, (request, response) => serverTableCallback.getAllGroups(request, response, client));
app.get(constants.routing.accountSetting, (request, response) => serverCallback.getTeacherInfo(request, response, client));
app.post(constants.routing.sendImage, (request, response) => serverCallback.updateTeacherIcon(request, response, client));
app.post(constants.routing.accountUpdate, (request, response) => serverCallback.updateTeacherData(request, response, client));
app.post(constants.routing.groups, (request, response) => serverCallback.insertIntoGroups(request, response, client));
app.post(constants.routing.table, async (request, response) => serverTableCallback.insertDataIntoStudentsTable(request, response, client));
app.post(constants.routing.update, (request, response) => serverTableCallback.updateDataIntoStudentsTable(request, response, client));
app.post(constants.routing.delete, (request, response) => serverTableCallback.deleteFromStudentTable(request, response, client));
app.post(constants.routing.deleteGroup, (request, response) => serverTableCallback.deleteGroup(request, response, client));
app.post(constants.routing.studentClear, (request, response) => serverTableCallback.deleteAllStudentFromGroup(request, response, client));
app.post(constants.routing.updateGroup, (request, response) => serverTableCallback.updateGroupName(request, response, client));
app.post(constants.routing.resetSettings, (request, response) => serverTableCallback.resetSettings(request, response, client));