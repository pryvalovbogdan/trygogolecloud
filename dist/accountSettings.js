/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/script/accountSettingValidation.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./server/helpers/constants.js":
/*!*************************************!*\
  !*** ./server/helpers/constants.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {module.exports = {
  port: process.env.PORT || 3000,
  routing: {
    authorization: '/authorization',
    registration: '/registration',
    groupStudent: '/groupStudent',
    getAllGroups: '/getAllGroups',
    accountSetting: '/accountSetting',
    groups: '/groups',
    table: '/',
    update: '/update',
    "delete": '/delete',
    accountUpdate: '/account-update',
    forgottenPassword: '/forgotten-password',
    deleteGroup: '/delete-group',
    studentClear: '/student-clear',
    updateGroup: '/update-group',
    sendImage: '/send-image',
    resetSettings: '/reset-settings'
  }
};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/assets/back-arrow.svg":
/*!***********************************!*\
  !*** ./src/assets/back-arrow.svg ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "004d176c2f765a34a789bf252771cd74.svg");

/***/ }),

/***/ "./src/assets/logout.svg":
/*!*******************************!*\
  !*** ./src/assets/logout.svg ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "495aece0584b27cd7d82f8a1187037a0.svg");

/***/ }),

/***/ "./src/script/accountSettingValidation.js":
/*!************************************************!*\
  !*** ./src/script/accountSettingValidation.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_language_language_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers/language/language.js */ "./src/script/helpers/language/language.js");
/* harmony import */ var _helpers_scriptValidation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers/scriptValidation */ "./src/script/helpers/scriptValidation.js");
/* harmony import */ var _style_accountSettings_less__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../style/accountSettings.less */ "./src/style/accountSettings.less");
/* harmony import */ var _style_accountSettings_less__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_accountSettings_less__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _style_header_less__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../style/header.less */ "./src/style/header.less");
/* harmony import */ var _style_header_less__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_style_header_less__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _helpers_ballsGame__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./helpers/ballsGame */ "./src/script/helpers/ballsGame.js");
/* harmony import */ var _helpers_ballsGame__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_helpers_ballsGame__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _src_assets_back_arrow_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../src/assets/back-arrow.svg */ "./src/assets/back-arrow.svg");
/* harmony import */ var _src_assets_logout_svg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../src/assets/logout.svg */ "./src/assets/logout.svg");
/* harmony import */ var _helpers_const__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./helpers/const */ "./src/script/helpers/const.js");





var constants = __webpack_require__(/*! ../../server/helpers/constants */ "./server/helpers/constants.js");

 // import logoTeacher from './images/batman.png';





var fullscreen = document.getElementById('fullscreen');
var exitCabinet = document.getElementById('exitCabinet');
var teacherIcon = document.getElementById('teacherIcon'); // add custom icon
// function showInputDownloadImage

exitCabinet.addEventListener('click', function () {
  document.location.href = "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_7__["webpackLocalHost"], "/index.html");
});
var login = document.getElementById("login");

login.oninput = function () {
  Object(_helpers_scriptValidation__WEBPACK_IMPORTED_MODULE_1__["checkValueLength"])(this, 14);
};

var password = document.getElementById("password1");

password.oninput = function () {
  Object(_helpers_scriptValidation__WEBPACK_IMPORTED_MODULE_1__["checkValueLength"])(this, 25);
};

var password2 = document.getElementById("password2");

password2.oninput = function () {
  Object(_helpers_scriptValidation__WEBPACK_IMPORTED_MODULE_1__["checkValueLength"])(this, 25);
};

var email = document.getElementById("email");

email.oninput = function () {
  Object(_helpers_scriptValidation__WEBPACK_IMPORTED_MODULE_1__["checkValueLength"])(this, 25);
};

var phone = document.getElementById("phone");

phone.oninput = function () {
  Object(_helpers_scriptValidation__WEBPACK_IMPORTED_MODULE_1__["checkValueLength"])(this, 13);
};

var aboutMyself = document.getElementById("aboutMyself");
aboutMyself.addEventListener('keydown', checkCurrentLengthText);
var aboutMyselfCounter = 500;
var textAreaCount = document.getElementById("textAreaCount");
var changeInputs = document.getElementById("changeInputs");
var SaveBtn = document.getElementById("SaveBtn");
changeInputs.addEventListener('click', function () {
  changeButtonsDisplay(SaveBtn, changeInputs);

  if (selectElementLanguage.value != "ru") {
    document.getElementById("loginTitle").innerText = "New Login";
    document.getElementById("password1Title").innerText = "New Password";
    document.getElementById("emailTitle").innerText = "New Email";
    document.getElementById("phoneTitle").innerText = "New Phone";
  } else {
    document.getElementById("loginTitle").innerText = "Новый логин";
    document.getElementById("password1Title").innerText = "Новый пароль";
    document.getElementById("emailTitle").innerText = "Новая почта";
    document.getElementById("phoneTitle").innerText = "Новый телефон";
  }
});
SaveBtn.addEventListener("click", function () {
  changeButtonsDisplay(SaveBtn, changeInputs);
  getValidation();

  if (selectElementLanguage.value != "ru") {
    document.getElementById("loginTitle").innerText = "Login";
    document.getElementById("password1Title").innerText = "Password";
    document.getElementById("emailTitle").innerText = "Email";
    document.getElementById("phoneTitle").innerText = "Phone";
  } else {
    document.getElementById("loginTitle").innerText = "Логин";
    document.getElementById("password1Title").innerText = "Пароль";
    document.getElementById("emailTitle").innerText = "Почта";
    document.getElementById("phoneTitle").innerText = "Телефон";
  }
});
var teacher = getUserData();
setTimeout(fillInput, 800);
var closeBtn = document.getElementById("close");
closeBtn.addEventListener('click', function () {
  document.location.href = "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_7__["webpackLocalHost"], "/table.html");
});
var selectElementLanguage = document.getElementById("selectElementLanguage");

selectElementLanguage.onchange = function () {
  Object(_helpers_language_language_js__WEBPACK_IMPORTED_MODULE_0__["changeLanguage"])("accSet");
};

function main() {
  if (sessionStorage.getItem("language") != null) {
    selectElementLanguage.value = sessionStorage.getItem("language");
    Object(_helpers_language_language_js__WEBPACK_IMPORTED_MODULE_0__["changeLanguage"])("accSet");
  } else {
    Object(_helpers_language_language_js__WEBPACK_IMPORTED_MODULE_0__["changeLanguage"])("accSet");
  }
}

function getUserData() {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_7__["localhostServ"]).concat(constants.routing.accountSetting));
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.send();

  xhr.onload = function () {
    teacher = JSON.parse(this.response);
    console.log(teacher);
  };
}

function fillInput() {
  document.getElementById("login").value = teacher[0].login;
  document.getElementById("email").value = teacher[0].email;
  document.getElementById("password1").value = teacher[0].password;
  document.getElementById("password2").value = teacher[0].password;
  document.getElementById("phone").value = teacher[0].phone_number;
  document.getElementById("aboutMyself").value = teacher[0].about_myself; // document.getElementById("teacherIcon").src = teacher[0].teacher_icon;
}

var err = "";

function getValidation() {
  // let elementValue = new User();
  var elementValue = {
    login: login.value.replace(/\s/g, ''),
    password: password.value,
    password2: password2.value,
    email: email.value,
    phone: phone.value.replace(/[+()-/\s]/g, ''),
    aboutMyself: aboutMyself = document.getElementById("aboutMyself").value,
    teachers_id: teacher[0].teachers_id // this.icon =  document.getElementById("icon").value;

  };
  console.log(elementValue);
  var message = document.getElementById("message");

  if (checkEmail(elementValue.email) && checklogin(elementValue.login) && checkPass(elementValue.password, elementValue.password2) && checkPhone(elementValue.phone, "380")) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_7__["localhostServ"]).concat(constants.routing.accountUpdate));
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(elementValue));
    message.innerHTML = "Changes included";
    console.log(elementValue); // setTimeout(document.location.href = `${webpackLocalHost}/table.html`, 1500);
  } else {
    message.innerHTML = err;
  }
}

function checklogin(login) {
  if (login.length < 1 || !isNaN(Number(login[0]))) {
    err = "Некорректный логин";
    return false;
  }

  return true;
}

function checkPass(pass1, pass2) {
  if (pass1.length < 5 || pass1 !== pass2) {
    err = "Некорректный пароль";
    return false;
  }

  return true;
}

function checkPhone(phone, countriCode) {
  if (phone.length !== 12 || phone.substring(0, 3) !== countriCode) {
    err = "Некорректныйтелефон";
    return false;
  }

  return true;
}

function checkEmail(email) {
  if (email.includes("@")) {
    var domen = email.substring(email.indexOf("@"), email.length);

    if (domen.length > 2) {
      return true;
    }
  }

  err = "Некорректный e-mail";
  return false;
}

function checkCurrentLengthText() {
  if (aboutMyself.value.length > aboutMyselfCounter) {
    return;
  }

  textAreaCount.innerText = String(aboutMyselfCounter - aboutMyself.value.length);
} //  toggle buttons change and save


function inputTogglerDisabled(className) {
  // 'custom-input'
  var inputCollection = document.getElementsByClassName(className);
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = inputCollection[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var item = _step.value;
      item.disabled === true ? item.disabled = false : item.disabled = true;
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }
}

function changeButtonsDisplay(button1, button2) {
  if (button2.style.display === 'none') {
    button1.style.display = 'none';
    button2.style.display = 'block';
  } else {
    button1.style.display = 'block';
    button2.style.display = 'none';
  }

  inputTogglerDisabled('custom-input');
} //fullscreen logic


fullscreen.addEventListener("click", function (e) {
  document.getElementById("direction").requestFullscreen();
});
var fileInput = document.getElementById("file-input");
fileInput.addEventListener("change", function (e) {
  console.log(e.target.files[0]);
  var fileReader = new FileReader();
  fileReader.readAsDataURL(e.target.files[0]);

  fileReader.onload = function (event) {
    var data = {
      img: event.target.result
    };
    var datasend = JSON.stringify(data);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_7__["localhostServ"]).concat(constants.routing.sendImage));
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(datasend);

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var img = document.getElementById("teacherIcon");
        localStorage.setItem("img", "".concat(JSON.parse(this.response)));
        img.src = "".concat(JSON.parse(this.response));
      }
    };
  };
});
document.addEventListener("DOMContentLoaded", function (e) {
  main();

  if (localStorage.getItem("img") !== null) {
    document.getElementById("teacherIcon").src = localStorage.getItem("img");
  }
});
var logoHome = document.getElementById("logoHome");
logoHome.addEventListener('click', function () {
  document.location.href = "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_7__["webpackLocalHost"], "/table.html");
});

/***/ }),

/***/ "./src/script/helpers/ballsGame.js":
/*!*****************************************!*\
  !*** ./src/script/helpers/ballsGame.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var arrayBalls = [];
var interval = undefined;
var canvas = document.getElementById("main-canvas");
var ctx = canvas.getContext('2d');
var start = document.getElementById('start');
var clear = document.getElementById('clear');
start.addEventListener('click', function () {
  startGame();
  toggleStartDisabled();
});
clear.addEventListener('click', function () {
  stop(interval);
  toggleClearDisabled();
}); // toggle class start and clear

function toggleStartDisabled() {
  start.setAttribute('disabled', 'true');
  clear.removeAttribute('disabled');
}

function toggleClearDisabled() {
  clear.setAttribute('disabled', 'true');
  start.removeAttribute('disabled');
}

function startGame() {
  interval = setInterval(function () {
    movement(arrayBalls);
  }, 10);
  return interval;
}

function stop() {
  clearInterval(interval);
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  arrayBalls = [];
  interval = undefined;
} // random circle


function randomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';

  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }

  return color;
}

function randomMovement() {
  return Math.floor(Math.random() * (5 - -5)) + -5;
}

function randomRadius() {
  return Math.floor(Math.random() * (55 - 5)) + 5;
} // View constructor


function View() {
  // this.balls = [];
  this.canvas = document.getElementById("main-canvas");
  this.ctx = this.canvas.getContext('2d');
  this.canvas.addEventListener('mousedown', this.onMouseDown.bind(this));
}

View.prototype.onMouseDown = function (e) {
  if (isNaN(interval)) {
    return;
  }

  var offsetX = e.offsetX,
      offsetY = e.offsetY;
  var x = offsetX;
  var y = offsetY;
  var radius = randomRadius();

  if (x - radius <= this.canvas.width) {
    x = x + radius;
  }

  if (x + radius >= this.canvas.width) {
    x = x - radius * 2;
  }

  if (y - radius <= this.canvas.height) {
    y = y + radius;
  }

  if (y + radius >= this.canvas.height) {
    y = y - radius * 2;
  }

  var ball = new Ball(x, y, radius); // this.balls.push(ball);

  arrayBalls.push(ball);
};

function drawBall(ball) {
  ctx.beginPath();
  ctx.arc(ball.x, ball.y, ball.radius, 0, 2 * Math.PI);
  ctx.fillStyle = ball.color;
  ctx.closePath();
  ctx.fill();
}

function movement(balls) {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  balls.forEach(function (ball) {
    if (ball.x + ball.speedX > canvas.width - ball.radius || ball.x + ball.speedX < ball.radius) {
      ball.speedX = -ball.speedX;
    }

    if (ball.y + ball.speedY > canvas.height - ball.radius || ball.y + ball.speedY < ball.radius) {
      ball.speedY = -ball.speedY;
    }

    ball.x = ball.x + ball.speedX;
    ball.y = ball.y + ball.speedY;
    drawBall(ball);
  });
} // Ball constructor


function Ball(x, y, radius) {
  this.x = x;
  this.y = y;
  this.radius = radius;
  this.color = randomColor();
  this.speedX = randomMovement();
  this.speedY = randomMovement();
} // init of app


var init = function init() {
  var view = new View();
};

document.addEventListener("DOMContentLoaded", init);

/***/ }),

/***/ "./src/script/helpers/const.js":
/*!*************************************!*\
  !*** ./src/script/helpers/const.js ***!
  \*************************************/
/*! exports provided: localhostServ, webpackLocalHost */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "localhostServ", function() { return localhostServ; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "webpackLocalHost", function() { return webpackLocalHost; });
var localhostServ = 'http://35.205.148.230/server/';
var webpackLocalHost = 'http://35.205.148.230/'; //
// const localhostServ = "http://localhost:3000";
// const webpackLocalHost = "http://localhost:9000";



/***/ }),

/***/ "./src/script/helpers/language/accountSettingsLanguage.js":
/*!****************************************************************!*\
  !*** ./src/script/helpers/language/accountSettingsLanguage.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      myAccount: "My Account",
      iconTitle: "Change your avatar",
      loginTitle: "Login",
      password1Title: "Password",
      password2Title: "Check password",
      emailTitle: "Email",
      phoneTitle: "Phone",
      aboutMyselfTitle: "About Myself",
      SaveBtn: "Save changes",
      close: "Close",
      chat: "Chat",
      chatWrapper: "Coming soon",
      gameBounce: "Bounce",
      start: "Start game",
      clear: "Clear",
      changeInputs: "Сhange"
    }
  },
  ru: {
    text: {
      myAccount: "Мой аккаунт",
      iconTitle: "Изменить свой аватар",
      loginTitle: "Логин",
      password1Title: "Пароль",
      password2Title: "Проверка пароля",
      emailTitle: "емейл",
      phoneTitle: "Телефон",
      aboutMyselfTitle: "Обо мне",
      SaveBtn: "Сохранить изменения",
      close: "Закрыть",
      chat: "Чат",
      chatWrapper: "В разработке)",
      gameBounce: "Игра Шарики",
      start: "Начать игру",
      clear: "Очистить",
      changeInputs: "Изменить"
    }
  }
});

/***/ }),

/***/ "./src/script/helpers/language/autorizationLanguage.js":
/*!*************************************************************!*\
  !*** ./src/script/helpers/language/autorizationLanguage.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      textAuthorization: "Authorization",
      labelLogin: "Login:",
      labelPassword: "Password:",
      registrationBtnAvt: "Registration",
      logInBtn: "Log In",
      forgottenPassword: "Forgotten Password",
      textForgottenPassword: "Password recovery",
      forgottenLabelLogin: "Login:",
      forgottenLabelPassword: "Keyword:",
      forgottenPassLabel: "Your password:",
      getPassword: "Get Password"
    },
    placeholder: {
      loginAvt: "write your login",
      passwordAvt: "write your password",
      forgottenLoginAvt: "write your login ",
      kayWordAvt: "write your keyword",
      forgottenPasswordMessage: "Your password"
    }
  },
  ru: {
    text: {
      textAuthorization: "Авторизация",
      labelLogin: "Логин:",
      labelPassword: "Пароль:",
      registrationBtnAvt: "Регистрация",
      logInBtn: "Войти",
      forgottenPassword: "Забыл пароль",
      textForgottenPassword: "Восстановление пароля",
      forgottenLabelLogin: "Логин:",
      forgottenLabelPassword: "Секретное слово:",
      forgottenPassLabel: "Ваш пароль:",
      getPassword: "Получить пароль"
    },
    placeholder: {
      loginAvt: "Введите ваш логин",
      passwordAvt: "Введите ваш пароль",
      forgottenLoginAvt: "Введите ваш логин",
      kayWordAvt: "Введите секретное слово",
      forgottenPasswordMessage: "Тут будет ваш пароль"
    }
  }
});

/***/ }),

/***/ "./src/script/helpers/language/indexLanguage.js":
/*!******************************************************!*\
  !*** ./src/script/helpers/language/indexLanguage.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      controlPanel: "Control Panel",
      exitCabinet: "Exit",
      addstudent: "Add student",
      labelName: "Name",
      labelLastname: "Surname",
      labelAge: "Age",
      labelCity: "City",
      Create: "Create",
      tableName: "Name",
      tableLastname: "Lastname",
      tableAge: "Age",
      tableCity: "City",
      tableButtons: "Button",
      clearInput: "Clear",
      printLabel: "Enter value",
      resultLabel: "Result",
      convertButton: "Convert",
      printLabel2: "Enter value",
      resultLabel2: "Result",
      getCurrency: "Convert",
      converterTitle: "Converter",
      btnChangeLength: "Length converter",
      btnChangeMoney: "Money converter",
      meter1: "Meter",
      verst1: "Verst",
      mile1: "Mile",
      foot1: "Foot",
      yard1: "Yard",
      meter2: "Meter",
      verst2: "Verst",
      mile2: "Mile",
      foot2: "Foot",
      yard2: "Yard",
      modes1: "Modes",
      students1: "Students",
      paint1: "Paint",
      converter1: "Converter",
      calculator1: "Calculator",
      studentsTitle: "Students"
    },
    placeholder: {
      Name: "Write name",
      Lastname: "Write lastname",
      Age: "Write age",
      City: "Write city",
      inputConverter: "Enter value",
      outputConverter: "Result",
      inputCurrency: "Enter value",
      outputResult: "Result",
      contrPanel: "Control Panel",
      lineThick: "Line thickness",
      lineCol: "Line color",
      clear: "Clear",
      labelPaint: "Paint",
      textSettings: "Settings",
      notoficationLabel: "Notifications",
      languageLabel: "Choose language",
      reset: "Reset"
    }
  },
  ru: {
    text: {
      controlPanel: "Панель управления",
      exitCabinet: "Выйти",
      addstudent: "Добавить Студента",
      labelName: "Имя",
      labelLastname: "Фамилия",
      labelAge: "Возраст",
      labelCity: "Город",
      Create: "Создать",
      tableName: "Имя",
      tableLastname: "Фамилия",
      tableAge: "Возраст",
      tableCity: "Город",
      tableButtons: "Кнопки",
      clearInput: "Очистить",
      printLabel: "Введите значение",
      resultLabel: "Результат",
      convertButton: "Конвертировать",
      printLabel2: "Введите значение",
      resultLabel2: "Результат",
      getCurrency: "Конвертировать",
      converterTitle: "Конвертер",
      btnChangeLength: "Конвертер расстояния",
      btnChangeMoney: "Конвертер валют",
      meter1: "Метр",
      verst1: "Верста",
      mile1: "Миля",
      foot1: "Фут",
      yard1: "Ярд",
      meter2: "Метр",
      verst2: "Верста",
      mile2: "Миля",
      foot2: "Фут",
      yard2: "Ярд",
      modes1: "Моды",
      students1: "Студент",
      paint1: "Рисовалка",
      converter1: "Конвертер",
      calculator1: "Калькулятор",
      studentsTitle: "Студенты",
      contrPanel: "Панель управления",
      lineThick: "Толщина линии",
      lineCol: "Цвет линии",
      clear: "Очистить",
      labelPaint: "Рисовалка",
      textSettings: "Настройки",
      notoficationLabel: "Уведомления",
      languageLabel: "Выбрать язык",
      reset: "Сбросить"
    },
    placeholder: {
      Name: "Введите имя",
      Lastname: "Введите фамилию",
      Age: "Введите возраст",
      City: "Введите город",
      inputConverter: "Введите значение",
      outputConverter: "Результат",
      inputCurrency: "Введите значение",
      outputResult: "Результат"
    }
  }
});

/***/ }),

/***/ "./src/script/helpers/language/language.js":
/*!*************************************************!*\
  !*** ./src/script/helpers/language/language.js ***!
  \*************************************************/
/*! exports provided: changeLanguage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeLanguage", function() { return changeLanguage; });
/* harmony import */ var _indexLanguage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./indexLanguage */ "./src/script/helpers/language/indexLanguage.js");
/* harmony import */ var _autorizationLanguage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./autorizationLanguage */ "./src/script/helpers/language/autorizationLanguage.js");
/* harmony import */ var _accountSettingsLanguage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accountSettingsLanguage */ "./src/script/helpers/language/accountSettingsLanguage.js");
/* harmony import */ var _registrationLanguage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registrationLanguage */ "./src/script/helpers/language/registrationLanguage.js");






function changeLanguage(page) {
  if (page === "author") {
    var pageCol = _autorizationLanguage__WEBPACK_IMPORTED_MODULE_1__["default"];
  } else if (page === "accSet") {
    var pageCol = _accountSettingsLanguage__WEBPACK_IMPORTED_MODULE_2__["default"];
  } else if (page === "index") {
    var pageCol = _indexLanguage__WEBPACK_IMPORTED_MODULE_0__["default"];
  } else if (page === "reg") {
    var pageCol = _registrationLanguage__WEBPACK_IMPORTED_MODULE_3__["default"];
  }

  var language = selectElementLanguage.value;

  if (language === "ar") {
    document.getElementById("direction").setAttribute("dir", "rtl");
    sessionStorage.setItem('language', language);
    language = "en";
  } else {
    document.getElementById("direction").setAttribute("dir", "ltl");
    sessionStorage.setItem('language', language);
  }

  changeText(pageCol[language].text);
  changePlaceHolder(pageCol[language].placeholder);
}

function changeText(obj) {
  for (var key in obj) {
    if (document.getElementById(key) !== null) {
      document.getElementById(key).innerHTML = obj[key];
    }
  }
}

;

function changePlaceHolder(obj) {
  for (var key in obj) {
    if (document.getElementById(key) !== null) {
      document.getElementById(key).placeholder = obj[key];
    }
  }
}

;

/***/ }),

/***/ "./src/script/helpers/language/registrationLanguage.js":
/*!*************************************************************!*\
  !*** ./src/script/helpers/language/registrationLanguage.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      close: "Return to login",
      regLabel: "Registration",
      loginLabel: "Login*",
      passLabel: "Password*",
      pass2Label: "Confirm password*",
      emailLabel: "E-mail*",
      phoneLabel: "Phone number*",
      keywordLabel: "Keyword",
      keywordNotification: "The keyword needed for password recovery",
      registrationBtn: "Sign up"
    },
    placeholder: {
      login: "Login",
      password1: "Password",
      password2: "Confirm Password",
      email: "E-mail",
      phone: "Phone number",
      keyword: "Keyword"
    }
  },
  ru: {
    text: {
      close: "Вернутся к авторизации",
      regLabel: "Регистрация",
      loginLabel: "Логин*",
      passLabel: "Пароль*",
      pass2Label: "Подтвердите пароль*",
      emailLabel: "Почта*",
      phoneLabel: "Телефонный номер*",
      keywordLabel: "Секретное слово",
      keywordNotification: "Секретное слово для восстановления пароля",
      registrationBtn: "Зарегистрироваться"
    },
    placeholder: {
      login: "Введите логин",
      password1: "Введите пароль",
      password2: "Повторите пароль",
      email: "Введите почту",
      phone: "Введите телефонный номер",
      keyword: "Введите секретное слово"
    }
  }
});

/***/ }),

/***/ "./src/script/helpers/scriptValidation.js":
/*!************************************************!*\
  !*** ./src/script/helpers/scriptValidation.js ***!
  \************************************************/
/*! exports provided: checkOnlyNumbers, checkValueLength */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkOnlyNumbers", function() { return checkOnlyNumbers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkValueLength", function() { return checkValueLength; });
function checkOnlyNumbers() {
  this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
  checkValueLength(this, 3);
}

function checkValueLength(input, maxLength) {
  if (input.value.length >= maxLength) {
    input.value = input.value.substr(0, maxLength);
  }
}



/***/ }),

/***/ "./src/style/accountSettings.less":
/*!****************************************!*\
  !*** ./src/style/accountSettings.less ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/style/header.less":
/*!*******************************!*\
  !*** ./src/style/header.less ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Byb2Nlc3MvYnJvd3Nlci5qcyIsIndlYnBhY2s6Ly8vLi9zZXJ2ZXIvaGVscGVycy9jb25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9iYWNrLWFycm93LnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2xvZ291dC5zdmciLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC9hY2NvdW50U2V0dGluZ1ZhbGlkYXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC9oZWxwZXJzL2JhbGxzR2FtZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0L2hlbHBlcnMvY29uc3QuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC9oZWxwZXJzL2xhbmd1YWdlL2FjY291bnRTZXR0aW5nc0xhbmd1YWdlLmpzIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHQvaGVscGVycy9sYW5ndWFnZS9hdXRvcml6YXRpb25MYW5ndWFnZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0L2hlbHBlcnMvbGFuZ3VhZ2UvaW5kZXhMYW5ndWFnZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0L2hlbHBlcnMvbGFuZ3VhZ2UvbGFuZ3VhZ2UuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC9oZWxwZXJzL2xhbmd1YWdlL3JlZ2lzdHJhdGlvbkxhbmd1YWdlLmpzIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHQvaGVscGVycy9zY3JpcHRWYWxpZGF0aW9uLmpzIiwid2VicGFjazovLy8uL3NyYy9zdHlsZS9hY2NvdW50U2V0dGluZ3MubGVzcz9hYzAyIiwid2VicGFjazovLy8uL3NyYy9zdHlsZS9oZWFkZXIubGVzcz8yNTk4Il0sIm5hbWVzIjpbIm1vZHVsZSIsImV4cG9ydHMiLCJwb3J0IiwicHJvY2VzcyIsImVudiIsIlBPUlQiLCJyb3V0aW5nIiwiYXV0aG9yaXphdGlvbiIsInJlZ2lzdHJhdGlvbiIsImdyb3VwU3R1ZGVudCIsImdldEFsbEdyb3VwcyIsImFjY291bnRTZXR0aW5nIiwiZ3JvdXBzIiwidGFibGUiLCJ1cGRhdGUiLCJhY2NvdW50VXBkYXRlIiwiZm9yZ290dGVuUGFzc3dvcmQiLCJkZWxldGVHcm91cCIsInN0dWRlbnRDbGVhciIsInVwZGF0ZUdyb3VwIiwic2VuZEltYWdlIiwicmVzZXRTZXR0aW5ncyIsImNvbnN0YW50cyIsInJlcXVpcmUiLCJmdWxsc2NyZWVuIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsImV4aXRDYWJpbmV0IiwidGVhY2hlckljb24iLCJhZGRFdmVudExpc3RlbmVyIiwibG9jYXRpb24iLCJocmVmIiwid2VicGFja0xvY2FsSG9zdCIsImxvZ2luIiwib25pbnB1dCIsImNoZWNrVmFsdWVMZW5ndGgiLCJwYXNzd29yZCIsInBhc3N3b3JkMiIsImVtYWlsIiwicGhvbmUiLCJhYm91dE15c2VsZiIsImNoZWNrQ3VycmVudExlbmd0aFRleHQiLCJhYm91dE15c2VsZkNvdW50ZXIiLCJ0ZXh0QXJlYUNvdW50IiwiY2hhbmdlSW5wdXRzIiwiU2F2ZUJ0biIsImNoYW5nZUJ1dHRvbnNEaXNwbGF5Iiwic2VsZWN0RWxlbWVudExhbmd1YWdlIiwidmFsdWUiLCJpbm5lclRleHQiLCJnZXRWYWxpZGF0aW9uIiwidGVhY2hlciIsImdldFVzZXJEYXRhIiwic2V0VGltZW91dCIsImZpbGxJbnB1dCIsImNsb3NlQnRuIiwib25jaGFuZ2UiLCJjaGFuZ2VMYW5ndWFnZSIsIm1haW4iLCJzZXNzaW9uU3RvcmFnZSIsImdldEl0ZW0iLCJ4aHIiLCJYTUxIdHRwUmVxdWVzdCIsIm9wZW4iLCJsb2NhbGhvc3RTZXJ2Iiwic2V0UmVxdWVzdEhlYWRlciIsInNlbmQiLCJvbmxvYWQiLCJKU09OIiwicGFyc2UiLCJyZXNwb25zZSIsImNvbnNvbGUiLCJsb2ciLCJwaG9uZV9udW1iZXIiLCJhYm91dF9teXNlbGYiLCJlcnIiLCJlbGVtZW50VmFsdWUiLCJyZXBsYWNlIiwidGVhY2hlcnNfaWQiLCJtZXNzYWdlIiwiY2hlY2tFbWFpbCIsImNoZWNrbG9naW4iLCJjaGVja1Bhc3MiLCJjaGVja1Bob25lIiwic3RyaW5naWZ5IiwiaW5uZXJIVE1MIiwibGVuZ3RoIiwiaXNOYU4iLCJOdW1iZXIiLCJwYXNzMSIsInBhc3MyIiwiY291bnRyaUNvZGUiLCJzdWJzdHJpbmciLCJpbmNsdWRlcyIsImRvbWVuIiwiaW5kZXhPZiIsIlN0cmluZyIsImlucHV0VG9nZ2xlckRpc2FibGVkIiwiY2xhc3NOYW1lIiwiaW5wdXRDb2xsZWN0aW9uIiwiZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSIsIml0ZW0iLCJkaXNhYmxlZCIsImJ1dHRvbjEiLCJidXR0b24yIiwic3R5bGUiLCJkaXNwbGF5IiwiZSIsInJlcXVlc3RGdWxsc2NyZWVuIiwiZmlsZUlucHV0IiwidGFyZ2V0IiwiZmlsZXMiLCJmaWxlUmVhZGVyIiwiRmlsZVJlYWRlciIsInJlYWRBc0RhdGFVUkwiLCJldmVudCIsImRhdGEiLCJpbWciLCJyZXN1bHQiLCJkYXRhc2VuZCIsIm9ucmVhZHlzdGF0ZWNoYW5nZSIsInJlYWR5U3RhdGUiLCJzdGF0dXMiLCJsb2NhbFN0b3JhZ2UiLCJzZXRJdGVtIiwic3JjIiwibG9nb0hvbWUiLCJhcnJheUJhbGxzIiwiaW50ZXJ2YWwiLCJ1bmRlZmluZWQiLCJjYW52YXMiLCJjdHgiLCJnZXRDb250ZXh0Iiwic3RhcnQiLCJjbGVhciIsInN0YXJ0R2FtZSIsInRvZ2dsZVN0YXJ0RGlzYWJsZWQiLCJzdG9wIiwidG9nZ2xlQ2xlYXJEaXNhYmxlZCIsInNldEF0dHJpYnV0ZSIsInJlbW92ZUF0dHJpYnV0ZSIsInNldEludGVydmFsIiwibW92ZW1lbnQiLCJjbGVhckludGVydmFsIiwiY2xlYXJSZWN0Iiwid2lkdGgiLCJoZWlnaHQiLCJyYW5kb21Db2xvciIsImxldHRlcnMiLCJjb2xvciIsImkiLCJNYXRoIiwiZmxvb3IiLCJyYW5kb20iLCJyYW5kb21Nb3ZlbWVudCIsInJhbmRvbVJhZGl1cyIsIlZpZXciLCJvbk1vdXNlRG93biIsImJpbmQiLCJwcm90b3R5cGUiLCJvZmZzZXRYIiwib2Zmc2V0WSIsIngiLCJ5IiwicmFkaXVzIiwiYmFsbCIsIkJhbGwiLCJwdXNoIiwiZHJhd0JhbGwiLCJiZWdpblBhdGgiLCJhcmMiLCJQSSIsImZpbGxTdHlsZSIsImNsb3NlUGF0aCIsImZpbGwiLCJiYWxscyIsImZvckVhY2giLCJzcGVlZFgiLCJzcGVlZFkiLCJpbml0IiwidmlldyIsImVuIiwidGV4dCIsIm15QWNjb3VudCIsImljb25UaXRsZSIsImxvZ2luVGl0bGUiLCJwYXNzd29yZDFUaXRsZSIsInBhc3N3b3JkMlRpdGxlIiwiZW1haWxUaXRsZSIsInBob25lVGl0bGUiLCJhYm91dE15c2VsZlRpdGxlIiwiY2xvc2UiLCJjaGF0IiwiY2hhdFdyYXBwZXIiLCJnYW1lQm91bmNlIiwicnUiLCJ0ZXh0QXV0aG9yaXphdGlvbiIsImxhYmVsTG9naW4iLCJsYWJlbFBhc3N3b3JkIiwicmVnaXN0cmF0aW9uQnRuQXZ0IiwibG9nSW5CdG4iLCJ0ZXh0Rm9yZ290dGVuUGFzc3dvcmQiLCJmb3Jnb3R0ZW5MYWJlbExvZ2luIiwiZm9yZ290dGVuTGFiZWxQYXNzd29yZCIsImZvcmdvdHRlblBhc3NMYWJlbCIsImdldFBhc3N3b3JkIiwicGxhY2Vob2xkZXIiLCJsb2dpbkF2dCIsInBhc3N3b3JkQXZ0IiwiZm9yZ290dGVuTG9naW5BdnQiLCJrYXlXb3JkQXZ0IiwiZm9yZ290dGVuUGFzc3dvcmRNZXNzYWdlIiwiY29udHJvbFBhbmVsIiwiYWRkc3R1ZGVudCIsImxhYmVsTmFtZSIsImxhYmVsTGFzdG5hbWUiLCJsYWJlbEFnZSIsImxhYmVsQ2l0eSIsIkNyZWF0ZSIsInRhYmxlTmFtZSIsInRhYmxlTGFzdG5hbWUiLCJ0YWJsZUFnZSIsInRhYmxlQ2l0eSIsInRhYmxlQnV0dG9ucyIsImNsZWFySW5wdXQiLCJwcmludExhYmVsIiwicmVzdWx0TGFiZWwiLCJjb252ZXJ0QnV0dG9uIiwicHJpbnRMYWJlbDIiLCJyZXN1bHRMYWJlbDIiLCJnZXRDdXJyZW5jeSIsImNvbnZlcnRlclRpdGxlIiwiYnRuQ2hhbmdlTGVuZ3RoIiwiYnRuQ2hhbmdlTW9uZXkiLCJtZXRlcjEiLCJ2ZXJzdDEiLCJtaWxlMSIsImZvb3QxIiwieWFyZDEiLCJtZXRlcjIiLCJ2ZXJzdDIiLCJtaWxlMiIsImZvb3QyIiwieWFyZDIiLCJtb2RlczEiLCJzdHVkZW50czEiLCJwYWludDEiLCJjb252ZXJ0ZXIxIiwiY2FsY3VsYXRvcjEiLCJzdHVkZW50c1RpdGxlIiwiTmFtZSIsIkxhc3RuYW1lIiwiQWdlIiwiQ2l0eSIsImlucHV0Q29udmVydGVyIiwib3V0cHV0Q29udmVydGVyIiwiaW5wdXRDdXJyZW5jeSIsIm91dHB1dFJlc3VsdCIsImNvbnRyUGFuZWwiLCJsaW5lVGhpY2siLCJsaW5lQ29sIiwibGFiZWxQYWludCIsInRleHRTZXR0aW5ncyIsIm5vdG9maWNhdGlvbkxhYmVsIiwibGFuZ3VhZ2VMYWJlbCIsInJlc2V0IiwicGFnZSIsInBhZ2VDb2wiLCJhdXRvclBhZ2UiLCJhY2NTZXRQYWdlIiwibGFuZ3VhZ2VCb3hJbmRleCIsInJlZ1BhZ2UiLCJsYW5ndWFnZSIsImNoYW5nZVRleHQiLCJjaGFuZ2VQbGFjZUhvbGRlciIsIm9iaiIsImtleSIsInJlZ0xhYmVsIiwibG9naW5MYWJlbCIsInBhc3NMYWJlbCIsInBhc3MyTGFiZWwiLCJlbWFpbExhYmVsIiwicGhvbmVMYWJlbCIsImtleXdvcmRMYWJlbCIsImtleXdvcmROb3RpZmljYXRpb24iLCJyZWdpc3RyYXRpb25CdG4iLCJwYXNzd29yZDEiLCJrZXl3b3JkIiwiY2hlY2tPbmx5TnVtYmVycyIsImlucHV0IiwibWF4TGVuZ3RoIiwic3Vic3RyIl0sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7QUNsRkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixzQkFBc0I7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLHFDQUFxQzs7QUFFckM7QUFDQTtBQUNBOztBQUVBLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsVUFBVTs7Ozs7Ozs7Ozs7O0FDdkx0Q0EscURBQU0sQ0FBQ0MsT0FBUCxHQUFpQjtBQUNmQyxNQUFJLEVBQUVDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxJQUFaLElBQW9CLElBRFg7QUFFZkMsU0FBTyxFQUFFO0FBQ1BDLGlCQUFhLEVBQUUsZ0JBRFI7QUFFUEMsZ0JBQVksRUFBRSxlQUZQO0FBR1BDLGdCQUFZLEVBQUUsZUFIUDtBQUlQQyxnQkFBWSxFQUFFLGVBSlA7QUFLUEMsa0JBQWMsRUFBRSxpQkFMVDtBQU1QQyxVQUFNLEVBQUUsU0FORDtBQU9QQyxTQUFLLEVBQUUsR0FQQTtBQVFQQyxVQUFNLEVBQUUsU0FSRDtBQVNQLGNBQVEsU0FURDtBQVVQQyxpQkFBYSxFQUFFLGlCQVZSO0FBV1BDLHFCQUFpQixFQUFFLHFCQVhaO0FBWVBDLGVBQVcsRUFBRSxlQVpOO0FBYVBDLGdCQUFZLEVBQUUsZ0JBYlA7QUFjUEMsZUFBVyxFQUFFLGVBZE47QUFlUEMsYUFBUyxFQUFFLGFBZko7QUFnQlBDLGlCQUFhLEVBQUU7QUFoQlI7QUFGTSxDQUFqQixDOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBZSxvRkFBdUIseUNBQXlDLEU7Ozs7Ozs7Ozs7OztBQ0EvRTtBQUFlLG9GQUF1Qix5Q0FBeUMsRTs7Ozs7Ozs7Ozs7O0FDQS9FO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLElBQUlDLFNBQVMsR0FBR0MsbUJBQU8sQ0FBQyxxRUFBRCxDQUF2Qjs7Q0FHQTs7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBLElBQUlDLFVBQVUsR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLENBQWpCO0FBQ0EsSUFBSUMsV0FBVyxHQUFHRixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsYUFBeEIsQ0FBbEI7QUFFQSxJQUFJRSxXQUFXLEdBQUdILFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixDQUFsQixDLENBRUE7QUFDQTs7QUFFQUMsV0FBVyxDQUFDRSxnQkFBWixDQUE2QixPQUE3QixFQUFzQyxZQUFZO0FBQzlDSixVQUFRLENBQUNLLFFBQVQsQ0FBa0JDLElBQWxCLGFBQTRCQywrREFBNUI7QUFDSCxDQUZEO0FBSUEsSUFBSUMsS0FBSyxHQUFHUixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsQ0FBWjs7QUFDQU8sS0FBSyxDQUFDQyxPQUFOLEdBQWdCLFlBQVk7QUFDeEJDLG9GQUFnQixDQUFDLElBQUQsRUFBTyxFQUFQLENBQWhCO0FBQ0gsQ0FGRDs7QUFHQSxJQUFJQyxRQUFRLEdBQUdYLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixDQUFmOztBQUNBVSxRQUFRLENBQUNGLE9BQVQsR0FBbUIsWUFBWTtBQUMzQkMsb0ZBQWdCLENBQUMsSUFBRCxFQUFPLEVBQVAsQ0FBaEI7QUFDSCxDQUZEOztBQUdBLElBQUlFLFNBQVMsR0FBR1osUUFBUSxDQUFDQyxjQUFULENBQXdCLFdBQXhCLENBQWhCOztBQUNBVyxTQUFTLENBQUNILE9BQVYsR0FBb0IsWUFBWTtBQUM1QkMsb0ZBQWdCLENBQUMsSUFBRCxFQUFPLEVBQVAsQ0FBaEI7QUFDSCxDQUZEOztBQUdBLElBQUlHLEtBQUssR0FBR2IsUUFBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLENBQVo7O0FBQ0FZLEtBQUssQ0FBQ0osT0FBTixHQUFnQixZQUFZO0FBQ3hCQyxvRkFBZ0IsQ0FBQyxJQUFELEVBQU8sRUFBUCxDQUFoQjtBQUNILENBRkQ7O0FBR0EsSUFBSUksS0FBSyxHQUFHZCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsQ0FBWjs7QUFDQWEsS0FBSyxDQUFDTCxPQUFOLEdBQWdCLFlBQVk7QUFDeEJDLG9GQUFnQixDQUFDLElBQUQsRUFBTyxFQUFQLENBQWhCO0FBQ0gsQ0FGRDs7QUFJQSxJQUFJSyxXQUFXLEdBQUdmLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixDQUFsQjtBQUNBYyxXQUFXLENBQUNYLGdCQUFaLENBQTZCLFNBQTdCLEVBQXdDWSxzQkFBeEM7QUFFQSxJQUFJQyxrQkFBa0IsR0FBRyxHQUF6QjtBQUVBLElBQUlDLGFBQWEsR0FBR2xCLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixlQUF4QixDQUFwQjtBQUNBLElBQUlrQixZQUFZLEdBQUduQixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsY0FBeEIsQ0FBbkI7QUFDQSxJQUFJbUIsT0FBTyxHQUFHcEIsUUFBUSxDQUFDQyxjQUFULENBQXdCLFNBQXhCLENBQWQ7QUFDQWtCLFlBQVksQ0FBQ2YsZ0JBQWIsQ0FBOEIsT0FBOUIsRUFBdUMsWUFBTTtBQUN6Q2lCLHNCQUFvQixDQUFDRCxPQUFELEVBQVVELFlBQVYsQ0FBcEI7O0FBQ0EsTUFBR0cscUJBQXFCLENBQUNDLEtBQXRCLElBQTZCLElBQWhDLEVBQXFDO0FBQ2pDdkIsWUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDdUIsU0FBdEMsR0FBZ0QsV0FBaEQ7QUFDQXhCLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixnQkFBeEIsRUFBMEN1QixTQUExQyxHQUFvRCxjQUFwRDtBQUNBeEIsWUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDdUIsU0FBdEMsR0FBZ0QsV0FBaEQ7QUFDQXhCLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixZQUF4QixFQUFzQ3VCLFNBQXRDLEdBQWdELFdBQWhEO0FBQ0gsR0FMRCxNQUtLO0FBQ0R4QixZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0N1QixTQUF0QyxHQUFnRCxhQUFoRDtBQUNBeEIsWUFBUSxDQUFDQyxjQUFULENBQXdCLGdCQUF4QixFQUEwQ3VCLFNBQTFDLEdBQW9ELGNBQXBEO0FBQ0F4QixZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0N1QixTQUF0QyxHQUFnRCxhQUFoRDtBQUNBeEIsWUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDdUIsU0FBdEMsR0FBZ0QsZUFBaEQ7QUFDSDtBQUVKLENBZEQ7QUFlQUosT0FBTyxDQUFDaEIsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBTTtBQUFDaUIsc0JBQW9CLENBQUNELE9BQUQsRUFBVUQsWUFBVixDQUFwQjtBQUE2Q00sZUFBYTs7QUFDL0YsTUFBR0gscUJBQXFCLENBQUNDLEtBQXRCLElBQTZCLElBQWhDLEVBQXFDO0FBQ2pDdkIsWUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDdUIsU0FBdEMsR0FBZ0QsT0FBaEQ7QUFDQXhCLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixnQkFBeEIsRUFBMEN1QixTQUExQyxHQUFvRCxVQUFwRDtBQUNBeEIsWUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDdUIsU0FBdEMsR0FBZ0QsT0FBaEQ7QUFDQXhCLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixZQUF4QixFQUFzQ3VCLFNBQXRDLEdBQWdELE9BQWhEO0FBQ0gsR0FMRCxNQUtLO0FBQ0R4QixZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0N1QixTQUF0QyxHQUFnRCxPQUFoRDtBQUNBeEIsWUFBUSxDQUFDQyxjQUFULENBQXdCLGdCQUF4QixFQUEwQ3VCLFNBQTFDLEdBQW9ELFFBQXBEO0FBQ0F4QixZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0N1QixTQUF0QyxHQUFnRCxPQUFoRDtBQUNBeEIsWUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDdUIsU0FBdEMsR0FBZ0QsU0FBaEQ7QUFDSDtBQUFDLENBWE47QUFjQSxJQUFJRSxPQUFPLEdBQUdDLFdBQVcsRUFBekI7QUFDQUMsVUFBVSxDQUFDQyxTQUFELEVBQVksR0FBWixDQUFWO0FBRUEsSUFBSUMsUUFBUSxHQUFHOUIsUUFBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLENBQWY7QUFDQTZCLFFBQVEsQ0FBQzFCLGdCQUFULENBQTBCLE9BQTFCLEVBQWtDLFlBQU07QUFBQ0osVUFBUSxDQUFDSyxRQUFULENBQWtCQyxJQUFsQixhQUE0QkMsK0RBQTVCO0FBQTBELENBQW5HO0FBRUEsSUFBSWUscUJBQXFCLEdBQUN0QixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsdUJBQXhCLENBQTFCOztBQUNBcUIscUJBQXFCLENBQUNTLFFBQXRCLEdBQWlDLFlBQUk7QUFBQ0Msc0ZBQWMsQ0FBQyxRQUFELENBQWQ7QUFBeUIsQ0FBL0Q7O0FBR0EsU0FBU0MsSUFBVCxHQUFlO0FBQ1gsTUFBSUMsY0FBYyxDQUFDQyxPQUFmLENBQXVCLFVBQXZCLEtBQW9DLElBQXhDLEVBQTZDO0FBQ3pDYix5QkFBcUIsQ0FBQ0MsS0FBdEIsR0FBNEJXLGNBQWMsQ0FBQ0MsT0FBZixDQUF1QixVQUF2QixDQUE1QjtBQUNBSCx3RkFBYyxDQUFDLFFBQUQsQ0FBZDtBQUNILEdBSEQsTUFHSztBQUNEQSx3RkFBYyxDQUFDLFFBQUQsQ0FBZDtBQUEwQjtBQUNqQzs7QUFFRCxTQUFTTCxXQUFULEdBQXVCO0FBQ25CLE1BQUlTLEdBQUcsR0FBRyxJQUFJQyxjQUFKLEVBQVY7QUFDQUQsS0FBRyxDQUFDRSxJQUFKLENBQVMsS0FBVCxZQUFtQkMsNERBQW5CLFNBQW1DMUMsU0FBUyxDQUFDaEIsT0FBVixDQUFrQkssY0FBckQ7QUFDQWtELEtBQUcsQ0FBQ0ksZ0JBQUosQ0FBcUIsY0FBckIsRUFBcUMsa0JBQXJDO0FBQ0FKLEtBQUcsQ0FBQ0ssSUFBSjs7QUFDQUwsS0FBRyxDQUFDTSxNQUFKLEdBQWEsWUFBWTtBQUNyQmhCLFdBQU8sR0FBR2lCLElBQUksQ0FBQ0MsS0FBTCxDQUFXLEtBQUtDLFFBQWhCLENBQVY7QUFDQUMsV0FBTyxDQUFDQyxHQUFSLENBQVlyQixPQUFaO0FBQ0gsR0FIRDtBQUlIOztBQUVELFNBQVNHLFNBQVQsR0FBcUI7QUFDakI3QixVQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsRUFBaUNzQixLQUFqQyxHQUF5Q0csT0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXbEIsS0FBcEQ7QUFDQVIsVUFBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLEVBQWlDc0IsS0FBakMsR0FBeUNHLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBV2IsS0FBcEQ7QUFDQWIsVUFBUSxDQUFDQyxjQUFULENBQXdCLFdBQXhCLEVBQXFDc0IsS0FBckMsR0FBNkNHLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBV2YsUUFBeEQ7QUFDQVgsVUFBUSxDQUFDQyxjQUFULENBQXdCLFdBQXhCLEVBQXFDc0IsS0FBckMsR0FBNkNHLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBV2YsUUFBeEQ7QUFDQVgsVUFBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLEVBQWlDc0IsS0FBakMsR0FBeUNHLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBV3NCLFlBQXBEO0FBQ0FoRCxVQUFRLENBQUNDLGNBQVQsQ0FBd0IsYUFBeEIsRUFBdUNzQixLQUF2QyxHQUErQ0csT0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXdUIsWUFBMUQsQ0FOaUIsQ0FPakI7QUFDSDs7QUFFRCxJQUFJQyxHQUFHLEdBQUcsRUFBVjs7QUFFQSxTQUFTekIsYUFBVCxHQUF5QjtBQUNyQjtBQUNBLE1BQUkwQixZQUFZLEdBQUc7QUFDZjNDLFNBQUssRUFBRUEsS0FBSyxDQUFDZSxLQUFOLENBQVk2QixPQUFaLENBQW9CLEtBQXBCLEVBQTJCLEVBQTNCLENBRFE7QUFFbkJ6QyxZQUFRLEVBQUNBLFFBQVEsQ0FBQ1ksS0FGQztBQUdmWCxhQUFTLEVBQUlBLFNBQVMsQ0FBQ1csS0FIUjtBQUlmVixTQUFLLEVBQUlBLEtBQUssQ0FBQ1UsS0FKQTtBQUtmVCxTQUFLLEVBQUdBLEtBQUssQ0FBQ1MsS0FBTixDQUFZNkIsT0FBWixDQUFvQixZQUFwQixFQUFrQyxFQUFsQyxDQUxPO0FBTWZyQyxlQUFXLEVBQUdBLFdBQVcsR0FBR2YsUUFBUSxDQUFDQyxjQUFULENBQXdCLGFBQXhCLEVBQXVDc0IsS0FOcEQ7QUFPZjhCLGVBQVcsRUFBRTNCLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBVzJCLFdBUFQsQ0FRZjs7QUFSZSxHQUFuQjtBQVVBUCxTQUFPLENBQUNDLEdBQVIsQ0FBWUksWUFBWjtBQUNBLE1BQUlHLE9BQU8sR0FBR3RELFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixDQUFkOztBQUNBLE1BQUlzRCxVQUFVLENBQUNKLFlBQVksQ0FBQ3RDLEtBQWQsQ0FBVixJQUFrQzJDLFVBQVUsQ0FBQ0wsWUFBWSxDQUFDM0MsS0FBZCxDQUE1QyxJQUFvRWlELFNBQVMsQ0FBQ04sWUFBWSxDQUFDeEMsUUFBZCxFQUF3QndDLFlBQVksQ0FBQ3ZDLFNBQXJDLENBQTdFLElBQWdJOEMsVUFBVSxDQUFDUCxZQUFZLENBQUNyQyxLQUFkLEVBQXFCLEtBQXJCLENBQTlJLEVBQTJLO0FBQ3ZLLFFBQUlzQixHQUFHLEdBQUcsSUFBSUMsY0FBSixFQUFWO0FBQ0FELE9BQUcsQ0FBQ0UsSUFBSixDQUFTLE1BQVQsWUFBb0JDLDREQUFwQixTQUFvQzFDLFNBQVMsQ0FBQ2hCLE9BQVYsQ0FBa0JTLGFBQXREO0FBQ0E4QyxPQUFHLENBQUNJLGdCQUFKLENBQXFCLGNBQXJCLEVBQXFDLGtCQUFyQztBQUNBSixPQUFHLENBQUNLLElBQUosQ0FBU0UsSUFBSSxDQUFDZ0IsU0FBTCxDQUFlUixZQUFmLENBQVQ7QUFDQUcsV0FBTyxDQUFDTSxTQUFSLEdBQW9CLGtCQUFwQjtBQUNBZCxXQUFPLENBQUNDLEdBQVIsQ0FBWUksWUFBWixFQU51SyxDQU92SztBQUNILEdBUkQsTUFRTztBQUNIRyxXQUFPLENBQUNNLFNBQVIsR0FBb0JWLEdBQXBCO0FBQ0g7QUFDSjs7QUFFRCxTQUFTTSxVQUFULENBQW9CaEQsS0FBcEIsRUFBMkI7QUFDdkIsTUFBSUEsS0FBSyxDQUFDcUQsTUFBTixHQUFlLENBQWYsSUFBb0IsQ0FBQ0MsS0FBSyxDQUFDQyxNQUFNLENBQUN2RCxLQUFLLENBQUMsQ0FBRCxDQUFOLENBQVAsQ0FBOUIsRUFBa0Q7QUFDOUMwQyxPQUFHLEdBQUcsb0JBQU47QUFDQSxXQUFPLEtBQVA7QUFDSDs7QUFDRCxTQUFPLElBQVA7QUFDSDs7QUFFRCxTQUFTTyxTQUFULENBQW1CTyxLQUFuQixFQUEwQkMsS0FBMUIsRUFBaUM7QUFDN0IsTUFBSUQsS0FBSyxDQUFDSCxNQUFOLEdBQWUsQ0FBZixJQUFvQkcsS0FBSyxLQUFLQyxLQUFsQyxFQUF5QztBQUNyQ2YsT0FBRyxHQUFHLHFCQUFOO0FBQ0EsV0FBTyxLQUFQO0FBQ0g7O0FBQ0QsU0FBTyxJQUFQO0FBQ0g7O0FBRUQsU0FBU1EsVUFBVCxDQUFvQjVDLEtBQXBCLEVBQTJCb0QsV0FBM0IsRUFBd0M7QUFDcEMsTUFBSXBELEtBQUssQ0FBQytDLE1BQU4sS0FBaUIsRUFBakIsSUFBdUIvQyxLQUFLLENBQUNxRCxTQUFOLENBQWdCLENBQWhCLEVBQW1CLENBQW5CLE1BQTBCRCxXQUFyRCxFQUFrRTtBQUM5RGhCLE9BQUcsR0FBRyxxQkFBTjtBQUNBLFdBQU8sS0FBUDtBQUNIOztBQUNELFNBQU8sSUFBUDtBQUNIOztBQUNELFNBQVNLLFVBQVQsQ0FBb0IxQyxLQUFwQixFQUEyQjtBQUN2QixNQUFJQSxLQUFLLENBQUN1RCxRQUFOLENBQWUsR0FBZixDQUFKLEVBQXlCO0FBQ3JCLFFBQUlDLEtBQUssR0FBR3hELEtBQUssQ0FBQ3NELFNBQU4sQ0FBZ0J0RCxLQUFLLENBQUN5RCxPQUFOLENBQWMsR0FBZCxDQUFoQixFQUFvQ3pELEtBQUssQ0FBQ2dELE1BQTFDLENBQVo7O0FBQ0EsUUFBSVEsS0FBSyxDQUFDUixNQUFOLEdBQWUsQ0FBbkIsRUFBc0I7QUFDbEIsYUFBTyxJQUFQO0FBQ0g7QUFFSjs7QUFDRFgsS0FBRyxHQUFHLHFCQUFOO0FBQ0EsU0FBTyxLQUFQO0FBQ0g7O0FBRUQsU0FBU2xDLHNCQUFULEdBQWtDO0FBQzlCLE1BQUlELFdBQVcsQ0FBQ1EsS0FBWixDQUFrQnNDLE1BQWxCLEdBQTJCNUMsa0JBQS9CLEVBQW1EO0FBQy9DO0FBQ0g7O0FBQ0ZDLGVBQWEsQ0FBQ00sU0FBZCxHQUEyQitDLE1BQU0sQ0FBQ3RELGtCQUFrQixHQUFHRixXQUFXLENBQUNRLEtBQVosQ0FBa0JzQyxNQUF4QyxDQUFqQztBQUNGLEMsQ0FFRDs7O0FBQ0ksU0FBU1csb0JBQVQsQ0FBOEJDLFNBQTlCLEVBQXlDO0FBQUc7QUFDeEMsTUFBSUMsZUFBZSxHQUFHMUUsUUFBUSxDQUFDMkUsc0JBQVQsQ0FBaUNGLFNBQWpDLENBQXRCO0FBRHFDO0FBQUE7QUFBQTs7QUFBQTtBQUVyQyx5QkFBaUJDLGVBQWpCLDhIQUFrQztBQUFBLFVBQXpCRSxJQUF5QjtBQUM5QkEsVUFBSSxDQUFDQyxRQUFMLEtBQWtCLElBQWxCLEdBQXlCRCxJQUFJLENBQUNDLFFBQUwsR0FBZ0IsS0FBekMsR0FBZ0RELElBQUksQ0FBQ0MsUUFBTCxHQUFnQixJQUFoRTtBQUNIO0FBSm9DO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLNUM7O0FBQ0csU0FBU3hELG9CQUFULENBQThCeUQsT0FBOUIsRUFBdUNDLE9BQXZDLEVBQWdEO0FBQzVDLE1BQUlBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjQyxPQUFkLEtBQTBCLE1BQTlCLEVBQXNDO0FBQ2xDSCxXQUFPLENBQUNFLEtBQVIsQ0FBY0MsT0FBZCxHQUF3QixNQUF4QjtBQUNBRixXQUFPLENBQUNDLEtBQVIsQ0FBY0MsT0FBZCxHQUF3QixPQUF4QjtBQUNILEdBSEQsTUFHTTtBQUNGSCxXQUFPLENBQUNFLEtBQVIsQ0FBY0MsT0FBZCxHQUF3QixPQUF4QjtBQUNBRixXQUFPLENBQUNDLEtBQVIsQ0FBY0MsT0FBZCxHQUF3QixNQUF4QjtBQUNIOztBQUNEVCxzQkFBb0IsQ0FBQyxjQUFELENBQXBCO0FBQ0gsQyxDQUVMOzs7QUFDQXpFLFVBQVUsQ0FBQ0ssZ0JBQVgsQ0FBNEIsT0FBNUIsRUFBcUMsVUFBQzhFLENBQUQsRUFBSztBQUV0Q2xGLFVBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ2tGLGlCQUFyQztBQUNILENBSEQ7QUFNQSxJQUFJQyxTQUFTLEdBQUdwRixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsQ0FBaEI7QUFFQW1GLFNBQVMsQ0FBQ2hGLGdCQUFWLENBQTJCLFFBQTNCLEVBQXFDLFVBQUM4RSxDQUFELEVBQU07QUFFdkNwQyxTQUFPLENBQUNDLEdBQVIsQ0FBYW1DLENBQUMsQ0FBQ0csTUFBRixDQUFTQyxLQUFULENBQWUsQ0FBZixDQUFiO0FBQ0EsTUFBSUMsVUFBVSxHQUFHLElBQUlDLFVBQUosRUFBakI7QUFDQUQsWUFBVSxDQUFDRSxhQUFYLENBQTJCUCxDQUFDLENBQUNHLE1BQUYsQ0FBU0MsS0FBVCxDQUFlLENBQWYsQ0FBM0I7O0FBQ0FDLFlBQVUsQ0FBQzdDLE1BQVgsR0FBb0IsVUFBV2dELEtBQVgsRUFBbUI7QUFDbkMsUUFBSUMsSUFBSSxHQUFHO0FBQ1BDLFNBQUcsRUFBR0YsS0FBSyxDQUFDTCxNQUFOLENBQWFRO0FBRFosS0FBWDtBQUdBLFFBQUlDLFFBQVEsR0FBR25ELElBQUksQ0FBQ2dCLFNBQUwsQ0FBZWdDLElBQWYsQ0FBZjtBQUNBLFFBQUl2RCxHQUFHLEdBQUcsSUFBSUMsY0FBSixFQUFWO0FBQ0FELE9BQUcsQ0FBQ0UsSUFBSixDQUFTLE1BQVQsWUFBbUJDLDREQUFuQixTQUFtQzFDLFNBQVMsQ0FBQ2hCLE9BQVYsQ0FBa0JjLFNBQXJEO0FBQ0F5QyxPQUFHLENBQUNJLGdCQUFKLENBQXFCLGNBQXJCLEVBQXFDLGtCQUFyQztBQUNBSixPQUFHLENBQUNLLElBQUosQ0FBU3FELFFBQVQ7O0FBQ0ExRCxPQUFHLENBQUMyRCxrQkFBSixHQUF5QixZQUFZO0FBQ2pDLFVBQUczRCxHQUFHLENBQUM0RCxVQUFKLEtBQW1CLENBQW5CLElBQXdCNUQsR0FBRyxDQUFDNkQsTUFBSixLQUFlLEdBQTFDLEVBQThDO0FBQzFDLFlBQUlMLEdBQUcsR0FBRzVGLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixDQUFWO0FBQ0FpRyxvQkFBWSxDQUFDQyxPQUFiLENBQXFCLEtBQXJCLFlBQStCeEQsSUFBSSxDQUFDQyxLQUFMLENBQVcsS0FBS0MsUUFBaEIsQ0FBL0I7QUFDQStDLFdBQUcsQ0FBQ1EsR0FBSixhQUFhekQsSUFBSSxDQUFDQyxLQUFMLENBQVcsS0FBS0MsUUFBaEIsQ0FBYjtBQUNIO0FBQ0osS0FORDtBQU9ILEdBaEJEO0FBa0JILENBdkJEO0FBeUJBN0MsUUFBUSxDQUFDSSxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOEMsVUFBVThFLENBQVYsRUFBYTtBQUN2RGpELE1BQUk7O0FBQ0osTUFBR2lFLFlBQVksQ0FBQy9ELE9BQWIsQ0FBcUIsS0FBckIsTUFBZ0MsSUFBbkMsRUFBd0M7QUFDcENuQyxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsYUFBeEIsRUFBdUNtRyxHQUF2QyxHQUE2Q0YsWUFBWSxDQUFDL0QsT0FBYixDQUFxQixLQUFyQixDQUE3QztBQUNIO0FBQ0osQ0FMRDtBQU1BLElBQUlrRSxRQUFRLEdBQUVyRyxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsVUFBeEIsQ0FBZDtBQUNBb0csUUFBUSxDQUFDakcsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBa0MsWUFBTTtBQUFDSixVQUFRLENBQUNLLFFBQVQsQ0FBa0JDLElBQWxCLGFBQTRCQywrREFBNUI7QUFBMEQsQ0FBbkcsRTs7Ozs7Ozs7Ozs7QUM3UEEsSUFBSStGLFVBQVUsR0FBRyxFQUFqQjtBQUNBLElBQUlDLFFBQVEsR0FBR0MsU0FBZjtBQUVBLElBQU1DLE1BQU0sR0FBR3pHLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixDQUFmO0FBQ0EsSUFBTXlHLEdBQUcsR0FBR0QsTUFBTSxDQUFDRSxVQUFQLENBQWtCLElBQWxCLENBQVo7QUFDQSxJQUFNQyxLQUFLLEdBQUc1RyxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsQ0FBZDtBQUNBLElBQU00RyxLQUFLLEdBQUc3RyxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsQ0FBZDtBQUVBMkcsS0FBSyxDQUFDeEcsZ0JBQU4sQ0FBdUIsT0FBdkIsRUFBZ0MsWUFBTTtBQUFDMEcsV0FBUztBQUFJQyxxQkFBbUI7QUFBRyxDQUExRTtBQUNBRixLQUFLLENBQUN6RyxnQkFBTixDQUF1QixPQUF2QixFQUFnQyxZQUFNO0FBQUM0RyxNQUFJLENBQUNULFFBQUQsQ0FBSjtBQUFnQlUscUJBQW1CO0FBQUcsQ0FBN0UsRSxDQUVBOztBQUNBLFNBQVNGLG1CQUFULEdBQStCO0FBQzdCSCxPQUFLLENBQUNNLFlBQU4sQ0FBbUIsVUFBbkIsRUFBK0IsTUFBL0I7QUFDQUwsT0FBSyxDQUFDTSxlQUFOLENBQXNCLFVBQXRCO0FBQ0Q7O0FBRUQsU0FBU0YsbUJBQVQsR0FBK0I7QUFDN0JKLE9BQUssQ0FBQ0ssWUFBTixDQUFtQixVQUFuQixFQUErQixNQUEvQjtBQUNBTixPQUFLLENBQUNPLGVBQU4sQ0FBc0IsVUFBdEI7QUFFRDs7QUFFRCxTQUFTTCxTQUFULEdBQXFCO0FBQ25CUCxVQUFRLEdBQUdhLFdBQVcsQ0FBQyxZQUFNO0FBQzNCQyxZQUFRLENBQUNmLFVBQUQsQ0FBUjtBQUNELEdBRnFCLEVBRW5CLEVBRm1CLENBQXRCO0FBR0EsU0FBT0MsUUFBUDtBQUNEOztBQUVELFNBQVNTLElBQVQsR0FBZ0I7QUFDZE0sZUFBYSxDQUFDZixRQUFELENBQWI7QUFDQUcsS0FBRyxDQUFDYSxTQUFKLENBQWMsQ0FBZCxFQUFpQixDQUFqQixFQUFvQmQsTUFBTSxDQUFDZSxLQUEzQixFQUFrQ2YsTUFBTSxDQUFDZ0IsTUFBekM7QUFDQW5CLFlBQVUsR0FBRyxFQUFiO0FBQ0FDLFVBQVEsR0FBR0MsU0FBWDtBQUNELEMsQ0FFRDs7O0FBQ0EsU0FBU2tCLFdBQVQsR0FBdUI7QUFDbkIsTUFBSUMsT0FBTyxHQUFHLGtCQUFkO0FBQ0EsTUFBSUMsS0FBSyxHQUFHLEdBQVo7O0FBQ0EsT0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHLENBQXBCLEVBQXVCQSxDQUFDLEVBQXhCLEVBQTRCO0FBQzFCRCxTQUFLLElBQUlELE9BQU8sQ0FBQ0csSUFBSSxDQUFDQyxLQUFMLENBQVdELElBQUksQ0FBQ0UsTUFBTCxLQUFnQixFQUEzQixDQUFELENBQWhCO0FBQ0Q7O0FBQ0QsU0FBT0osS0FBUDtBQUNIOztBQUVELFNBQVNLLGNBQVQsR0FBMEI7QUFDeEIsU0FBT0gsSUFBSSxDQUFDQyxLQUFMLENBQVdELElBQUksQ0FBQ0UsTUFBTCxNQUFpQixJQUFJLENBQUMsQ0FBdEIsQ0FBWCxJQUF3QyxDQUFDLENBQWhEO0FBQ0Q7O0FBRUQsU0FBU0UsWUFBVCxHQUF3QjtBQUN0QixTQUFPSixJQUFJLENBQUNDLEtBQUwsQ0FBV0QsSUFBSSxDQUFDRSxNQUFMLE1BQWlCLEtBQUssQ0FBdEIsQ0FBWCxJQUF1QyxDQUE5QztBQUNELEMsQ0FFRDs7O0FBQ0EsU0FBU0csSUFBVCxHQUFnQjtBQUNkO0FBQ0EsT0FBSzFCLE1BQUwsR0FBY3pHLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixDQUFkO0FBQ0EsT0FBS3lHLEdBQUwsR0FBVyxLQUFLRCxNQUFMLENBQVlFLFVBQVosQ0FBdUIsSUFBdkIsQ0FBWDtBQUNBLE9BQUtGLE1BQUwsQ0FBWXJHLGdCQUFaLENBQTZCLFdBQTdCLEVBQTBDLEtBQUtnSSxXQUFMLENBQWlCQyxJQUFqQixDQUFzQixJQUF0QixDQUExQztBQUVEOztBQUVERixJQUFJLENBQUNHLFNBQUwsQ0FBZUYsV0FBZixHQUE2QixVQUFTbEQsQ0FBVCxFQUFZO0FBQ3ZDLE1BQUlwQixLQUFLLENBQUN5QyxRQUFELENBQVQsRUFBcUI7QUFDbkI7QUFDRDs7QUFIc0MsTUFJaENnQyxPQUpnQyxHQUlackQsQ0FKWSxDQUloQ3FELE9BSmdDO0FBQUEsTUFJdkJDLE9BSnVCLEdBSVp0RCxDQUpZLENBSXZCc0QsT0FKdUI7QUFLdkMsTUFBSUMsQ0FBQyxHQUFHRixPQUFSO0FBQ0EsTUFBSUcsQ0FBQyxHQUFHRixPQUFSO0FBRUEsTUFBTUcsTUFBTSxHQUFHVCxZQUFZLEVBQTNCOztBQUNBLE1BQUlPLENBQUMsR0FBR0UsTUFBSixJQUFjLEtBQUtsQyxNQUFMLENBQVllLEtBQTlCLEVBQXFDO0FBQ25DaUIsS0FBQyxHQUFHQSxDQUFDLEdBQUdFLE1BQVI7QUFDRDs7QUFDRCxNQUFJRixDQUFDLEdBQUdFLE1BQUosSUFBYyxLQUFLbEMsTUFBTCxDQUFZZSxLQUE5QixFQUFxQztBQUNuQ2lCLEtBQUMsR0FBR0EsQ0FBQyxHQUFHRSxNQUFNLEdBQUcsQ0FBakI7QUFDRDs7QUFDRCxNQUFJRCxDQUFDLEdBQUdDLE1BQUosSUFBYyxLQUFLbEMsTUFBTCxDQUFZZ0IsTUFBOUIsRUFBc0M7QUFDcENpQixLQUFDLEdBQUdBLENBQUMsR0FBR0MsTUFBUjtBQUNEOztBQUNELE1BQUlELENBQUMsR0FBR0MsTUFBSixJQUFjLEtBQUtsQyxNQUFMLENBQVlnQixNQUE5QixFQUFzQztBQUNwQ2lCLEtBQUMsR0FBR0EsQ0FBQyxHQUFHQyxNQUFNLEdBQUcsQ0FBakI7QUFDRDs7QUFFRCxNQUFNQyxJQUFJLEdBQUcsSUFBSUMsSUFBSixDQUFTSixDQUFULEVBQVlDLENBQVosRUFBZUMsTUFBZixDQUFiLENBdEJ1QyxDQXVCdkM7O0FBQ0FyQyxZQUFVLENBQUN3QyxJQUFYLENBQWdCRixJQUFoQjtBQUNELENBekJEOztBQTJCQSxTQUFTRyxRQUFULENBQW1CSCxJQUFuQixFQUF5QjtBQUN2QmxDLEtBQUcsQ0FBQ3NDLFNBQUo7QUFDQXRDLEtBQUcsQ0FBQ3VDLEdBQUosQ0FBUUwsSUFBSSxDQUFDSCxDQUFiLEVBQWdCRyxJQUFJLENBQUNGLENBQXJCLEVBQXdCRSxJQUFJLENBQUNELE1BQTdCLEVBQXFDLENBQXJDLEVBQXdDLElBQUliLElBQUksQ0FBQ29CLEVBQWpEO0FBQ0F4QyxLQUFHLENBQUN5QyxTQUFKLEdBQWdCUCxJQUFJLENBQUNoQixLQUFyQjtBQUNBbEIsS0FBRyxDQUFDMEMsU0FBSjtBQUNBMUMsS0FBRyxDQUFDMkMsSUFBSjtBQUNEOztBQUVELFNBQVNoQyxRQUFULENBQW1CaUMsS0FBbkIsRUFBMEI7QUFDeEI1QyxLQUFHLENBQUNhLFNBQUosQ0FBYyxDQUFkLEVBQWlCLENBQWpCLEVBQW9CZCxNQUFNLENBQUNlLEtBQTNCLEVBQWtDZixNQUFNLENBQUNnQixNQUF6QztBQUNBNkIsT0FBSyxDQUFDQyxPQUFOLENBQWMsVUFBQVgsSUFBSSxFQUFJO0FBQ3BCLFFBQUdBLElBQUksQ0FBQ0gsQ0FBTCxHQUFTRyxJQUFJLENBQUNZLE1BQWQsR0FBdUIvQyxNQUFNLENBQUNlLEtBQVAsR0FBZW9CLElBQUksQ0FBQ0QsTUFBM0MsSUFBcURDLElBQUksQ0FBQ0gsQ0FBTCxHQUFTRyxJQUFJLENBQUNZLE1BQWQsR0FBdUJaLElBQUksQ0FBQ0QsTUFBcEYsRUFBNEY7QUFDMUZDLFVBQUksQ0FBQ1ksTUFBTCxHQUFjLENBQUNaLElBQUksQ0FBQ1ksTUFBcEI7QUFDRDs7QUFDRCxRQUFHWixJQUFJLENBQUNGLENBQUwsR0FBU0UsSUFBSSxDQUFDYSxNQUFkLEdBQXVCaEQsTUFBTSxDQUFDZ0IsTUFBUCxHQUFnQm1CLElBQUksQ0FBQ0QsTUFBNUMsSUFBc0RDLElBQUksQ0FBQ0YsQ0FBTCxHQUFTRSxJQUFJLENBQUNhLE1BQWQsR0FBdUJiLElBQUksQ0FBQ0QsTUFBckYsRUFBNkY7QUFDM0ZDLFVBQUksQ0FBQ2EsTUFBTCxHQUFjLENBQUNiLElBQUksQ0FBQ2EsTUFBcEI7QUFDRDs7QUFDRGIsUUFBSSxDQUFDSCxDQUFMLEdBQVNHLElBQUksQ0FBQ0gsQ0FBTCxHQUFTRyxJQUFJLENBQUNZLE1BQXZCO0FBQ0FaLFFBQUksQ0FBQ0YsQ0FBTCxHQUFTRSxJQUFJLENBQUNGLENBQUwsR0FBU0UsSUFBSSxDQUFDYSxNQUF2QjtBQUNBVixZQUFRLENBQUNILElBQUQsQ0FBUjtBQUNELEdBVkQ7QUFXRCxDLENBRUQ7OztBQUNBLFNBQVNDLElBQVQsQ0FBY0osQ0FBZCxFQUFpQkMsQ0FBakIsRUFBb0JDLE1BQXBCLEVBQTRCO0FBQzFCLE9BQUtGLENBQUwsR0FBU0EsQ0FBVDtBQUNBLE9BQUtDLENBQUwsR0FBU0EsQ0FBVDtBQUNBLE9BQUtDLE1BQUwsR0FBY0EsTUFBZDtBQUNBLE9BQUtmLEtBQUwsR0FBYUYsV0FBVyxFQUF4QjtBQUNBLE9BQUs4QixNQUFMLEdBQWN2QixjQUFjLEVBQTVCO0FBQ0EsT0FBS3dCLE1BQUwsR0FBY3hCLGNBQWMsRUFBNUI7QUFFRCxDLENBQ0Q7OztBQUNBLElBQU15QixJQUFJLEdBQUcsU0FBUEEsSUFBTyxHQUFNO0FBQ2pCLE1BQU1DLElBQUksR0FBRyxJQUFJeEIsSUFBSixFQUFiO0FBQ0QsQ0FGRDs7QUFJQW5JLFFBQVEsQ0FBQ0ksZ0JBQVQsQ0FBMEIsa0JBQTFCLEVBQStDc0osSUFBL0MsRTs7Ozs7Ozs7Ozs7O0FDN0hBO0FBQUE7QUFBQTtBQUFBLElBQU1uSCxhQUFhLEdBQUcsK0JBQXRCO0FBQ0EsSUFBTWhDLGdCQUFnQixHQUFHLHdCQUF6QixDLENBRUE7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQ1RBO0FBQWM7QUFDVnFKLElBQUUsRUFBQztBQUFFQyxRQUFJLEVBQUM7QUFDRkMsZUFBUyxFQUFFLFlBRFQ7QUFFRkMsZUFBUyxFQUFFLG9CQUZUO0FBR0ZDLGdCQUFVLEVBQUUsT0FIVjtBQUlGQyxvQkFBYyxFQUFFLFVBSmQ7QUFLRkMsb0JBQWMsRUFBRSxnQkFMZDtBQU1GQyxnQkFBVSxFQUFFLE9BTlY7QUFPRkMsZ0JBQVUsRUFBRSxPQVBWO0FBUUZDLHNCQUFnQixFQUFFLGNBUmhCO0FBU0ZqSixhQUFPLEVBQUUsY0FUUDtBQVVGa0osV0FBSyxFQUFFLE9BVkw7QUFXRkMsVUFBSSxFQUFDLE1BWEg7QUFZRkMsaUJBQVcsRUFBQyxhQVpWO0FBYUZDLGdCQUFVLEVBQUMsUUFiVDtBQWNGN0QsV0FBSyxFQUFDLFlBZEo7QUFlRkMsV0FBSyxFQUFDLE9BZko7QUFnQkYxRixrQkFBWSxFQUFDO0FBaEJYO0FBQVAsR0FETztBQW9CVnVKLElBQUUsRUFBQztBQUFFYixRQUFJLEVBQUU7QUFDSEMsZUFBUyxFQUFFLGFBRFI7QUFFSEMsZUFBUyxFQUFFLHNCQUZSO0FBR0hDLGdCQUFVLEVBQUUsT0FIVDtBQUlIQyxvQkFBYyxFQUFFLFFBSmI7QUFLSEMsb0JBQWMsRUFBRSxpQkFMYjtBQU1IQyxnQkFBVSxFQUFFLE9BTlQ7QUFPSEMsZ0JBQVUsRUFBRSxTQVBUO0FBUUhDLHNCQUFnQixFQUFFLFNBUmY7QUFTSGpKLGFBQU8sRUFBRSxxQkFUTjtBQVVIa0osV0FBSyxFQUFFLFNBVko7QUFXSEMsVUFBSSxFQUFDLEtBWEY7QUFZSEMsaUJBQVcsRUFBQyxlQVpUO0FBYUhDLGdCQUFVLEVBQUMsYUFiUjtBQWNIN0QsV0FBSyxFQUFDLGFBZEg7QUFlSEMsV0FBSyxFQUFDLFVBZkg7QUFnQkgxRixrQkFBWSxFQUFDO0FBaEJWO0FBQVI7QUFwQk8sQ0FBZCxFOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFlO0FBQ1h5SSxJQUFFLEVBQUM7QUFBRUMsUUFBSSxFQUFDO0FBQ0ZjLHVCQUFpQixFQUFDLGVBRGhCO0FBRUZDLGdCQUFVLEVBQUMsUUFGVDtBQUdGQyxtQkFBYSxFQUFDLFdBSFo7QUFJRkMsd0JBQWtCLEVBQUMsY0FKakI7QUFLRkMsY0FBUSxFQUFDLFFBTFA7QUFNRnhMLHVCQUFpQixFQUFDLG9CQU5oQjtBQU9GeUwsMkJBQXFCLEVBQUMsbUJBUHBCO0FBUUZDLHlCQUFtQixFQUFDLFFBUmxCO0FBU0ZDLDRCQUFzQixFQUFDLFVBVHJCO0FBVUZDLHdCQUFrQixFQUFDLGdCQVZqQjtBQVdGQyxpQkFBVyxFQUFDO0FBWFYsS0FBUDtBQWFDQyxlQUFXLEVBQUM7QUFDUkMsY0FBUSxFQUFDLGtCQUREO0FBRVJDLGlCQUFXLEVBQUMscUJBRko7QUFHUkMsdUJBQWlCLEVBQUMsbUJBSFY7QUFJUkMsZ0JBQVUsRUFBQyxvQkFKSDtBQUtSQyw4QkFBd0IsRUFBQztBQUxqQjtBQWJiLEdBRFE7QUF1QlhoQixJQUFFLEVBQUM7QUFBRWIsUUFBSSxFQUFFO0FBQ0hjLHVCQUFpQixFQUFDLGFBRGY7QUFFSEMsZ0JBQVUsRUFBQyxRQUZSO0FBR0hDLG1CQUFhLEVBQUMsU0FIWDtBQUlIQyx3QkFBa0IsRUFBQyxhQUpoQjtBQUtIQyxjQUFRLEVBQUMsT0FMTjtBQU1IeEwsdUJBQWlCLEVBQUMsY0FOZjtBQU9IeUwsMkJBQXFCLEVBQUMsdUJBUG5CO0FBUUhDLHlCQUFtQixFQUFDLFFBUmpCO0FBU0hDLDRCQUFzQixFQUFDLGtCQVRwQjtBQVVIQyx3QkFBa0IsRUFBQyxhQVZoQjtBQVdIQyxpQkFBVyxFQUFDO0FBWFQsS0FBUjtBQWFDQyxlQUFXLEVBQUM7QUFDUkMsY0FBUSxFQUFDLG1CQUREO0FBRVJDLGlCQUFXLEVBQUMsb0JBRko7QUFHUkMsdUJBQWlCLEVBQUMsbUJBSFY7QUFJUkMsZ0JBQVUsRUFBQyx5QkFKSDtBQUtSQyw4QkFBd0IsRUFBQztBQUxqQjtBQWJiO0FBdkJRLENBQWYsRTs7Ozs7Ozs7Ozs7O0FDQUE7QUFBZTtBQUNYOUIsSUFBRSxFQUFDO0FBQUVDLFFBQUksRUFBQztBQUNGOEIsa0JBQVksRUFBQyxlQURYO0FBRUZ6TCxpQkFBVyxFQUFFLE1BRlg7QUFHRjBMLGdCQUFVLEVBQUUsYUFIVjtBQUlGQyxlQUFTLEVBQUUsTUFKVDtBQUtGQyxtQkFBYSxFQUFFLFNBTGI7QUFNRkMsY0FBUSxFQUFFLEtBTlI7QUFPRkMsZUFBUyxFQUFFLE1BUFQ7QUFRRkMsWUFBTSxFQUFFLFFBUk47QUFTRkMsZUFBUyxFQUFFLE1BVFQ7QUFVRkMsbUJBQWEsRUFBRSxVQVZiO0FBV0ZDLGNBQVEsRUFBRSxLQVhSO0FBWUZDLGVBQVMsRUFBRSxNQVpUO0FBYUZDLGtCQUFZLEVBQUUsUUFiWjtBQWNGQyxnQkFBVSxFQUFDLE9BZFQ7QUFlRkMsZ0JBQVUsRUFBQyxhQWZUO0FBZ0JGQyxpQkFBVyxFQUFDLFFBaEJWO0FBaUJGQyxtQkFBYSxFQUFDLFNBakJaO0FBa0JGQyxpQkFBVyxFQUFDLGFBbEJWO0FBbUJGQyxrQkFBWSxFQUFDLFFBbkJYO0FBb0JGQyxpQkFBVyxFQUFDLFNBcEJWO0FBcUJGQyxvQkFBYyxFQUFDLFdBckJiO0FBc0JGQyxxQkFBZSxFQUFDLGtCQXRCZDtBQXVCRkMsb0JBQWMsRUFBQyxpQkF2QmI7QUF3QkZDLFlBQU0sRUFBQyxPQXhCTDtBQXlCRkMsWUFBTSxFQUFDLE9BekJMO0FBMEJGQyxXQUFLLEVBQUMsTUExQko7QUEyQkZDLFdBQUssRUFBQyxNQTNCSjtBQTRCRkMsV0FBSyxFQUFDLE1BNUJKO0FBNkJGQyxZQUFNLEVBQUMsT0E3Qkw7QUE4QkZDLFlBQU0sRUFBQyxPQTlCTDtBQStCRkMsV0FBSyxFQUFDLE1BL0JKO0FBZ0NGQyxXQUFLLEVBQUMsTUFoQ0o7QUFpQ0ZDLFdBQUssRUFBQyxNQWpDSjtBQWtDRkMsWUFBTSxFQUFDLE9BbENMO0FBbUNGQyxlQUFTLEVBQUMsVUFuQ1I7QUFvQ0ZDLFlBQU0sRUFBQyxPQXBDTDtBQXFDRkMsZ0JBQVUsRUFBQyxXQXJDVDtBQXNDRkMsaUJBQVcsRUFBQyxZQXRDVjtBQXVDRkMsbUJBQWEsRUFBQztBQXZDWixLQUFQO0FBeUNQM0MsZUFBVyxFQUFDO0FBQ1I0QyxVQUFJLEVBQUMsWUFERztBQUVSQyxjQUFRLEVBQUMsZ0JBRkQ7QUFHUkMsU0FBRyxFQUFDLFdBSEk7QUFJUkMsVUFBSSxFQUFDLFlBSkc7QUFLUkMsb0JBQWMsRUFBQyxhQUxQO0FBTVJDLHFCQUFlLEVBQUMsUUFOUjtBQU9SQyxtQkFBYSxFQUFDLGFBUE47QUFRUkMsa0JBQVksRUFBQyxRQVJMO0FBU1JDLGdCQUFVLEVBQUMsZUFUSDtBQVVSQyxlQUFTLEVBQUMsZ0JBVkY7QUFXUkMsYUFBTyxFQUFDLFlBWEE7QUFZUjlILFdBQUssRUFBQyxPQVpFO0FBYVIrSCxnQkFBVSxFQUFDLE9BYkg7QUFjUkMsa0JBQVksRUFBQyxVQWRMO0FBZVJDLHVCQUFpQixFQUFDLGVBZlY7QUFnQlJDLG1CQUFhLEVBQUMsaUJBaEJOO0FBaUJSQyxXQUFLLEVBQUM7QUFqQkU7QUF6Q0wsR0FEUTtBQWdFWHRFLElBQUUsRUFBQztBQUFFYixRQUFJLEVBQUU7QUFDSDhCLGtCQUFZLEVBQUMsbUJBRFY7QUFFSHpMLGlCQUFXLEVBQUUsT0FGVjtBQUdIMEwsZ0JBQVUsRUFBRSxtQkFIVDtBQUlIQyxlQUFTLEVBQUUsS0FKUjtBQUtIQyxtQkFBYSxFQUFFLFNBTFo7QUFNSEMsY0FBUSxFQUFFLFNBTlA7QUFPSEMsZUFBUyxFQUFFLE9BUFI7QUFRSEMsWUFBTSxFQUFFLFNBUkw7QUFTSEMsZUFBUyxFQUFFLEtBVFI7QUFVSEMsbUJBQWEsRUFBRSxTQVZaO0FBV0hDLGNBQVEsRUFBRSxTQVhQO0FBWUhDLGVBQVMsRUFBRSxPQVpSO0FBYUhDLGtCQUFZLEVBQUUsUUFiWDtBQWNIQyxnQkFBVSxFQUFDLFVBZFI7QUFlSEMsZ0JBQVUsRUFBQyxrQkFmUjtBQWdCSEMsaUJBQVcsRUFBQyxXQWhCVDtBQWlCSEMsbUJBQWEsRUFBQyxnQkFqQlg7QUFrQkhDLGlCQUFXLEVBQUMsa0JBbEJUO0FBbUJIQyxrQkFBWSxFQUFDLFdBbkJWO0FBb0JIQyxpQkFBVyxFQUFDLGdCQXBCVDtBQXFCSEMsb0JBQWMsRUFBQyxXQXJCWjtBQXNCSEMscUJBQWUsRUFBQyxzQkF0QmI7QUF1QkhDLG9CQUFjLEVBQUMsaUJBdkJaO0FBd0JIQyxZQUFNLEVBQUMsTUF4Qko7QUF5QkhDLFlBQU0sRUFBQyxRQXpCSjtBQTBCSEMsV0FBSyxFQUFDLE1BMUJIO0FBMkJIQyxXQUFLLEVBQUMsS0EzQkg7QUE0QkhDLFdBQUssRUFBQyxLQTVCSDtBQTZCSEMsWUFBTSxFQUFDLE1BN0JKO0FBOEJIQyxZQUFNLEVBQUMsUUE5Qko7QUErQkhDLFdBQUssRUFBQyxNQS9CSDtBQWdDSEMsV0FBSyxFQUFDLEtBaENIO0FBaUNIQyxXQUFLLEVBQUMsS0FqQ0g7QUFrQ0hDLFlBQU0sRUFBQyxNQWxDSjtBQW1DSEMsZUFBUyxFQUFDLFNBbkNQO0FBb0NIQyxZQUFNLEVBQUMsV0FwQ0o7QUFxQ0hDLGdCQUFVLEVBQUMsV0FyQ1I7QUFzQ0hDLGlCQUFXLEVBQUMsYUF0Q1Q7QUF1Q0hDLG1CQUFhLEVBQUMsVUF2Q1g7QUF3Q0hTLGdCQUFVLEVBQUMsbUJBeENSO0FBeUNIQyxlQUFTLEVBQUMsZUF6Q1A7QUEwQ0hDLGFBQU8sRUFBQyxZQTFDTDtBQTJDSDlILFdBQUssRUFBQyxVQTNDSDtBQTRDSCtILGdCQUFVLEVBQUMsV0E1Q1I7QUE2Q0hDLGtCQUFZLEVBQUMsV0E3Q1Y7QUE4Q0hDLHVCQUFpQixFQUFDLGFBOUNmO0FBK0NIQyxtQkFBYSxFQUFDLGNBL0NYO0FBZ0RIQyxXQUFLLEVBQUM7QUFoREgsS0FBUjtBQWtEQzNELGVBQVcsRUFBQztBQUNSNEMsVUFBSSxFQUFDLGFBREc7QUFFUkMsY0FBUSxFQUFDLGlCQUZEO0FBR1JDLFNBQUcsRUFBQyxpQkFISTtBQUlSQyxVQUFJLEVBQUMsZUFKRztBQUtSQyxvQkFBYyxFQUFDLGtCQUxQO0FBTVJDLHFCQUFlLEVBQUMsV0FOUjtBQU9SQyxtQkFBYSxFQUFDLGtCQVBOO0FBUVJDLGtCQUFZLEVBQUM7QUFSTDtBQWxEYjtBQWhFUSxDQUFmLEU7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsU0FBU3hNLGNBQVQsQ0FBd0JpTixJQUF4QixFQUE4QjtBQUMxQixNQUFJQSxJQUFJLEtBQUssUUFBYixFQUFzQjtBQUNsQixRQUFJQyxPQUFPLEdBQUNDLDZEQUFaO0FBQ0gsR0FGRCxNQUVPLElBQUlGLElBQUksS0FBSyxRQUFiLEVBQXNCO0FBQ3pCLFFBQUlDLE9BQU8sR0FBQ0UsZ0VBQVo7QUFDSCxHQUZNLE1BRUEsSUFBR0gsSUFBSSxLQUFLLE9BQVosRUFBb0I7QUFDdkIsUUFBSUMsT0FBTyxHQUFDRyxzREFBWjtBQUNILEdBRk0sTUFFQSxJQUFHSixJQUFJLEtBQUcsS0FBVixFQUFnQjtBQUNuQixRQUFJQyxPQUFPLEdBQUNJLDZEQUFaO0FBQ0g7O0FBQ0QsTUFBSUMsUUFBUSxHQUFHak8scUJBQXFCLENBQUNDLEtBQXJDOztBQUNBLE1BQUlnTyxRQUFRLEtBQUcsSUFBZixFQUFvQjtBQUNoQnZQLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ2lILFlBQXJDLENBQWtELEtBQWxELEVBQXdELEtBQXhEO0FBQ0FoRixrQkFBYyxDQUFDaUUsT0FBZixDQUF1QixVQUF2QixFQUFtQ29KLFFBQW5DO0FBQ0FBLFlBQVEsR0FBQyxJQUFUO0FBQ0gsR0FKRCxNQUlPO0FBQ0h2UCxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsV0FBeEIsRUFBcUNpSCxZQUFyQyxDQUFrRCxLQUFsRCxFQUF3RCxLQUF4RDtBQUNBaEYsa0JBQWMsQ0FBQ2lFLE9BQWYsQ0FBdUIsVUFBdkIsRUFBbUNvSixRQUFuQztBQUNIOztBQUNEQyxZQUFVLENBQUNOLE9BQU8sQ0FBQ0ssUUFBRCxDQUFQLENBQWtCMUYsSUFBbkIsQ0FBVjtBQUNBNEYsbUJBQWlCLENBQUNQLE9BQU8sQ0FBQ0ssUUFBRCxDQUFQLENBQWtCbEUsV0FBbkIsQ0FBakI7QUFDSDs7QUFDRCxTQUFTbUUsVUFBVCxDQUFvQkUsR0FBcEIsRUFBeUI7QUFDckIsT0FBSyxJQUFNQyxHQUFYLElBQWtCRCxHQUFsQixFQUF1QjtBQUNuQixRQUFHMVAsUUFBUSxDQUFDQyxjQUFULENBQXdCMFAsR0FBeEIsTUFBK0IsSUFBbEMsRUFBd0M7QUFDcEMzUCxjQUFRLENBQUNDLGNBQVQsQ0FBd0IwUCxHQUF4QixFQUE2Qi9MLFNBQTdCLEdBQXlDOEwsR0FBRyxDQUFDQyxHQUFELENBQTVDO0FBQ0g7QUFDSjtBQUNKOztBQUFBOztBQUNELFNBQVNGLGlCQUFULENBQTJCQyxHQUEzQixFQUFnQztBQUM1QixPQUFLLElBQU1DLEdBQVgsSUFBa0JELEdBQWxCLEVBQXVCO0FBQ25CLFFBQUcxUCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IwUCxHQUF4QixNQUErQixJQUFsQyxFQUF3QztBQUNwQzNQLGNBQVEsQ0FBQ0MsY0FBVCxDQUF3QjBQLEdBQXhCLEVBQTZCdEUsV0FBN0IsR0FBMkNxRSxHQUFHLENBQUNDLEdBQUQsQ0FBOUM7QUFDSDtBQUNKO0FBQ0o7O0FBQUEsQzs7Ozs7Ozs7Ozs7O0FDeENEO0FBQWU7QUFDWC9GLElBQUUsRUFBQztBQUFFQyxRQUFJLEVBQUM7QUFDRlMsV0FBSyxFQUFFLGlCQURMO0FBRUZzRixjQUFRLEVBQUUsY0FGUjtBQUdGQyxnQkFBVSxFQUFFLFFBSFY7QUFJRkMsZUFBUyxFQUFFLFdBSlQ7QUFLRkMsZ0JBQVUsRUFBRSxtQkFMVjtBQU1GQyxnQkFBVSxFQUFFLFNBTlY7QUFPRkMsZ0JBQVUsRUFBRSxlQVBWO0FBUUZDLGtCQUFZLEVBQUUsU0FSWjtBQVNGQyx5QkFBbUIsRUFBQywwQ0FUbEI7QUFVRkMscUJBQWUsRUFBQztBQVZkLEtBQVA7QUFZQy9FLGVBQVcsRUFBQztBQUNSN0ssV0FBSyxFQUFDLE9BREU7QUFFUjZQLGVBQVMsRUFBQyxVQUZGO0FBR1J6UCxlQUFTLEVBQUMsa0JBSEY7QUFJUkMsV0FBSyxFQUFDLFFBSkU7QUFLUkMsV0FBSyxFQUFDLGNBTEU7QUFNUndQLGFBQU8sRUFBRTtBQU5EO0FBWmIsR0FEUTtBQXVCWDVGLElBQUUsRUFBQztBQUFFYixRQUFJLEVBQUU7QUFDSFMsV0FBSyxFQUFFLHdCQURKO0FBRUhzRixjQUFRLEVBQUUsYUFGUDtBQUdIQyxnQkFBVSxFQUFFLFFBSFQ7QUFJSEMsZUFBUyxFQUFFLFNBSlI7QUFLSEMsZ0JBQVUsRUFBRSxxQkFMVDtBQU1IQyxnQkFBVSxFQUFFLFFBTlQ7QUFPSEMsZ0JBQVUsRUFBRSxtQkFQVDtBQVFIQyxrQkFBWSxFQUFFLGlCQVJYO0FBU0hDLHlCQUFtQixFQUFDLDJDQVRqQjtBQVVIQyxxQkFBZSxFQUFDO0FBVmIsS0FBUjtBQVlDL0UsZUFBVyxFQUFDO0FBQ1I3SyxXQUFLLEVBQUMsZUFERTtBQUVSNlAsZUFBUyxFQUFDLGdCQUZGO0FBR1J6UCxlQUFTLEVBQUMsa0JBSEY7QUFJUkMsV0FBSyxFQUFDLGVBSkU7QUFLUkMsV0FBSyxFQUFDLDBCQUxFO0FBTVJ3UCxhQUFPLEVBQUU7QUFORDtBQVpiO0FBdkJRLENBQWYsRTs7Ozs7Ozs7Ozs7O0FDQUM7QUFBQTtBQUFBO0FBQUEsU0FBU0MsZ0JBQVQsR0FBMkI7QUFDdkIsT0FBS2hQLEtBQUwsR0FBYSxLQUFLQSxLQUFMLENBQVc2QixPQUFYLENBQW1CLFVBQW5CLEVBQStCLEVBQS9CLEVBQW1DQSxPQUFuQyxDQUEyQyxXQUEzQyxFQUF3RCxJQUF4RCxDQUFiO0FBQ0oxQyxrQkFBZ0IsQ0FBQyxJQUFELEVBQU8sQ0FBUCxDQUFoQjtBQUNBOztBQUVELFNBQVNBLGdCQUFULENBQTBCOFAsS0FBMUIsRUFBaUNDLFNBQWpDLEVBQTRDO0FBQ3ZDLE1BQUdELEtBQUssQ0FBQ2pQLEtBQU4sQ0FBWXNDLE1BQVosSUFBc0I0TSxTQUF6QixFQUFxQztBQUNqQ0QsU0FBSyxDQUFDalAsS0FBTixHQUFjaVAsS0FBSyxDQUFDalAsS0FBTixDQUFZbVAsTUFBWixDQUFtQixDQUFuQixFQUFzQkQsU0FBdEIsQ0FBZDtBQUNIO0FBQ0w7Ozs7Ozs7Ozs7Ozs7QUNURCx1Qzs7Ozs7Ozs7Ozs7QUNBQSx1QyIsImZpbGUiOiJhY2NvdW50U2V0dGluZ3MuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9zY3JpcHQvYWNjb3VudFNldHRpbmdWYWxpZGF0aW9uLmpzXCIpO1xuIiwiLy8gc2hpbSBmb3IgdXNpbmcgcHJvY2VzcyBpbiBicm93c2VyXG52YXIgcHJvY2VzcyA9IG1vZHVsZS5leHBvcnRzID0ge307XG5cbi8vIGNhY2hlZCBmcm9tIHdoYXRldmVyIGdsb2JhbCBpcyBwcmVzZW50IHNvIHRoYXQgdGVzdCBydW5uZXJzIHRoYXQgc3R1YiBpdFxuLy8gZG9uJ3QgYnJlYWsgdGhpbmdzLiAgQnV0IHdlIG5lZWQgdG8gd3JhcCBpdCBpbiBhIHRyeSBjYXRjaCBpbiBjYXNlIGl0IGlzXG4vLyB3cmFwcGVkIGluIHN0cmljdCBtb2RlIGNvZGUgd2hpY2ggZG9lc24ndCBkZWZpbmUgYW55IGdsb2JhbHMuICBJdCdzIGluc2lkZSBhXG4vLyBmdW5jdGlvbiBiZWNhdXNlIHRyeS9jYXRjaGVzIGRlb3B0aW1pemUgaW4gY2VydGFpbiBlbmdpbmVzLlxuXG52YXIgY2FjaGVkU2V0VGltZW91dDtcbnZhciBjYWNoZWRDbGVhclRpbWVvdXQ7XG5cbmZ1bmN0aW9uIGRlZmF1bHRTZXRUaW1vdXQoKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdzZXRUaW1lb3V0IGhhcyBub3QgYmVlbiBkZWZpbmVkJyk7XG59XG5mdW5jdGlvbiBkZWZhdWx0Q2xlYXJUaW1lb3V0ICgpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ2NsZWFyVGltZW91dCBoYXMgbm90IGJlZW4gZGVmaW5lZCcpO1xufVxuKGZ1bmN0aW9uICgpIHtcbiAgICB0cnkge1xuICAgICAgICBpZiAodHlwZW9mIHNldFRpbWVvdXQgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBzZXRUaW1lb3V0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IGRlZmF1bHRTZXRUaW1vdXQ7XG4gICAgICAgIH1cbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBkZWZhdWx0U2V0VGltb3V0O1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICBpZiAodHlwZW9mIGNsZWFyVGltZW91dCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gY2xlYXJUaW1lb3V0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gZGVmYXVsdENsZWFyVGltZW91dDtcbiAgICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gZGVmYXVsdENsZWFyVGltZW91dDtcbiAgICB9XG59ICgpKVxuZnVuY3Rpb24gcnVuVGltZW91dChmdW4pIHtcbiAgICBpZiAoY2FjaGVkU2V0VGltZW91dCA9PT0gc2V0VGltZW91dCkge1xuICAgICAgICAvL25vcm1hbCBlbnZpcm9tZW50cyBpbiBzYW5lIHNpdHVhdGlvbnNcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9XG4gICAgLy8gaWYgc2V0VGltZW91dCB3YXNuJ3QgYXZhaWxhYmxlIGJ1dCB3YXMgbGF0dGVyIGRlZmluZWRcbiAgICBpZiAoKGNhY2hlZFNldFRpbWVvdXQgPT09IGRlZmF1bHRTZXRUaW1vdXQgfHwgIWNhY2hlZFNldFRpbWVvdXQpICYmIHNldFRpbWVvdXQpIHtcbiAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IHNldFRpbWVvdXQ7XG4gICAgICAgIHJldHVybiBzZXRUaW1lb3V0KGZ1biwgMCk7XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIC8vIHdoZW4gd2hlbiBzb21lYm9keSBoYXMgc2NyZXdlZCB3aXRoIHNldFRpbWVvdXQgYnV0IG5vIEkuRS4gbWFkZG5lc3NcbiAgICAgICAgcmV0dXJuIGNhY2hlZFNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9IGNhdGNoKGUpe1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgLy8gV2hlbiB3ZSBhcmUgaW4gSS5FLiBidXQgdGhlIHNjcmlwdCBoYXMgYmVlbiBldmFsZWQgc28gSS5FLiBkb2Vzbid0IHRydXN0IHRoZSBnbG9iYWwgb2JqZWN0IHdoZW4gY2FsbGVkIG5vcm1hbGx5XG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKG51bGwsIGZ1biwgMCk7XG4gICAgICAgIH0gY2F0Y2goZSl7XG4gICAgICAgICAgICAvLyBzYW1lIGFzIGFib3ZlIGJ1dCB3aGVuIGl0J3MgYSB2ZXJzaW9uIG9mIEkuRS4gdGhhdCBtdXN0IGhhdmUgdGhlIGdsb2JhbCBvYmplY3QgZm9yICd0aGlzJywgaG9wZnVsbHkgb3VyIGNvbnRleHQgY29ycmVjdCBvdGhlcndpc2UgaXQgd2lsbCB0aHJvdyBhIGdsb2JhbCBlcnJvclxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZFNldFRpbWVvdXQuY2FsbCh0aGlzLCBmdW4sIDApO1xuICAgICAgICB9XG4gICAgfVxuXG5cbn1cbmZ1bmN0aW9uIHJ1bkNsZWFyVGltZW91dChtYXJrZXIpIHtcbiAgICBpZiAoY2FjaGVkQ2xlYXJUaW1lb3V0ID09PSBjbGVhclRpbWVvdXQpIHtcbiAgICAgICAgLy9ub3JtYWwgZW52aXJvbWVudHMgaW4gc2FuZSBzaXR1YXRpb25zXG4gICAgICAgIHJldHVybiBjbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9XG4gICAgLy8gaWYgY2xlYXJUaW1lb3V0IHdhc24ndCBhdmFpbGFibGUgYnV0IHdhcyBsYXR0ZXIgZGVmaW5lZFxuICAgIGlmICgoY2FjaGVkQ2xlYXJUaW1lb3V0ID09PSBkZWZhdWx0Q2xlYXJUaW1lb3V0IHx8ICFjYWNoZWRDbGVhclRpbWVvdXQpICYmIGNsZWFyVGltZW91dCkge1xuICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBjbGVhclRpbWVvdXQ7XG4gICAgICAgIHJldHVybiBjbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gd2hlbiB3aGVuIHNvbWVib2R5IGhhcyBzY3Jld2VkIHdpdGggc2V0VGltZW91dCBidXQgbm8gSS5FLiBtYWRkbmVzc1xuICAgICAgICByZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0KG1hcmtlcik7XG4gICAgfSBjYXRjaCAoZSl7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICAvLyBXaGVuIHdlIGFyZSBpbiBJLkUuIGJ1dCB0aGUgc2NyaXB0IGhhcyBiZWVuIGV2YWxlZCBzbyBJLkUuIGRvZXNuJ3QgIHRydXN0IHRoZSBnbG9iYWwgb2JqZWN0IHdoZW4gY2FsbGVkIG5vcm1hbGx5XG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0LmNhbGwobnVsbCwgbWFya2VyKTtcbiAgICAgICAgfSBjYXRjaCAoZSl7XG4gICAgICAgICAgICAvLyBzYW1lIGFzIGFib3ZlIGJ1dCB3aGVuIGl0J3MgYSB2ZXJzaW9uIG9mIEkuRS4gdGhhdCBtdXN0IGhhdmUgdGhlIGdsb2JhbCBvYmplY3QgZm9yICd0aGlzJywgaG9wZnVsbHkgb3VyIGNvbnRleHQgY29ycmVjdCBvdGhlcndpc2UgaXQgd2lsbCB0aHJvdyBhIGdsb2JhbCBlcnJvci5cbiAgICAgICAgICAgIC8vIFNvbWUgdmVyc2lvbnMgb2YgSS5FLiBoYXZlIGRpZmZlcmVudCBydWxlcyBmb3IgY2xlYXJUaW1lb3V0IHZzIHNldFRpbWVvdXRcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQuY2FsbCh0aGlzLCBtYXJrZXIpO1xuICAgICAgICB9XG4gICAgfVxuXG5cblxufVxudmFyIHF1ZXVlID0gW107XG52YXIgZHJhaW5pbmcgPSBmYWxzZTtcbnZhciBjdXJyZW50UXVldWU7XG52YXIgcXVldWVJbmRleCA9IC0xO1xuXG5mdW5jdGlvbiBjbGVhblVwTmV4dFRpY2soKSB7XG4gICAgaWYgKCFkcmFpbmluZyB8fCAhY3VycmVudFF1ZXVlKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgZHJhaW5pbmcgPSBmYWxzZTtcbiAgICBpZiAoY3VycmVudFF1ZXVlLmxlbmd0aCkge1xuICAgICAgICBxdWV1ZSA9IGN1cnJlbnRRdWV1ZS5jb25jYXQocXVldWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHF1ZXVlSW5kZXggPSAtMTtcbiAgICB9XG4gICAgaWYgKHF1ZXVlLmxlbmd0aCkge1xuICAgICAgICBkcmFpblF1ZXVlKCk7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBkcmFpblF1ZXVlKCkge1xuICAgIGlmIChkcmFpbmluZykge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIHZhciB0aW1lb3V0ID0gcnVuVGltZW91dChjbGVhblVwTmV4dFRpY2spO1xuICAgIGRyYWluaW5nID0gdHJ1ZTtcblxuICAgIHZhciBsZW4gPSBxdWV1ZS5sZW5ndGg7XG4gICAgd2hpbGUobGVuKSB7XG4gICAgICAgIGN1cnJlbnRRdWV1ZSA9IHF1ZXVlO1xuICAgICAgICBxdWV1ZSA9IFtdO1xuICAgICAgICB3aGlsZSAoKytxdWV1ZUluZGV4IDwgbGVuKSB7XG4gICAgICAgICAgICBpZiAoY3VycmVudFF1ZXVlKSB7XG4gICAgICAgICAgICAgICAgY3VycmVudFF1ZXVlW3F1ZXVlSW5kZXhdLnJ1bigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHF1ZXVlSW5kZXggPSAtMTtcbiAgICAgICAgbGVuID0gcXVldWUubGVuZ3RoO1xuICAgIH1cbiAgICBjdXJyZW50UXVldWUgPSBudWxsO1xuICAgIGRyYWluaW5nID0gZmFsc2U7XG4gICAgcnVuQ2xlYXJUaW1lb3V0KHRpbWVvdXQpO1xufVxuXG5wcm9jZXNzLm5leHRUaWNrID0gZnVuY3Rpb24gKGZ1bikge1xuICAgIHZhciBhcmdzID0gbmV3IEFycmF5KGFyZ3VtZW50cy5sZW5ndGggLSAxKTtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGFyZ3NbaSAtIDFdID0gYXJndW1lbnRzW2ldO1xuICAgICAgICB9XG4gICAgfVxuICAgIHF1ZXVlLnB1c2gobmV3IEl0ZW0oZnVuLCBhcmdzKSk7XG4gICAgaWYgKHF1ZXVlLmxlbmd0aCA9PT0gMSAmJiAhZHJhaW5pbmcpIHtcbiAgICAgICAgcnVuVGltZW91dChkcmFpblF1ZXVlKTtcbiAgICB9XG59O1xuXG4vLyB2OCBsaWtlcyBwcmVkaWN0aWJsZSBvYmplY3RzXG5mdW5jdGlvbiBJdGVtKGZ1biwgYXJyYXkpIHtcbiAgICB0aGlzLmZ1biA9IGZ1bjtcbiAgICB0aGlzLmFycmF5ID0gYXJyYXk7XG59XG5JdGVtLnByb3RvdHlwZS5ydW4gPSBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5mdW4uYXBwbHkobnVsbCwgdGhpcy5hcnJheSk7XG59O1xucHJvY2Vzcy50aXRsZSA9ICdicm93c2VyJztcbnByb2Nlc3MuYnJvd3NlciA9IHRydWU7XG5wcm9jZXNzLmVudiA9IHt9O1xucHJvY2Vzcy5hcmd2ID0gW107XG5wcm9jZXNzLnZlcnNpb24gPSAnJzsgLy8gZW1wdHkgc3RyaW5nIHRvIGF2b2lkIHJlZ2V4cCBpc3N1ZXNcbnByb2Nlc3MudmVyc2lvbnMgPSB7fTtcblxuZnVuY3Rpb24gbm9vcCgpIHt9XG5cbnByb2Nlc3Mub24gPSBub29wO1xucHJvY2Vzcy5hZGRMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLm9uY2UgPSBub29wO1xucHJvY2Vzcy5vZmYgPSBub29wO1xucHJvY2Vzcy5yZW1vdmVMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUFsbExpc3RlbmVycyA9IG5vb3A7XG5wcm9jZXNzLmVtaXQgPSBub29wO1xucHJvY2Vzcy5wcmVwZW5kTGlzdGVuZXIgPSBub29wO1xucHJvY2Vzcy5wcmVwZW5kT25jZUxpc3RlbmVyID0gbm9vcDtcblxucHJvY2Vzcy5saXN0ZW5lcnMgPSBmdW5jdGlvbiAobmFtZSkgeyByZXR1cm4gW10gfVxuXG5wcm9jZXNzLmJpbmRpbmcgPSBmdW5jdGlvbiAobmFtZSkge1xuICAgIHRocm93IG5ldyBFcnJvcigncHJvY2Vzcy5iaW5kaW5nIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5cbnByb2Nlc3MuY3dkID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gJy8nIH07XG5wcm9jZXNzLmNoZGlyID0gZnVuY3Rpb24gKGRpcikge1xuICAgIHRocm93IG5ldyBFcnJvcigncHJvY2Vzcy5jaGRpciBpcyBub3Qgc3VwcG9ydGVkJyk7XG59O1xucHJvY2Vzcy51bWFzayA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gMDsgfTtcbiIsIm1vZHVsZS5leHBvcnRzID0ge1xuICBwb3J0OiBwcm9jZXNzLmVudi5QT1JUIHx8IDMwMDAsXG4gIHJvdXRpbmc6IHtcbiAgICBhdXRob3JpemF0aW9uOiAnL2F1dGhvcml6YXRpb24nLFxuICAgIHJlZ2lzdHJhdGlvbjogJy9yZWdpc3RyYXRpb24nLFxuICAgIGdyb3VwU3R1ZGVudDogJy9ncm91cFN0dWRlbnQnLFxuICAgIGdldEFsbEdyb3VwczogJy9nZXRBbGxHcm91cHMnLFxuICAgIGFjY291bnRTZXR0aW5nOiAnL2FjY291bnRTZXR0aW5nJyxcbiAgICBncm91cHM6ICcvZ3JvdXBzJyxcbiAgICB0YWJsZTogJy8nLFxuICAgIHVwZGF0ZTogJy91cGRhdGUnLFxuICAgIGRlbGV0ZTogJy9kZWxldGUnLFxuICAgIGFjY291bnRVcGRhdGU6ICcvYWNjb3VudC11cGRhdGUnLFxuICAgIGZvcmdvdHRlblBhc3N3b3JkOiAnL2ZvcmdvdHRlbi1wYXNzd29yZCcsXG4gICAgZGVsZXRlR3JvdXA6ICcvZGVsZXRlLWdyb3VwJyxcbiAgICBzdHVkZW50Q2xlYXI6ICcvc3R1ZGVudC1jbGVhcicsXG4gICAgdXBkYXRlR3JvdXA6ICcvdXBkYXRlLWdyb3VwJyxcbiAgICBzZW5kSW1hZ2U6ICcvc2VuZC1pbWFnZScsXG4gICAgcmVzZXRTZXR0aW5nczogJy9yZXNldC1zZXR0aW5ncydcblxuICB9XG59O1xuIiwiZXhwb3J0IGRlZmF1bHQgX193ZWJwYWNrX3B1YmxpY19wYXRoX18gKyBcIjAwNGQxNzZjMmY3NjVhMzRhNzg5YmYyNTI3NzFjZDc0LnN2Z1wiOyIsImV4cG9ydCBkZWZhdWx0IF9fd2VicGFja19wdWJsaWNfcGF0aF9fICsgXCI0OTVhZWNlMDU4NGIyN2NkN2Q4MmY4YTExODcwMzdhMC5zdmdcIjsiLCJpbXBvcnQge2NoYW5nZUxhbmd1YWdlfSBmcm9tICcuL2hlbHBlcnMvbGFuZ3VhZ2UvbGFuZ3VhZ2UuanMnO1xuaW1wb3J0IHtjaGVja1ZhbHVlTGVuZ3RofSBmcm9tIFwiLi9oZWxwZXJzL3NjcmlwdFZhbGlkYXRpb25cIjtcbmltcG9ydCBcIi4uL3N0eWxlL2FjY291bnRTZXR0aW5ncy5sZXNzXCI7XG5pbXBvcnQgXCIuLi9zdHlsZS9oZWFkZXIubGVzc1wiXG52YXIgY29uc3RhbnRzID0gcmVxdWlyZSgnLi4vLi4vc2VydmVyL2hlbHBlcnMvY29uc3RhbnRzJyk7XG5cbmltcG9ydCBiYWxsc0dhbWUgZnJvbSBcIi4vaGVscGVycy9iYWxsc0dhbWVcIlxuLy8gaW1wb3J0IGxvZ29UZWFjaGVyIGZyb20gJy4vaW1hZ2VzL2JhdG1hbi5wbmcnO1xuaW1wb3J0IGJhY2tBcnJvdyBmcm9tICcuLi8uLi9zcmMvYXNzZXRzL2JhY2stYXJyb3cuc3ZnJztcbmltcG9ydCBsb2dvdXQgZnJvbSAnLi4vLi4vc3JjL2Fzc2V0cy9sb2dvdXQuc3ZnJztcblxuaW1wb3J0IHt3ZWJwYWNrTG9jYWxIb3N0fSBmcm9tIFwiLi9oZWxwZXJzL2NvbnN0XCI7XG5pbXBvcnQge2xvY2FsaG9zdFNlcnZ9IGZyb20gXCIuL2hlbHBlcnMvY29uc3RcIjtcblxudmFyIGZ1bGxzY3JlZW4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZnVsbHNjcmVlbicpO1xudmFyIGV4aXRDYWJpbmV0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2V4aXRDYWJpbmV0Jyk7XG5cbnZhciB0ZWFjaGVySWNvbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd0ZWFjaGVySWNvbicpO1xuXG4vLyBhZGQgY3VzdG9tIGljb25cbi8vIGZ1bmN0aW9uIHNob3dJbnB1dERvd25sb2FkSW1hZ2VcblxuZXhpdENhYmluZXQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgZG9jdW1lbnQubG9jYXRpb24uaHJlZiA9IGAke3dlYnBhY2tMb2NhbEhvc3R9L2luZGV4Lmh0bWxgXG59KTtcblxudmFyIGxvZ2luID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJsb2dpblwiKTtcbmxvZ2luLm9uaW5wdXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgY2hlY2tWYWx1ZUxlbmd0aCh0aGlzLCAxNCk7XG59IDtcbnZhciBwYXNzd29yZCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmQxXCIpO1xucGFzc3dvcmQub25pbnB1dCA9IGZ1bmN0aW9uICgpIHtcbiAgICBjaGVja1ZhbHVlTGVuZ3RoKHRoaXMsIDI1KTtcbn0gO1xudmFyIHBhc3N3b3JkMiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmQyXCIpO1xucGFzc3dvcmQyLm9uaW5wdXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgY2hlY2tWYWx1ZUxlbmd0aCh0aGlzLCAyNSk7XG59IDtcbnZhciBlbWFpbCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZW1haWxcIik7XG5lbWFpbC5vbmlucHV0ID0gZnVuY3Rpb24gKCkge1xuICAgIGNoZWNrVmFsdWVMZW5ndGgodGhpcywgMjUpO1xufSA7XG52YXIgcGhvbmUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBob25lXCIpO1xucGhvbmUub25pbnB1dCA9IGZ1bmN0aW9uICgpIHtcbiAgICBjaGVja1ZhbHVlTGVuZ3RoKHRoaXMsIDEzKTtcbn0gO1xuXG5sZXQgYWJvdXRNeXNlbGYgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImFib3V0TXlzZWxmXCIpO1xuYWJvdXRNeXNlbGYuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIGNoZWNrQ3VycmVudExlbmd0aFRleHQpO1xuXG5sZXQgYWJvdXRNeXNlbGZDb3VudGVyID0gNTAwO1xuXG5sZXQgdGV4dEFyZWFDb3VudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidGV4dEFyZWFDb3VudFwiKTtcbmxldCBjaGFuZ2VJbnB1dHMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNoYW5nZUlucHV0c1wiKTtcbmxldCBTYXZlQnRuID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJTYXZlQnRuXCIpO1xuY2hhbmdlSW5wdXRzLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xuICAgIGNoYW5nZUJ1dHRvbnNEaXNwbGF5KFNhdmVCdG4sIGNoYW5nZUlucHV0cyk7XG4gICAgaWYoc2VsZWN0RWxlbWVudExhbmd1YWdlLnZhbHVlIT1cInJ1XCIpe1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImxvZ2luVGl0bGVcIikuaW5uZXJUZXh0PVwiTmV3IExvZ2luXCI7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmQxVGl0bGVcIikuaW5uZXJUZXh0PVwiTmV3IFBhc3N3b3JkXCI7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZW1haWxUaXRsZVwiKS5pbm5lclRleHQ9XCJOZXcgRW1haWxcIjtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwaG9uZVRpdGxlXCIpLmlubmVyVGV4dD1cIk5ldyBQaG9uZVwiO1xuICAgIH1lbHNle1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImxvZ2luVGl0bGVcIikuaW5uZXJUZXh0PVwi0J3QvtCy0YvQuSDQu9C+0LPQuNC9XCI7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmQxVGl0bGVcIikuaW5uZXJUZXh0PVwi0J3QvtCy0YvQuSDQv9Cw0YDQvtC70YxcIjtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJlbWFpbFRpdGxlXCIpLmlubmVyVGV4dD1cItCd0L7QstCw0Y8g0L/QvtGH0YLQsFwiO1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBob25lVGl0bGVcIikuaW5uZXJUZXh0PVwi0J3QvtCy0YvQuSDRgtC10LvQtdGE0L7QvVwiO1xuICAgIH1cblxufSk7XG5TYXZlQnRuLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCAoKSA9PiB7Y2hhbmdlQnV0dG9uc0Rpc3BsYXkoU2F2ZUJ0biwgY2hhbmdlSW5wdXRzKTsgZ2V0VmFsaWRhdGlvbigpO1xuICAgIGlmKHNlbGVjdEVsZW1lbnRMYW5ndWFnZS52YWx1ZSE9XCJydVwiKXtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJsb2dpblRpdGxlXCIpLmlubmVyVGV4dD1cIkxvZ2luXCI7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmQxVGl0bGVcIikuaW5uZXJUZXh0PVwiUGFzc3dvcmRcIjtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJlbWFpbFRpdGxlXCIpLmlubmVyVGV4dD1cIkVtYWlsXCI7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGhvbmVUaXRsZVwiKS5pbm5lclRleHQ9XCJQaG9uZVwiO1xuICAgIH1lbHNle1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImxvZ2luVGl0bGVcIikuaW5uZXJUZXh0PVwi0JvQvtCz0LjQvVwiO1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBhc3N3b3JkMVRpdGxlXCIpLmlubmVyVGV4dD1cItCf0LDRgNC+0LvRjFwiO1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImVtYWlsVGl0bGVcIikuaW5uZXJUZXh0PVwi0J/QvtGH0YLQsFwiO1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBob25lVGl0bGVcIikuaW5uZXJUZXh0PVwi0KLQtdC70LXRhNC+0L1cIjtcbiAgICB9fSApO1xuXG5cbmxldCB0ZWFjaGVyID0gZ2V0VXNlckRhdGEoKTtcbnNldFRpbWVvdXQoZmlsbElucHV0LCA4MDApO1xuXG5sZXQgY2xvc2VCdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNsb3NlXCIpO1xuY2xvc2VCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCgpID0+IHtkb2N1bWVudC5sb2NhdGlvbi5ocmVmID0gYCR7d2VicGFja0xvY2FsSG9zdH0vdGFibGUuaHRtbGB9KTtcblxudmFyIHNlbGVjdEVsZW1lbnRMYW5ndWFnZT1kb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNlbGVjdEVsZW1lbnRMYW5ndWFnZVwiKTtcbnNlbGVjdEVsZW1lbnRMYW5ndWFnZS5vbmNoYW5nZSA9ICgpPT57Y2hhbmdlTGFuZ3VhZ2UoXCJhY2NTZXRcIil9O1xuXG5cbmZ1bmN0aW9uIG1haW4oKXtcbiAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShcImxhbmd1YWdlXCIpIT1udWxsKXtcbiAgICAgICAgc2VsZWN0RWxlbWVudExhbmd1YWdlLnZhbHVlPXNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJsYW5ndWFnZVwiKTtcbiAgICAgICAgY2hhbmdlTGFuZ3VhZ2UoXCJhY2NTZXRcIik7XG4gICAgfWVsc2V7XG4gICAgICAgIGNoYW5nZUxhbmd1YWdlKFwiYWNjU2V0XCIpO31cbn1cblxuZnVuY3Rpb24gZ2V0VXNlckRhdGEoKSB7XG4gICAgbGV0IHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuICAgIHhoci5vcGVuKFwiR0VUXCIsIGAke2xvY2FsaG9zdFNlcnZ9JHtjb25zdGFudHMucm91dGluZy5hY2NvdW50U2V0dGluZ31gKTtcbiAgICB4aHIuc2V0UmVxdWVzdEhlYWRlcihcIkNvbnRlbnQtdHlwZVwiLCBcImFwcGxpY2F0aW9uL2pzb25cIik7XG4gICAgeGhyLnNlbmQoKTtcbiAgICB4aHIub25sb2FkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0ZWFjaGVyID0gSlNPTi5wYXJzZSh0aGlzLnJlc3BvbnNlKTtcbiAgICAgICAgY29uc29sZS5sb2codGVhY2hlcik7XG4gICAgfTtcbn1cblxuZnVuY3Rpb24gZmlsbElucHV0KCkge1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibG9naW5cIikudmFsdWUgPSB0ZWFjaGVyWzBdLmxvZ2luO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZW1haWxcIikudmFsdWUgPSB0ZWFjaGVyWzBdLmVtYWlsO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmQxXCIpLnZhbHVlID0gdGVhY2hlclswXS5wYXNzd29yZDtcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBhc3N3b3JkMlwiKS52YWx1ZSA9IHRlYWNoZXJbMF0ucGFzc3dvcmQ7XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwaG9uZVwiKS52YWx1ZSA9IHRlYWNoZXJbMF0ucGhvbmVfbnVtYmVyO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYWJvdXRNeXNlbGZcIikudmFsdWUgPSB0ZWFjaGVyWzBdLmFib3V0X215c2VsZjtcbiAgICAvLyBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRlYWNoZXJJY29uXCIpLnNyYyA9IHRlYWNoZXJbMF0udGVhY2hlcl9pY29uO1xufVxuXG5sZXQgZXJyID0gXCJcIjtcblxuZnVuY3Rpb24gZ2V0VmFsaWRhdGlvbigpIHtcbiAgICAvLyBsZXQgZWxlbWVudFZhbHVlID0gbmV3IFVzZXIoKTtcbiAgICBsZXQgZWxlbWVudFZhbHVlID0ge1xuICAgICAgICBsb2dpbjogbG9naW4udmFsdWUucmVwbGFjZSgvXFxzL2csICcnKSxcbiAgICBwYXNzd29yZDpwYXNzd29yZC52YWx1ZSxcbiAgICAgICAgcGFzc3dvcmQyOiAgIHBhc3N3b3JkMi52YWx1ZSxcbiAgICAgICAgZW1haWw6ICAgZW1haWwudmFsdWUsXG4gICAgICAgIHBob25lOiAgcGhvbmUudmFsdWUucmVwbGFjZSgvWysoKS0vXFxzXS9nLCAnJyksXG4gICAgICAgIGFib3V0TXlzZWxmOiAgYWJvdXRNeXNlbGYgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImFib3V0TXlzZWxmXCIpLnZhbHVlLFxuICAgICAgICB0ZWFjaGVyc19pZDogdGVhY2hlclswXS50ZWFjaGVyc19pZCxcbiAgICAgICAgLy8gdGhpcy5pY29uID0gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaWNvblwiKS52YWx1ZTtcbiAgICB9O1xuICAgIGNvbnNvbGUubG9nKGVsZW1lbnRWYWx1ZSlcbiAgICBsZXQgbWVzc2FnZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWVzc2FnZVwiKTtcbiAgICBpZiAoY2hlY2tFbWFpbChlbGVtZW50VmFsdWUuZW1haWwpICYmIGNoZWNrbG9naW4oZWxlbWVudFZhbHVlLmxvZ2luKSAmJiBjaGVja1Bhc3MoZWxlbWVudFZhbHVlLnBhc3N3b3JkLCBlbGVtZW50VmFsdWUucGFzc3dvcmQyKSAmJiBjaGVja1Bob25lKGVsZW1lbnRWYWx1ZS5waG9uZSwgXCIzODBcIikpIHtcbiAgICAgICAgbGV0IHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuICAgICAgICB4aHIub3BlbihcIlBPU1RcIiwgYCR7bG9jYWxob3N0U2Vydn0ke2NvbnN0YW50cy5yb3V0aW5nLmFjY291bnRVcGRhdGV9YCk7XG4gICAgICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKFwiQ29udGVudC10eXBlXCIsIFwiYXBwbGljYXRpb24vanNvblwiKTtcbiAgICAgICAgeGhyLnNlbmQoSlNPTi5zdHJpbmdpZnkoZWxlbWVudFZhbHVlKSk7XG4gICAgICAgIG1lc3NhZ2UuaW5uZXJIVE1MID0gXCJDaGFuZ2VzIGluY2x1ZGVkXCI7XG4gICAgICAgIGNvbnNvbGUubG9nKGVsZW1lbnRWYWx1ZSk7XG4gICAgICAgIC8vIHNldFRpbWVvdXQoZG9jdW1lbnQubG9jYXRpb24uaHJlZiA9IGAke3dlYnBhY2tMb2NhbEhvc3R9L3RhYmxlLmh0bWxgLCAxNTAwKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBtZXNzYWdlLmlubmVySFRNTCA9IGVycjtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGNoZWNrbG9naW4obG9naW4pIHtcbiAgICBpZiAobG9naW4ubGVuZ3RoIDwgMSB8fCAhaXNOYU4oTnVtYmVyKGxvZ2luWzBdKSkpIHtcbiAgICAgICAgZXJyID0gXCLQndC10LrQvtGA0YDQtdC60YLQvdGL0Lkg0LvQvtCz0LjQvVwiO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xufVxuXG5mdW5jdGlvbiBjaGVja1Bhc3MocGFzczEsIHBhc3MyKSB7XG4gICAgaWYgKHBhc3MxLmxlbmd0aCA8IDUgfHwgcGFzczEgIT09IHBhc3MyKSB7XG4gICAgICAgIGVyciA9IFwi0J3QtdC60L7RgNGA0LXQutGC0L3Ri9C5INC/0LDRgNC+0LvRjFwiO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xufVxuXG5mdW5jdGlvbiBjaGVja1Bob25lKHBob25lLCBjb3VudHJpQ29kZSkge1xuICAgIGlmIChwaG9uZS5sZW5ndGggIT09IDEyIHx8IHBob25lLnN1YnN0cmluZygwLCAzKSAhPT0gY291bnRyaUNvZGUpIHtcbiAgICAgICAgZXJyID0gXCLQndC10LrQvtGA0YDQtdC60YLQvdGL0LnRgtC10LvQtdGE0L7QvVwiO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xufVxuZnVuY3Rpb24gY2hlY2tFbWFpbChlbWFpbCkge1xuICAgIGlmIChlbWFpbC5pbmNsdWRlcyhcIkBcIikpIHtcbiAgICAgICAgbGV0IGRvbWVuID0gZW1haWwuc3Vic3RyaW5nKGVtYWlsLmluZGV4T2YoXCJAXCIpLCBlbWFpbC5sZW5ndGgpO1xuICAgICAgICBpZiAoZG9tZW4ubGVuZ3RoID4gMikge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cblxuICAgIH1cbiAgICBlcnIgPSBcItCd0LXQutC+0YDRgNC10LrRgtC90YvQuSBlLW1haWxcIjtcbiAgICByZXR1cm4gZmFsc2U7XG59XG5cbmZ1bmN0aW9uIGNoZWNrQ3VycmVudExlbmd0aFRleHQoKSB7XG4gICAgaWYgKGFib3V0TXlzZWxmLnZhbHVlLmxlbmd0aCA+IGFib3V0TXlzZWxmQ291bnRlcikge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgdGV4dEFyZWFDb3VudC5pbm5lclRleHQgPSAgU3RyaW5nKGFib3V0TXlzZWxmQ291bnRlciAtIGFib3V0TXlzZWxmLnZhbHVlLmxlbmd0aCk7XG59XG5cbi8vICB0b2dnbGUgYnV0dG9ucyBjaGFuZ2UgYW5kIHNhdmVcbiAgICBmdW5jdGlvbiBpbnB1dFRvZ2dsZXJEaXNhYmxlZChjbGFzc05hbWUpIHsgIC8vICdjdXN0b20taW5wdXQnXG4gICAgICAgIGxldCBpbnB1dENvbGxlY3Rpb24gPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCBjbGFzc05hbWUpO1xuICAgICAgICBmb3IgKGxldCBpdGVtIG9mIGlucHV0Q29sbGVjdGlvbikge1xuICAgICAgICAgICAgaXRlbS5kaXNhYmxlZCA9PT0gdHJ1ZSA/IGl0ZW0uZGlzYWJsZWQgPSBmYWxzZTogaXRlbS5kaXNhYmxlZCA9IHRydWU7XG4gICAgICAgIH1cbn1cbiAgICBmdW5jdGlvbiBjaGFuZ2VCdXR0b25zRGlzcGxheShidXR0b24xLCBidXR0b24yKSB7XG4gICAgICAgIGlmIChidXR0b24yLnN0eWxlLmRpc3BsYXkgPT09ICdub25lJykge1xuICAgICAgICAgICAgYnV0dG9uMS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICAgICAgICAgICAgYnV0dG9uMi5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJztcbiAgICAgICAgfWVsc2Uge1xuICAgICAgICAgICAgYnV0dG9uMS5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJztcbiAgICAgICAgICAgIGJ1dHRvbjIuc3R5bGUuZGlzcGxheSA9ICdub25lJztcbiAgICAgICAgfVxuICAgICAgICBpbnB1dFRvZ2dsZXJEaXNhYmxlZCgnY3VzdG9tLWlucHV0Jyk7XG4gICAgfVxuXG4vL2Z1bGxzY3JlZW4gbG9naWNcbmZ1bGxzY3JlZW4uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIChlKT0+e1xuXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJkaXJlY3Rpb25cIikucmVxdWVzdEZ1bGxzY3JlZW4oKTtcbn0pO1xuXG5cbnZhciBmaWxlSW5wdXQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImZpbGUtaW5wdXRcIik7XG5cbmZpbGVJbnB1dC5hZGRFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsIChlKSA9PntcblxuICAgIGNvbnNvbGUubG9nKCBlLnRhcmdldC5maWxlc1swXSlcbiAgICB2YXIgZmlsZVJlYWRlciA9IG5ldyBGaWxlUmVhZGVyO1xuICAgIGZpbGVSZWFkZXIucmVhZEFzRGF0YVVSTCAoIGUudGFyZ2V0LmZpbGVzWzBdICk7XG4gICAgZmlsZVJlYWRlci5vbmxvYWQgPSBmdW5jdGlvbiAoIGV2ZW50ICkge1xuICAgICAgICB2YXIgZGF0YSA9IHtcbiAgICAgICAgICAgIGltZyA6IGV2ZW50LnRhcmdldC5yZXN1bHRcbiAgICAgICAgfVxuICAgICAgICB2YXIgZGF0YXNlbmQgPSBKU09OLnN0cmluZ2lmeShkYXRhKVxuICAgICAgICB2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgICAgIHhoci5vcGVuKFwiUE9TVFwiLGAke2xvY2FsaG9zdFNlcnZ9JHtjb25zdGFudHMucm91dGluZy5zZW5kSW1hZ2V9YCk7XG4gICAgICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKFwiQ29udGVudC10eXBlXCIsIFwiYXBwbGljYXRpb24vanNvblwiKTtcbiAgICAgICAgeGhyLnNlbmQoZGF0YXNlbmQpO1xuICAgICAgICB4aHIub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYoeGhyLnJlYWR5U3RhdGUgPT09IDQgJiYgeGhyLnN0YXR1cyA9PT0gMjAwKXtcbiAgICAgICAgICAgICAgICB2YXIgaW1nID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0ZWFjaGVySWNvblwiKTtcbiAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImltZ1wiLCBgJHtKU09OLnBhcnNlKHRoaXMucmVzcG9uc2UpfWApO1xuICAgICAgICAgICAgICAgIGltZy5zcmMgPSBgJHtKU09OLnBhcnNlKHRoaXMucmVzcG9uc2UpfWBcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxufSk7XG5cbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgbWFpbigpO1xuICAgIGlmKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiaW1nXCIpICE9PSBudWxsKXtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0ZWFjaGVySWNvblwiKS5zcmMgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImltZ1wiKVxuICAgIH1cbn0pO1xubGV0IGxvZ29Ib21lPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImxvZ29Ib21lXCIpO1xubG9nb0hvbWUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCgpID0+IHtkb2N1bWVudC5sb2NhdGlvbi5ocmVmID0gYCR7d2VicGFja0xvY2FsSG9zdH0vdGFibGUuaHRtbGB9KTtcbiIsImxldCBhcnJheUJhbGxzID0gW107XG5sZXQgaW50ZXJ2YWwgPSB1bmRlZmluZWQ7XG5cbmNvbnN0IGNhbnZhcyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFpbi1jYW52YXNcIik7XG5jb25zdCBjdHggPSBjYW52YXMuZ2V0Q29udGV4dCgnMmQnKTtcbmNvbnN0IHN0YXJ0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3N0YXJ0Jyk7XG5jb25zdCBjbGVhciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjbGVhcicpO1xuXG5zdGFydC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtzdGFydEdhbWUoKTsgdG9nZ2xlU3RhcnREaXNhYmxlZCgpfSApO1xuY2xlYXIuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7c3RvcChpbnRlcnZhbCk7IHRvZ2dsZUNsZWFyRGlzYWJsZWQoKX0pO1xuXG4vLyB0b2dnbGUgY2xhc3Mgc3RhcnQgYW5kIGNsZWFyXG5mdW5jdGlvbiB0b2dnbGVTdGFydERpc2FibGVkKCkge1xuICBzdGFydC5zZXRBdHRyaWJ1dGUoJ2Rpc2FibGVkJywgJ3RydWUnKTtcbiAgY2xlYXIucmVtb3ZlQXR0cmlidXRlKCdkaXNhYmxlZCcpO1xufVxuXG5mdW5jdGlvbiB0b2dnbGVDbGVhckRpc2FibGVkKCkge1xuICBjbGVhci5zZXRBdHRyaWJ1dGUoJ2Rpc2FibGVkJywgJ3RydWUnKTtcbiAgc3RhcnQucmVtb3ZlQXR0cmlidXRlKCdkaXNhYmxlZCcpO1xuXG59XG5cbmZ1bmN0aW9uIHN0YXJ0R2FtZSgpIHtcbiAgaW50ZXJ2YWwgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XG4gICAgbW92ZW1lbnQoYXJyYXlCYWxscylcbiAgfSwgMTApO1xuICByZXR1cm4gaW50ZXJ2YWxcbn1cblxuZnVuY3Rpb24gc3RvcCgpIHtcbiAgY2xlYXJJbnRlcnZhbChpbnRlcnZhbCk7XG4gIGN0eC5jbGVhclJlY3QoMCwgMCwgY2FudmFzLndpZHRoLCBjYW52YXMuaGVpZ2h0KTtcbiAgYXJyYXlCYWxscyA9IFtdO1xuICBpbnRlcnZhbCA9IHVuZGVmaW5lZDtcbn1cblxuLy8gcmFuZG9tIGNpcmNsZVxuZnVuY3Rpb24gcmFuZG9tQ29sb3IoKSB7XG4gICAgbGV0IGxldHRlcnMgPSAnMDEyMzQ1Njc4OUFCQ0RFRic7XG4gICAgbGV0IGNvbG9yID0gJyMnO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgNjsgaSsrKSB7XG4gICAgICBjb2xvciArPSBsZXR0ZXJzW01hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIDE2KV07XG4gICAgfVxuICAgIHJldHVybiBjb2xvcjtcbn1cblxuZnVuY3Rpb24gcmFuZG9tTW92ZW1lbnQoKSB7XG4gIHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAoNSAtIC01KSkgKyAoLTUpO1xufVxuXG5mdW5jdGlvbiByYW5kb21SYWRpdXMoKSB7XG4gIHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAoNTUgLSA1KSkgKyA1O1xufVxuXG4vLyBWaWV3IGNvbnN0cnVjdG9yXG5mdW5jdGlvbiBWaWV3KCkge1xuICAvLyB0aGlzLmJhbGxzID0gW107XG4gIHRoaXMuY2FudmFzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYWluLWNhbnZhc1wiKTtcbiAgdGhpcy5jdHggPSB0aGlzLmNhbnZhcy5nZXRDb250ZXh0KCcyZCcpO1xuICB0aGlzLmNhbnZhcy5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCB0aGlzLm9uTW91c2VEb3duLmJpbmQodGhpcykpO1xuXG59XG5cblZpZXcucHJvdG90eXBlLm9uTW91c2VEb3duID0gZnVuY3Rpb24oZSkge1xuICBpZiAoaXNOYU4oaW50ZXJ2YWwpKSB7XG4gICAgcmV0dXJuXG4gIH1cbiAgY29uc3Qge29mZnNldFgsIG9mZnNldFl9ID0gZTtcbiAgbGV0IHggPSBvZmZzZXRYO1xuICBsZXQgeSA9IG9mZnNldFk7XG5cbiAgY29uc3QgcmFkaXVzID0gcmFuZG9tUmFkaXVzKCk7XG4gIGlmICh4IC0gcmFkaXVzIDw9IHRoaXMuY2FudmFzLndpZHRoKSB7XG4gICAgeCA9IHggKyByYWRpdXM7XG4gIH1cbiAgaWYgKHggKyByYWRpdXMgPj0gdGhpcy5jYW52YXMud2lkdGgpIHtcbiAgICB4ID0geCAtIHJhZGl1cyAqIDI7XG4gIH1cbiAgaWYgKHkgLSByYWRpdXMgPD0gdGhpcy5jYW52YXMuaGVpZ2h0KSB7XG4gICAgeSA9IHkgKyByYWRpdXM7XG4gIH1cbiAgaWYgKHkgKyByYWRpdXMgPj0gdGhpcy5jYW52YXMuaGVpZ2h0KSB7XG4gICAgeSA9IHkgLSByYWRpdXMgKiAyO1xuICB9XG5cbiAgY29uc3QgYmFsbCA9IG5ldyBCYWxsKHgsIHksIHJhZGl1cyk7XG4gIC8vIHRoaXMuYmFsbHMucHVzaChiYWxsKTtcbiAgYXJyYXlCYWxscy5wdXNoKGJhbGwpO1xufTtcblxuZnVuY3Rpb24gZHJhd0JhbGwgKGJhbGwpIHtcbiAgY3R4LmJlZ2luUGF0aCgpO1xuICBjdHguYXJjKGJhbGwueCwgYmFsbC55LCBiYWxsLnJhZGl1cywgMCwgMiAqIE1hdGguUEkpO1xuICBjdHguZmlsbFN0eWxlID0gYmFsbC5jb2xvcjtcbiAgY3R4LmNsb3NlUGF0aCgpO1xuICBjdHguZmlsbCgpO1xufVxuXG5mdW5jdGlvbiBtb3ZlbWVudCAoYmFsbHMpIHtcbiAgY3R4LmNsZWFyUmVjdCgwLCAwLCBjYW52YXMud2lkdGgsIGNhbnZhcy5oZWlnaHQpO1xuICBiYWxscy5mb3JFYWNoKGJhbGwgPT4ge1xuICAgIGlmKGJhbGwueCArIGJhbGwuc3BlZWRYID4gY2FudmFzLndpZHRoIC0gYmFsbC5yYWRpdXMgfHwgYmFsbC54ICsgYmFsbC5zcGVlZFggPCBiYWxsLnJhZGl1cykge1xuICAgICAgYmFsbC5zcGVlZFggPSAtYmFsbC5zcGVlZFg7XG4gICAgfVxuICAgIGlmKGJhbGwueSArIGJhbGwuc3BlZWRZID4gY2FudmFzLmhlaWdodCAtIGJhbGwucmFkaXVzIHx8IGJhbGwueSArIGJhbGwuc3BlZWRZIDwgYmFsbC5yYWRpdXMpIHtcbiAgICAgIGJhbGwuc3BlZWRZID0gLWJhbGwuc3BlZWRZO1xuICAgIH1cbiAgICBiYWxsLnggPSBiYWxsLnggKyBiYWxsLnNwZWVkWDtcbiAgICBiYWxsLnkgPSBiYWxsLnkgKyBiYWxsLnNwZWVkWTtcbiAgICBkcmF3QmFsbChiYWxsKTtcbiAgfSlcbn1cblxuLy8gQmFsbCBjb25zdHJ1Y3RvclxuZnVuY3Rpb24gQmFsbCh4LCB5LCByYWRpdXMpIHtcbiAgdGhpcy54ID0geDtcbiAgdGhpcy55ID0geTtcbiAgdGhpcy5yYWRpdXMgPSByYWRpdXM7XG4gIHRoaXMuY29sb3IgPSByYW5kb21Db2xvcigpO1xuICB0aGlzLnNwZWVkWCA9IHJhbmRvbU1vdmVtZW50KCk7XG4gIHRoaXMuc3BlZWRZID0gcmFuZG9tTW92ZW1lbnQoKTtcblxufVxuLy8gaW5pdCBvZiBhcHBcbmNvbnN0IGluaXQgPSAoKSA9PiB7XG4gIGNvbnN0IHZpZXcgPSBuZXcgVmlldygpO1xufTtcblxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgIGluaXQpO1xuIiwiXG5cblxuXG5jb25zdCBsb2NhbGhvc3RTZXJ2ID0gJ2h0dHA6Ly8zNS4yMDUuMTQ4LjIzMC9zZXJ2ZXIvJztcbmNvbnN0IHdlYnBhY2tMb2NhbEhvc3QgPSAnaHR0cDovLzM1LjIwNS4xNDguMjMwLyc7XG5cbi8vXG4vLyBjb25zdCBsb2NhbGhvc3RTZXJ2ID0gXCJodHRwOi8vbG9jYWxob3N0OjMwMDBcIjtcbi8vIGNvbnN0IHdlYnBhY2tMb2NhbEhvc3QgPSBcImh0dHA6Ly9sb2NhbGhvc3Q6OTAwMFwiO1xuXG5cblxuXG5leHBvcnQge1xuICAgIGxvY2FsaG9zdFNlcnYsXG4gICAgd2VicGFja0xvY2FsSG9zdCxcblxufTsiLCJleHBvcnQgZGVmYXVsdHtcbiAgICBlbjp7IHRleHQ6e1xuICAgICAgICAgICAgbXlBY2NvdW50OiBcIk15IEFjY291bnRcIixcbiAgICAgICAgICAgIGljb25UaXRsZTogXCJDaGFuZ2UgeW91ciBhdmF0YXJcIixcbiAgICAgICAgICAgIGxvZ2luVGl0bGU6IFwiTG9naW5cIixcbiAgICAgICAgICAgIHBhc3N3b3JkMVRpdGxlOiBcIlBhc3N3b3JkXCIsXG4gICAgICAgICAgICBwYXNzd29yZDJUaXRsZTogXCJDaGVjayBwYXNzd29yZFwiLFxuICAgICAgICAgICAgZW1haWxUaXRsZTogXCJFbWFpbFwiLFxuICAgICAgICAgICAgcGhvbmVUaXRsZTogXCJQaG9uZVwiLFxuICAgICAgICAgICAgYWJvdXRNeXNlbGZUaXRsZTogXCJBYm91dCBNeXNlbGZcIixcbiAgICAgICAgICAgIFNhdmVCdG46IFwiU2F2ZSBjaGFuZ2VzXCIsXG4gICAgICAgICAgICBjbG9zZTogXCJDbG9zZVwiLFxuICAgICAgICAgICAgY2hhdDpcIkNoYXRcIixcbiAgICAgICAgICAgIGNoYXRXcmFwcGVyOlwiQ29taW5nIHNvb25cIixcbiAgICAgICAgICAgIGdhbWVCb3VuY2U6XCJCb3VuY2VcIixcbiAgICAgICAgICAgIHN0YXJ0OlwiU3RhcnQgZ2FtZVwiLFxuICAgICAgICAgICAgY2xlYXI6XCJDbGVhclwiLFxuICAgICAgICAgICAgY2hhbmdlSW5wdXRzOlwi0KFoYW5nZVwiLFxuICAgICAgICB9XG4gICAgICB9LFxuICAgIHJ1OnsgdGV4dDoge1xuICAgICAgICAgICAgbXlBY2NvdW50OiBcItCc0L7QuSDQsNC60LrQsNGD0L3RglwiLFxuICAgICAgICAgICAgaWNvblRpdGxlOiBcItCY0LfQvNC10L3QuNGC0Ywg0YHQstC+0Lkg0LDQstCw0YLQsNGAXCIsXG4gICAgICAgICAgICBsb2dpblRpdGxlOiBcItCb0L7Qs9C40L1cIixcbiAgICAgICAgICAgIHBhc3N3b3JkMVRpdGxlOiBcItCf0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgcGFzc3dvcmQyVGl0bGU6IFwi0J/RgNC+0LLQtdGA0LrQsCDQv9Cw0YDQvtC70Y9cIixcbiAgICAgICAgICAgIGVtYWlsVGl0bGU6IFwi0LXQvNC10LnQu1wiLFxuICAgICAgICAgICAgcGhvbmVUaXRsZTogXCLQotC10LvQtdGE0L7QvVwiLFxuICAgICAgICAgICAgYWJvdXRNeXNlbGZUaXRsZTogXCLQntCx0L4g0LzQvdC1XCIsXG4gICAgICAgICAgICBTYXZlQnRuOiBcItCh0L7RhdGA0LDQvdC40YLRjCDQuNC30LzQtdC90LXQvdC40Y9cIixcbiAgICAgICAgICAgIGNsb3NlOiBcItCX0LDQutGA0YvRgtGMXCIsXG4gICAgICAgICAgICBjaGF0Olwi0KfQsNGCXCIsXG4gICAgICAgICAgICBjaGF0V3JhcHBlcjpcItCSINGA0LDQt9GA0LDQsdC+0YLQutC1KVwiLFxuICAgICAgICAgICAgZ2FtZUJvdW5jZTpcItCY0LPRgNCwINCo0LDRgNC40LrQuFwiLFxuICAgICAgICAgICAgc3RhcnQ6XCLQndCw0YfQsNGC0Ywg0LjQs9GA0YNcIixcbiAgICAgICAgICAgIGNsZWFyOlwi0J7Rh9C40YHRgtC40YLRjFwiLFxuICAgICAgICAgICAgY2hhbmdlSW5wdXRzOlwi0JjQt9C80LXQvdC40YLRjFwiLFxuICAgICAgICB9XG4gICAgfVxufTsiLCJleHBvcnQgZGVmYXVsdCB7XG4gICAgZW46eyB0ZXh0OntcbiAgICAgICAgICAgIHRleHRBdXRob3JpemF0aW9uOlwiQXV0aG9yaXphdGlvblwiLFxuICAgICAgICAgICAgbGFiZWxMb2dpbjpcIkxvZ2luOlwiLFxuICAgICAgICAgICAgbGFiZWxQYXNzd29yZDpcIlBhc3N3b3JkOlwiLFxuICAgICAgICAgICAgcmVnaXN0cmF0aW9uQnRuQXZ0OlwiUmVnaXN0cmF0aW9uXCIsXG4gICAgICAgICAgICBsb2dJbkJ0bjpcIkxvZyBJblwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc3dvcmQ6XCJGb3Jnb3R0ZW4gUGFzc3dvcmRcIixcbiAgICAgICAgICAgIHRleHRGb3Jnb3R0ZW5QYXNzd29yZDpcIlBhc3N3b3JkIHJlY292ZXJ5XCIsXG4gICAgICAgICAgICBmb3Jnb3R0ZW5MYWJlbExvZ2luOlwiTG9naW46XCIsXG4gICAgICAgICAgICBmb3Jnb3R0ZW5MYWJlbFBhc3N3b3JkOlwiS2V5d29yZDpcIixcbiAgICAgICAgICAgIGZvcmdvdHRlblBhc3NMYWJlbDpcIllvdXIgcGFzc3dvcmQ6XCIsXG4gICAgICAgICAgICBnZXRQYXNzd29yZDpcIkdldCBQYXNzd29yZFwiLFxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBsb2dpbkF2dDpcIndyaXRlIHlvdXIgbG9naW5cIixcbiAgICAgICAgICAgIHBhc3N3b3JkQXZ0Olwid3JpdGUgeW91ciBwYXNzd29yZFwiLFxuICAgICAgICAgICAgZm9yZ290dGVuTG9naW5BdnQ6XCJ3cml0ZSB5b3VyIGxvZ2luIFwiLFxuICAgICAgICAgICAga2F5V29yZEF2dDpcIndyaXRlIHlvdXIga2V5d29yZFwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc3dvcmRNZXNzYWdlOlwiWW91ciBwYXNzd29yZFwiXG4gICAgICAgIH1cblxuICAgIH0sXG4gICAgcnU6eyB0ZXh0OiB7XG4gICAgICAgICAgICB0ZXh0QXV0aG9yaXphdGlvbjpcItCQ0LLRgtC+0YDQuNC30LDRhtC40Y9cIixcbiAgICAgICAgICAgIGxhYmVsTG9naW46XCLQm9C+0LPQuNC9OlwiLFxuICAgICAgICAgICAgbGFiZWxQYXNzd29yZDpcItCf0LDRgNC+0LvRjDpcIixcbiAgICAgICAgICAgIHJlZ2lzdHJhdGlvbkJ0bkF2dDpcItCg0LXQs9C40YHRgtGA0LDRhtC40Y9cIixcbiAgICAgICAgICAgIGxvZ0luQnRuOlwi0JLQvtC50YLQuFwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc3dvcmQ6XCLQl9Cw0LHRi9C7INC/0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgdGV4dEZvcmdvdHRlblBhc3N3b3JkOlwi0JLQvtGB0YHRgtCw0L3QvtCy0LvQtdC90LjQtSDQv9Cw0YDQvtC70Y9cIixcbiAgICAgICAgICAgIGZvcmdvdHRlbkxhYmVsTG9naW46XCLQm9C+0LPQuNC9OlwiLFxuICAgICAgICAgICAgZm9yZ290dGVuTGFiZWxQYXNzd29yZDpcItCh0LXQutGA0LXRgtC90L7QtSDRgdC70L7QstC+OlwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc0xhYmVsOlwi0JLQsNGIINC/0LDRgNC+0LvRjDpcIixcbiAgICAgICAgICAgIGdldFBhc3N3b3JkOlwi0J/QvtC70YPRh9C40YLRjCDQv9Cw0YDQvtC70YxcIixcbiAgICAgICAgfSxcbiAgICAgICAgcGxhY2Vob2xkZXI6e1xuICAgICAgICAgICAgbG9naW5BdnQ6XCLQktCy0LXQtNC40YLQtSDQstCw0Ygg0LvQvtCz0LjQvVwiLFxuICAgICAgICAgICAgcGFzc3dvcmRBdnQ6XCLQktCy0LXQtNC40YLQtSDQstCw0Ygg0L/QsNGA0L7Qu9GMXCIsXG4gICAgICAgICAgICBmb3Jnb3R0ZW5Mb2dpbkF2dDpcItCS0LLQtdC00LjRgtC1INCy0LDRiCDQu9C+0LPQuNC9XCIsXG4gICAgICAgICAgICBrYXlXb3JkQXZ0Olwi0JLQstC10LTQuNGC0LUg0YHQtdC60YDQtdGC0L3QvtC1INGB0LvQvtCy0L5cIixcbiAgICAgICAgICAgIGZvcmdvdHRlblBhc3N3b3JkTWVzc2FnZTpcItCi0YPRgiDQsdGD0LTQtdGCINCy0LDRiCDQv9Cw0YDQvtC70YxcIixcbiAgICAgICAgfVxuICAgIH1cbn07XG4iLCJleHBvcnQgZGVmYXVsdCB7XG4gICAgZW46eyB0ZXh0OntcbiAgICAgICAgICAgIGNvbnRyb2xQYW5lbDpcIkNvbnRyb2wgUGFuZWxcIixcbiAgICAgICAgICAgIGV4aXRDYWJpbmV0OiBcIkV4aXRcIixcbiAgICAgICAgICAgIGFkZHN0dWRlbnQ6IFwiQWRkIHN0dWRlbnRcIixcbiAgICAgICAgICAgIGxhYmVsTmFtZTogXCJOYW1lXCIsXG4gICAgICAgICAgICBsYWJlbExhc3RuYW1lOiBcIlN1cm5hbWVcIixcbiAgICAgICAgICAgIGxhYmVsQWdlOiBcIkFnZVwiLFxuICAgICAgICAgICAgbGFiZWxDaXR5OiBcIkNpdHlcIixcbiAgICAgICAgICAgIENyZWF0ZTogXCJDcmVhdGVcIixcbiAgICAgICAgICAgIHRhYmxlTmFtZTogXCJOYW1lXCIsXG4gICAgICAgICAgICB0YWJsZUxhc3RuYW1lOiBcIkxhc3RuYW1lXCIsXG4gICAgICAgICAgICB0YWJsZUFnZTogXCJBZ2VcIixcbiAgICAgICAgICAgIHRhYmxlQ2l0eTogXCJDaXR5XCIsXG4gICAgICAgICAgICB0YWJsZUJ1dHRvbnM6IFwiQnV0dG9uXCIsXG4gICAgICAgICAgICBjbGVhcklucHV0OlwiQ2xlYXJcIixcbiAgICAgICAgICAgIHByaW50TGFiZWw6XCJFbnRlciB2YWx1ZVwiLFxuICAgICAgICAgICAgcmVzdWx0TGFiZWw6XCJSZXN1bHRcIixcbiAgICAgICAgICAgIGNvbnZlcnRCdXR0b246XCJDb252ZXJ0XCIsXG4gICAgICAgICAgICBwcmludExhYmVsMjpcIkVudGVyIHZhbHVlXCIsXG4gICAgICAgICAgICByZXN1bHRMYWJlbDI6XCJSZXN1bHRcIixcbiAgICAgICAgICAgIGdldEN1cnJlbmN5OlwiQ29udmVydFwiLFxuICAgICAgICAgICAgY29udmVydGVyVGl0bGU6XCJDb252ZXJ0ZXJcIixcbiAgICAgICAgICAgIGJ0bkNoYW5nZUxlbmd0aDpcIkxlbmd0aCBjb252ZXJ0ZXJcIixcbiAgICAgICAgICAgIGJ0bkNoYW5nZU1vbmV5OlwiTW9uZXkgY29udmVydGVyXCIsXG4gICAgICAgICAgICBtZXRlcjE6XCJNZXRlclwiLFxuICAgICAgICAgICAgdmVyc3QxOlwiVmVyc3RcIixcbiAgICAgICAgICAgIG1pbGUxOlwiTWlsZVwiLFxuICAgICAgICAgICAgZm9vdDE6XCJGb290XCIsXG4gICAgICAgICAgICB5YXJkMTpcIllhcmRcIixcbiAgICAgICAgICAgIG1ldGVyMjpcIk1ldGVyXCIsXG4gICAgICAgICAgICB2ZXJzdDI6XCJWZXJzdFwiLFxuICAgICAgICAgICAgbWlsZTI6XCJNaWxlXCIsXG4gICAgICAgICAgICBmb290MjpcIkZvb3RcIixcbiAgICAgICAgICAgIHlhcmQyOlwiWWFyZFwiLFxuICAgICAgICAgICAgbW9kZXMxOlwiTW9kZXNcIixcbiAgICAgICAgICAgIHN0dWRlbnRzMTpcIlN0dWRlbnRzXCIsXG4gICAgICAgICAgICBwYWludDE6XCJQYWludFwiLFxuICAgICAgICAgICAgY29udmVydGVyMTpcIkNvbnZlcnRlclwiLFxuICAgICAgICAgICAgY2FsY3VsYXRvcjE6XCJDYWxjdWxhdG9yXCIsXG4gICAgICAgICAgICBzdHVkZW50c1RpdGxlOlwiU3R1ZGVudHNcIixcbiAgICAgICAgfSxcbnBsYWNlaG9sZGVyOntcbiAgICBOYW1lOlwiV3JpdGUgbmFtZVwiLFxuICAgIExhc3RuYW1lOlwiV3JpdGUgbGFzdG5hbWVcIixcbiAgICBBZ2U6XCJXcml0ZSBhZ2VcIixcbiAgICBDaXR5OlwiV3JpdGUgY2l0eVwiLFxuICAgIGlucHV0Q29udmVydGVyOlwiRW50ZXIgdmFsdWVcIixcbiAgICBvdXRwdXRDb252ZXJ0ZXI6XCJSZXN1bHRcIixcbiAgICBpbnB1dEN1cnJlbmN5OlwiRW50ZXIgdmFsdWVcIixcbiAgICBvdXRwdXRSZXN1bHQ6XCJSZXN1bHRcIixcbiAgICBjb250clBhbmVsOlwiQ29udHJvbCBQYW5lbFwiLFxuICAgIGxpbmVUaGljazpcIkxpbmUgdGhpY2tuZXNzXCIsXG4gICAgbGluZUNvbDpcIkxpbmUgY29sb3JcIixcbiAgICBjbGVhcjpcIkNsZWFyXCIsXG4gICAgbGFiZWxQYWludDpcIlBhaW50XCIsXG4gICAgdGV4dFNldHRpbmdzOlwiU2V0dGluZ3NcIixcbiAgICBub3RvZmljYXRpb25MYWJlbDpcIk5vdGlmaWNhdGlvbnNcIixcbiAgICBsYW5ndWFnZUxhYmVsOlwiQ2hvb3NlIGxhbmd1YWdlXCIsXG4gICAgcmVzZXQ6XCJSZXNldFwiXG5cbn1cblxuICAgIH0sXG4gICAgcnU6eyB0ZXh0OiB7XG4gICAgICAgICAgICBjb250cm9sUGFuZWw6XCLQn9Cw0L3QtdC70Ywg0YPQv9GA0LDQstC70LXQvdC40Y9cIixcbiAgICAgICAgICAgIGV4aXRDYWJpbmV0OiBcItCS0YvQudGC0LhcIixcbiAgICAgICAgICAgIGFkZHN0dWRlbnQ6IFwi0JTQvtCx0LDQstC40YLRjCDQodGC0YPQtNC10L3RgtCwXCIsXG4gICAgICAgICAgICBsYWJlbE5hbWU6IFwi0JjQvNGPXCIsXG4gICAgICAgICAgICBsYWJlbExhc3RuYW1lOiBcItCk0LDQvNC40LvQuNGPXCIsXG4gICAgICAgICAgICBsYWJlbEFnZTogXCLQktC+0LfRgNCw0YHRglwiLFxuICAgICAgICAgICAgbGFiZWxDaXR5OiBcItCT0L7RgNC+0LRcIixcbiAgICAgICAgICAgIENyZWF0ZTogXCLQodC+0LfQtNCw0YLRjFwiLFxuICAgICAgICAgICAgdGFibGVOYW1lOiBcItCY0LzRj1wiLFxuICAgICAgICAgICAgdGFibGVMYXN0bmFtZTogXCLQpNCw0LzQuNC70LjRj1wiLFxuICAgICAgICAgICAgdGFibGVBZ2U6IFwi0JLQvtC30YDQsNGB0YJcIixcbiAgICAgICAgICAgIHRhYmxlQ2l0eTogXCLQk9C+0YDQvtC0XCIsXG4gICAgICAgICAgICB0YWJsZUJ1dHRvbnM6IFwi0JrQvdC+0L/QutC4XCIsXG4gICAgICAgICAgICBjbGVhcklucHV0Olwi0J7Rh9C40YHRgtC40YLRjFwiLFxuICAgICAgICAgICAgcHJpbnRMYWJlbDpcItCS0LLQtdC00LjRgtC1INC30L3QsNGH0LXQvdC40LVcIixcbiAgICAgICAgICAgIHJlc3VsdExhYmVsOlwi0KDQtdC30YPQu9GM0YLQsNGCXCIsXG4gICAgICAgICAgICBjb252ZXJ0QnV0dG9uOlwi0JrQvtC90LLQtdGA0YLQuNGA0L7QstCw0YLRjFwiLFxuICAgICAgICAgICAgcHJpbnRMYWJlbDI6XCLQktCy0LXQtNC40YLQtSDQt9C90LDRh9C10L3QuNC1XCIsXG4gICAgICAgICAgICByZXN1bHRMYWJlbDI6XCLQoNC10LfRg9C70YzRgtCw0YJcIixcbiAgICAgICAgICAgIGdldEN1cnJlbmN5Olwi0JrQvtC90LLQtdGA0YLQuNGA0L7QstCw0YLRjFwiLFxuICAgICAgICAgICAgY29udmVydGVyVGl0bGU6XCLQmtC+0L3QstC10YDRgtC10YBcIixcbiAgICAgICAgICAgIGJ0bkNoYW5nZUxlbmd0aDpcItCa0L7QvdCy0LXRgNGC0LXRgCDRgNCw0YHRgdGC0L7Rj9C90LjRj1wiLFxuICAgICAgICAgICAgYnRuQ2hhbmdlTW9uZXk6XCLQmtC+0L3QstC10YDRgtC10YAg0LLQsNC70Y7RglwiLFxuICAgICAgICAgICAgbWV0ZXIxOlwi0JzQtdGC0YBcIixcbiAgICAgICAgICAgIHZlcnN0MTpcItCS0LXRgNGB0YLQsFwiLFxuICAgICAgICAgICAgbWlsZTE6XCLQnNC40LvRj1wiLFxuICAgICAgICAgICAgZm9vdDE6XCLQpNGD0YJcIixcbiAgICAgICAgICAgIHlhcmQxOlwi0K/RgNC0XCIsXG4gICAgICAgICAgICBtZXRlcjI6XCLQnNC10YLRgFwiLFxuICAgICAgICAgICAgdmVyc3QyOlwi0JLQtdGA0YHRgtCwXCIsXG4gICAgICAgICAgICBtaWxlMjpcItCc0LjQu9GPXCIsXG4gICAgICAgICAgICBmb290MjpcItCk0YPRglwiLFxuICAgICAgICAgICAgeWFyZDI6XCLQr9GA0LRcIixcbiAgICAgICAgICAgIG1vZGVzMTpcItCc0L7QtNGLXCIsXG4gICAgICAgICAgICBzdHVkZW50czE6XCLQodGC0YPQtNC10L3RglwiLFxuICAgICAgICAgICAgcGFpbnQxOlwi0KDQuNGB0L7QstCw0LvQutCwXCIsXG4gICAgICAgICAgICBjb252ZXJ0ZXIxOlwi0JrQvtC90LLQtdGA0YLQtdGAXCIsXG4gICAgICAgICAgICBjYWxjdWxhdG9yMTpcItCa0LDQu9GM0LrRg9C70Y/RgtC+0YBcIixcbiAgICAgICAgICAgIHN0dWRlbnRzVGl0bGU6XCLQodGC0YPQtNC10L3RgtGLXCIsXG4gICAgICAgICAgICBjb250clBhbmVsOlwi0J/QsNC90LXQu9GMINGD0L/RgNCw0LLQu9C10L3QuNGPXCIsXG4gICAgICAgICAgICBsaW5lVGhpY2s6XCLQotC+0LvRidC40L3QsCDQu9C40L3QuNC4XCIsXG4gICAgICAgICAgICBsaW5lQ29sOlwi0KbQstC10YIg0LvQuNC90LjQuFwiLFxuICAgICAgICAgICAgY2xlYXI6XCLQntGH0LjRgdGC0LjRgtGMXCIsXG4gICAgICAgICAgICBsYWJlbFBhaW50Olwi0KDQuNGB0L7QstCw0LvQutCwXCIsXG4gICAgICAgICAgICB0ZXh0U2V0dGluZ3M6XCLQndCw0YHRgtGA0L7QudC60LhcIixcbiAgICAgICAgICAgIG5vdG9maWNhdGlvbkxhYmVsOlwi0KPQstC10LTQvtC80LvQtdC90LjRj1wiLFxuICAgICAgICAgICAgbGFuZ3VhZ2VMYWJlbDpcItCS0YvQsdGA0LDRgtGMINGP0LfRi9C6XCIsXG4gICAgICAgICAgICByZXNldDpcItCh0LHRgNC+0YHQuNGC0YxcIlxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBOYW1lOlwi0JLQstC10LTQuNGC0LUg0LjQvNGPXCIsXG4gICAgICAgICAgICBMYXN0bmFtZTpcItCS0LLQtdC00LjRgtC1INGE0LDQvNC40LvQuNGOXCIsXG4gICAgICAgICAgICBBZ2U6XCLQktCy0LXQtNC40YLQtSDQstC+0LfRgNCw0YHRglwiLFxuICAgICAgICAgICAgQ2l0eTpcItCS0LLQtdC00LjRgtC1INCz0L7RgNC+0LRcIixcbiAgICAgICAgICAgIGlucHV0Q29udmVydGVyOlwi0JLQstC10LTQuNGC0LUg0LfQvdCw0YfQtdC90LjQtVwiLFxuICAgICAgICAgICAgb3V0cHV0Q29udmVydGVyOlwi0KDQtdC30YPQu9GM0YLQsNGCXCIsXG4gICAgICAgICAgICBpbnB1dEN1cnJlbmN5Olwi0JLQstC10LTQuNGC0LUg0LfQvdCw0YfQtdC90LjQtVwiLFxuICAgICAgICAgICAgb3V0cHV0UmVzdWx0Olwi0KDQtdC30YPQu9GM0YLQsNGCXCIsXG4gICAgICAgIH1cbiAgICB9XG59O1xuXG5cbiIsImltcG9ydCBsYW5ndWFnZUJveEluZGV4IGZyb20gXCIuL2luZGV4TGFuZ3VhZ2VcIjtcbmltcG9ydCBhdXRvclBhZ2UgZnJvbSBcIi4vYXV0b3JpemF0aW9uTGFuZ3VhZ2VcIjtcbmltcG9ydCBhY2NTZXRQYWdlIGZyb20gXCIuL2FjY291bnRTZXR0aW5nc0xhbmd1YWdlXCI7XG5pbXBvcnQgcmVnUGFnZSBmcm9tIFwiLi9yZWdpc3RyYXRpb25MYW5ndWFnZVwiXG5leHBvcnQge2NoYW5nZUxhbmd1YWdlfTtcbmZ1bmN0aW9uIGNoYW5nZUxhbmd1YWdlKHBhZ2UpIHtcbiAgICBpZiAocGFnZSA9PT0gXCJhdXRob3JcIil7XG4gICAgICAgIHZhciBwYWdlQ29sPWF1dG9yUGFnZTtcbiAgICB9IGVsc2UgaWYgKHBhZ2UgPT09IFwiYWNjU2V0XCIpe1xuICAgICAgICB2YXIgcGFnZUNvbD1hY2NTZXRQYWdlO1xuICAgIH0gZWxzZSBpZihwYWdlID09PSBcImluZGV4XCIpe1xuICAgICAgICB2YXIgcGFnZUNvbD1sYW5ndWFnZUJveEluZGV4O1xuICAgIH0gZWxzZSBpZihwYWdlPT09XCJyZWdcIil7XG4gICAgICAgIHZhciBwYWdlQ29sPXJlZ1BhZ2U7XG4gICAgfVxuICAgIHZhciBsYW5ndWFnZSA9IHNlbGVjdEVsZW1lbnRMYW5ndWFnZS52YWx1ZTtcbiAgICBpZiAobGFuZ3VhZ2U9PT1cImFyXCIpe1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRpcmVjdGlvblwiKS5zZXRBdHRyaWJ1dGUoXCJkaXJcIixcInJ0bFwiKTtcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnbGFuZ3VhZ2UnLCBsYW5ndWFnZSk7XG4gICAgICAgIGxhbmd1YWdlPVwiZW5cIjtcbiAgICB9IGVsc2Uge1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRpcmVjdGlvblwiKS5zZXRBdHRyaWJ1dGUoXCJkaXJcIixcImx0bFwiKTtcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnbGFuZ3VhZ2UnLCBsYW5ndWFnZSk7XG4gICAgfVxuICAgIGNoYW5nZVRleHQocGFnZUNvbFtsYW5ndWFnZV0udGV4dCk7XG4gICAgY2hhbmdlUGxhY2VIb2xkZXIocGFnZUNvbFtsYW5ndWFnZV0ucGxhY2Vob2xkZXIpXG59XG5mdW5jdGlvbiBjaGFuZ2VUZXh0KG9iaikge1xuICAgIGZvciAoY29uc3Qga2V5IGluIG9iaikge1xuICAgICAgICBpZihkb2N1bWVudC5nZXRFbGVtZW50QnlJZChrZXkpIT09bnVsbCkge1xuICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoa2V5KS5pbm5lckhUTUwgPSBvYmpba2V5XTtcbiAgICAgICAgfVxuICAgIH1cbn07XG5mdW5jdGlvbiBjaGFuZ2VQbGFjZUhvbGRlcihvYmopIHtcbiAgICBmb3IgKGNvbnN0IGtleSBpbiBvYmopIHtcbiAgICAgICAgaWYoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoa2V5KSE9PW51bGwpIHtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGtleSkucGxhY2Vob2xkZXIgPSBvYmpba2V5XTtcbiAgICAgICAgfVxuICAgIH1cbn07XG4iLCJleHBvcnQgZGVmYXVsdCB7XG4gICAgZW46eyB0ZXh0OntcbiAgICAgICAgICAgIGNsb3NlOiBcIlJldHVybiB0byBsb2dpblwiLFxuICAgICAgICAgICAgcmVnTGFiZWw6IFwiUmVnaXN0cmF0aW9uXCIsXG4gICAgICAgICAgICBsb2dpbkxhYmVsOiBcIkxvZ2luKlwiLFxuICAgICAgICAgICAgcGFzc0xhYmVsOiBcIlBhc3N3b3JkKlwiLFxuICAgICAgICAgICAgcGFzczJMYWJlbDogXCJDb25maXJtIHBhc3N3b3JkKlwiLFxuICAgICAgICAgICAgZW1haWxMYWJlbDogXCJFLW1haWwqXCIsXG4gICAgICAgICAgICBwaG9uZUxhYmVsOiBcIlBob25lIG51bWJlcipcIixcbiAgICAgICAgICAgIGtleXdvcmRMYWJlbDogXCJLZXl3b3JkXCIsXG4gICAgICAgICAgICBrZXl3b3JkTm90aWZpY2F0aW9uOlwiVGhlIGtleXdvcmQgbmVlZGVkIGZvciBwYXNzd29yZCByZWNvdmVyeVwiLFxuICAgICAgICAgICAgcmVnaXN0cmF0aW9uQnRuOlwiU2lnbiB1cFwiLFxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBsb2dpbjpcIkxvZ2luXCIsXG4gICAgICAgICAgICBwYXNzd29yZDE6XCJQYXNzd29yZFwiLFxuICAgICAgICAgICAgcGFzc3dvcmQyOlwiQ29uZmlybSBQYXNzd29yZFwiLFxuICAgICAgICAgICAgZW1haWw6XCJFLW1haWxcIixcbiAgICAgICAgICAgIHBob25lOlwiUGhvbmUgbnVtYmVyXCIsXG4gICAgICAgICAgICBrZXl3b3JkOiBcIktleXdvcmRcIixcbiAgICAgICAgfVxuXG4gICAgfSxcbiAgICBydTp7IHRleHQ6IHtcbiAgICAgICAgICAgIGNsb3NlOiBcItCS0LXRgNC90YPRgtGB0Y8g0Log0LDQstGC0L7RgNC40LfQsNGG0LjQuFwiLFxuICAgICAgICAgICAgcmVnTGFiZWw6IFwi0KDQtdCz0LjRgdGC0YDQsNGG0LjRj1wiLFxuICAgICAgICAgICAgbG9naW5MYWJlbDogXCLQm9C+0LPQuNC9KlwiLFxuICAgICAgICAgICAgcGFzc0xhYmVsOiBcItCf0LDRgNC+0LvRjCpcIixcbiAgICAgICAgICAgIHBhc3MyTGFiZWw6IFwi0J/QvtC00YLQstC10YDQtNC40YLQtSDQv9Cw0YDQvtC70YwqXCIsXG4gICAgICAgICAgICBlbWFpbExhYmVsOiBcItCf0L7Rh9GC0LAqXCIsXG4gICAgICAgICAgICBwaG9uZUxhYmVsOiBcItCi0LXQu9C10YTQvtC90L3Ri9C5INC90L7QvNC10YAqXCIsXG4gICAgICAgICAgICBrZXl3b3JkTGFiZWw6IFwi0KHQtdC60YDQtdGC0L3QvtC1INGB0LvQvtCy0L5cIixcbiAgICAgICAgICAgIGtleXdvcmROb3RpZmljYXRpb246XCLQodC10LrRgNC10YLQvdC+0LUg0YHQu9C+0LLQviDQtNC70Y8g0LLQvtGB0YHRgtCw0L3QvtCy0LvQtdC90LjRjyDQv9Cw0YDQvtC70Y9cIixcbiAgICAgICAgICAgIHJlZ2lzdHJhdGlvbkJ0bjpcItCX0LDRgNC10LPQuNGB0YLRgNC40YDQvtCy0LDRgtGM0YHRj1wiLFxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBsb2dpbjpcItCS0LLQtdC00LjRgtC1INC70L7Qs9C40L1cIixcbiAgICAgICAgICAgIHBhc3N3b3JkMTpcItCS0LLQtdC00LjRgtC1INC/0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgcGFzc3dvcmQyOlwi0J/QvtCy0YLQvtGA0LjRgtC1INC/0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgZW1haWw6XCLQktCy0LXQtNC40YLQtSDQv9C+0YfRgtGDXCIsXG4gICAgICAgICAgICBwaG9uZTpcItCS0LLQtdC00LjRgtC1INGC0LXQu9C10YTQvtC90L3Ri9C5INC90L7QvNC10YBcIixcbiAgICAgICAgICAgIGtleXdvcmQ6IFwi0JLQstC10LTQuNGC0LUg0YHQtdC60YDQtdGC0L3QvtC1INGB0LvQvtCy0L5cIixcbiAgICAgICAgfVxuICAgIH1cbn07IiwiIGZ1bmN0aW9uIGNoZWNrT25seU51bWJlcnMoKXtcbiAgICAgdGhpcy52YWx1ZSA9IHRoaXMudmFsdWUucmVwbGFjZSgvW14wLTkuXS9nLCAnJykucmVwbGFjZSgvKFxcLi4qKVxcLi9nLCAnJDEnKTtcbiBjaGVja1ZhbHVlTGVuZ3RoKHRoaXMsIDMpO1xufVxuXG5mdW5jdGlvbiBjaGVja1ZhbHVlTGVuZ3RoKGlucHV0LCBtYXhMZW5ndGgpIHtcbiAgICAgaWYoaW5wdXQudmFsdWUubGVuZ3RoID49IG1heExlbmd0aCkgIHtcbiAgICAgICAgIGlucHV0LnZhbHVlID0gaW5wdXQudmFsdWUuc3Vic3RyKDAsIG1heExlbmd0aCk7XG4gICAgIH1cbn1cblxuIGV4cG9ydCB7Y2hlY2tPbmx5TnVtYmVycywgY2hlY2tWYWx1ZUxlbmd0aH1cblxuIiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIl0sInNvdXJjZVJvb3QiOiIifQ==