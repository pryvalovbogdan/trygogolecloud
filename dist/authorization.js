/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/script/authorizationValidation.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./server/helpers/constants.js":
/*!*************************************!*\
  !*** ./server/helpers/constants.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {module.exports = {
  port: process.env.PORT || 3000,
  routing: {
    authorization: '/authorization',
    registration: '/registration',
    groupStudent: '/groupStudent',
    getAllGroups: '/getAllGroups',
    accountSetting: '/accountSetting',
    groups: '/groups',
    table: '/',
    update: '/update',
    "delete": '/delete',
    accountUpdate: '/account-update',
    forgottenPassword: '/forgotten-password',
    deleteGroup: '/delete-group',
    studentClear: '/student-clear',
    updateGroup: '/update-group',
    sendImage: '/send-image',
    resetSettings: '/reset-settings'
  }
};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/script/authorizationValidation.js":
/*!***********************************************!*\
  !*** ./src/script/authorizationValidation.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_language_language_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers/language/language.js */ "./src/script/helpers/language/language.js");
/* harmony import */ var _helpers_const__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers/const */ "./src/script/helpers/const.js");
/* harmony import */ var _style_autorization_less__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../style/autorization.less */ "./src/style/autorization.less");
/* harmony import */ var _style_autorization_less__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_autorization_less__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _style_footer_less__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../style/footer.less */ "./src/style/footer.less");
/* harmony import */ var _style_footer_less__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_style_footer_less__WEBPACK_IMPORTED_MODULE_3__);




var constants = __webpack_require__(/*! ../../server/helpers/constants */ "./server/helpers/constants.js");



var loginAvt = document.getElementById("loginAvt");
var passwordAvt = document.getElementById("passwordAvt");
var logInBtn = document.getElementById("logInBtn");
var registrationBtn = document.getElementById("registrationBtnAvt");
var massage = document.getElementById("message");
var selectElementLanguage = document.getElementById("selectElementLanguage");
var hidenPasword = document.getElementById("wraperForgottenPassword");
var keyWordAvt = document.getElementById("kayWordAvt");
var forgottenLoginAvt = document.getElementById("forgottenLoginAvt");
var getPasswordBtn = document.getElementById("getPassword");
var forgottenPasswordMessage = document.getElementById("forgottenPasswordMessage");
var btnCloseModal = document.getElementById("close");
var forgottenPasswordBtn = document.getElementById("forgottenPassword");
var logoHome = document.getElementById("logoHome");
logoHome.addEventListener('click', function () {
  document.location.href = "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_1__["webpackLocalHost"], "/index.html");
});

selectElementLanguage.onchange = function () {
  Object(_helpers_language_language_js__WEBPACK_IMPORTED_MODULE_0__["changeLanguage"])("author");
};

loginAvt.onkeydown = function (e) {
  if (!e.key.match(/[0-9\/A-Z]/i)) {
    return false;
  }
};

main();

function main() {
  if (sessionStorage.getItem("language") != null) {
    selectElementLanguage.value = sessionStorage.getItem("language");
    Object(_helpers_language_language_js__WEBPACK_IMPORTED_MODULE_0__["changeLanguage"])("author");
  } else {
    Object(_helpers_language_language_js__WEBPACK_IMPORTED_MODULE_0__["changeLanguage"])("author");
  }
}

var loginValue = {};
logInBtn.addEventListener("click", getElementValue);
registrationBtn.addEventListener("click", function () {
  document.location.href = "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_1__["webpackLocalHost"], "/registration.html");
});

function getElementValue() {
  loginValue = {
    login: loginAvt.value,
    password: passwordAvt.value
  };
  localStorage.setItem('loginName', loginAvt.value);
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_1__["localhostServ"]).concat(constants.routing.authorization));
  xhr.setRequestHeader("Content-type", "application/json");

  xhr.onreadystatechange = function () {
    if (xhr.status == 401) {
      document.getElementById("loginAvt").style = "border-color: #900;\n" + " background-color: #FDD";
      document.getElementById("passwordAvt").style = "border-color: #900;\n" + " background-color: #FDD";

      if (selectElementLanguage.value === "ru") {
        var err = "Введите корректный логин и пароль!";
      } else {
        var err = "Insert correct login or password!!!";
      }

      massage.innerText = err;
    } else if (xhr.status === 200 && xhr.readyState === 4) {
      var newStudentValue = JSON.parse(this.response);
      localStorage.setItem("teachers_id", newStudentValue[0].teachers_id);
      document.location.href = "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_1__["webpackLocalHost"], "/table.html");
    }
  };

  xhr.send(JSON.stringify(loginValue));
}

forgottenPasswordBtn.addEventListener("click", displayHiden);
btnCloseModal.addEventListener("click", displayNone);
var forgottenpassword = {};
getPasswordBtn.addEventListener("click", getPasswordValue);

function getPasswordValue() {
  var passwordValue = {
    login: forgottenLoginAvt.value,
    keyword: keyWordAvt.value
  };
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_1__["localhostServ"]).concat(constants.routing.forgottenPassword));
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.send(JSON.stringify(passwordValue));

  xhr.onload = function () {
    if (xhr.status == 401) {
      document.getElementById("forgottenLoginAvt").style = "border-color: #900;\n" + " background-color: #FDD";
      document.getElementById("kayWordAvt").style = "border-color: #900;\n" + " background-color: #FDD";

      if (selectElementLanguage.value === "ru") {
        forgottenPasswordMessage.value = "Введите корректный логин и слово!";
      } else {
        forgottenPasswordMessage.value = "Insert correct login or kay word!";
      }
    } else {
      forgottenPasswordMessage.value = JSON.parse(this.response);
    }
  };
}

function displayHiden() {
  hidenPasword.classList.toggle("hidden");
}

function displayNone() {
  hidenPasword.classList.toggle("hidden");
}

/***/ }),

/***/ "./src/script/helpers/const.js":
/*!*************************************!*\
  !*** ./src/script/helpers/const.js ***!
  \*************************************/
/*! exports provided: localhostServ, webpackLocalHost */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "localhostServ", function() { return localhostServ; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "webpackLocalHost", function() { return webpackLocalHost; });
var localhostServ = 'http://35.205.148.230/server/';
var webpackLocalHost = 'http://35.205.148.230/'; //
// const localhostServ = "http://localhost:3000";
// const webpackLocalHost = "http://localhost:9000";



/***/ }),

/***/ "./src/script/helpers/language/accountSettingsLanguage.js":
/*!****************************************************************!*\
  !*** ./src/script/helpers/language/accountSettingsLanguage.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      myAccount: "My Account",
      iconTitle: "Change your avatar",
      loginTitle: "Login",
      password1Title: "Password",
      password2Title: "Check password",
      emailTitle: "Email",
      phoneTitle: "Phone",
      aboutMyselfTitle: "About Myself",
      SaveBtn: "Save changes",
      close: "Close",
      chat: "Chat",
      chatWrapper: "Coming soon",
      gameBounce: "Bounce",
      start: "Start game",
      clear: "Clear",
      changeInputs: "Сhange"
    }
  },
  ru: {
    text: {
      myAccount: "Мой аккаунт",
      iconTitle: "Изменить свой аватар",
      loginTitle: "Логин",
      password1Title: "Пароль",
      password2Title: "Проверка пароля",
      emailTitle: "емейл",
      phoneTitle: "Телефон",
      aboutMyselfTitle: "Обо мне",
      SaveBtn: "Сохранить изменения",
      close: "Закрыть",
      chat: "Чат",
      chatWrapper: "В разработке)",
      gameBounce: "Игра Шарики",
      start: "Начать игру",
      clear: "Очистить",
      changeInputs: "Изменить"
    }
  }
});

/***/ }),

/***/ "./src/script/helpers/language/autorizationLanguage.js":
/*!*************************************************************!*\
  !*** ./src/script/helpers/language/autorizationLanguage.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      textAuthorization: "Authorization",
      labelLogin: "Login:",
      labelPassword: "Password:",
      registrationBtnAvt: "Registration",
      logInBtn: "Log In",
      forgottenPassword: "Forgotten Password",
      textForgottenPassword: "Password recovery",
      forgottenLabelLogin: "Login:",
      forgottenLabelPassword: "Keyword:",
      forgottenPassLabel: "Your password:",
      getPassword: "Get Password"
    },
    placeholder: {
      loginAvt: "write your login",
      passwordAvt: "write your password",
      forgottenLoginAvt: "write your login ",
      kayWordAvt: "write your keyword",
      forgottenPasswordMessage: "Your password"
    }
  },
  ru: {
    text: {
      textAuthorization: "Авторизация",
      labelLogin: "Логин:",
      labelPassword: "Пароль:",
      registrationBtnAvt: "Регистрация",
      logInBtn: "Войти",
      forgottenPassword: "Забыл пароль",
      textForgottenPassword: "Восстановление пароля",
      forgottenLabelLogin: "Логин:",
      forgottenLabelPassword: "Секретное слово:",
      forgottenPassLabel: "Ваш пароль:",
      getPassword: "Получить пароль"
    },
    placeholder: {
      loginAvt: "Введите ваш логин",
      passwordAvt: "Введите ваш пароль",
      forgottenLoginAvt: "Введите ваш логин",
      kayWordAvt: "Введите секретное слово",
      forgottenPasswordMessage: "Тут будет ваш пароль"
    }
  }
});

/***/ }),

/***/ "./src/script/helpers/language/indexLanguage.js":
/*!******************************************************!*\
  !*** ./src/script/helpers/language/indexLanguage.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      controlPanel: "Control Panel",
      exitCabinet: "Exit",
      addstudent: "Add student",
      labelName: "Name",
      labelLastname: "Surname",
      labelAge: "Age",
      labelCity: "City",
      Create: "Create",
      tableName: "Name",
      tableLastname: "Lastname",
      tableAge: "Age",
      tableCity: "City",
      tableButtons: "Button",
      clearInput: "Clear",
      printLabel: "Enter value",
      resultLabel: "Result",
      convertButton: "Convert",
      printLabel2: "Enter value",
      resultLabel2: "Result",
      getCurrency: "Convert",
      converterTitle: "Converter",
      btnChangeLength: "Length converter",
      btnChangeMoney: "Money converter",
      meter1: "Meter",
      verst1: "Verst",
      mile1: "Mile",
      foot1: "Foot",
      yard1: "Yard",
      meter2: "Meter",
      verst2: "Verst",
      mile2: "Mile",
      foot2: "Foot",
      yard2: "Yard",
      modes1: "Modes",
      students1: "Students",
      paint1: "Paint",
      converter1: "Converter",
      calculator1: "Calculator",
      studentsTitle: "Students"
    },
    placeholder: {
      Name: "Write name",
      Lastname: "Write lastname",
      Age: "Write age",
      City: "Write city",
      inputConverter: "Enter value",
      outputConverter: "Result",
      inputCurrency: "Enter value",
      outputResult: "Result",
      contrPanel: "Control Panel",
      lineThick: "Line thickness",
      lineCol: "Line color",
      clear: "Clear",
      labelPaint: "Paint",
      textSettings: "Settings",
      notoficationLabel: "Notifications",
      languageLabel: "Choose language",
      reset: "Reset"
    }
  },
  ru: {
    text: {
      controlPanel: "Панель управления",
      exitCabinet: "Выйти",
      addstudent: "Добавить Студента",
      labelName: "Имя",
      labelLastname: "Фамилия",
      labelAge: "Возраст",
      labelCity: "Город",
      Create: "Создать",
      tableName: "Имя",
      tableLastname: "Фамилия",
      tableAge: "Возраст",
      tableCity: "Город",
      tableButtons: "Кнопки",
      clearInput: "Очистить",
      printLabel: "Введите значение",
      resultLabel: "Результат",
      convertButton: "Конвертировать",
      printLabel2: "Введите значение",
      resultLabel2: "Результат",
      getCurrency: "Конвертировать",
      converterTitle: "Конвертер",
      btnChangeLength: "Конвертер расстояния",
      btnChangeMoney: "Конвертер валют",
      meter1: "Метр",
      verst1: "Верста",
      mile1: "Миля",
      foot1: "Фут",
      yard1: "Ярд",
      meter2: "Метр",
      verst2: "Верста",
      mile2: "Миля",
      foot2: "Фут",
      yard2: "Ярд",
      modes1: "Моды",
      students1: "Студент",
      paint1: "Рисовалка",
      converter1: "Конвертер",
      calculator1: "Калькулятор",
      studentsTitle: "Студенты",
      contrPanel: "Панель управления",
      lineThick: "Толщина линии",
      lineCol: "Цвет линии",
      clear: "Очистить",
      labelPaint: "Рисовалка",
      textSettings: "Настройки",
      notoficationLabel: "Уведомления",
      languageLabel: "Выбрать язык",
      reset: "Сбросить"
    },
    placeholder: {
      Name: "Введите имя",
      Lastname: "Введите фамилию",
      Age: "Введите возраст",
      City: "Введите город",
      inputConverter: "Введите значение",
      outputConverter: "Результат",
      inputCurrency: "Введите значение",
      outputResult: "Результат"
    }
  }
});

/***/ }),

/***/ "./src/script/helpers/language/language.js":
/*!*************************************************!*\
  !*** ./src/script/helpers/language/language.js ***!
  \*************************************************/
/*! exports provided: changeLanguage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeLanguage", function() { return changeLanguage; });
/* harmony import */ var _indexLanguage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./indexLanguage */ "./src/script/helpers/language/indexLanguage.js");
/* harmony import */ var _autorizationLanguage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./autorizationLanguage */ "./src/script/helpers/language/autorizationLanguage.js");
/* harmony import */ var _accountSettingsLanguage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accountSettingsLanguage */ "./src/script/helpers/language/accountSettingsLanguage.js");
/* harmony import */ var _registrationLanguage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registrationLanguage */ "./src/script/helpers/language/registrationLanguage.js");






function changeLanguage(page) {
  if (page === "author") {
    var pageCol = _autorizationLanguage__WEBPACK_IMPORTED_MODULE_1__["default"];
  } else if (page === "accSet") {
    var pageCol = _accountSettingsLanguage__WEBPACK_IMPORTED_MODULE_2__["default"];
  } else if (page === "index") {
    var pageCol = _indexLanguage__WEBPACK_IMPORTED_MODULE_0__["default"];
  } else if (page === "reg") {
    var pageCol = _registrationLanguage__WEBPACK_IMPORTED_MODULE_3__["default"];
  }

  var language = selectElementLanguage.value;

  if (language === "ar") {
    document.getElementById("direction").setAttribute("dir", "rtl");
    sessionStorage.setItem('language', language);
    language = "en";
  } else {
    document.getElementById("direction").setAttribute("dir", "ltl");
    sessionStorage.setItem('language', language);
  }

  changeText(pageCol[language].text);
  changePlaceHolder(pageCol[language].placeholder);
}

function changeText(obj) {
  for (var key in obj) {
    if (document.getElementById(key) !== null) {
      document.getElementById(key).innerHTML = obj[key];
    }
  }
}

;

function changePlaceHolder(obj) {
  for (var key in obj) {
    if (document.getElementById(key) !== null) {
      document.getElementById(key).placeholder = obj[key];
    }
  }
}

;

/***/ }),

/***/ "./src/script/helpers/language/registrationLanguage.js":
/*!*************************************************************!*\
  !*** ./src/script/helpers/language/registrationLanguage.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      close: "Return to login",
      regLabel: "Registration",
      loginLabel: "Login*",
      passLabel: "Password*",
      pass2Label: "Confirm password*",
      emailLabel: "E-mail*",
      phoneLabel: "Phone number*",
      keywordLabel: "Keyword",
      keywordNotification: "The keyword needed for password recovery",
      registrationBtn: "Sign up"
    },
    placeholder: {
      login: "Login",
      password1: "Password",
      password2: "Confirm Password",
      email: "E-mail",
      phone: "Phone number",
      keyword: "Keyword"
    }
  },
  ru: {
    text: {
      close: "Вернутся к авторизации",
      regLabel: "Регистрация",
      loginLabel: "Логин*",
      passLabel: "Пароль*",
      pass2Label: "Подтвердите пароль*",
      emailLabel: "Почта*",
      phoneLabel: "Телефонный номер*",
      keywordLabel: "Секретное слово",
      keywordNotification: "Секретное слово для восстановления пароля",
      registrationBtn: "Зарегистрироваться"
    },
    placeholder: {
      login: "Введите логин",
      password1: "Введите пароль",
      password2: "Повторите пароль",
      email: "Введите почту",
      phone: "Введите телефонный номер",
      keyword: "Введите секретное слово"
    }
  }
});

/***/ }),

/***/ "./src/style/autorization.less":
/*!*************************************!*\
  !*** ./src/style/autorization.less ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/style/footer.less":
/*!*******************************!*\
  !*** ./src/style/footer.less ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Byb2Nlc3MvYnJvd3Nlci5qcyIsIndlYnBhY2s6Ly8vLi9zZXJ2ZXIvaGVscGVycy9jb25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC9hdXRob3JpemF0aW9uVmFsaWRhdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0L2hlbHBlcnMvY29uc3QuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC9oZWxwZXJzL2xhbmd1YWdlL2FjY291bnRTZXR0aW5nc0xhbmd1YWdlLmpzIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHQvaGVscGVycy9sYW5ndWFnZS9hdXRvcml6YXRpb25MYW5ndWFnZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0L2hlbHBlcnMvbGFuZ3VhZ2UvaW5kZXhMYW5ndWFnZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0L2hlbHBlcnMvbGFuZ3VhZ2UvbGFuZ3VhZ2UuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC9oZWxwZXJzL2xhbmd1YWdlL3JlZ2lzdHJhdGlvbkxhbmd1YWdlLmpzIiwid2VicGFjazovLy8uL3NyYy9zdHlsZS9hdXRvcml6YXRpb24ubGVzcz80YzZjIiwid2VicGFjazovLy8uL3NyYy9zdHlsZS9mb290ZXIubGVzcz9iOWI2Il0sIm5hbWVzIjpbIm1vZHVsZSIsImV4cG9ydHMiLCJwb3J0IiwicHJvY2VzcyIsImVudiIsIlBPUlQiLCJyb3V0aW5nIiwiYXV0aG9yaXphdGlvbiIsInJlZ2lzdHJhdGlvbiIsImdyb3VwU3R1ZGVudCIsImdldEFsbEdyb3VwcyIsImFjY291bnRTZXR0aW5nIiwiZ3JvdXBzIiwidGFibGUiLCJ1cGRhdGUiLCJhY2NvdW50VXBkYXRlIiwiZm9yZ290dGVuUGFzc3dvcmQiLCJkZWxldGVHcm91cCIsInN0dWRlbnRDbGVhciIsInVwZGF0ZUdyb3VwIiwic2VuZEltYWdlIiwicmVzZXRTZXR0aW5ncyIsImNvbnN0YW50cyIsInJlcXVpcmUiLCJsb2dpbkF2dCIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJwYXNzd29yZEF2dCIsImxvZ0luQnRuIiwicmVnaXN0cmF0aW9uQnRuIiwibWFzc2FnZSIsInNlbGVjdEVsZW1lbnRMYW5ndWFnZSIsImhpZGVuUGFzd29yZCIsImtleVdvcmRBdnQiLCJmb3Jnb3R0ZW5Mb2dpbkF2dCIsImdldFBhc3N3b3JkQnRuIiwiZm9yZ290dGVuUGFzc3dvcmRNZXNzYWdlIiwiYnRuQ2xvc2VNb2RhbCIsImZvcmdvdHRlblBhc3N3b3JkQnRuIiwibG9nb0hvbWUiLCJhZGRFdmVudExpc3RlbmVyIiwibG9jYXRpb24iLCJocmVmIiwid2VicGFja0xvY2FsSG9zdCIsIm9uY2hhbmdlIiwiY2hhbmdlTGFuZ3VhZ2UiLCJvbmtleWRvd24iLCJlIiwia2V5IiwibWF0Y2giLCJtYWluIiwic2Vzc2lvblN0b3JhZ2UiLCJnZXRJdGVtIiwidmFsdWUiLCJsb2dpblZhbHVlIiwiZ2V0RWxlbWVudFZhbHVlIiwibG9naW4iLCJwYXNzd29yZCIsImxvY2FsU3RvcmFnZSIsInNldEl0ZW0iLCJ4aHIiLCJYTUxIdHRwUmVxdWVzdCIsIm9wZW4iLCJsb2NhbGhvc3RTZXJ2Iiwic2V0UmVxdWVzdEhlYWRlciIsIm9ucmVhZHlzdGF0ZWNoYW5nZSIsInN0YXR1cyIsInN0eWxlIiwiZXJyIiwiaW5uZXJUZXh0IiwicmVhZHlTdGF0ZSIsIm5ld1N0dWRlbnRWYWx1ZSIsIkpTT04iLCJwYXJzZSIsInJlc3BvbnNlIiwidGVhY2hlcnNfaWQiLCJzZW5kIiwic3RyaW5naWZ5IiwiZGlzcGxheUhpZGVuIiwiZGlzcGxheU5vbmUiLCJmb3Jnb3R0ZW5wYXNzd29yZCIsImdldFBhc3N3b3JkVmFsdWUiLCJwYXNzd29yZFZhbHVlIiwia2V5d29yZCIsIm9ubG9hZCIsImNsYXNzTGlzdCIsInRvZ2dsZSIsImVuIiwidGV4dCIsIm15QWNjb3VudCIsImljb25UaXRsZSIsImxvZ2luVGl0bGUiLCJwYXNzd29yZDFUaXRsZSIsInBhc3N3b3JkMlRpdGxlIiwiZW1haWxUaXRsZSIsInBob25lVGl0bGUiLCJhYm91dE15c2VsZlRpdGxlIiwiU2F2ZUJ0biIsImNsb3NlIiwiY2hhdCIsImNoYXRXcmFwcGVyIiwiZ2FtZUJvdW5jZSIsInN0YXJ0IiwiY2xlYXIiLCJjaGFuZ2VJbnB1dHMiLCJydSIsInRleHRBdXRob3JpemF0aW9uIiwibGFiZWxMb2dpbiIsImxhYmVsUGFzc3dvcmQiLCJyZWdpc3RyYXRpb25CdG5BdnQiLCJ0ZXh0Rm9yZ290dGVuUGFzc3dvcmQiLCJmb3Jnb3R0ZW5MYWJlbExvZ2luIiwiZm9yZ290dGVuTGFiZWxQYXNzd29yZCIsImZvcmdvdHRlblBhc3NMYWJlbCIsImdldFBhc3N3b3JkIiwicGxhY2Vob2xkZXIiLCJrYXlXb3JkQXZ0IiwiY29udHJvbFBhbmVsIiwiZXhpdENhYmluZXQiLCJhZGRzdHVkZW50IiwibGFiZWxOYW1lIiwibGFiZWxMYXN0bmFtZSIsImxhYmVsQWdlIiwibGFiZWxDaXR5IiwiQ3JlYXRlIiwidGFibGVOYW1lIiwidGFibGVMYXN0bmFtZSIsInRhYmxlQWdlIiwidGFibGVDaXR5IiwidGFibGVCdXR0b25zIiwiY2xlYXJJbnB1dCIsInByaW50TGFiZWwiLCJyZXN1bHRMYWJlbCIsImNvbnZlcnRCdXR0b24iLCJwcmludExhYmVsMiIsInJlc3VsdExhYmVsMiIsImdldEN1cnJlbmN5IiwiY29udmVydGVyVGl0bGUiLCJidG5DaGFuZ2VMZW5ndGgiLCJidG5DaGFuZ2VNb25leSIsIm1ldGVyMSIsInZlcnN0MSIsIm1pbGUxIiwiZm9vdDEiLCJ5YXJkMSIsIm1ldGVyMiIsInZlcnN0MiIsIm1pbGUyIiwiZm9vdDIiLCJ5YXJkMiIsIm1vZGVzMSIsInN0dWRlbnRzMSIsInBhaW50MSIsImNvbnZlcnRlcjEiLCJjYWxjdWxhdG9yMSIsInN0dWRlbnRzVGl0bGUiLCJOYW1lIiwiTGFzdG5hbWUiLCJBZ2UiLCJDaXR5IiwiaW5wdXRDb252ZXJ0ZXIiLCJvdXRwdXRDb252ZXJ0ZXIiLCJpbnB1dEN1cnJlbmN5Iiwib3V0cHV0UmVzdWx0IiwiY29udHJQYW5lbCIsImxpbmVUaGljayIsImxpbmVDb2wiLCJsYWJlbFBhaW50IiwidGV4dFNldHRpbmdzIiwibm90b2ZpY2F0aW9uTGFiZWwiLCJsYW5ndWFnZUxhYmVsIiwicmVzZXQiLCJwYWdlIiwicGFnZUNvbCIsImF1dG9yUGFnZSIsImFjY1NldFBhZ2UiLCJsYW5ndWFnZUJveEluZGV4IiwicmVnUGFnZSIsImxhbmd1YWdlIiwic2V0QXR0cmlidXRlIiwiY2hhbmdlVGV4dCIsImNoYW5nZVBsYWNlSG9sZGVyIiwib2JqIiwiaW5uZXJIVE1MIiwicmVnTGFiZWwiLCJsb2dpbkxhYmVsIiwicGFzc0xhYmVsIiwicGFzczJMYWJlbCIsImVtYWlsTGFiZWwiLCJwaG9uZUxhYmVsIiwia2V5d29yZExhYmVsIiwia2V5d29yZE5vdGlmaWNhdGlvbiIsInBhc3N3b3JkMSIsInBhc3N3b3JkMiIsImVtYWlsIiwicGhvbmUiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLHNCQUFzQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEscUNBQXFDOztBQUVyQztBQUNBO0FBQ0E7O0FBRUEsMkJBQTJCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QixVQUFVOzs7Ozs7Ozs7Ozs7QUN2THRDQSxxREFBTSxDQUFDQyxPQUFQLEdBQWlCO0FBQ2ZDLE1BQUksRUFBRUMsT0FBTyxDQUFDQyxHQUFSLENBQVlDLElBQVosSUFBb0IsSUFEWDtBQUVmQyxTQUFPLEVBQUU7QUFDUEMsaUJBQWEsRUFBRSxnQkFEUjtBQUVQQyxnQkFBWSxFQUFFLGVBRlA7QUFHUEMsZ0JBQVksRUFBRSxlQUhQO0FBSVBDLGdCQUFZLEVBQUUsZUFKUDtBQUtQQyxrQkFBYyxFQUFFLGlCQUxUO0FBTVBDLFVBQU0sRUFBRSxTQU5EO0FBT1BDLFNBQUssRUFBRSxHQVBBO0FBUVBDLFVBQU0sRUFBRSxTQVJEO0FBU1AsY0FBUSxTQVREO0FBVVBDLGlCQUFhLEVBQUUsaUJBVlI7QUFXUEMscUJBQWlCLEVBQUUscUJBWFo7QUFZUEMsZUFBVyxFQUFFLGVBWk47QUFhUEMsZ0JBQVksRUFBRSxnQkFiUDtBQWNQQyxlQUFXLEVBQUUsZUFkTjtBQWVQQyxhQUFTLEVBQUUsYUFmSjtBQWdCUEMsaUJBQWEsRUFBRTtBQWhCUjtBQUZNLENBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7QUFDQSxJQUFJQyxTQUFTLEdBQUdDLG1CQUFPLENBQUMscUVBQUQsQ0FBdkI7O0FBRUE7QUFDQTtBQUVBLElBQUlDLFFBQVEsR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLFVBQXhCLENBQWY7QUFDQSxJQUFJQyxXQUFXLEdBQUdGLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixhQUF4QixDQUFsQjtBQUNBLElBQUlFLFFBQVEsR0FBR0gsUUFBUSxDQUFDQyxjQUFULENBQXdCLFVBQXhCLENBQWY7QUFDQSxJQUFJRyxlQUFlLEdBQUdKLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixvQkFBeEIsQ0FBdEI7QUFDQSxJQUFJSSxPQUFPLEdBQUdMLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixDQUFkO0FBQ0EsSUFBSUsscUJBQXFCLEdBQUNOLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3Qix1QkFBeEIsQ0FBMUI7QUFFQSxJQUFJTSxZQUFZLEdBQUdQLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3Qix5QkFBeEIsQ0FBbkI7QUFDQSxJQUFJTyxVQUFVLEdBQUdSLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixZQUF4QixDQUFqQjtBQUNBLElBQUlRLGlCQUFpQixHQUFHVCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsbUJBQXhCLENBQXhCO0FBQ0EsSUFBSVMsY0FBYyxHQUFHVixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsYUFBeEIsQ0FBckI7QUFDQSxJQUFJVSx3QkFBd0IsR0FBR1gsUUFBUSxDQUFDQyxjQUFULENBQXdCLDBCQUF4QixDQUEvQjtBQUNBLElBQUlXLGFBQWEsR0FBR1osUUFBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLENBQXBCO0FBQ0EsSUFBSVksb0JBQW9CLEdBQUdiLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixtQkFBeEIsQ0FBM0I7QUFFQSxJQUFJYSxRQUFRLEdBQUVkLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixVQUF4QixDQUFkO0FBQ0FhLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBa0MsWUFBTTtBQUFDZixVQUFRLENBQUNnQixRQUFULENBQWtCQyxJQUFsQixhQUE0QkMsK0RBQTVCO0FBQTBELENBQW5HOztBQUVBWixxQkFBcUIsQ0FBQ2EsUUFBdEIsR0FBaUMsWUFBSTtBQUFDQyxzRkFBYyxDQUFDLFFBQUQsQ0FBZDtBQUF5QixDQUEvRDs7QUFDQXJCLFFBQVEsQ0FBQ3NCLFNBQVQsR0FBcUIsVUFBVUMsQ0FBVixFQUFhO0FBQzlCLE1BQUcsQ0FBQ0EsQ0FBQyxDQUFDQyxHQUFGLENBQU1DLEtBQU4sQ0FBWSxhQUFaLENBQUosRUFBK0I7QUFDM0IsV0FBTyxLQUFQO0FBQ0g7QUFDSixDQUpEOztBQUtBQyxJQUFJOztBQUNKLFNBQVNBLElBQVQsR0FBZTtBQUNYLE1BQUlDLGNBQWMsQ0FBQ0MsT0FBZixDQUF1QixVQUF2QixLQUFvQyxJQUF4QyxFQUE2QztBQUN6Q3JCLHlCQUFxQixDQUFDc0IsS0FBdEIsR0FBNEJGLGNBQWMsQ0FBQ0MsT0FBZixDQUF1QixVQUF2QixDQUE1QjtBQUNBUCx3RkFBYyxDQUFDLFFBQUQsQ0FBZDtBQUNILEdBSEQsTUFHSztBQUNEQSx3RkFBYyxDQUFDLFFBQUQsQ0FBZDtBQUEwQjtBQUNqQzs7QUFDRCxJQUFJUyxVQUFVLEdBQUcsRUFBakI7QUFDQTFCLFFBQVEsQ0FBQ1ksZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUNlLGVBQW5DO0FBQ0ExQixlQUFlLENBQUNXLGdCQUFoQixDQUFpQyxPQUFqQyxFQUEwQyxZQUFZO0FBQ2xEZixVQUFRLENBQUNnQixRQUFULENBQWtCQyxJQUFsQixhQUE0QkMsK0RBQTVCO0FBQ0gsQ0FGRDs7QUFJQSxTQUFTWSxlQUFULEdBQTJCO0FBQ3ZCRCxZQUFVLEdBQUc7QUFBQ0UsU0FBSyxFQUFFaEMsUUFBUSxDQUFDNkIsS0FBakI7QUFBd0JJLFlBQVEsRUFBRTlCLFdBQVcsQ0FBQzBCO0FBQTlDLEdBQWI7QUFDQUssY0FBWSxDQUFDQyxPQUFiLENBQXFCLFdBQXJCLEVBQWtDbkMsUUFBUSxDQUFDNkIsS0FBM0M7QUFDQSxNQUFJTyxHQUFHLEdBQUcsSUFBSUMsY0FBSixFQUFWO0FBQ0FELEtBQUcsQ0FBQ0UsSUFBSixDQUFTLE1BQVQsWUFBb0JDLDREQUFwQixTQUFvQ3pDLFNBQVMsQ0FBQ2hCLE9BQVYsQ0FBa0JDLGFBQXREO0FBQ0FxRCxLQUFHLENBQUNJLGdCQUFKLENBQXFCLGNBQXJCLEVBQXFDLGtCQUFyQzs7QUFFQUosS0FBRyxDQUFDSyxrQkFBSixHQUF5QixZQUFZO0FBQ2pDLFFBQUlMLEdBQUcsQ0FBQ00sTUFBSixJQUFjLEdBQWxCLEVBQXVCO0FBQ25CekMsY0FBUSxDQUFDQyxjQUFULENBQXdCLFVBQXhCLEVBQW9DeUMsS0FBcEMsR0FBMEMsMEJBQXlCLHlCQUFuRTtBQUNBMUMsY0FBUSxDQUFDQyxjQUFULENBQXdCLGFBQXhCLEVBQXVDeUMsS0FBdkMsR0FBNkMsMEJBQXlCLHlCQUF0RTs7QUFDQSxVQUFJcEMscUJBQXFCLENBQUNzQixLQUF0QixLQUFnQyxJQUFwQyxFQUEwQztBQUN2QyxZQUFJZSxHQUFHLEdBQUcsb0NBQVY7QUFDRixPQUZELE1BRU87QUFDSixZQUFJQSxHQUFHLEdBQUcscUNBQVY7QUFDRjs7QUFDRHRDLGFBQU8sQ0FBQ3VDLFNBQVIsR0FBb0JELEdBQXBCO0FBQ0gsS0FURCxNQVNPLElBQUdSLEdBQUcsQ0FBQ00sTUFBSixLQUFlLEdBQWYsSUFBc0JOLEdBQUcsQ0FBQ1UsVUFBSixLQUFtQixDQUE1QyxFQUErQztBQUNsRCxVQUFJQyxlQUFlLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXLEtBQUtDLFFBQWhCLENBQXRCO0FBQ0NoQixrQkFBWSxDQUFDQyxPQUFiLENBQXFCLGFBQXJCLEVBQW9DWSxlQUFlLENBQUMsQ0FBRCxDQUFmLENBQW1CSSxXQUF2RDtBQUNEbEQsY0FBUSxDQUFDZ0IsUUFBVCxDQUFrQkMsSUFBbEIsYUFBNEJDLCtEQUE1QjtBQUNIO0FBQ0osR0FmRDs7QUFnQkFpQixLQUFHLENBQUNnQixJQUFKLENBQVNKLElBQUksQ0FBQ0ssU0FBTCxDQUFldkIsVUFBZixDQUFUO0FBQ0g7O0FBRURoQixvQkFBb0IsQ0FBQ0UsZ0JBQXJCLENBQXNDLE9BQXRDLEVBQStDc0MsWUFBL0M7QUFDQXpDLGFBQWEsQ0FBQ0csZ0JBQWQsQ0FBK0IsT0FBL0IsRUFBd0N1QyxXQUF4QztBQUNBLElBQUlDLGlCQUFpQixHQUFHLEVBQXhCO0FBQ0E3QyxjQUFjLENBQUNLLGdCQUFmLENBQWdDLE9BQWhDLEVBQXlDeUMsZ0JBQXpDOztBQUVBLFNBQVNBLGdCQUFULEdBQTRCO0FBQ3hCLE1BQUlDLGFBQWEsR0FBRztBQUFDMUIsU0FBSyxFQUFFdEIsaUJBQWlCLENBQUNtQixLQUExQjtBQUFpQzhCLFdBQU8sRUFBRWxELFVBQVUsQ0FBQ29CO0FBQXJELEdBQXBCO0FBQ0EsTUFBSU8sR0FBRyxHQUFHLElBQUlDLGNBQUosRUFBVjtBQUNBRCxLQUFHLENBQUNFLElBQUosQ0FBUyxNQUFULFlBQW9CQyw0REFBcEIsU0FBb0N6QyxTQUFTLENBQUNoQixPQUFWLENBQWtCVSxpQkFBdEQ7QUFDQTRDLEtBQUcsQ0FBQ0ksZ0JBQUosQ0FBcUIsY0FBckIsRUFBcUMsa0JBQXJDO0FBQ0FKLEtBQUcsQ0FBQ2dCLElBQUosQ0FBU0osSUFBSSxDQUFDSyxTQUFMLENBQWVLLGFBQWYsQ0FBVDs7QUFDQXRCLEtBQUcsQ0FBQ3dCLE1BQUosR0FBYSxZQUFZO0FBQ3JCLFFBQUl4QixHQUFHLENBQUNNLE1BQUosSUFBYyxHQUFsQixFQUF1QjtBQUNuQnpDLGNBQVEsQ0FBQ0MsY0FBVCxDQUF3QixtQkFBeEIsRUFBNkN5QyxLQUE3QyxHQUFtRCwwQkFBeUIseUJBQTVFO0FBQ0ExQyxjQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0N5QyxLQUF0QyxHQUE0QywwQkFBeUIseUJBQXJFOztBQUNBLFVBQUlwQyxxQkFBcUIsQ0FBQ3NCLEtBQXRCLEtBQWdDLElBQXBDLEVBQTBDO0FBQ3RDakIsZ0NBQXdCLENBQUNpQixLQUF6QixHQUFnQyxtQ0FBaEM7QUFDSCxPQUZELE1BRU87QUFDSGpCLGdDQUF3QixDQUFDaUIsS0FBekIsR0FBaUMsbUNBQWpDO0FBQ0g7QUFDSixLQVJELE1BUU87QUFDSGpCLDhCQUF3QixDQUFDaUIsS0FBekIsR0FBaUNtQixJQUFJLENBQUNDLEtBQUwsQ0FBVyxLQUFLQyxRQUFoQixDQUFqQztBQUNIO0FBQ0osR0FaRDtBQWFIOztBQUVELFNBQVNJLFlBQVQsR0FBdUI7QUFDbkI5QyxjQUFZLENBQUNxRCxTQUFiLENBQXVCQyxNQUF2QixDQUE4QixRQUE5QjtBQUNIOztBQUNELFNBQVNQLFdBQVQsR0FBc0I7QUFDbEIvQyxjQUFZLENBQUNxRCxTQUFiLENBQXVCQyxNQUF2QixDQUE4QixRQUE5QjtBQUNILEM7Ozs7Ozs7Ozs7OztBQ25HRDtBQUFBO0FBQUE7QUFBQSxJQUFNdkIsYUFBYSxHQUFHLCtCQUF0QjtBQUNBLElBQU1wQixnQkFBZ0IsR0FBRyx3QkFBekIsQyxDQUVBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNUQTtBQUFjO0FBQ1Y0QyxJQUFFLEVBQUM7QUFBRUMsUUFBSSxFQUFDO0FBQ0ZDLGVBQVMsRUFBRSxZQURUO0FBRUZDLGVBQVMsRUFBRSxvQkFGVDtBQUdGQyxnQkFBVSxFQUFFLE9BSFY7QUFJRkMsb0JBQWMsRUFBRSxVQUpkO0FBS0ZDLG9CQUFjLEVBQUUsZ0JBTGQ7QUFNRkMsZ0JBQVUsRUFBRSxPQU5WO0FBT0ZDLGdCQUFVLEVBQUUsT0FQVjtBQVFGQyxzQkFBZ0IsRUFBRSxjQVJoQjtBQVNGQyxhQUFPLEVBQUUsY0FUUDtBQVVGQyxXQUFLLEVBQUUsT0FWTDtBQVdGQyxVQUFJLEVBQUMsTUFYSDtBQVlGQyxpQkFBVyxFQUFDLGFBWlY7QUFhRkMsZ0JBQVUsRUFBQyxRQWJUO0FBY0ZDLFdBQUssRUFBQyxZQWRKO0FBZUZDLFdBQUssRUFBQyxPQWZKO0FBZ0JGQyxrQkFBWSxFQUFDO0FBaEJYO0FBQVAsR0FETztBQW9CVkMsSUFBRSxFQUFDO0FBQUVqQixRQUFJLEVBQUU7QUFDSEMsZUFBUyxFQUFFLGFBRFI7QUFFSEMsZUFBUyxFQUFFLHNCQUZSO0FBR0hDLGdCQUFVLEVBQUUsT0FIVDtBQUlIQyxvQkFBYyxFQUFFLFFBSmI7QUFLSEMsb0JBQWMsRUFBRSxpQkFMYjtBQU1IQyxnQkFBVSxFQUFFLE9BTlQ7QUFPSEMsZ0JBQVUsRUFBRSxTQVBUO0FBUUhDLHNCQUFnQixFQUFFLFNBUmY7QUFTSEMsYUFBTyxFQUFFLHFCQVROO0FBVUhDLFdBQUssRUFBRSxTQVZKO0FBV0hDLFVBQUksRUFBQyxLQVhGO0FBWUhDLGlCQUFXLEVBQUMsZUFaVDtBQWFIQyxnQkFBVSxFQUFDLGFBYlI7QUFjSEMsV0FBSyxFQUFDLGFBZEg7QUFlSEMsV0FBSyxFQUFDLFVBZkg7QUFnQkhDLGtCQUFZLEVBQUM7QUFoQlY7QUFBUjtBQXBCTyxDQUFkLEU7Ozs7Ozs7Ozs7OztBQ0FBO0FBQWU7QUFDWGpCLElBQUUsRUFBQztBQUFFQyxRQUFJLEVBQUM7QUFDRmtCLHVCQUFpQixFQUFDLGVBRGhCO0FBRUZDLGdCQUFVLEVBQUMsUUFGVDtBQUdGQyxtQkFBYSxFQUFDLFdBSFo7QUFJRkMsd0JBQWtCLEVBQUMsY0FKakI7QUFLRmpGLGNBQVEsRUFBQyxRQUxQO0FBTUZaLHVCQUFpQixFQUFDLG9CQU5oQjtBQU9GOEYsMkJBQXFCLEVBQUMsbUJBUHBCO0FBUUZDLHlCQUFtQixFQUFDLFFBUmxCO0FBU0ZDLDRCQUFzQixFQUFDLFVBVHJCO0FBVUZDLHdCQUFrQixFQUFDLGdCQVZqQjtBQVdGQyxpQkFBVyxFQUFDO0FBWFYsS0FBUDtBQWFDQyxlQUFXLEVBQUM7QUFDUjNGLGNBQVEsRUFBQyxrQkFERDtBQUVSRyxpQkFBVyxFQUFDLHFCQUZKO0FBR1JPLHVCQUFpQixFQUFDLG1CQUhWO0FBSVJrRixnQkFBVSxFQUFDLG9CQUpIO0FBS1JoRiw4QkFBd0IsRUFBQztBQUxqQjtBQWJiLEdBRFE7QUF1QlhxRSxJQUFFLEVBQUM7QUFBRWpCLFFBQUksRUFBRTtBQUNIa0IsdUJBQWlCLEVBQUMsYUFEZjtBQUVIQyxnQkFBVSxFQUFDLFFBRlI7QUFHSEMsbUJBQWEsRUFBQyxTQUhYO0FBSUhDLHdCQUFrQixFQUFDLGFBSmhCO0FBS0hqRixjQUFRLEVBQUMsT0FMTjtBQU1IWix1QkFBaUIsRUFBQyxjQU5mO0FBT0g4RiwyQkFBcUIsRUFBQyx1QkFQbkI7QUFRSEMseUJBQW1CLEVBQUMsUUFSakI7QUFTSEMsNEJBQXNCLEVBQUMsa0JBVHBCO0FBVUhDLHdCQUFrQixFQUFDLGFBVmhCO0FBV0hDLGlCQUFXLEVBQUM7QUFYVCxLQUFSO0FBYUNDLGVBQVcsRUFBQztBQUNSM0YsY0FBUSxFQUFDLG1CQUREO0FBRVJHLGlCQUFXLEVBQUMsb0JBRko7QUFHUk8sdUJBQWlCLEVBQUMsbUJBSFY7QUFJUmtGLGdCQUFVLEVBQUMseUJBSkg7QUFLUmhGLDhCQUF3QixFQUFDO0FBTGpCO0FBYmI7QUF2QlEsQ0FBZixFOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFlO0FBQ1htRCxJQUFFLEVBQUM7QUFBRUMsUUFBSSxFQUFDO0FBQ0Y2QixrQkFBWSxFQUFDLGVBRFg7QUFFRkMsaUJBQVcsRUFBRSxNQUZYO0FBR0ZDLGdCQUFVLEVBQUUsYUFIVjtBQUlGQyxlQUFTLEVBQUUsTUFKVDtBQUtGQyxtQkFBYSxFQUFFLFNBTGI7QUFNRkMsY0FBUSxFQUFFLEtBTlI7QUFPRkMsZUFBUyxFQUFFLE1BUFQ7QUFRRkMsWUFBTSxFQUFFLFFBUk47QUFTRkMsZUFBUyxFQUFFLE1BVFQ7QUFVRkMsbUJBQWEsRUFBRSxVQVZiO0FBV0ZDLGNBQVEsRUFBRSxLQVhSO0FBWUZDLGVBQVMsRUFBRSxNQVpUO0FBYUZDLGtCQUFZLEVBQUUsUUFiWjtBQWNGQyxnQkFBVSxFQUFDLE9BZFQ7QUFlRkMsZ0JBQVUsRUFBQyxhQWZUO0FBZ0JGQyxpQkFBVyxFQUFDLFFBaEJWO0FBaUJGQyxtQkFBYSxFQUFDLFNBakJaO0FBa0JGQyxpQkFBVyxFQUFDLGFBbEJWO0FBbUJGQyxrQkFBWSxFQUFDLFFBbkJYO0FBb0JGQyxpQkFBVyxFQUFDLFNBcEJWO0FBcUJGQyxvQkFBYyxFQUFDLFdBckJiO0FBc0JGQyxxQkFBZSxFQUFDLGtCQXRCZDtBQXVCRkMsb0JBQWMsRUFBQyxpQkF2QmI7QUF3QkZDLFlBQU0sRUFBQyxPQXhCTDtBQXlCRkMsWUFBTSxFQUFDLE9BekJMO0FBMEJGQyxXQUFLLEVBQUMsTUExQko7QUEyQkZDLFdBQUssRUFBQyxNQTNCSjtBQTRCRkMsV0FBSyxFQUFDLE1BNUJKO0FBNkJGQyxZQUFNLEVBQUMsT0E3Qkw7QUE4QkZDLFlBQU0sRUFBQyxPQTlCTDtBQStCRkMsV0FBSyxFQUFDLE1BL0JKO0FBZ0NGQyxXQUFLLEVBQUMsTUFoQ0o7QUFpQ0ZDLFdBQUssRUFBQyxNQWpDSjtBQWtDRkMsWUFBTSxFQUFDLE9BbENMO0FBbUNGQyxlQUFTLEVBQUMsVUFuQ1I7QUFvQ0ZDLFlBQU0sRUFBQyxPQXBDTDtBQXFDRkMsZ0JBQVUsRUFBQyxXQXJDVDtBQXNDRkMsaUJBQVcsRUFBQyxZQXRDVjtBQXVDRkMsbUJBQWEsRUFBQztBQXZDWixLQUFQO0FBeUNQeEMsZUFBVyxFQUFDO0FBQ1J5QyxVQUFJLEVBQUMsWUFERztBQUVSQyxjQUFRLEVBQUMsZ0JBRkQ7QUFHUkMsU0FBRyxFQUFDLFdBSEk7QUFJUkMsVUFBSSxFQUFDLFlBSkc7QUFLUkMsb0JBQWMsRUFBQyxhQUxQO0FBTVJDLHFCQUFlLEVBQUMsUUFOUjtBQU9SQyxtQkFBYSxFQUFDLGFBUE47QUFRUkMsa0JBQVksRUFBQyxRQVJMO0FBU1JDLGdCQUFVLEVBQUMsZUFUSDtBQVVSQyxlQUFTLEVBQUMsZ0JBVkY7QUFXUkMsYUFBTyxFQUFDLFlBWEE7QUFZUi9ELFdBQUssRUFBQyxPQVpFO0FBYVJnRSxnQkFBVSxFQUFDLE9BYkg7QUFjUkMsa0JBQVksRUFBQyxVQWRMO0FBZVJDLHVCQUFpQixFQUFDLGVBZlY7QUFnQlJDLG1CQUFhLEVBQUMsaUJBaEJOO0FBaUJSQyxXQUFLLEVBQUM7QUFqQkU7QUF6Q0wsR0FEUTtBQWdFWGxFLElBQUUsRUFBQztBQUFFakIsUUFBSSxFQUFFO0FBQ0g2QixrQkFBWSxFQUFDLG1CQURWO0FBRUhDLGlCQUFXLEVBQUUsT0FGVjtBQUdIQyxnQkFBVSxFQUFFLG1CQUhUO0FBSUhDLGVBQVMsRUFBRSxLQUpSO0FBS0hDLG1CQUFhLEVBQUUsU0FMWjtBQU1IQyxjQUFRLEVBQUUsU0FOUDtBQU9IQyxlQUFTLEVBQUUsT0FQUjtBQVFIQyxZQUFNLEVBQUUsU0FSTDtBQVNIQyxlQUFTLEVBQUUsS0FUUjtBQVVIQyxtQkFBYSxFQUFFLFNBVlo7QUFXSEMsY0FBUSxFQUFFLFNBWFA7QUFZSEMsZUFBUyxFQUFFLE9BWlI7QUFhSEMsa0JBQVksRUFBRSxRQWJYO0FBY0hDLGdCQUFVLEVBQUMsVUFkUjtBQWVIQyxnQkFBVSxFQUFDLGtCQWZSO0FBZ0JIQyxpQkFBVyxFQUFDLFdBaEJUO0FBaUJIQyxtQkFBYSxFQUFDLGdCQWpCWDtBQWtCSEMsaUJBQVcsRUFBQyxrQkFsQlQ7QUFtQkhDLGtCQUFZLEVBQUMsV0FuQlY7QUFvQkhDLGlCQUFXLEVBQUMsZ0JBcEJUO0FBcUJIQyxvQkFBYyxFQUFDLFdBckJaO0FBc0JIQyxxQkFBZSxFQUFDLHNCQXRCYjtBQXVCSEMsb0JBQWMsRUFBQyxpQkF2Qlo7QUF3QkhDLFlBQU0sRUFBQyxNQXhCSjtBQXlCSEMsWUFBTSxFQUFDLFFBekJKO0FBMEJIQyxXQUFLLEVBQUMsTUExQkg7QUEyQkhDLFdBQUssRUFBQyxLQTNCSDtBQTRCSEMsV0FBSyxFQUFDLEtBNUJIO0FBNkJIQyxZQUFNLEVBQUMsTUE3Qko7QUE4QkhDLFlBQU0sRUFBQyxRQTlCSjtBQStCSEMsV0FBSyxFQUFDLE1BL0JIO0FBZ0NIQyxXQUFLLEVBQUMsS0FoQ0g7QUFpQ0hDLFdBQUssRUFBQyxLQWpDSDtBQWtDSEMsWUFBTSxFQUFDLE1BbENKO0FBbUNIQyxlQUFTLEVBQUMsU0FuQ1A7QUFvQ0hDLFlBQU0sRUFBQyxXQXBDSjtBQXFDSEMsZ0JBQVUsRUFBQyxXQXJDUjtBQXNDSEMsaUJBQVcsRUFBQyxhQXRDVDtBQXVDSEMsbUJBQWEsRUFBQyxVQXZDWDtBQXdDSFMsZ0JBQVUsRUFBQyxtQkF4Q1I7QUF5Q0hDLGVBQVMsRUFBQyxlQXpDUDtBQTBDSEMsYUFBTyxFQUFDLFlBMUNMO0FBMkNIL0QsV0FBSyxFQUFDLFVBM0NIO0FBNENIZ0UsZ0JBQVUsRUFBQyxXQTVDUjtBQTZDSEMsa0JBQVksRUFBQyxXQTdDVjtBQThDSEMsdUJBQWlCLEVBQUMsYUE5Q2Y7QUErQ0hDLG1CQUFhLEVBQUMsY0EvQ1g7QUFnREhDLFdBQUssRUFBQztBQWhESCxLQUFSO0FBa0RDeEQsZUFBVyxFQUFDO0FBQ1J5QyxVQUFJLEVBQUMsYUFERztBQUVSQyxjQUFRLEVBQUMsaUJBRkQ7QUFHUkMsU0FBRyxFQUFDLGlCQUhJO0FBSVJDLFVBQUksRUFBQyxlQUpHO0FBS1JDLG9CQUFjLEVBQUMsa0JBTFA7QUFNUkMscUJBQWUsRUFBQyxXQU5SO0FBT1JDLG1CQUFhLEVBQUMsa0JBUE47QUFRUkMsa0JBQVksRUFBQztBQVJMO0FBbERiO0FBaEVRLENBQWYsRTs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxTQUFTdEgsY0FBVCxDQUF3QitILElBQXhCLEVBQThCO0FBQzFCLE1BQUlBLElBQUksS0FBSyxRQUFiLEVBQXNCO0FBQ2xCLFFBQUlDLE9BQU8sR0FBQ0MsNkRBQVo7QUFDSCxHQUZELE1BRU8sSUFBSUYsSUFBSSxLQUFLLFFBQWIsRUFBc0I7QUFDekIsUUFBSUMsT0FBTyxHQUFDRSxnRUFBWjtBQUNILEdBRk0sTUFFQSxJQUFHSCxJQUFJLEtBQUssT0FBWixFQUFvQjtBQUN2QixRQUFJQyxPQUFPLEdBQUNHLHNEQUFaO0FBQ0gsR0FGTSxNQUVBLElBQUdKLElBQUksS0FBRyxLQUFWLEVBQWdCO0FBQ25CLFFBQUlDLE9BQU8sR0FBQ0ksNkRBQVo7QUFDSDs7QUFDRCxNQUFJQyxRQUFRLEdBQUduSixxQkFBcUIsQ0FBQ3NCLEtBQXJDOztBQUNBLE1BQUk2SCxRQUFRLEtBQUcsSUFBZixFQUFvQjtBQUNoQnpKLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ3lKLFlBQXJDLENBQWtELEtBQWxELEVBQXdELEtBQXhEO0FBQ0FoSSxrQkFBYyxDQUFDUSxPQUFmLENBQXVCLFVBQXZCLEVBQW1DdUgsUUFBbkM7QUFDQUEsWUFBUSxHQUFDLElBQVQ7QUFDSCxHQUpELE1BSU87QUFDSHpKLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ3lKLFlBQXJDLENBQWtELEtBQWxELEVBQXdELEtBQXhEO0FBQ0FoSSxrQkFBYyxDQUFDUSxPQUFmLENBQXVCLFVBQXZCLEVBQW1DdUgsUUFBbkM7QUFDSDs7QUFDREUsWUFBVSxDQUFDUCxPQUFPLENBQUNLLFFBQUQsQ0FBUCxDQUFrQjFGLElBQW5CLENBQVY7QUFDQTZGLG1CQUFpQixDQUFDUixPQUFPLENBQUNLLFFBQUQsQ0FBUCxDQUFrQi9ELFdBQW5CLENBQWpCO0FBQ0g7O0FBQ0QsU0FBU2lFLFVBQVQsQ0FBb0JFLEdBQXBCLEVBQXlCO0FBQ3JCLE9BQUssSUFBTXRJLEdBQVgsSUFBa0JzSSxHQUFsQixFQUF1QjtBQUNuQixRQUFHN0osUUFBUSxDQUFDQyxjQUFULENBQXdCc0IsR0FBeEIsTUFBK0IsSUFBbEMsRUFBd0M7QUFDcEN2QixjQUFRLENBQUNDLGNBQVQsQ0FBd0JzQixHQUF4QixFQUE2QnVJLFNBQTdCLEdBQXlDRCxHQUFHLENBQUN0SSxHQUFELENBQTVDO0FBQ0g7QUFDSjtBQUNKOztBQUFBOztBQUNELFNBQVNxSSxpQkFBVCxDQUEyQkMsR0FBM0IsRUFBZ0M7QUFDNUIsT0FBSyxJQUFNdEksR0FBWCxJQUFrQnNJLEdBQWxCLEVBQXVCO0FBQ25CLFFBQUc3SixRQUFRLENBQUNDLGNBQVQsQ0FBd0JzQixHQUF4QixNQUErQixJQUFsQyxFQUF3QztBQUNwQ3ZCLGNBQVEsQ0FBQ0MsY0FBVCxDQUF3QnNCLEdBQXhCLEVBQTZCbUUsV0FBN0IsR0FBMkNtRSxHQUFHLENBQUN0SSxHQUFELENBQTlDO0FBQ0g7QUFDSjtBQUNKOztBQUFBLEM7Ozs7Ozs7Ozs7OztBQ3hDRDtBQUFlO0FBQ1h1QyxJQUFFLEVBQUM7QUFBRUMsUUFBSSxFQUFDO0FBQ0ZVLFdBQUssRUFBRSxpQkFETDtBQUVGc0YsY0FBUSxFQUFFLGNBRlI7QUFHRkMsZ0JBQVUsRUFBRSxRQUhWO0FBSUZDLGVBQVMsRUFBRSxXQUpUO0FBS0ZDLGdCQUFVLEVBQUUsbUJBTFY7QUFNRkMsZ0JBQVUsRUFBRSxTQU5WO0FBT0ZDLGdCQUFVLEVBQUUsZUFQVjtBQVFGQyxrQkFBWSxFQUFFLFNBUlo7QUFTRkMseUJBQW1CLEVBQUMsMENBVGxCO0FBVUZsSyxxQkFBZSxFQUFDO0FBVmQsS0FBUDtBQVlDc0YsZUFBVyxFQUFDO0FBQ1IzRCxXQUFLLEVBQUMsT0FERTtBQUVSd0ksZUFBUyxFQUFDLFVBRkY7QUFHUkMsZUFBUyxFQUFDLGtCQUhGO0FBSVJDLFdBQUssRUFBQyxRQUpFO0FBS1JDLFdBQUssRUFBQyxjQUxFO0FBTVJoSCxhQUFPLEVBQUU7QUFORDtBQVpiLEdBRFE7QUF1QlhzQixJQUFFLEVBQUM7QUFBRWpCLFFBQUksRUFBRTtBQUNIVSxXQUFLLEVBQUUsd0JBREo7QUFFSHNGLGNBQVEsRUFBRSxhQUZQO0FBR0hDLGdCQUFVLEVBQUUsUUFIVDtBQUlIQyxlQUFTLEVBQUUsU0FKUjtBQUtIQyxnQkFBVSxFQUFFLHFCQUxUO0FBTUhDLGdCQUFVLEVBQUUsUUFOVDtBQU9IQyxnQkFBVSxFQUFFLG1CQVBUO0FBUUhDLGtCQUFZLEVBQUUsaUJBUlg7QUFTSEMseUJBQW1CLEVBQUMsMkNBVGpCO0FBVUhsSyxxQkFBZSxFQUFDO0FBVmIsS0FBUjtBQVlDc0YsZUFBVyxFQUFDO0FBQ1IzRCxXQUFLLEVBQUMsZUFERTtBQUVSd0ksZUFBUyxFQUFDLGdCQUZGO0FBR1JDLGVBQVMsRUFBQyxrQkFIRjtBQUlSQyxXQUFLLEVBQUMsZUFKRTtBQUtSQyxXQUFLLEVBQUMsMEJBTEU7QUFNUmhILGFBQU8sRUFBRTtBQU5EO0FBWmI7QUF2QlEsQ0FBZixFOzs7Ozs7Ozs7OztBQ0FBLHVDOzs7Ozs7Ozs7OztBQ0FBLHVDIiwiZmlsZSI6ImF1dGhvcml6YXRpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9zY3JpcHQvYXV0aG9yaXphdGlvblZhbGlkYXRpb24uanNcIik7XG4iLCIvLyBzaGltIGZvciB1c2luZyBwcm9jZXNzIGluIGJyb3dzZXJcbnZhciBwcm9jZXNzID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcblxuLy8gY2FjaGVkIGZyb20gd2hhdGV2ZXIgZ2xvYmFsIGlzIHByZXNlbnQgc28gdGhhdCB0ZXN0IHJ1bm5lcnMgdGhhdCBzdHViIGl0XG4vLyBkb24ndCBicmVhayB0aGluZ3MuICBCdXQgd2UgbmVlZCB0byB3cmFwIGl0IGluIGEgdHJ5IGNhdGNoIGluIGNhc2UgaXQgaXNcbi8vIHdyYXBwZWQgaW4gc3RyaWN0IG1vZGUgY29kZSB3aGljaCBkb2Vzbid0IGRlZmluZSBhbnkgZ2xvYmFscy4gIEl0J3MgaW5zaWRlIGFcbi8vIGZ1bmN0aW9uIGJlY2F1c2UgdHJ5L2NhdGNoZXMgZGVvcHRpbWl6ZSBpbiBjZXJ0YWluIGVuZ2luZXMuXG5cbnZhciBjYWNoZWRTZXRUaW1lb3V0O1xudmFyIGNhY2hlZENsZWFyVGltZW91dDtcblxuZnVuY3Rpb24gZGVmYXVsdFNldFRpbW91dCgpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3NldFRpbWVvdXQgaGFzIG5vdCBiZWVuIGRlZmluZWQnKTtcbn1cbmZ1bmN0aW9uIGRlZmF1bHRDbGVhclRpbWVvdXQgKCkge1xuICAgIHRocm93IG5ldyBFcnJvcignY2xlYXJUaW1lb3V0IGhhcyBub3QgYmVlbiBkZWZpbmVkJyk7XG59XG4oZnVuY3Rpb24gKCkge1xuICAgIHRyeSB7XG4gICAgICAgIGlmICh0eXBlb2Ygc2V0VGltZW91dCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IHNldFRpbWVvdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gZGVmYXVsdFNldFRpbW91dDtcbiAgICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IGRlZmF1bHRTZXRUaW1vdXQ7XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIGlmICh0eXBlb2YgY2xlYXJUaW1lb3V0ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBjbGVhclRpbWVvdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xuICAgICAgICB9XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xuICAgIH1cbn0gKCkpXG5mdW5jdGlvbiBydW5UaW1lb3V0KGZ1bikge1xuICAgIGlmIChjYWNoZWRTZXRUaW1lb3V0ID09PSBzZXRUaW1lb3V0KSB7XG4gICAgICAgIC8vbm9ybWFsIGVudmlyb21lbnRzIGluIHNhbmUgc2l0dWF0aW9uc1xuICAgICAgICByZXR1cm4gc2V0VGltZW91dChmdW4sIDApO1xuICAgIH1cbiAgICAvLyBpZiBzZXRUaW1lb3V0IHdhc24ndCBhdmFpbGFibGUgYnV0IHdhcyBsYXR0ZXIgZGVmaW5lZFxuICAgIGlmICgoY2FjaGVkU2V0VGltZW91dCA9PT0gZGVmYXVsdFNldFRpbW91dCB8fCAhY2FjaGVkU2V0VGltZW91dCkgJiYgc2V0VGltZW91dCkge1xuICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gc2V0VGltZW91dDtcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gd2hlbiB3aGVuIHNvbWVib2R5IGhhcyBzY3Jld2VkIHdpdGggc2V0VGltZW91dCBidXQgbm8gSS5FLiBtYWRkbmVzc1xuICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dChmdW4sIDApO1xuICAgIH0gY2F0Y2goZSl7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICAvLyBXaGVuIHdlIGFyZSBpbiBJLkUuIGJ1dCB0aGUgc2NyaXB0IGhhcyBiZWVuIGV2YWxlZCBzbyBJLkUuIGRvZXNuJ3QgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRTZXRUaW1lb3V0LmNhbGwobnVsbCwgZnVuLCAwKTtcbiAgICAgICAgfSBjYXRjaChlKXtcbiAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yXG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKHRoaXMsIGZ1biwgMCk7XG4gICAgICAgIH1cbiAgICB9XG5cblxufVxuZnVuY3Rpb24gcnVuQ2xlYXJUaW1lb3V0KG1hcmtlcikge1xuICAgIGlmIChjYWNoZWRDbGVhclRpbWVvdXQgPT09IGNsZWFyVGltZW91dCkge1xuICAgICAgICAvL25vcm1hbCBlbnZpcm9tZW50cyBpbiBzYW5lIHNpdHVhdGlvbnNcbiAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH1cbiAgICAvLyBpZiBjbGVhclRpbWVvdXQgd2Fzbid0IGF2YWlsYWJsZSBidXQgd2FzIGxhdHRlciBkZWZpbmVkXG4gICAgaWYgKChjYWNoZWRDbGVhclRpbWVvdXQgPT09IGRlZmF1bHRDbGVhclRpbWVvdXQgfHwgIWNhY2hlZENsZWFyVGltZW91dCkgJiYgY2xlYXJUaW1lb3V0KSB7XG4gICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGNsZWFyVGltZW91dDtcbiAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICAvLyB3aGVuIHdoZW4gc29tZWJvZHkgaGFzIHNjcmV3ZWQgd2l0aCBzZXRUaW1lb3V0IGJ1dCBubyBJLkUuIG1hZGRuZXNzXG4gICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9IGNhdGNoIChlKXtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIC8vIFdoZW4gd2UgYXJlIGluIEkuRS4gYnV0IHRoZSBzY3JpcHQgaGFzIGJlZW4gZXZhbGVkIHNvIEkuRS4gZG9lc24ndCAgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQuY2FsbChudWxsLCBtYXJrZXIpO1xuICAgICAgICB9IGNhdGNoIChlKXtcbiAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yLlxuICAgICAgICAgICAgLy8gU29tZSB2ZXJzaW9ucyBvZiBJLkUuIGhhdmUgZGlmZmVyZW50IHJ1bGVzIGZvciBjbGVhclRpbWVvdXQgdnMgc2V0VGltZW91dFxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dC5jYWxsKHRoaXMsIG1hcmtlcik7XG4gICAgICAgIH1cbiAgICB9XG5cblxuXG59XG52YXIgcXVldWUgPSBbXTtcbnZhciBkcmFpbmluZyA9IGZhbHNlO1xudmFyIGN1cnJlbnRRdWV1ZTtcbnZhciBxdWV1ZUluZGV4ID0gLTE7XG5cbmZ1bmN0aW9uIGNsZWFuVXBOZXh0VGljaygpIHtcbiAgICBpZiAoIWRyYWluaW5nIHx8ICFjdXJyZW50UXVldWUpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBkcmFpbmluZyA9IGZhbHNlO1xuICAgIGlmIChjdXJyZW50UXVldWUubGVuZ3RoKSB7XG4gICAgICAgIHF1ZXVlID0gY3VycmVudFF1ZXVlLmNvbmNhdChxdWV1ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgIH1cbiAgICBpZiAocXVldWUubGVuZ3RoKSB7XG4gICAgICAgIGRyYWluUXVldWUoKTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGRyYWluUXVldWUoKSB7XG4gICAgaWYgKGRyYWluaW5nKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdmFyIHRpbWVvdXQgPSBydW5UaW1lb3V0KGNsZWFuVXBOZXh0VGljayk7XG4gICAgZHJhaW5pbmcgPSB0cnVlO1xuXG4gICAgdmFyIGxlbiA9IHF1ZXVlLmxlbmd0aDtcbiAgICB3aGlsZShsZW4pIHtcbiAgICAgICAgY3VycmVudFF1ZXVlID0gcXVldWU7XG4gICAgICAgIHF1ZXVlID0gW107XG4gICAgICAgIHdoaWxlICgrK3F1ZXVlSW5kZXggPCBsZW4pIHtcbiAgICAgICAgICAgIGlmIChjdXJyZW50UXVldWUpIHtcbiAgICAgICAgICAgICAgICBjdXJyZW50UXVldWVbcXVldWVJbmRleF0ucnVuKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgICAgICBsZW4gPSBxdWV1ZS5sZW5ndGg7XG4gICAgfVxuICAgIGN1cnJlbnRRdWV1ZSA9IG51bGw7XG4gICAgZHJhaW5pbmcgPSBmYWxzZTtcbiAgICBydW5DbGVhclRpbWVvdXQodGltZW91dCk7XG59XG5cbnByb2Nlc3MubmV4dFRpY2sgPSBmdW5jdGlvbiAoZnVuKSB7XG4gICAgdmFyIGFyZ3MgPSBuZXcgQXJyYXkoYXJndW1lbnRzLmxlbmd0aCAtIDEpO1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgYXJnc1tpIC0gMV0gPSBhcmd1bWVudHNbaV07XG4gICAgICAgIH1cbiAgICB9XG4gICAgcXVldWUucHVzaChuZXcgSXRlbShmdW4sIGFyZ3MpKTtcbiAgICBpZiAocXVldWUubGVuZ3RoID09PSAxICYmICFkcmFpbmluZykge1xuICAgICAgICBydW5UaW1lb3V0KGRyYWluUXVldWUpO1xuICAgIH1cbn07XG5cbi8vIHY4IGxpa2VzIHByZWRpY3RpYmxlIG9iamVjdHNcbmZ1bmN0aW9uIEl0ZW0oZnVuLCBhcnJheSkge1xuICAgIHRoaXMuZnVuID0gZnVuO1xuICAgIHRoaXMuYXJyYXkgPSBhcnJheTtcbn1cbkl0ZW0ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmZ1bi5hcHBseShudWxsLCB0aGlzLmFycmF5KTtcbn07XG5wcm9jZXNzLnRpdGxlID0gJ2Jyb3dzZXInO1xucHJvY2Vzcy5icm93c2VyID0gdHJ1ZTtcbnByb2Nlc3MuZW52ID0ge307XG5wcm9jZXNzLmFyZ3YgPSBbXTtcbnByb2Nlc3MudmVyc2lvbiA9ICcnOyAvLyBlbXB0eSBzdHJpbmcgdG8gYXZvaWQgcmVnZXhwIGlzc3Vlc1xucHJvY2Vzcy52ZXJzaW9ucyA9IHt9O1xuXG5mdW5jdGlvbiBub29wKCkge31cblxucHJvY2Vzcy5vbiA9IG5vb3A7XG5wcm9jZXNzLmFkZExpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3Mub25jZSA9IG5vb3A7XG5wcm9jZXNzLm9mZiA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUxpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlQWxsTGlzdGVuZXJzID0gbm9vcDtcbnByb2Nlc3MuZW1pdCA9IG5vb3A7XG5wcm9jZXNzLnByZXBlbmRMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLnByZXBlbmRPbmNlTGlzdGVuZXIgPSBub29wO1xuXG5wcm9jZXNzLmxpc3RlbmVycyA9IGZ1bmN0aW9uIChuYW1lKSB7IHJldHVybiBbXSB9XG5cbnByb2Nlc3MuYmluZGluZyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmJpbmRpbmcgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcblxucHJvY2Vzcy5jd2QgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAnLycgfTtcbnByb2Nlc3MuY2hkaXIgPSBmdW5jdGlvbiAoZGlyKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmNoZGlyIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5wcm9jZXNzLnVtYXNrID0gZnVuY3Rpb24oKSB7IHJldHVybiAwOyB9O1xuIiwibW9kdWxlLmV4cG9ydHMgPSB7XG4gIHBvcnQ6IHByb2Nlc3MuZW52LlBPUlQgfHwgMzAwMCxcbiAgcm91dGluZzoge1xuICAgIGF1dGhvcml6YXRpb246ICcvYXV0aG9yaXphdGlvbicsXG4gICAgcmVnaXN0cmF0aW9uOiAnL3JlZ2lzdHJhdGlvbicsXG4gICAgZ3JvdXBTdHVkZW50OiAnL2dyb3VwU3R1ZGVudCcsXG4gICAgZ2V0QWxsR3JvdXBzOiAnL2dldEFsbEdyb3VwcycsXG4gICAgYWNjb3VudFNldHRpbmc6ICcvYWNjb3VudFNldHRpbmcnLFxuICAgIGdyb3VwczogJy9ncm91cHMnLFxuICAgIHRhYmxlOiAnLycsXG4gICAgdXBkYXRlOiAnL3VwZGF0ZScsXG4gICAgZGVsZXRlOiAnL2RlbGV0ZScsXG4gICAgYWNjb3VudFVwZGF0ZTogJy9hY2NvdW50LXVwZGF0ZScsXG4gICAgZm9yZ290dGVuUGFzc3dvcmQ6ICcvZm9yZ290dGVuLXBhc3N3b3JkJyxcbiAgICBkZWxldGVHcm91cDogJy9kZWxldGUtZ3JvdXAnLFxuICAgIHN0dWRlbnRDbGVhcjogJy9zdHVkZW50LWNsZWFyJyxcbiAgICB1cGRhdGVHcm91cDogJy91cGRhdGUtZ3JvdXAnLFxuICAgIHNlbmRJbWFnZTogJy9zZW5kLWltYWdlJyxcbiAgICByZXNldFNldHRpbmdzOiAnL3Jlc2V0LXNldHRpbmdzJ1xuXG4gIH1cbn07XG4iLCJpbXBvcnQge2NoYW5nZUxhbmd1YWdlfSBmcm9tICcuL2hlbHBlcnMvbGFuZ3VhZ2UvbGFuZ3VhZ2UuanMnXG5pbXBvcnQge3dlYnBhY2tMb2NhbEhvc3R9IGZyb20gXCIuL2hlbHBlcnMvY29uc3RcIjtcbmltcG9ydCB7bG9jYWxob3N0U2Vydn0gZnJvbSBcIi4vaGVscGVycy9jb25zdFwiO1xudmFyIGNvbnN0YW50cyA9IHJlcXVpcmUoJy4uLy4uL3NlcnZlci9oZWxwZXJzL2NvbnN0YW50cycpO1xuXG5pbXBvcnQgXCIuLi9zdHlsZS9hdXRvcml6YXRpb24ubGVzc1wiO1xuaW1wb3J0IFwiLi4vc3R5bGUvZm9vdGVyLmxlc3NcIjtcblxudmFyIGxvZ2luQXZ0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJsb2dpbkF2dFwiKTtcbnZhciBwYXNzd29yZEF2dCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmRBdnRcIik7XG52YXIgbG9nSW5CdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImxvZ0luQnRuXCIpO1xudmFyIHJlZ2lzdHJhdGlvbkJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicmVnaXN0cmF0aW9uQnRuQXZ0XCIpO1xudmFyIG1hc3NhZ2UgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1lc3NhZ2VcIik7XG52YXIgc2VsZWN0RWxlbWVudExhbmd1YWdlPWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2VsZWN0RWxlbWVudExhbmd1YWdlXCIpO1xuXG52YXIgaGlkZW5QYXN3b3JkID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ3cmFwZXJGb3Jnb3R0ZW5QYXNzd29yZFwiKTtcbnZhciBrZXlXb3JkQXZ0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJrYXlXb3JkQXZ0XCIpO1xudmFyIGZvcmdvdHRlbkxvZ2luQXZ0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJmb3Jnb3R0ZW5Mb2dpbkF2dFwiKTtcbnZhciBnZXRQYXNzd29yZEJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZ2V0UGFzc3dvcmRcIik7XG52YXIgZm9yZ290dGVuUGFzc3dvcmRNZXNzYWdlID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJmb3Jnb3R0ZW5QYXNzd29yZE1lc3NhZ2VcIik7XG52YXIgYnRuQ2xvc2VNb2RhbCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2xvc2VcIik7XG52YXIgZm9yZ290dGVuUGFzc3dvcmRCdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImZvcmdvdHRlblBhc3N3b3JkXCIpO1xuXG5sZXQgbG9nb0hvbWU9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibG9nb0hvbWVcIik7XG5sb2dvSG9tZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsKCkgPT4ge2RvY3VtZW50LmxvY2F0aW9uLmhyZWYgPSBgJHt3ZWJwYWNrTG9jYWxIb3N0fS9pbmRleC5odG1sYH0pO1xuXG5zZWxlY3RFbGVtZW50TGFuZ3VhZ2Uub25jaGFuZ2UgPSAoKT0+e2NoYW5nZUxhbmd1YWdlKFwiYXV0aG9yXCIpfTtcbmxvZ2luQXZ0Lm9ua2V5ZG93biA9IGZ1bmN0aW9uIChlKSB7XG4gICAgaWYoIWUua2V5Lm1hdGNoKC9bMC05XFwvQS1aXS9pKSl7XG4gICAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cbn07XG5tYWluKCk7XG5mdW5jdGlvbiBtYWluKCl7XG4gICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJsYW5ndWFnZVwiKSE9bnVsbCl7XG4gICAgICAgIHNlbGVjdEVsZW1lbnRMYW5ndWFnZS52YWx1ZT1zZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFwibGFuZ3VhZ2VcIik7XG4gICAgICAgIGNoYW5nZUxhbmd1YWdlKFwiYXV0aG9yXCIpO1xuICAgIH1lbHNle1xuICAgICAgICBjaGFuZ2VMYW5ndWFnZShcImF1dGhvclwiKTt9XG59XG52YXIgbG9naW5WYWx1ZSA9IHt9O1xubG9nSW5CdG4uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGdldEVsZW1lbnRWYWx1ZSk7XG5yZWdpc3RyYXRpb25CdG4uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcbiAgICBkb2N1bWVudC5sb2NhdGlvbi5ocmVmID0gYCR7d2VicGFja0xvY2FsSG9zdH0vcmVnaXN0cmF0aW9uLmh0bWxgXG59KTtcblxuZnVuY3Rpb24gZ2V0RWxlbWVudFZhbHVlKCkge1xuICAgIGxvZ2luVmFsdWUgPSB7bG9naW46IGxvZ2luQXZ0LnZhbHVlLCBwYXNzd29yZDogcGFzc3dvcmRBdnQudmFsdWV9O1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdsb2dpbk5hbWUnLCBsb2dpbkF2dC52YWx1ZSk7XG4gICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuICAgIHhoci5vcGVuKFwiUE9TVFwiLCBgJHtsb2NhbGhvc3RTZXJ2fSR7Y29uc3RhbnRzLnJvdXRpbmcuYXV0aG9yaXphdGlvbn1gKTtcbiAgICB4aHIuc2V0UmVxdWVzdEhlYWRlcihcIkNvbnRlbnQtdHlwZVwiLCBcImFwcGxpY2F0aW9uL2pzb25cIik7XG5cbiAgICB4aHIub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoeGhyLnN0YXR1cyA9PSA0MDEpIHtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibG9naW5BdnRcIikuc3R5bGU9XCJib3JkZXItY29sb3I6ICM5MDA7XFxuXCIgK1wiIGJhY2tncm91bmQtY29sb3I6ICNGRERcIjtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmRBdnRcIikuc3R5bGU9XCJib3JkZXItY29sb3I6ICM5MDA7XFxuXCIgK1wiIGJhY2tncm91bmQtY29sb3I6ICNGRERcIjtcbiAgICAgICAgICAgIGlmIChzZWxlY3RFbGVtZW50TGFuZ3VhZ2UudmFsdWUgPT09IFwicnVcIikge1xuICAgICAgICAgICAgICAgdmFyIGVyciA9IFwi0JLQstC10LTQuNGC0LUg0LrQvtGA0YDQtdC60YLQvdGL0Lkg0LvQvtCz0LjQvSDQuCDQv9Cw0YDQvtC70YwhXCI7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgdmFyIGVyciA9IFwiSW5zZXJ0IGNvcnJlY3QgbG9naW4gb3IgcGFzc3dvcmQhISFcIlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbWFzc2FnZS5pbm5lclRleHQgPSBlcnI7XG4gICAgICAgIH0gZWxzZSBpZih4aHIuc3RhdHVzID09PSAyMDAgJiYgeGhyLnJlYWR5U3RhdGUgPT09IDQpIHtcbiAgICAgICAgICAgIHZhciBuZXdTdHVkZW50VmFsdWUgPSBKU09OLnBhcnNlKHRoaXMucmVzcG9uc2UpO1xuICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwidGVhY2hlcnNfaWRcIiwgbmV3U3R1ZGVudFZhbHVlWzBdLnRlYWNoZXJzX2lkKTtcbiAgICAgICAgICAgIGRvY3VtZW50LmxvY2F0aW9uLmhyZWYgPSBgJHt3ZWJwYWNrTG9jYWxIb3N0fS90YWJsZS5odG1sYFxuICAgICAgICB9XG4gICAgfVxuICAgIHhoci5zZW5kKEpTT04uc3RyaW5naWZ5KGxvZ2luVmFsdWUpKTtcbn1cblxuZm9yZ290dGVuUGFzc3dvcmRCdG4uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGRpc3BsYXlIaWRlbik7XG5idG5DbG9zZU1vZGFsLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBkaXNwbGF5Tm9uZSk7XG52YXIgZm9yZ290dGVucGFzc3dvcmQgPSB7fTtcbmdldFBhc3N3b3JkQnRuLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBnZXRQYXNzd29yZFZhbHVlKTtcblxuZnVuY3Rpb24gZ2V0UGFzc3dvcmRWYWx1ZSgpIHtcbiAgICB2YXIgcGFzc3dvcmRWYWx1ZSA9IHtsb2dpbjogZm9yZ290dGVuTG9naW5BdnQudmFsdWUsIGtleXdvcmQ6IGtleVdvcmRBdnQudmFsdWV9O1xuICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcbiAgICB4aHIub3BlbihcIlBPU1RcIiwgYCR7bG9jYWxob3N0U2Vydn0ke2NvbnN0YW50cy5yb3V0aW5nLmZvcmdvdHRlblBhc3N3b3JkfWApO1xuICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKFwiQ29udGVudC10eXBlXCIsIFwiYXBwbGljYXRpb24vanNvblwiKTtcbiAgICB4aHIuc2VuZChKU09OLnN0cmluZ2lmeShwYXNzd29yZFZhbHVlKSk7XG4gICAgeGhyLm9ubG9hZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHhoci5zdGF0dXMgPT0gNDAxKSB7XG4gICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImZvcmdvdHRlbkxvZ2luQXZ0XCIpLnN0eWxlPVwiYm9yZGVyLWNvbG9yOiAjOTAwO1xcblwiICtcIiBiYWNrZ3JvdW5kLWNvbG9yOiAjRkREXCI7XG4gICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImtheVdvcmRBdnRcIikuc3R5bGU9XCJib3JkZXItY29sb3I6ICM5MDA7XFxuXCIgK1wiIGJhY2tncm91bmQtY29sb3I6ICNGRERcIjtcbiAgICAgICAgICAgIGlmIChzZWxlY3RFbGVtZW50TGFuZ3VhZ2UudmFsdWUgPT09IFwicnVcIikge1xuICAgICAgICAgICAgICAgIGZvcmdvdHRlblBhc3N3b3JkTWVzc2FnZS52YWx1ZSA9XCLQktCy0LXQtNC40YLQtSDQutC+0YDRgNC10LrRgtC90YvQuSDQu9C+0LPQuNC9INC4INGB0LvQvtCy0L4hXCI7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGZvcmdvdHRlblBhc3N3b3JkTWVzc2FnZS52YWx1ZSA9IFwiSW5zZXJ0IGNvcnJlY3QgbG9naW4gb3Iga2F5IHdvcmQhXCI7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBmb3Jnb3R0ZW5QYXNzd29yZE1lc3NhZ2UudmFsdWUgPSBKU09OLnBhcnNlKHRoaXMucmVzcG9uc2UpXG4gICAgICAgIH1cbiAgICB9XG59XG5cbmZ1bmN0aW9uIGRpc3BsYXlIaWRlbigpe1xuICAgIGhpZGVuUGFzd29yZC5jbGFzc0xpc3QudG9nZ2xlKFwiaGlkZGVuXCIpO1xufVxuZnVuY3Rpb24gZGlzcGxheU5vbmUoKXtcbiAgICBoaWRlblBhc3dvcmQuY2xhc3NMaXN0LnRvZ2dsZShcImhpZGRlblwiKTtcbn1cblxuXG5cblxuIiwiXG5cblxuXG5jb25zdCBsb2NhbGhvc3RTZXJ2ID0gJ2h0dHA6Ly8zNS4yMDUuMTQ4LjIzMC9zZXJ2ZXIvJztcbmNvbnN0IHdlYnBhY2tMb2NhbEhvc3QgPSAnaHR0cDovLzM1LjIwNS4xNDguMjMwLyc7XG5cbi8vXG4vLyBjb25zdCBsb2NhbGhvc3RTZXJ2ID0gXCJodHRwOi8vbG9jYWxob3N0OjMwMDBcIjtcbi8vIGNvbnN0IHdlYnBhY2tMb2NhbEhvc3QgPSBcImh0dHA6Ly9sb2NhbGhvc3Q6OTAwMFwiO1xuXG5cblxuXG5leHBvcnQge1xuICAgIGxvY2FsaG9zdFNlcnYsXG4gICAgd2VicGFja0xvY2FsSG9zdCxcblxufTsiLCJleHBvcnQgZGVmYXVsdHtcbiAgICBlbjp7IHRleHQ6e1xuICAgICAgICAgICAgbXlBY2NvdW50OiBcIk15IEFjY291bnRcIixcbiAgICAgICAgICAgIGljb25UaXRsZTogXCJDaGFuZ2UgeW91ciBhdmF0YXJcIixcbiAgICAgICAgICAgIGxvZ2luVGl0bGU6IFwiTG9naW5cIixcbiAgICAgICAgICAgIHBhc3N3b3JkMVRpdGxlOiBcIlBhc3N3b3JkXCIsXG4gICAgICAgICAgICBwYXNzd29yZDJUaXRsZTogXCJDaGVjayBwYXNzd29yZFwiLFxuICAgICAgICAgICAgZW1haWxUaXRsZTogXCJFbWFpbFwiLFxuICAgICAgICAgICAgcGhvbmVUaXRsZTogXCJQaG9uZVwiLFxuICAgICAgICAgICAgYWJvdXRNeXNlbGZUaXRsZTogXCJBYm91dCBNeXNlbGZcIixcbiAgICAgICAgICAgIFNhdmVCdG46IFwiU2F2ZSBjaGFuZ2VzXCIsXG4gICAgICAgICAgICBjbG9zZTogXCJDbG9zZVwiLFxuICAgICAgICAgICAgY2hhdDpcIkNoYXRcIixcbiAgICAgICAgICAgIGNoYXRXcmFwcGVyOlwiQ29taW5nIHNvb25cIixcbiAgICAgICAgICAgIGdhbWVCb3VuY2U6XCJCb3VuY2VcIixcbiAgICAgICAgICAgIHN0YXJ0OlwiU3RhcnQgZ2FtZVwiLFxuICAgICAgICAgICAgY2xlYXI6XCJDbGVhclwiLFxuICAgICAgICAgICAgY2hhbmdlSW5wdXRzOlwi0KFoYW5nZVwiLFxuICAgICAgICB9XG4gICAgICB9LFxuICAgIHJ1OnsgdGV4dDoge1xuICAgICAgICAgICAgbXlBY2NvdW50OiBcItCc0L7QuSDQsNC60LrQsNGD0L3RglwiLFxuICAgICAgICAgICAgaWNvblRpdGxlOiBcItCY0LfQvNC10L3QuNGC0Ywg0YHQstC+0Lkg0LDQstCw0YLQsNGAXCIsXG4gICAgICAgICAgICBsb2dpblRpdGxlOiBcItCb0L7Qs9C40L1cIixcbiAgICAgICAgICAgIHBhc3N3b3JkMVRpdGxlOiBcItCf0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgcGFzc3dvcmQyVGl0bGU6IFwi0J/RgNC+0LLQtdGA0LrQsCDQv9Cw0YDQvtC70Y9cIixcbiAgICAgICAgICAgIGVtYWlsVGl0bGU6IFwi0LXQvNC10LnQu1wiLFxuICAgICAgICAgICAgcGhvbmVUaXRsZTogXCLQotC10LvQtdGE0L7QvVwiLFxuICAgICAgICAgICAgYWJvdXRNeXNlbGZUaXRsZTogXCLQntCx0L4g0LzQvdC1XCIsXG4gICAgICAgICAgICBTYXZlQnRuOiBcItCh0L7RhdGA0LDQvdC40YLRjCDQuNC30LzQtdC90LXQvdC40Y9cIixcbiAgICAgICAgICAgIGNsb3NlOiBcItCX0LDQutGA0YvRgtGMXCIsXG4gICAgICAgICAgICBjaGF0Olwi0KfQsNGCXCIsXG4gICAgICAgICAgICBjaGF0V3JhcHBlcjpcItCSINGA0LDQt9GA0LDQsdC+0YLQutC1KVwiLFxuICAgICAgICAgICAgZ2FtZUJvdW5jZTpcItCY0LPRgNCwINCo0LDRgNC40LrQuFwiLFxuICAgICAgICAgICAgc3RhcnQ6XCLQndCw0YfQsNGC0Ywg0LjQs9GA0YNcIixcbiAgICAgICAgICAgIGNsZWFyOlwi0J7Rh9C40YHRgtC40YLRjFwiLFxuICAgICAgICAgICAgY2hhbmdlSW5wdXRzOlwi0JjQt9C80LXQvdC40YLRjFwiLFxuICAgICAgICB9XG4gICAgfVxufTsiLCJleHBvcnQgZGVmYXVsdCB7XG4gICAgZW46eyB0ZXh0OntcbiAgICAgICAgICAgIHRleHRBdXRob3JpemF0aW9uOlwiQXV0aG9yaXphdGlvblwiLFxuICAgICAgICAgICAgbGFiZWxMb2dpbjpcIkxvZ2luOlwiLFxuICAgICAgICAgICAgbGFiZWxQYXNzd29yZDpcIlBhc3N3b3JkOlwiLFxuICAgICAgICAgICAgcmVnaXN0cmF0aW9uQnRuQXZ0OlwiUmVnaXN0cmF0aW9uXCIsXG4gICAgICAgICAgICBsb2dJbkJ0bjpcIkxvZyBJblwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc3dvcmQ6XCJGb3Jnb3R0ZW4gUGFzc3dvcmRcIixcbiAgICAgICAgICAgIHRleHRGb3Jnb3R0ZW5QYXNzd29yZDpcIlBhc3N3b3JkIHJlY292ZXJ5XCIsXG4gICAgICAgICAgICBmb3Jnb3R0ZW5MYWJlbExvZ2luOlwiTG9naW46XCIsXG4gICAgICAgICAgICBmb3Jnb3R0ZW5MYWJlbFBhc3N3b3JkOlwiS2V5d29yZDpcIixcbiAgICAgICAgICAgIGZvcmdvdHRlblBhc3NMYWJlbDpcIllvdXIgcGFzc3dvcmQ6XCIsXG4gICAgICAgICAgICBnZXRQYXNzd29yZDpcIkdldCBQYXNzd29yZFwiLFxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBsb2dpbkF2dDpcIndyaXRlIHlvdXIgbG9naW5cIixcbiAgICAgICAgICAgIHBhc3N3b3JkQXZ0Olwid3JpdGUgeW91ciBwYXNzd29yZFwiLFxuICAgICAgICAgICAgZm9yZ290dGVuTG9naW5BdnQ6XCJ3cml0ZSB5b3VyIGxvZ2luIFwiLFxuICAgICAgICAgICAga2F5V29yZEF2dDpcIndyaXRlIHlvdXIga2V5d29yZFwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc3dvcmRNZXNzYWdlOlwiWW91ciBwYXNzd29yZFwiXG4gICAgICAgIH1cblxuICAgIH0sXG4gICAgcnU6eyB0ZXh0OiB7XG4gICAgICAgICAgICB0ZXh0QXV0aG9yaXphdGlvbjpcItCQ0LLRgtC+0YDQuNC30LDRhtC40Y9cIixcbiAgICAgICAgICAgIGxhYmVsTG9naW46XCLQm9C+0LPQuNC9OlwiLFxuICAgICAgICAgICAgbGFiZWxQYXNzd29yZDpcItCf0LDRgNC+0LvRjDpcIixcbiAgICAgICAgICAgIHJlZ2lzdHJhdGlvbkJ0bkF2dDpcItCg0LXQs9C40YHRgtGA0LDRhtC40Y9cIixcbiAgICAgICAgICAgIGxvZ0luQnRuOlwi0JLQvtC50YLQuFwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc3dvcmQ6XCLQl9Cw0LHRi9C7INC/0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgdGV4dEZvcmdvdHRlblBhc3N3b3JkOlwi0JLQvtGB0YHRgtCw0L3QvtCy0LvQtdC90LjQtSDQv9Cw0YDQvtC70Y9cIixcbiAgICAgICAgICAgIGZvcmdvdHRlbkxhYmVsTG9naW46XCLQm9C+0LPQuNC9OlwiLFxuICAgICAgICAgICAgZm9yZ290dGVuTGFiZWxQYXNzd29yZDpcItCh0LXQutGA0LXRgtC90L7QtSDRgdC70L7QstC+OlwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc0xhYmVsOlwi0JLQsNGIINC/0LDRgNC+0LvRjDpcIixcbiAgICAgICAgICAgIGdldFBhc3N3b3JkOlwi0J/QvtC70YPRh9C40YLRjCDQv9Cw0YDQvtC70YxcIixcbiAgICAgICAgfSxcbiAgICAgICAgcGxhY2Vob2xkZXI6e1xuICAgICAgICAgICAgbG9naW5BdnQ6XCLQktCy0LXQtNC40YLQtSDQstCw0Ygg0LvQvtCz0LjQvVwiLFxuICAgICAgICAgICAgcGFzc3dvcmRBdnQ6XCLQktCy0LXQtNC40YLQtSDQstCw0Ygg0L/QsNGA0L7Qu9GMXCIsXG4gICAgICAgICAgICBmb3Jnb3R0ZW5Mb2dpbkF2dDpcItCS0LLQtdC00LjRgtC1INCy0LDRiCDQu9C+0LPQuNC9XCIsXG4gICAgICAgICAgICBrYXlXb3JkQXZ0Olwi0JLQstC10LTQuNGC0LUg0YHQtdC60YDQtdGC0L3QvtC1INGB0LvQvtCy0L5cIixcbiAgICAgICAgICAgIGZvcmdvdHRlblBhc3N3b3JkTWVzc2FnZTpcItCi0YPRgiDQsdGD0LTQtdGCINCy0LDRiCDQv9Cw0YDQvtC70YxcIixcbiAgICAgICAgfVxuICAgIH1cbn07XG4iLCJleHBvcnQgZGVmYXVsdCB7XG4gICAgZW46eyB0ZXh0OntcbiAgICAgICAgICAgIGNvbnRyb2xQYW5lbDpcIkNvbnRyb2wgUGFuZWxcIixcbiAgICAgICAgICAgIGV4aXRDYWJpbmV0OiBcIkV4aXRcIixcbiAgICAgICAgICAgIGFkZHN0dWRlbnQ6IFwiQWRkIHN0dWRlbnRcIixcbiAgICAgICAgICAgIGxhYmVsTmFtZTogXCJOYW1lXCIsXG4gICAgICAgICAgICBsYWJlbExhc3RuYW1lOiBcIlN1cm5hbWVcIixcbiAgICAgICAgICAgIGxhYmVsQWdlOiBcIkFnZVwiLFxuICAgICAgICAgICAgbGFiZWxDaXR5OiBcIkNpdHlcIixcbiAgICAgICAgICAgIENyZWF0ZTogXCJDcmVhdGVcIixcbiAgICAgICAgICAgIHRhYmxlTmFtZTogXCJOYW1lXCIsXG4gICAgICAgICAgICB0YWJsZUxhc3RuYW1lOiBcIkxhc3RuYW1lXCIsXG4gICAgICAgICAgICB0YWJsZUFnZTogXCJBZ2VcIixcbiAgICAgICAgICAgIHRhYmxlQ2l0eTogXCJDaXR5XCIsXG4gICAgICAgICAgICB0YWJsZUJ1dHRvbnM6IFwiQnV0dG9uXCIsXG4gICAgICAgICAgICBjbGVhcklucHV0OlwiQ2xlYXJcIixcbiAgICAgICAgICAgIHByaW50TGFiZWw6XCJFbnRlciB2YWx1ZVwiLFxuICAgICAgICAgICAgcmVzdWx0TGFiZWw6XCJSZXN1bHRcIixcbiAgICAgICAgICAgIGNvbnZlcnRCdXR0b246XCJDb252ZXJ0XCIsXG4gICAgICAgICAgICBwcmludExhYmVsMjpcIkVudGVyIHZhbHVlXCIsXG4gICAgICAgICAgICByZXN1bHRMYWJlbDI6XCJSZXN1bHRcIixcbiAgICAgICAgICAgIGdldEN1cnJlbmN5OlwiQ29udmVydFwiLFxuICAgICAgICAgICAgY29udmVydGVyVGl0bGU6XCJDb252ZXJ0ZXJcIixcbiAgICAgICAgICAgIGJ0bkNoYW5nZUxlbmd0aDpcIkxlbmd0aCBjb252ZXJ0ZXJcIixcbiAgICAgICAgICAgIGJ0bkNoYW5nZU1vbmV5OlwiTW9uZXkgY29udmVydGVyXCIsXG4gICAgICAgICAgICBtZXRlcjE6XCJNZXRlclwiLFxuICAgICAgICAgICAgdmVyc3QxOlwiVmVyc3RcIixcbiAgICAgICAgICAgIG1pbGUxOlwiTWlsZVwiLFxuICAgICAgICAgICAgZm9vdDE6XCJGb290XCIsXG4gICAgICAgICAgICB5YXJkMTpcIllhcmRcIixcbiAgICAgICAgICAgIG1ldGVyMjpcIk1ldGVyXCIsXG4gICAgICAgICAgICB2ZXJzdDI6XCJWZXJzdFwiLFxuICAgICAgICAgICAgbWlsZTI6XCJNaWxlXCIsXG4gICAgICAgICAgICBmb290MjpcIkZvb3RcIixcbiAgICAgICAgICAgIHlhcmQyOlwiWWFyZFwiLFxuICAgICAgICAgICAgbW9kZXMxOlwiTW9kZXNcIixcbiAgICAgICAgICAgIHN0dWRlbnRzMTpcIlN0dWRlbnRzXCIsXG4gICAgICAgICAgICBwYWludDE6XCJQYWludFwiLFxuICAgICAgICAgICAgY29udmVydGVyMTpcIkNvbnZlcnRlclwiLFxuICAgICAgICAgICAgY2FsY3VsYXRvcjE6XCJDYWxjdWxhdG9yXCIsXG4gICAgICAgICAgICBzdHVkZW50c1RpdGxlOlwiU3R1ZGVudHNcIixcbiAgICAgICAgfSxcbnBsYWNlaG9sZGVyOntcbiAgICBOYW1lOlwiV3JpdGUgbmFtZVwiLFxuICAgIExhc3RuYW1lOlwiV3JpdGUgbGFzdG5hbWVcIixcbiAgICBBZ2U6XCJXcml0ZSBhZ2VcIixcbiAgICBDaXR5OlwiV3JpdGUgY2l0eVwiLFxuICAgIGlucHV0Q29udmVydGVyOlwiRW50ZXIgdmFsdWVcIixcbiAgICBvdXRwdXRDb252ZXJ0ZXI6XCJSZXN1bHRcIixcbiAgICBpbnB1dEN1cnJlbmN5OlwiRW50ZXIgdmFsdWVcIixcbiAgICBvdXRwdXRSZXN1bHQ6XCJSZXN1bHRcIixcbiAgICBjb250clBhbmVsOlwiQ29udHJvbCBQYW5lbFwiLFxuICAgIGxpbmVUaGljazpcIkxpbmUgdGhpY2tuZXNzXCIsXG4gICAgbGluZUNvbDpcIkxpbmUgY29sb3JcIixcbiAgICBjbGVhcjpcIkNsZWFyXCIsXG4gICAgbGFiZWxQYWludDpcIlBhaW50XCIsXG4gICAgdGV4dFNldHRpbmdzOlwiU2V0dGluZ3NcIixcbiAgICBub3RvZmljYXRpb25MYWJlbDpcIk5vdGlmaWNhdGlvbnNcIixcbiAgICBsYW5ndWFnZUxhYmVsOlwiQ2hvb3NlIGxhbmd1YWdlXCIsXG4gICAgcmVzZXQ6XCJSZXNldFwiXG5cbn1cblxuICAgIH0sXG4gICAgcnU6eyB0ZXh0OiB7XG4gICAgICAgICAgICBjb250cm9sUGFuZWw6XCLQn9Cw0L3QtdC70Ywg0YPQv9GA0LDQstC70LXQvdC40Y9cIixcbiAgICAgICAgICAgIGV4aXRDYWJpbmV0OiBcItCS0YvQudGC0LhcIixcbiAgICAgICAgICAgIGFkZHN0dWRlbnQ6IFwi0JTQvtCx0LDQstC40YLRjCDQodGC0YPQtNC10L3RgtCwXCIsXG4gICAgICAgICAgICBsYWJlbE5hbWU6IFwi0JjQvNGPXCIsXG4gICAgICAgICAgICBsYWJlbExhc3RuYW1lOiBcItCk0LDQvNC40LvQuNGPXCIsXG4gICAgICAgICAgICBsYWJlbEFnZTogXCLQktC+0LfRgNCw0YHRglwiLFxuICAgICAgICAgICAgbGFiZWxDaXR5OiBcItCT0L7RgNC+0LRcIixcbiAgICAgICAgICAgIENyZWF0ZTogXCLQodC+0LfQtNCw0YLRjFwiLFxuICAgICAgICAgICAgdGFibGVOYW1lOiBcItCY0LzRj1wiLFxuICAgICAgICAgICAgdGFibGVMYXN0bmFtZTogXCLQpNCw0LzQuNC70LjRj1wiLFxuICAgICAgICAgICAgdGFibGVBZ2U6IFwi0JLQvtC30YDQsNGB0YJcIixcbiAgICAgICAgICAgIHRhYmxlQ2l0eTogXCLQk9C+0YDQvtC0XCIsXG4gICAgICAgICAgICB0YWJsZUJ1dHRvbnM6IFwi0JrQvdC+0L/QutC4XCIsXG4gICAgICAgICAgICBjbGVhcklucHV0Olwi0J7Rh9C40YHRgtC40YLRjFwiLFxuICAgICAgICAgICAgcHJpbnRMYWJlbDpcItCS0LLQtdC00LjRgtC1INC30L3QsNGH0LXQvdC40LVcIixcbiAgICAgICAgICAgIHJlc3VsdExhYmVsOlwi0KDQtdC30YPQu9GM0YLQsNGCXCIsXG4gICAgICAgICAgICBjb252ZXJ0QnV0dG9uOlwi0JrQvtC90LLQtdGA0YLQuNGA0L7QstCw0YLRjFwiLFxuICAgICAgICAgICAgcHJpbnRMYWJlbDI6XCLQktCy0LXQtNC40YLQtSDQt9C90LDRh9C10L3QuNC1XCIsXG4gICAgICAgICAgICByZXN1bHRMYWJlbDI6XCLQoNC10LfRg9C70YzRgtCw0YJcIixcbiAgICAgICAgICAgIGdldEN1cnJlbmN5Olwi0JrQvtC90LLQtdGA0YLQuNGA0L7QstCw0YLRjFwiLFxuICAgICAgICAgICAgY29udmVydGVyVGl0bGU6XCLQmtC+0L3QstC10YDRgtC10YBcIixcbiAgICAgICAgICAgIGJ0bkNoYW5nZUxlbmd0aDpcItCa0L7QvdCy0LXRgNGC0LXRgCDRgNCw0YHRgdGC0L7Rj9C90LjRj1wiLFxuICAgICAgICAgICAgYnRuQ2hhbmdlTW9uZXk6XCLQmtC+0L3QstC10YDRgtC10YAg0LLQsNC70Y7RglwiLFxuICAgICAgICAgICAgbWV0ZXIxOlwi0JzQtdGC0YBcIixcbiAgICAgICAgICAgIHZlcnN0MTpcItCS0LXRgNGB0YLQsFwiLFxuICAgICAgICAgICAgbWlsZTE6XCLQnNC40LvRj1wiLFxuICAgICAgICAgICAgZm9vdDE6XCLQpNGD0YJcIixcbiAgICAgICAgICAgIHlhcmQxOlwi0K/RgNC0XCIsXG4gICAgICAgICAgICBtZXRlcjI6XCLQnNC10YLRgFwiLFxuICAgICAgICAgICAgdmVyc3QyOlwi0JLQtdGA0YHRgtCwXCIsXG4gICAgICAgICAgICBtaWxlMjpcItCc0LjQu9GPXCIsXG4gICAgICAgICAgICBmb290MjpcItCk0YPRglwiLFxuICAgICAgICAgICAgeWFyZDI6XCLQr9GA0LRcIixcbiAgICAgICAgICAgIG1vZGVzMTpcItCc0L7QtNGLXCIsXG4gICAgICAgICAgICBzdHVkZW50czE6XCLQodGC0YPQtNC10L3RglwiLFxuICAgICAgICAgICAgcGFpbnQxOlwi0KDQuNGB0L7QstCw0LvQutCwXCIsXG4gICAgICAgICAgICBjb252ZXJ0ZXIxOlwi0JrQvtC90LLQtdGA0YLQtdGAXCIsXG4gICAgICAgICAgICBjYWxjdWxhdG9yMTpcItCa0LDQu9GM0LrRg9C70Y/RgtC+0YBcIixcbiAgICAgICAgICAgIHN0dWRlbnRzVGl0bGU6XCLQodGC0YPQtNC10L3RgtGLXCIsXG4gICAgICAgICAgICBjb250clBhbmVsOlwi0J/QsNC90LXQu9GMINGD0L/RgNCw0LLQu9C10L3QuNGPXCIsXG4gICAgICAgICAgICBsaW5lVGhpY2s6XCLQotC+0LvRidC40L3QsCDQu9C40L3QuNC4XCIsXG4gICAgICAgICAgICBsaW5lQ29sOlwi0KbQstC10YIg0LvQuNC90LjQuFwiLFxuICAgICAgICAgICAgY2xlYXI6XCLQntGH0LjRgdGC0LjRgtGMXCIsXG4gICAgICAgICAgICBsYWJlbFBhaW50Olwi0KDQuNGB0L7QstCw0LvQutCwXCIsXG4gICAgICAgICAgICB0ZXh0U2V0dGluZ3M6XCLQndCw0YHRgtGA0L7QudC60LhcIixcbiAgICAgICAgICAgIG5vdG9maWNhdGlvbkxhYmVsOlwi0KPQstC10LTQvtC80LvQtdC90LjRj1wiLFxuICAgICAgICAgICAgbGFuZ3VhZ2VMYWJlbDpcItCS0YvQsdGA0LDRgtGMINGP0LfRi9C6XCIsXG4gICAgICAgICAgICByZXNldDpcItCh0LHRgNC+0YHQuNGC0YxcIlxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBOYW1lOlwi0JLQstC10LTQuNGC0LUg0LjQvNGPXCIsXG4gICAgICAgICAgICBMYXN0bmFtZTpcItCS0LLQtdC00LjRgtC1INGE0LDQvNC40LvQuNGOXCIsXG4gICAgICAgICAgICBBZ2U6XCLQktCy0LXQtNC40YLQtSDQstC+0LfRgNCw0YHRglwiLFxuICAgICAgICAgICAgQ2l0eTpcItCS0LLQtdC00LjRgtC1INCz0L7RgNC+0LRcIixcbiAgICAgICAgICAgIGlucHV0Q29udmVydGVyOlwi0JLQstC10LTQuNGC0LUg0LfQvdCw0YfQtdC90LjQtVwiLFxuICAgICAgICAgICAgb3V0cHV0Q29udmVydGVyOlwi0KDQtdC30YPQu9GM0YLQsNGCXCIsXG4gICAgICAgICAgICBpbnB1dEN1cnJlbmN5Olwi0JLQstC10LTQuNGC0LUg0LfQvdCw0YfQtdC90LjQtVwiLFxuICAgICAgICAgICAgb3V0cHV0UmVzdWx0Olwi0KDQtdC30YPQu9GM0YLQsNGCXCIsXG4gICAgICAgIH1cbiAgICB9XG59O1xuXG5cbiIsImltcG9ydCBsYW5ndWFnZUJveEluZGV4IGZyb20gXCIuL2luZGV4TGFuZ3VhZ2VcIjtcbmltcG9ydCBhdXRvclBhZ2UgZnJvbSBcIi4vYXV0b3JpemF0aW9uTGFuZ3VhZ2VcIjtcbmltcG9ydCBhY2NTZXRQYWdlIGZyb20gXCIuL2FjY291bnRTZXR0aW5nc0xhbmd1YWdlXCI7XG5pbXBvcnQgcmVnUGFnZSBmcm9tIFwiLi9yZWdpc3RyYXRpb25MYW5ndWFnZVwiXG5leHBvcnQge2NoYW5nZUxhbmd1YWdlfTtcbmZ1bmN0aW9uIGNoYW5nZUxhbmd1YWdlKHBhZ2UpIHtcbiAgICBpZiAocGFnZSA9PT0gXCJhdXRob3JcIil7XG4gICAgICAgIHZhciBwYWdlQ29sPWF1dG9yUGFnZTtcbiAgICB9IGVsc2UgaWYgKHBhZ2UgPT09IFwiYWNjU2V0XCIpe1xuICAgICAgICB2YXIgcGFnZUNvbD1hY2NTZXRQYWdlO1xuICAgIH0gZWxzZSBpZihwYWdlID09PSBcImluZGV4XCIpe1xuICAgICAgICB2YXIgcGFnZUNvbD1sYW5ndWFnZUJveEluZGV4O1xuICAgIH0gZWxzZSBpZihwYWdlPT09XCJyZWdcIil7XG4gICAgICAgIHZhciBwYWdlQ29sPXJlZ1BhZ2U7XG4gICAgfVxuICAgIHZhciBsYW5ndWFnZSA9IHNlbGVjdEVsZW1lbnRMYW5ndWFnZS52YWx1ZTtcbiAgICBpZiAobGFuZ3VhZ2U9PT1cImFyXCIpe1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRpcmVjdGlvblwiKS5zZXRBdHRyaWJ1dGUoXCJkaXJcIixcInJ0bFwiKTtcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnbGFuZ3VhZ2UnLCBsYW5ndWFnZSk7XG4gICAgICAgIGxhbmd1YWdlPVwiZW5cIjtcbiAgICB9IGVsc2Uge1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRpcmVjdGlvblwiKS5zZXRBdHRyaWJ1dGUoXCJkaXJcIixcImx0bFwiKTtcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnbGFuZ3VhZ2UnLCBsYW5ndWFnZSk7XG4gICAgfVxuICAgIGNoYW5nZVRleHQocGFnZUNvbFtsYW5ndWFnZV0udGV4dCk7XG4gICAgY2hhbmdlUGxhY2VIb2xkZXIocGFnZUNvbFtsYW5ndWFnZV0ucGxhY2Vob2xkZXIpXG59XG5mdW5jdGlvbiBjaGFuZ2VUZXh0KG9iaikge1xuICAgIGZvciAoY29uc3Qga2V5IGluIG9iaikge1xuICAgICAgICBpZihkb2N1bWVudC5nZXRFbGVtZW50QnlJZChrZXkpIT09bnVsbCkge1xuICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoa2V5KS5pbm5lckhUTUwgPSBvYmpba2V5XTtcbiAgICAgICAgfVxuICAgIH1cbn07XG5mdW5jdGlvbiBjaGFuZ2VQbGFjZUhvbGRlcihvYmopIHtcbiAgICBmb3IgKGNvbnN0IGtleSBpbiBvYmopIHtcbiAgICAgICAgaWYoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoa2V5KSE9PW51bGwpIHtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGtleSkucGxhY2Vob2xkZXIgPSBvYmpba2V5XTtcbiAgICAgICAgfVxuICAgIH1cbn07XG4iLCJleHBvcnQgZGVmYXVsdCB7XG4gICAgZW46eyB0ZXh0OntcbiAgICAgICAgICAgIGNsb3NlOiBcIlJldHVybiB0byBsb2dpblwiLFxuICAgICAgICAgICAgcmVnTGFiZWw6IFwiUmVnaXN0cmF0aW9uXCIsXG4gICAgICAgICAgICBsb2dpbkxhYmVsOiBcIkxvZ2luKlwiLFxuICAgICAgICAgICAgcGFzc0xhYmVsOiBcIlBhc3N3b3JkKlwiLFxuICAgICAgICAgICAgcGFzczJMYWJlbDogXCJDb25maXJtIHBhc3N3b3JkKlwiLFxuICAgICAgICAgICAgZW1haWxMYWJlbDogXCJFLW1haWwqXCIsXG4gICAgICAgICAgICBwaG9uZUxhYmVsOiBcIlBob25lIG51bWJlcipcIixcbiAgICAgICAgICAgIGtleXdvcmRMYWJlbDogXCJLZXl3b3JkXCIsXG4gICAgICAgICAgICBrZXl3b3JkTm90aWZpY2F0aW9uOlwiVGhlIGtleXdvcmQgbmVlZGVkIGZvciBwYXNzd29yZCByZWNvdmVyeVwiLFxuICAgICAgICAgICAgcmVnaXN0cmF0aW9uQnRuOlwiU2lnbiB1cFwiLFxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBsb2dpbjpcIkxvZ2luXCIsXG4gICAgICAgICAgICBwYXNzd29yZDE6XCJQYXNzd29yZFwiLFxuICAgICAgICAgICAgcGFzc3dvcmQyOlwiQ29uZmlybSBQYXNzd29yZFwiLFxuICAgICAgICAgICAgZW1haWw6XCJFLW1haWxcIixcbiAgICAgICAgICAgIHBob25lOlwiUGhvbmUgbnVtYmVyXCIsXG4gICAgICAgICAgICBrZXl3b3JkOiBcIktleXdvcmRcIixcbiAgICAgICAgfVxuXG4gICAgfSxcbiAgICBydTp7IHRleHQ6IHtcbiAgICAgICAgICAgIGNsb3NlOiBcItCS0LXRgNC90YPRgtGB0Y8g0Log0LDQstGC0L7RgNC40LfQsNGG0LjQuFwiLFxuICAgICAgICAgICAgcmVnTGFiZWw6IFwi0KDQtdCz0LjRgdGC0YDQsNGG0LjRj1wiLFxuICAgICAgICAgICAgbG9naW5MYWJlbDogXCLQm9C+0LPQuNC9KlwiLFxuICAgICAgICAgICAgcGFzc0xhYmVsOiBcItCf0LDRgNC+0LvRjCpcIixcbiAgICAgICAgICAgIHBhc3MyTGFiZWw6IFwi0J/QvtC00YLQstC10YDQtNC40YLQtSDQv9Cw0YDQvtC70YwqXCIsXG4gICAgICAgICAgICBlbWFpbExhYmVsOiBcItCf0L7Rh9GC0LAqXCIsXG4gICAgICAgICAgICBwaG9uZUxhYmVsOiBcItCi0LXQu9C10YTQvtC90L3Ri9C5INC90L7QvNC10YAqXCIsXG4gICAgICAgICAgICBrZXl3b3JkTGFiZWw6IFwi0KHQtdC60YDQtdGC0L3QvtC1INGB0LvQvtCy0L5cIixcbiAgICAgICAgICAgIGtleXdvcmROb3RpZmljYXRpb246XCLQodC10LrRgNC10YLQvdC+0LUg0YHQu9C+0LLQviDQtNC70Y8g0LLQvtGB0YHRgtCw0L3QvtCy0LvQtdC90LjRjyDQv9Cw0YDQvtC70Y9cIixcbiAgICAgICAgICAgIHJlZ2lzdHJhdGlvbkJ0bjpcItCX0LDRgNC10LPQuNGB0YLRgNC40YDQvtCy0LDRgtGM0YHRj1wiLFxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBsb2dpbjpcItCS0LLQtdC00LjRgtC1INC70L7Qs9C40L1cIixcbiAgICAgICAgICAgIHBhc3N3b3JkMTpcItCS0LLQtdC00LjRgtC1INC/0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgcGFzc3dvcmQyOlwi0J/QvtCy0YLQvtGA0LjRgtC1INC/0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgZW1haWw6XCLQktCy0LXQtNC40YLQtSDQv9C+0YfRgtGDXCIsXG4gICAgICAgICAgICBwaG9uZTpcItCS0LLQtdC00LjRgtC1INGC0LXQu9C10YTQvtC90L3Ri9C5INC90L7QvNC10YBcIixcbiAgICAgICAgICAgIGtleXdvcmQ6IFwi0JLQstC10LTQuNGC0LUg0YHQtdC60YDQtdGC0L3QvtC1INGB0LvQvtCy0L5cIixcbiAgICAgICAgfVxuICAgIH1cbn07IiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIl0sInNvdXJjZVJvb3QiOiIifQ==