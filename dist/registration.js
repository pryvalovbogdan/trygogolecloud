/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/script/registrationValidation.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./server/helpers/constants.js":
/*!*************************************!*\
  !*** ./server/helpers/constants.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {module.exports = {
  port: process.env.PORT || 3000,
  routing: {
    authorization: '/authorization',
    registration: '/registration',
    groupStudent: '/groupStudent',
    getAllGroups: '/getAllGroups',
    accountSetting: '/accountSetting',
    groups: '/groups',
    table: '/',
    update: '/update',
    "delete": '/delete',
    accountUpdate: '/account-update',
    forgottenPassword: '/forgotten-password',
    deleteGroup: '/delete-group',
    studentClear: '/student-clear',
    updateGroup: '/update-group',
    sendImage: '/send-image',
    resetSettings: '/reset-settings'
  }
};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/script/helpers/const.js":
/*!*************************************!*\
  !*** ./src/script/helpers/const.js ***!
  \*************************************/
/*! exports provided: localhostServ, webpackLocalHost */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "localhostServ", function() { return localhostServ; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "webpackLocalHost", function() { return webpackLocalHost; });
var localhostServ = 'http://35.205.148.230/server/';
var webpackLocalHost = 'http://35.205.148.230/'; //
// const localhostServ = "http://localhost:3000";
// const webpackLocalHost = "http://localhost:9000";



/***/ }),

/***/ "./src/script/helpers/language/accountSettingsLanguage.js":
/*!****************************************************************!*\
  !*** ./src/script/helpers/language/accountSettingsLanguage.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      myAccount: "My Account",
      iconTitle: "Change your avatar",
      loginTitle: "Login",
      password1Title: "Password",
      password2Title: "Check password",
      emailTitle: "Email",
      phoneTitle: "Phone",
      aboutMyselfTitle: "About Myself",
      SaveBtn: "Save changes",
      close: "Close",
      chat: "Chat",
      chatWrapper: "Coming soon",
      gameBounce: "Bounce",
      start: "Start game",
      clear: "Clear",
      changeInputs: "Сhange"
    }
  },
  ru: {
    text: {
      myAccount: "Мой аккаунт",
      iconTitle: "Изменить свой аватар",
      loginTitle: "Логин",
      password1Title: "Пароль",
      password2Title: "Проверка пароля",
      emailTitle: "емейл",
      phoneTitle: "Телефон",
      aboutMyselfTitle: "Обо мне",
      SaveBtn: "Сохранить изменения",
      close: "Закрыть",
      chat: "Чат",
      chatWrapper: "В разработке)",
      gameBounce: "Игра Шарики",
      start: "Начать игру",
      clear: "Очистить",
      changeInputs: "Изменить"
    }
  }
});

/***/ }),

/***/ "./src/script/helpers/language/autorizationLanguage.js":
/*!*************************************************************!*\
  !*** ./src/script/helpers/language/autorizationLanguage.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      textAuthorization: "Authorization",
      labelLogin: "Login:",
      labelPassword: "Password:",
      registrationBtnAvt: "Registration",
      logInBtn: "Log In",
      forgottenPassword: "Forgotten Password",
      textForgottenPassword: "Password recovery",
      forgottenLabelLogin: "Login:",
      forgottenLabelPassword: "Keyword:",
      forgottenPassLabel: "Your password:",
      getPassword: "Get Password"
    },
    placeholder: {
      loginAvt: "write your login",
      passwordAvt: "write your password",
      forgottenLoginAvt: "write your login ",
      kayWordAvt: "write your keyword",
      forgottenPasswordMessage: "Your password"
    }
  },
  ru: {
    text: {
      textAuthorization: "Авторизация",
      labelLogin: "Логин:",
      labelPassword: "Пароль:",
      registrationBtnAvt: "Регистрация",
      logInBtn: "Войти",
      forgottenPassword: "Забыл пароль",
      textForgottenPassword: "Восстановление пароля",
      forgottenLabelLogin: "Логин:",
      forgottenLabelPassword: "Секретное слово:",
      forgottenPassLabel: "Ваш пароль:",
      getPassword: "Получить пароль"
    },
    placeholder: {
      loginAvt: "Введите ваш логин",
      passwordAvt: "Введите ваш пароль",
      forgottenLoginAvt: "Введите ваш логин",
      kayWordAvt: "Введите секретное слово",
      forgottenPasswordMessage: "Тут будет ваш пароль"
    }
  }
});

/***/ }),

/***/ "./src/script/helpers/language/indexLanguage.js":
/*!******************************************************!*\
  !*** ./src/script/helpers/language/indexLanguage.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      controlPanel: "Control Panel",
      exitCabinet: "Exit",
      addstudent: "Add student",
      labelName: "Name",
      labelLastname: "Surname",
      labelAge: "Age",
      labelCity: "City",
      Create: "Create",
      tableName: "Name",
      tableLastname: "Lastname",
      tableAge: "Age",
      tableCity: "City",
      tableButtons: "Button",
      clearInput: "Clear",
      printLabel: "Enter value",
      resultLabel: "Result",
      convertButton: "Convert",
      printLabel2: "Enter value",
      resultLabel2: "Result",
      getCurrency: "Convert",
      converterTitle: "Converter",
      btnChangeLength: "Length converter",
      btnChangeMoney: "Money converter",
      meter1: "Meter",
      verst1: "Verst",
      mile1: "Mile",
      foot1: "Foot",
      yard1: "Yard",
      meter2: "Meter",
      verst2: "Verst",
      mile2: "Mile",
      foot2: "Foot",
      yard2: "Yard",
      modes1: "Modes",
      students1: "Students",
      paint1: "Paint",
      converter1: "Converter",
      calculator1: "Calculator",
      studentsTitle: "Students"
    },
    placeholder: {
      Name: "Write name",
      Lastname: "Write lastname",
      Age: "Write age",
      City: "Write city",
      inputConverter: "Enter value",
      outputConverter: "Result",
      inputCurrency: "Enter value",
      outputResult: "Result",
      contrPanel: "Control Panel",
      lineThick: "Line thickness",
      lineCol: "Line color",
      clear: "Clear",
      labelPaint: "Paint",
      textSettings: "Settings",
      notoficationLabel: "Notifications",
      languageLabel: "Choose language",
      reset: "Reset"
    }
  },
  ru: {
    text: {
      controlPanel: "Панель управления",
      exitCabinet: "Выйти",
      addstudent: "Добавить Студента",
      labelName: "Имя",
      labelLastname: "Фамилия",
      labelAge: "Возраст",
      labelCity: "Город",
      Create: "Создать",
      tableName: "Имя",
      tableLastname: "Фамилия",
      tableAge: "Возраст",
      tableCity: "Город",
      tableButtons: "Кнопки",
      clearInput: "Очистить",
      printLabel: "Введите значение",
      resultLabel: "Результат",
      convertButton: "Конвертировать",
      printLabel2: "Введите значение",
      resultLabel2: "Результат",
      getCurrency: "Конвертировать",
      converterTitle: "Конвертер",
      btnChangeLength: "Конвертер расстояния",
      btnChangeMoney: "Конвертер валют",
      meter1: "Метр",
      verst1: "Верста",
      mile1: "Миля",
      foot1: "Фут",
      yard1: "Ярд",
      meter2: "Метр",
      verst2: "Верста",
      mile2: "Миля",
      foot2: "Фут",
      yard2: "Ярд",
      modes1: "Моды",
      students1: "Студент",
      paint1: "Рисовалка",
      converter1: "Конвертер",
      calculator1: "Калькулятор",
      studentsTitle: "Студенты",
      contrPanel: "Панель управления",
      lineThick: "Толщина линии",
      lineCol: "Цвет линии",
      clear: "Очистить",
      labelPaint: "Рисовалка",
      textSettings: "Настройки",
      notoficationLabel: "Уведомления",
      languageLabel: "Выбрать язык",
      reset: "Сбросить"
    },
    placeholder: {
      Name: "Введите имя",
      Lastname: "Введите фамилию",
      Age: "Введите возраст",
      City: "Введите город",
      inputConverter: "Введите значение",
      outputConverter: "Результат",
      inputCurrency: "Введите значение",
      outputResult: "Результат"
    }
  }
});

/***/ }),

/***/ "./src/script/helpers/language/language.js":
/*!*************************************************!*\
  !*** ./src/script/helpers/language/language.js ***!
  \*************************************************/
/*! exports provided: changeLanguage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeLanguage", function() { return changeLanguage; });
/* harmony import */ var _indexLanguage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./indexLanguage */ "./src/script/helpers/language/indexLanguage.js");
/* harmony import */ var _autorizationLanguage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./autorizationLanguage */ "./src/script/helpers/language/autorizationLanguage.js");
/* harmony import */ var _accountSettingsLanguage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accountSettingsLanguage */ "./src/script/helpers/language/accountSettingsLanguage.js");
/* harmony import */ var _registrationLanguage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registrationLanguage */ "./src/script/helpers/language/registrationLanguage.js");






function changeLanguage(page) {
  if (page === "author") {
    var pageCol = _autorizationLanguage__WEBPACK_IMPORTED_MODULE_1__["default"];
  } else if (page === "accSet") {
    var pageCol = _accountSettingsLanguage__WEBPACK_IMPORTED_MODULE_2__["default"];
  } else if (page === "index") {
    var pageCol = _indexLanguage__WEBPACK_IMPORTED_MODULE_0__["default"];
  } else if (page === "reg") {
    var pageCol = _registrationLanguage__WEBPACK_IMPORTED_MODULE_3__["default"];
  }

  var language = selectElementLanguage.value;

  if (language === "ar") {
    document.getElementById("direction").setAttribute("dir", "rtl");
    sessionStorage.setItem('language', language);
    language = "en";
  } else {
    document.getElementById("direction").setAttribute("dir", "ltl");
    sessionStorage.setItem('language', language);
  }

  changeText(pageCol[language].text);
  changePlaceHolder(pageCol[language].placeholder);
}

function changeText(obj) {
  for (var key in obj) {
    if (document.getElementById(key) !== null) {
      document.getElementById(key).innerHTML = obj[key];
    }
  }
}

;

function changePlaceHolder(obj) {
  for (var key in obj) {
    if (document.getElementById(key) !== null) {
      document.getElementById(key).placeholder = obj[key];
    }
  }
}

;

/***/ }),

/***/ "./src/script/helpers/language/registrationLanguage.js":
/*!*************************************************************!*\
  !*** ./src/script/helpers/language/registrationLanguage.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  en: {
    text: {
      close: "Return to login",
      regLabel: "Registration",
      loginLabel: "Login*",
      passLabel: "Password*",
      pass2Label: "Confirm password*",
      emailLabel: "E-mail*",
      phoneLabel: "Phone number*",
      keywordLabel: "Keyword",
      keywordNotification: "The keyword needed for password recovery",
      registrationBtn: "Sign up"
    },
    placeholder: {
      login: "Login",
      password1: "Password",
      password2: "Confirm Password",
      email: "E-mail",
      phone: "Phone number",
      keyword: "Keyword"
    }
  },
  ru: {
    text: {
      close: "Вернутся к авторизации",
      regLabel: "Регистрация",
      loginLabel: "Логин*",
      passLabel: "Пароль*",
      pass2Label: "Подтвердите пароль*",
      emailLabel: "Почта*",
      phoneLabel: "Телефонный номер*",
      keywordLabel: "Секретное слово",
      keywordNotification: "Секретное слово для восстановления пароля",
      registrationBtn: "Зарегистрироваться"
    },
    placeholder: {
      login: "Введите логин",
      password1: "Введите пароль",
      password2: "Повторите пароль",
      email: "Введите почту",
      phone: "Введите телефонный номер",
      keyword: "Введите секретное слово"
    }
  }
});

/***/ }),

/***/ "./src/script/registrationValidation.js":
/*!**********************************************!*\
  !*** ./src/script/registrationValidation.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_const__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers/const */ "./src/script/helpers/const.js");
/* harmony import */ var _helpers_language_language_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers/language/language.js */ "./src/script/helpers/language/language.js");
/* harmony import */ var _style_footer_less__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../style/footer.less */ "./src/style/footer.less");
/* harmony import */ var _style_footer_less__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_footer_less__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _src_style_registration_less__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../src/style/registration.less */ "./src/style/registration.less");
/* harmony import */ var _src_style_registration_less__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_src_style_registration_less__WEBPACK_IMPORTED_MODULE_3__);





var constants = __webpack_require__(/*! ../../server/helpers/constants */ "./server/helpers/constants.js");

var selectElementLanguage = document.getElementById("selectElementLanguage");

selectElementLanguage.onchange = function () {
  Object(_helpers_language_language_js__WEBPACK_IMPORTED_MODULE_1__["changeLanguage"])("reg");
};

main();

function main() {
  if (sessionStorage.getItem("language") != null) {
    selectElementLanguage.value = sessionStorage.getItem("language");
    Object(_helpers_language_language_js__WEBPACK_IMPORTED_MODULE_1__["changeLanguage"])("reg");
  } else {
    Object(_helpers_language_language_js__WEBPACK_IMPORTED_MODULE_1__["changeLanguage"])("reg");
  }
}

var registrationBtn = document.getElementById("registrationBtn");
registrationBtn.addEventListener("click", getValidation);
var err = "";

var User = function User() {
  this.login = document.getElementById("login").value.replace(/\s/g, '');
  this.password = document.getElementById("password1").value;
  this.password2 = document.getElementById("password2").value;
  this.email = document.getElementById("email").value;
  this.phone = document.getElementById("phone").value.replace(/[+()-/\s]/g, '');
  this.keyword = document.getElementById("keyword").value;
};

console.log(document.getElementById("keyword").value);

function getValidation() {
  var elementValue = new User();
  var message = document.getElementById("message");

  if (checkLogin(elementValue.login) && checkPass(elementValue.password, elementValue.password2) && checkEmail(elementValue.email) && checkPhone(elementValue.phone, "380")) {
    delete elementValue.password2;
    registrationBtn.disabled = true;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_0__["localhostServ"]).concat(constants.routing.registration));
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(elementValue));

    xhr.onreadystatechange = function () {
      if (xhr.status === 400) {
        if (selectElementLanguage.value === "ru") {
          err = "Такой логин уже существует";
        } else {
          err = "This login already exists";
        }

        document.getElementById("errorLogin").innerHTML = err;
        document.getElementById("login").style = "border-color: #900;\n" + " background-color: #FDD";
        registrationBtn.disabled = false;
      } else if (xhr.status === 200) {
        message.innerHTML = "Регистрация успешна";
        setTimeout(function () {
          return document.location.href = "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_0__["webpackLocalHost"], "/index.html");
        }, 1500);
      }
    };
  }
}

function checkLogin(login) {
  if (login.length < 5 || !isNaN(Number(login[0])) || login.match(/[?!,.а-яА-ЯёЁ\s]+$/i) || login.length > 30) {
    if (selectElementLanguage.value === "ru") {
      err = "Некорректный логин";
    } else {
      err = "Incorrect login";
    }

    document.getElementById("errorLogin").innerHTML = err;
    document.getElementById("login").style = "border-color: #900;\n" + " background-color: #FDD";
    return false;
  }

  document.getElementById("errorLogin").innerHTML = "";
  document.getElementById("login").style = "none";
  return true;
}

function checkPass(pass1, pass2) {
  if (pass1.length < 5 || pass1.length > 15) {
    if (selectElementLanguage.value === "ru") {
      err = "Некорректный пароль";
    } else {
      err = "Incorrect password";
    }

    document.getElementById("errorPassword1").innerHTML = err;
    document.getElementById("password1").style = "border-color: #900;\n" + " background-color: #FDD";
    return false;
  } else if (pass1 !== pass2) {
    if (selectElementLanguage.value === "ru") {
      err = "Пароли не совпадают";
    } else {
      err = "Passwords do not match";
    }

    document.getElementById("errorPassword2").innerHTML = err;
    document.getElementById("password2").style = "border-color: #900;\n" + " background-color: #FDD";
    return false;
  }

  document.getElementById("password1").style = "none";
  document.getElementById("password2").style = "none";
  document.getElementById("errorPassword1").innerHTML = "";
  document.getElementById("errorPassword2").innerHTML = "";
  return true;
}

function checkPhone(phone, countriCode) {
  if (phone.length !== 12 || phone.substring(0, 3) != countriCode) {
    if (selectElementLanguage.value === "ru") {
      err = "Некорректрый телефон";
    } else {
      err = "Incorrect phone";
    }

    document.getElementById("errorPhone").innerHTML = err;
    document.getElementById("phone").style = "border-color: #900;\n" + " background-color: #FDD";
    return false;
  }

  document.getElementById("phone").style = "none";
  document.getElementById("errorPhone").innerHTML = "";
  return true;
}

function checkEmail(email) {
  if (email.includes("@")) {
    var domen = email.substring(email.indexOf("@"), email.length);

    if (domen.length > 2) {
      document.getElementById("errorEmail").innerHTML = "";
      document.getElementById("email").style = "none";
      return true;
    }
  }

  if (selectElementLanguage.value === "ru") {
    err = "Некорректный e-mail";
  } else {
    err = "incorrect email";
  }

  document.getElementById("errorEmail").innerHTML = err;
  document.getElementById("email").style = "border-color: #900;\n" + " background-color: #FDD";
  return false;
}

var closeBtn = document.getElementById("close");
closeBtn.addEventListener('click', function () {
  document.location.href = "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_0__["webpackLocalHost"], "/index.html");
});
var logoHome = document.getElementById("logoHome");
logoHome.addEventListener('click', function () {
  document.location.href = "".concat(_helpers_const__WEBPACK_IMPORTED_MODULE_0__["webpackLocalHost"], "/index.html");
});


/***/ }),

/***/ "./src/style/footer.less":
/*!*******************************!*\
  !*** ./src/style/footer.less ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/style/registration.less":
/*!*************************************!*\
  !*** ./src/style/registration.less ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Byb2Nlc3MvYnJvd3Nlci5qcyIsIndlYnBhY2s6Ly8vLi9zZXJ2ZXIvaGVscGVycy9jb25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC9oZWxwZXJzL2NvbnN0LmpzIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHQvaGVscGVycy9sYW5ndWFnZS9hY2NvdW50U2V0dGluZ3NMYW5ndWFnZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0L2hlbHBlcnMvbGFuZ3VhZ2UvYXV0b3JpemF0aW9uTGFuZ3VhZ2UuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC9oZWxwZXJzL2xhbmd1YWdlL2luZGV4TGFuZ3VhZ2UuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC9oZWxwZXJzL2xhbmd1YWdlL2xhbmd1YWdlLmpzIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHQvaGVscGVycy9sYW5ndWFnZS9yZWdpc3RyYXRpb25MYW5ndWFnZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0L3JlZ2lzdHJhdGlvblZhbGlkYXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3N0eWxlL2Zvb3Rlci5sZXNzP2I5YjYiLCJ3ZWJwYWNrOi8vLy4vc3JjL3N0eWxlL3JlZ2lzdHJhdGlvbi5sZXNzP2E2YmYiXSwibmFtZXMiOlsibW9kdWxlIiwiZXhwb3J0cyIsInBvcnQiLCJwcm9jZXNzIiwiZW52IiwiUE9SVCIsInJvdXRpbmciLCJhdXRob3JpemF0aW9uIiwicmVnaXN0cmF0aW9uIiwiZ3JvdXBTdHVkZW50IiwiZ2V0QWxsR3JvdXBzIiwiYWNjb3VudFNldHRpbmciLCJncm91cHMiLCJ0YWJsZSIsInVwZGF0ZSIsImFjY291bnRVcGRhdGUiLCJmb3Jnb3R0ZW5QYXNzd29yZCIsImRlbGV0ZUdyb3VwIiwic3R1ZGVudENsZWFyIiwidXBkYXRlR3JvdXAiLCJzZW5kSW1hZ2UiLCJyZXNldFNldHRpbmdzIiwibG9jYWxob3N0U2VydiIsIndlYnBhY2tMb2NhbEhvc3QiLCJlbiIsInRleHQiLCJteUFjY291bnQiLCJpY29uVGl0bGUiLCJsb2dpblRpdGxlIiwicGFzc3dvcmQxVGl0bGUiLCJwYXNzd29yZDJUaXRsZSIsImVtYWlsVGl0bGUiLCJwaG9uZVRpdGxlIiwiYWJvdXRNeXNlbGZUaXRsZSIsIlNhdmVCdG4iLCJjbG9zZSIsImNoYXQiLCJjaGF0V3JhcHBlciIsImdhbWVCb3VuY2UiLCJzdGFydCIsImNsZWFyIiwiY2hhbmdlSW5wdXRzIiwicnUiLCJ0ZXh0QXV0aG9yaXphdGlvbiIsImxhYmVsTG9naW4iLCJsYWJlbFBhc3N3b3JkIiwicmVnaXN0cmF0aW9uQnRuQXZ0IiwibG9nSW5CdG4iLCJ0ZXh0Rm9yZ290dGVuUGFzc3dvcmQiLCJmb3Jnb3R0ZW5MYWJlbExvZ2luIiwiZm9yZ290dGVuTGFiZWxQYXNzd29yZCIsImZvcmdvdHRlblBhc3NMYWJlbCIsImdldFBhc3N3b3JkIiwicGxhY2Vob2xkZXIiLCJsb2dpbkF2dCIsInBhc3N3b3JkQXZ0IiwiZm9yZ290dGVuTG9naW5BdnQiLCJrYXlXb3JkQXZ0IiwiZm9yZ290dGVuUGFzc3dvcmRNZXNzYWdlIiwiY29udHJvbFBhbmVsIiwiZXhpdENhYmluZXQiLCJhZGRzdHVkZW50IiwibGFiZWxOYW1lIiwibGFiZWxMYXN0bmFtZSIsImxhYmVsQWdlIiwibGFiZWxDaXR5IiwiQ3JlYXRlIiwidGFibGVOYW1lIiwidGFibGVMYXN0bmFtZSIsInRhYmxlQWdlIiwidGFibGVDaXR5IiwidGFibGVCdXR0b25zIiwiY2xlYXJJbnB1dCIsInByaW50TGFiZWwiLCJyZXN1bHRMYWJlbCIsImNvbnZlcnRCdXR0b24iLCJwcmludExhYmVsMiIsInJlc3VsdExhYmVsMiIsImdldEN1cnJlbmN5IiwiY29udmVydGVyVGl0bGUiLCJidG5DaGFuZ2VMZW5ndGgiLCJidG5DaGFuZ2VNb25leSIsIm1ldGVyMSIsInZlcnN0MSIsIm1pbGUxIiwiZm9vdDEiLCJ5YXJkMSIsIm1ldGVyMiIsInZlcnN0MiIsIm1pbGUyIiwiZm9vdDIiLCJ5YXJkMiIsIm1vZGVzMSIsInN0dWRlbnRzMSIsInBhaW50MSIsImNvbnZlcnRlcjEiLCJjYWxjdWxhdG9yMSIsInN0dWRlbnRzVGl0bGUiLCJOYW1lIiwiTGFzdG5hbWUiLCJBZ2UiLCJDaXR5IiwiaW5wdXRDb252ZXJ0ZXIiLCJvdXRwdXRDb252ZXJ0ZXIiLCJpbnB1dEN1cnJlbmN5Iiwib3V0cHV0UmVzdWx0IiwiY29udHJQYW5lbCIsImxpbmVUaGljayIsImxpbmVDb2wiLCJsYWJlbFBhaW50IiwidGV4dFNldHRpbmdzIiwibm90b2ZpY2F0aW9uTGFiZWwiLCJsYW5ndWFnZUxhYmVsIiwicmVzZXQiLCJjaGFuZ2VMYW5ndWFnZSIsInBhZ2UiLCJwYWdlQ29sIiwiYXV0b3JQYWdlIiwiYWNjU2V0UGFnZSIsImxhbmd1YWdlQm94SW5kZXgiLCJyZWdQYWdlIiwibGFuZ3VhZ2UiLCJzZWxlY3RFbGVtZW50TGFuZ3VhZ2UiLCJ2YWx1ZSIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJzZXRBdHRyaWJ1dGUiLCJzZXNzaW9uU3RvcmFnZSIsInNldEl0ZW0iLCJjaGFuZ2VUZXh0IiwiY2hhbmdlUGxhY2VIb2xkZXIiLCJvYmoiLCJrZXkiLCJpbm5lckhUTUwiLCJyZWdMYWJlbCIsImxvZ2luTGFiZWwiLCJwYXNzTGFiZWwiLCJwYXNzMkxhYmVsIiwiZW1haWxMYWJlbCIsInBob25lTGFiZWwiLCJrZXl3b3JkTGFiZWwiLCJrZXl3b3JkTm90aWZpY2F0aW9uIiwicmVnaXN0cmF0aW9uQnRuIiwibG9naW4iLCJwYXNzd29yZDEiLCJwYXNzd29yZDIiLCJlbWFpbCIsInBob25lIiwia2V5d29yZCIsImNvbnN0YW50cyIsInJlcXVpcmUiLCJvbmNoYW5nZSIsIm1haW4iLCJnZXRJdGVtIiwiYWRkRXZlbnRMaXN0ZW5lciIsImdldFZhbGlkYXRpb24iLCJlcnIiLCJVc2VyIiwicmVwbGFjZSIsInBhc3N3b3JkIiwiY29uc29sZSIsImxvZyIsImVsZW1lbnRWYWx1ZSIsIm1lc3NhZ2UiLCJjaGVja0xvZ2luIiwiY2hlY2tQYXNzIiwiY2hlY2tFbWFpbCIsImNoZWNrUGhvbmUiLCJkaXNhYmxlZCIsInhociIsIlhNTEh0dHBSZXF1ZXN0Iiwib3BlbiIsInNldFJlcXVlc3RIZWFkZXIiLCJzZW5kIiwiSlNPTiIsInN0cmluZ2lmeSIsIm9ucmVhZHlzdGF0ZWNoYW5nZSIsInN0YXR1cyIsInN0eWxlIiwic2V0VGltZW91dCIsImxvY2F0aW9uIiwiaHJlZiIsImxlbmd0aCIsImlzTmFOIiwiTnVtYmVyIiwibWF0Y2giLCJwYXNzMSIsInBhc3MyIiwiY291bnRyaUNvZGUiLCJzdWJzdHJpbmciLCJpbmNsdWRlcyIsImRvbWVuIiwiaW5kZXhPZiIsImNsb3NlQnRuIiwibG9nb0hvbWUiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLHNCQUFzQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEscUNBQXFDOztBQUVyQztBQUNBO0FBQ0E7O0FBRUEsMkJBQTJCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QixVQUFVOzs7Ozs7Ozs7Ozs7QUN2THRDQSxxREFBTSxDQUFDQyxPQUFQLEdBQWlCO0FBQ2ZDLE1BQUksRUFBRUMsT0FBTyxDQUFDQyxHQUFSLENBQVlDLElBQVosSUFBb0IsSUFEWDtBQUVmQyxTQUFPLEVBQUU7QUFDUEMsaUJBQWEsRUFBRSxnQkFEUjtBQUVQQyxnQkFBWSxFQUFFLGVBRlA7QUFHUEMsZ0JBQVksRUFBRSxlQUhQO0FBSVBDLGdCQUFZLEVBQUUsZUFKUDtBQUtQQyxrQkFBYyxFQUFFLGlCQUxUO0FBTVBDLFVBQU0sRUFBRSxTQU5EO0FBT1BDLFNBQUssRUFBRSxHQVBBO0FBUVBDLFVBQU0sRUFBRSxTQVJEO0FBU1AsY0FBUSxTQVREO0FBVVBDLGlCQUFhLEVBQUUsaUJBVlI7QUFXUEMscUJBQWlCLEVBQUUscUJBWFo7QUFZUEMsZUFBVyxFQUFFLGVBWk47QUFhUEMsZ0JBQVksRUFBRSxnQkFiUDtBQWNQQyxlQUFXLEVBQUUsZUFkTjtBQWVQQyxhQUFTLEVBQUUsYUFmSjtBQWdCUEMsaUJBQWEsRUFBRTtBQWhCUjtBQUZNLENBQWpCLEM7Ozs7Ozs7Ozs7Ozs7QUNJQTtBQUFBO0FBQUE7QUFBQSxJQUFNQyxhQUFhLEdBQUcsK0JBQXRCO0FBQ0EsSUFBTUMsZ0JBQWdCLEdBQUcsd0JBQXpCLEMsQ0FFQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDVEE7QUFBYztBQUNWQyxJQUFFLEVBQUM7QUFBRUMsUUFBSSxFQUFDO0FBQ0ZDLGVBQVMsRUFBRSxZQURUO0FBRUZDLGVBQVMsRUFBRSxvQkFGVDtBQUdGQyxnQkFBVSxFQUFFLE9BSFY7QUFJRkMsb0JBQWMsRUFBRSxVQUpkO0FBS0ZDLG9CQUFjLEVBQUUsZ0JBTGQ7QUFNRkMsZ0JBQVUsRUFBRSxPQU5WO0FBT0ZDLGdCQUFVLEVBQUUsT0FQVjtBQVFGQyxzQkFBZ0IsRUFBRSxjQVJoQjtBQVNGQyxhQUFPLEVBQUUsY0FUUDtBQVVGQyxXQUFLLEVBQUUsT0FWTDtBQVdGQyxVQUFJLEVBQUMsTUFYSDtBQVlGQyxpQkFBVyxFQUFDLGFBWlY7QUFhRkMsZ0JBQVUsRUFBQyxRQWJUO0FBY0ZDLFdBQUssRUFBQyxZQWRKO0FBZUZDLFdBQUssRUFBQyxPQWZKO0FBZ0JGQyxrQkFBWSxFQUFDO0FBaEJYO0FBQVAsR0FETztBQW9CVkMsSUFBRSxFQUFDO0FBQUVqQixRQUFJLEVBQUU7QUFDSEMsZUFBUyxFQUFFLGFBRFI7QUFFSEMsZUFBUyxFQUFFLHNCQUZSO0FBR0hDLGdCQUFVLEVBQUUsT0FIVDtBQUlIQyxvQkFBYyxFQUFFLFFBSmI7QUFLSEMsb0JBQWMsRUFBRSxpQkFMYjtBQU1IQyxnQkFBVSxFQUFFLE9BTlQ7QUFPSEMsZ0JBQVUsRUFBRSxTQVBUO0FBUUhDLHNCQUFnQixFQUFFLFNBUmY7QUFTSEMsYUFBTyxFQUFFLHFCQVROO0FBVUhDLFdBQUssRUFBRSxTQVZKO0FBV0hDLFVBQUksRUFBQyxLQVhGO0FBWUhDLGlCQUFXLEVBQUMsZUFaVDtBQWFIQyxnQkFBVSxFQUFDLGFBYlI7QUFjSEMsV0FBSyxFQUFDLGFBZEg7QUFlSEMsV0FBSyxFQUFDLFVBZkg7QUFnQkhDLGtCQUFZLEVBQUM7QUFoQlY7QUFBUjtBQXBCTyxDQUFkLEU7Ozs7Ozs7Ozs7OztBQ0FBO0FBQWU7QUFDWGpCLElBQUUsRUFBQztBQUFFQyxRQUFJLEVBQUM7QUFDRmtCLHVCQUFpQixFQUFDLGVBRGhCO0FBRUZDLGdCQUFVLEVBQUMsUUFGVDtBQUdGQyxtQkFBYSxFQUFDLFdBSFo7QUFJRkMsd0JBQWtCLEVBQUMsY0FKakI7QUFLRkMsY0FBUSxFQUFDLFFBTFA7QUFNRi9CLHVCQUFpQixFQUFDLG9CQU5oQjtBQU9GZ0MsMkJBQXFCLEVBQUMsbUJBUHBCO0FBUUZDLHlCQUFtQixFQUFDLFFBUmxCO0FBU0ZDLDRCQUFzQixFQUFDLFVBVHJCO0FBVUZDLHdCQUFrQixFQUFDLGdCQVZqQjtBQVdGQyxpQkFBVyxFQUFDO0FBWFYsS0FBUDtBQWFDQyxlQUFXLEVBQUM7QUFDUkMsY0FBUSxFQUFDLGtCQUREO0FBRVJDLGlCQUFXLEVBQUMscUJBRko7QUFHUkMsdUJBQWlCLEVBQUMsbUJBSFY7QUFJUkMsZ0JBQVUsRUFBQyxvQkFKSDtBQUtSQyw4QkFBd0IsRUFBQztBQUxqQjtBQWJiLEdBRFE7QUF1QlhoQixJQUFFLEVBQUM7QUFBRWpCLFFBQUksRUFBRTtBQUNIa0IsdUJBQWlCLEVBQUMsYUFEZjtBQUVIQyxnQkFBVSxFQUFDLFFBRlI7QUFHSEMsbUJBQWEsRUFBQyxTQUhYO0FBSUhDLHdCQUFrQixFQUFDLGFBSmhCO0FBS0hDLGNBQVEsRUFBQyxPQUxOO0FBTUgvQix1QkFBaUIsRUFBQyxjQU5mO0FBT0hnQywyQkFBcUIsRUFBQyx1QkFQbkI7QUFRSEMseUJBQW1CLEVBQUMsUUFSakI7QUFTSEMsNEJBQXNCLEVBQUMsa0JBVHBCO0FBVUhDLHdCQUFrQixFQUFDLGFBVmhCO0FBV0hDLGlCQUFXLEVBQUM7QUFYVCxLQUFSO0FBYUNDLGVBQVcsRUFBQztBQUNSQyxjQUFRLEVBQUMsbUJBREQ7QUFFUkMsaUJBQVcsRUFBQyxvQkFGSjtBQUdSQyx1QkFBaUIsRUFBQyxtQkFIVjtBQUlSQyxnQkFBVSxFQUFDLHlCQUpIO0FBS1JDLDhCQUF3QixFQUFDO0FBTGpCO0FBYmI7QUF2QlEsQ0FBZixFOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFlO0FBQ1hsQyxJQUFFLEVBQUM7QUFBRUMsUUFBSSxFQUFDO0FBQ0ZrQyxrQkFBWSxFQUFDLGVBRFg7QUFFRkMsaUJBQVcsRUFBRSxNQUZYO0FBR0ZDLGdCQUFVLEVBQUUsYUFIVjtBQUlGQyxlQUFTLEVBQUUsTUFKVDtBQUtGQyxtQkFBYSxFQUFFLFNBTGI7QUFNRkMsY0FBUSxFQUFFLEtBTlI7QUFPRkMsZUFBUyxFQUFFLE1BUFQ7QUFRRkMsWUFBTSxFQUFFLFFBUk47QUFTRkMsZUFBUyxFQUFFLE1BVFQ7QUFVRkMsbUJBQWEsRUFBRSxVQVZiO0FBV0ZDLGNBQVEsRUFBRSxLQVhSO0FBWUZDLGVBQVMsRUFBRSxNQVpUO0FBYUZDLGtCQUFZLEVBQUUsUUFiWjtBQWNGQyxnQkFBVSxFQUFDLE9BZFQ7QUFlRkMsZ0JBQVUsRUFBQyxhQWZUO0FBZ0JGQyxpQkFBVyxFQUFDLFFBaEJWO0FBaUJGQyxtQkFBYSxFQUFDLFNBakJaO0FBa0JGQyxpQkFBVyxFQUFDLGFBbEJWO0FBbUJGQyxrQkFBWSxFQUFDLFFBbkJYO0FBb0JGQyxpQkFBVyxFQUFDLFNBcEJWO0FBcUJGQyxvQkFBYyxFQUFDLFdBckJiO0FBc0JGQyxxQkFBZSxFQUFDLGtCQXRCZDtBQXVCRkMsb0JBQWMsRUFBQyxpQkF2QmI7QUF3QkZDLFlBQU0sRUFBQyxPQXhCTDtBQXlCRkMsWUFBTSxFQUFDLE9BekJMO0FBMEJGQyxXQUFLLEVBQUMsTUExQko7QUEyQkZDLFdBQUssRUFBQyxNQTNCSjtBQTRCRkMsV0FBSyxFQUFDLE1BNUJKO0FBNkJGQyxZQUFNLEVBQUMsT0E3Qkw7QUE4QkZDLFlBQU0sRUFBQyxPQTlCTDtBQStCRkMsV0FBSyxFQUFDLE1BL0JKO0FBZ0NGQyxXQUFLLEVBQUMsTUFoQ0o7QUFpQ0ZDLFdBQUssRUFBQyxNQWpDSjtBQWtDRkMsWUFBTSxFQUFDLE9BbENMO0FBbUNGQyxlQUFTLEVBQUMsVUFuQ1I7QUFvQ0ZDLFlBQU0sRUFBQyxPQXBDTDtBQXFDRkMsZ0JBQVUsRUFBQyxXQXJDVDtBQXNDRkMsaUJBQVcsRUFBQyxZQXRDVjtBQXVDRkMsbUJBQWEsRUFBQztBQXZDWixLQUFQO0FBeUNQNUMsZUFBVyxFQUFDO0FBQ1I2QyxVQUFJLEVBQUMsWUFERztBQUVSQyxjQUFRLEVBQUMsZ0JBRkQ7QUFHUkMsU0FBRyxFQUFDLFdBSEk7QUFJUkMsVUFBSSxFQUFDLFlBSkc7QUFLUkMsb0JBQWMsRUFBQyxhQUxQO0FBTVJDLHFCQUFlLEVBQUMsUUFOUjtBQU9SQyxtQkFBYSxFQUFDLGFBUE47QUFRUkMsa0JBQVksRUFBQyxRQVJMO0FBU1JDLGdCQUFVLEVBQUMsZUFUSDtBQVVSQyxlQUFTLEVBQUMsZ0JBVkY7QUFXUkMsYUFBTyxFQUFDLFlBWEE7QUFZUnBFLFdBQUssRUFBQyxPQVpFO0FBYVJxRSxnQkFBVSxFQUFDLE9BYkg7QUFjUkMsa0JBQVksRUFBQyxVQWRMO0FBZVJDLHVCQUFpQixFQUFDLGVBZlY7QUFnQlJDLG1CQUFhLEVBQUMsaUJBaEJOO0FBaUJSQyxXQUFLLEVBQUM7QUFqQkU7QUF6Q0wsR0FEUTtBQWdFWHZFLElBQUUsRUFBQztBQUFFakIsUUFBSSxFQUFFO0FBQ0hrQyxrQkFBWSxFQUFDLG1CQURWO0FBRUhDLGlCQUFXLEVBQUUsT0FGVjtBQUdIQyxnQkFBVSxFQUFFLG1CQUhUO0FBSUhDLGVBQVMsRUFBRSxLQUpSO0FBS0hDLG1CQUFhLEVBQUUsU0FMWjtBQU1IQyxjQUFRLEVBQUUsU0FOUDtBQU9IQyxlQUFTLEVBQUUsT0FQUjtBQVFIQyxZQUFNLEVBQUUsU0FSTDtBQVNIQyxlQUFTLEVBQUUsS0FUUjtBQVVIQyxtQkFBYSxFQUFFLFNBVlo7QUFXSEMsY0FBUSxFQUFFLFNBWFA7QUFZSEMsZUFBUyxFQUFFLE9BWlI7QUFhSEMsa0JBQVksRUFBRSxRQWJYO0FBY0hDLGdCQUFVLEVBQUMsVUFkUjtBQWVIQyxnQkFBVSxFQUFDLGtCQWZSO0FBZ0JIQyxpQkFBVyxFQUFDLFdBaEJUO0FBaUJIQyxtQkFBYSxFQUFDLGdCQWpCWDtBQWtCSEMsaUJBQVcsRUFBQyxrQkFsQlQ7QUFtQkhDLGtCQUFZLEVBQUMsV0FuQlY7QUFvQkhDLGlCQUFXLEVBQUMsZ0JBcEJUO0FBcUJIQyxvQkFBYyxFQUFDLFdBckJaO0FBc0JIQyxxQkFBZSxFQUFDLHNCQXRCYjtBQXVCSEMsb0JBQWMsRUFBQyxpQkF2Qlo7QUF3QkhDLFlBQU0sRUFBQyxNQXhCSjtBQXlCSEMsWUFBTSxFQUFDLFFBekJKO0FBMEJIQyxXQUFLLEVBQUMsTUExQkg7QUEyQkhDLFdBQUssRUFBQyxLQTNCSDtBQTRCSEMsV0FBSyxFQUFDLEtBNUJIO0FBNkJIQyxZQUFNLEVBQUMsTUE3Qko7QUE4QkhDLFlBQU0sRUFBQyxRQTlCSjtBQStCSEMsV0FBSyxFQUFDLE1BL0JIO0FBZ0NIQyxXQUFLLEVBQUMsS0FoQ0g7QUFpQ0hDLFdBQUssRUFBQyxLQWpDSDtBQWtDSEMsWUFBTSxFQUFDLE1BbENKO0FBbUNIQyxlQUFTLEVBQUMsU0FuQ1A7QUFvQ0hDLFlBQU0sRUFBQyxXQXBDSjtBQXFDSEMsZ0JBQVUsRUFBQyxXQXJDUjtBQXNDSEMsaUJBQVcsRUFBQyxhQXRDVDtBQXVDSEMsbUJBQWEsRUFBQyxVQXZDWDtBQXdDSFMsZ0JBQVUsRUFBQyxtQkF4Q1I7QUF5Q0hDLGVBQVMsRUFBQyxlQXpDUDtBQTBDSEMsYUFBTyxFQUFDLFlBMUNMO0FBMkNIcEUsV0FBSyxFQUFDLFVBM0NIO0FBNENIcUUsZ0JBQVUsRUFBQyxXQTVDUjtBQTZDSEMsa0JBQVksRUFBQyxXQTdDVjtBQThDSEMsdUJBQWlCLEVBQUMsYUE5Q2Y7QUErQ0hDLG1CQUFhLEVBQUMsY0EvQ1g7QUFnREhDLFdBQUssRUFBQztBQWhESCxLQUFSO0FBa0RDNUQsZUFBVyxFQUFDO0FBQ1I2QyxVQUFJLEVBQUMsYUFERztBQUVSQyxjQUFRLEVBQUMsaUJBRkQ7QUFHUkMsU0FBRyxFQUFDLGlCQUhJO0FBSVJDLFVBQUksRUFBQyxlQUpHO0FBS1JDLG9CQUFjLEVBQUMsa0JBTFA7QUFNUkMscUJBQWUsRUFBQyxXQU5SO0FBT1JDLG1CQUFhLEVBQUMsa0JBUE47QUFRUkMsa0JBQVksRUFBQztBQVJMO0FBbERiO0FBaEVRLENBQWYsRTs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxTQUFTUyxjQUFULENBQXdCQyxJQUF4QixFQUE4QjtBQUMxQixNQUFJQSxJQUFJLEtBQUssUUFBYixFQUFzQjtBQUNsQixRQUFJQyxPQUFPLEdBQUNDLDZEQUFaO0FBQ0gsR0FGRCxNQUVPLElBQUlGLElBQUksS0FBSyxRQUFiLEVBQXNCO0FBQ3pCLFFBQUlDLE9BQU8sR0FBQ0UsZ0VBQVo7QUFDSCxHQUZNLE1BRUEsSUFBR0gsSUFBSSxLQUFLLE9BQVosRUFBb0I7QUFDdkIsUUFBSUMsT0FBTyxHQUFDRyxzREFBWjtBQUNILEdBRk0sTUFFQSxJQUFHSixJQUFJLEtBQUcsS0FBVixFQUFnQjtBQUNuQixRQUFJQyxPQUFPLEdBQUNJLDZEQUFaO0FBQ0g7O0FBQ0QsTUFBSUMsUUFBUSxHQUFHQyxxQkFBcUIsQ0FBQ0MsS0FBckM7O0FBQ0EsTUFBSUYsUUFBUSxLQUFHLElBQWYsRUFBb0I7QUFDaEJHLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ0MsWUFBckMsQ0FBa0QsS0FBbEQsRUFBd0QsS0FBeEQ7QUFDQUMsa0JBQWMsQ0FBQ0MsT0FBZixDQUF1QixVQUF2QixFQUFtQ1AsUUFBbkM7QUFDQUEsWUFBUSxHQUFDLElBQVQ7QUFDSCxHQUpELE1BSU87QUFDSEcsWUFBUSxDQUFDQyxjQUFULENBQXdCLFdBQXhCLEVBQXFDQyxZQUFyQyxDQUFrRCxLQUFsRCxFQUF3RCxLQUF4RDtBQUNBQyxrQkFBYyxDQUFDQyxPQUFmLENBQXVCLFVBQXZCLEVBQW1DUCxRQUFuQztBQUNIOztBQUNEUSxZQUFVLENBQUNiLE9BQU8sQ0FBQ0ssUUFBRCxDQUFQLENBQWtCaEcsSUFBbkIsQ0FBVjtBQUNBeUcsbUJBQWlCLENBQUNkLE9BQU8sQ0FBQ0ssUUFBRCxDQUFQLENBQWtCcEUsV0FBbkIsQ0FBakI7QUFDSDs7QUFDRCxTQUFTNEUsVUFBVCxDQUFvQkUsR0FBcEIsRUFBeUI7QUFDckIsT0FBSyxJQUFNQyxHQUFYLElBQWtCRCxHQUFsQixFQUF1QjtBQUNuQixRQUFHUCxRQUFRLENBQUNDLGNBQVQsQ0FBd0JPLEdBQXhCLE1BQStCLElBQWxDLEVBQXdDO0FBQ3BDUixjQUFRLENBQUNDLGNBQVQsQ0FBd0JPLEdBQXhCLEVBQTZCQyxTQUE3QixHQUF5Q0YsR0FBRyxDQUFDQyxHQUFELENBQTVDO0FBQ0g7QUFDSjtBQUNKOztBQUFBOztBQUNELFNBQVNGLGlCQUFULENBQTJCQyxHQUEzQixFQUFnQztBQUM1QixPQUFLLElBQU1DLEdBQVgsSUFBa0JELEdBQWxCLEVBQXVCO0FBQ25CLFFBQUdQLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3Qk8sR0FBeEIsTUFBK0IsSUFBbEMsRUFBd0M7QUFDcENSLGNBQVEsQ0FBQ0MsY0FBVCxDQUF3Qk8sR0FBeEIsRUFBNkIvRSxXQUE3QixHQUEyQzhFLEdBQUcsQ0FBQ0MsR0FBRCxDQUE5QztBQUNIO0FBQ0o7QUFDSjs7QUFBQSxDOzs7Ozs7Ozs7Ozs7QUN4Q0Q7QUFBZTtBQUNYNUcsSUFBRSxFQUFDO0FBQUVDLFFBQUksRUFBQztBQUNGVSxXQUFLLEVBQUUsaUJBREw7QUFFRm1HLGNBQVEsRUFBRSxjQUZSO0FBR0ZDLGdCQUFVLEVBQUUsUUFIVjtBQUlGQyxlQUFTLEVBQUUsV0FKVDtBQUtGQyxnQkFBVSxFQUFFLG1CQUxWO0FBTUZDLGdCQUFVLEVBQUUsU0FOVjtBQU9GQyxnQkFBVSxFQUFFLGVBUFY7QUFRRkMsa0JBQVksRUFBRSxTQVJaO0FBU0ZDLHlCQUFtQixFQUFDLDBDQVRsQjtBQVVGQyxxQkFBZSxFQUFDO0FBVmQsS0FBUDtBQVlDekYsZUFBVyxFQUFDO0FBQ1IwRixXQUFLLEVBQUMsT0FERTtBQUVSQyxlQUFTLEVBQUMsVUFGRjtBQUdSQyxlQUFTLEVBQUMsa0JBSEY7QUFJUkMsV0FBSyxFQUFDLFFBSkU7QUFLUkMsV0FBSyxFQUFDLGNBTEU7QUFNUkMsYUFBTyxFQUFFO0FBTkQ7QUFaYixHQURRO0FBdUJYMUcsSUFBRSxFQUFDO0FBQUVqQixRQUFJLEVBQUU7QUFDSFUsV0FBSyxFQUFFLHdCQURKO0FBRUhtRyxjQUFRLEVBQUUsYUFGUDtBQUdIQyxnQkFBVSxFQUFFLFFBSFQ7QUFJSEMsZUFBUyxFQUFFLFNBSlI7QUFLSEMsZ0JBQVUsRUFBRSxxQkFMVDtBQU1IQyxnQkFBVSxFQUFFLFFBTlQ7QUFPSEMsZ0JBQVUsRUFBRSxtQkFQVDtBQVFIQyxrQkFBWSxFQUFFLGlCQVJYO0FBU0hDLHlCQUFtQixFQUFDLDJDQVRqQjtBQVVIQyxxQkFBZSxFQUFDO0FBVmIsS0FBUjtBQVlDekYsZUFBVyxFQUFDO0FBQ1IwRixXQUFLLEVBQUMsZUFERTtBQUVSQyxlQUFTLEVBQUMsZ0JBRkY7QUFHUkMsZUFBUyxFQUFDLGtCQUhGO0FBSVJDLFdBQUssRUFBQyxlQUpFO0FBS1JDLFdBQUssRUFBQywwQkFMRTtBQU1SQyxhQUFPLEVBQUU7QUFORDtBQVpiO0FBdkJRLENBQWYsRTs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxJQUFJQyxTQUFTLEdBQUdDLG1CQUFPLENBQUMscUVBQUQsQ0FBdkI7O0FBRUEsSUFBSTVCLHFCQUFxQixHQUFHRSxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsdUJBQXhCLENBQTVCOztBQUNBSCxxQkFBcUIsQ0FBQzZCLFFBQXRCLEdBQWlDLFlBQU07QUFBQ3JDLHNGQUFjLENBQUMsS0FBRCxDQUFkO0FBQXNCLENBQTlEOztBQUNBc0MsSUFBSTs7QUFFSixTQUFTQSxJQUFULEdBQWdCO0FBQ1osTUFBSXpCLGNBQWMsQ0FBQzBCLE9BQWYsQ0FBdUIsVUFBdkIsS0FBb0MsSUFBeEMsRUFBNkM7QUFDekMvQix5QkFBcUIsQ0FBQ0MsS0FBdEIsR0FBNEJJLGNBQWMsQ0FBQzBCLE9BQWYsQ0FBdUIsVUFBdkIsQ0FBNUI7QUFDQXZDLHdGQUFjLENBQUMsS0FBRCxDQUFkO0FBQ0gsR0FIRCxNQUdLO0FBQ0RBLHdGQUFjLENBQUMsS0FBRCxDQUFkO0FBQXVCO0FBQzlCOztBQUVELElBQUk0QixlQUFlLEdBQUdsQixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsaUJBQXhCLENBQXRCO0FBQ0FpQixlQUFlLENBQUNZLGdCQUFoQixDQUFpQyxPQUFqQyxFQUEwQ0MsYUFBMUM7QUFDQSxJQUFJQyxHQUFHLEdBQUcsRUFBVjs7QUFDQSxJQUFJQyxJQUFJLEdBQUcsU0FBUEEsSUFBTyxHQUFZO0FBQ25CLE9BQUtkLEtBQUwsR0FBYW5CLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixPQUF4QixFQUFpQ0YsS0FBakMsQ0FBdUNtQyxPQUF2QyxDQUErQyxLQUEvQyxFQUFzRCxFQUF0RCxDQUFiO0FBQ0EsT0FBS0MsUUFBTCxHQUFnQm5DLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ0YsS0FBckQ7QUFDQSxPQUFLc0IsU0FBTCxHQUFpQnJCLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ0YsS0FBdEQ7QUFDQSxPQUFLdUIsS0FBTCxHQUFhdEIsUUFBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLEVBQWlDRixLQUE5QztBQUNBLE9BQUt3QixLQUFMLEdBQWF2QixRQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsRUFBaUNGLEtBQWpDLENBQXVDbUMsT0FBdkMsQ0FBK0MsWUFBL0MsRUFBNkQsRUFBN0QsQ0FBYjtBQUNBLE9BQUtWLE9BQUwsR0FBZXhCLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixFQUFtQ0YsS0FBbEQ7QUFFSCxDQVJEOztBQVNBcUMsT0FBTyxDQUFDQyxHQUFSLENBQVlyQyxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsU0FBeEIsRUFBbUNGLEtBQS9DOztBQUNBLFNBQVNnQyxhQUFULEdBQXlCO0FBQ3JCLE1BQUlPLFlBQVksR0FBRyxJQUFJTCxJQUFKLEVBQW5CO0FBQ0EsTUFBSU0sT0FBTyxHQUFHdkMsUUFBUSxDQUFDQyxjQUFULENBQXdCLFNBQXhCLENBQWQ7O0FBQ0EsTUFBSXVDLFVBQVUsQ0FBQ0YsWUFBWSxDQUFDbkIsS0FBZCxDQUFWLElBQWtDc0IsU0FBUyxDQUFDSCxZQUFZLENBQUNILFFBQWQsRUFBd0JHLFlBQVksQ0FBQ2pCLFNBQXJDLENBQTNDLElBQThGcUIsVUFBVSxDQUFDSixZQUFZLENBQUNoQixLQUFkLENBQXhHLElBQWdJcUIsVUFBVSxDQUFDTCxZQUFZLENBQUNmLEtBQWQsRUFBcUIsS0FBckIsQ0FBOUksRUFBMks7QUFDdkssV0FBT2UsWUFBWSxDQUFDakIsU0FBcEI7QUFDQUgsbUJBQWUsQ0FBQzBCLFFBQWhCLEdBQXlCLElBQXpCO0FBQ0EsUUFBSUMsR0FBRyxHQUFHLElBQUlDLGNBQUosRUFBVjtBQUNBRCxPQUFHLENBQUNFLElBQUosQ0FBUyxNQUFULFlBQW9CckosNERBQXBCLFNBQW9DK0gsU0FBUyxDQUFDL0ksT0FBVixDQUFrQkUsWUFBdEQ7QUFDQWlLLE9BQUcsQ0FBQ0csZ0JBQUosQ0FBcUIsY0FBckIsRUFBcUMsa0JBQXJDO0FBQ0FILE9BQUcsQ0FBQ0ksSUFBSixDQUFTQyxJQUFJLENBQUNDLFNBQUwsQ0FBZWIsWUFBZixDQUFUOztBQUNBTyxPQUFHLENBQUNPLGtCQUFKLEdBQXlCLFlBQVk7QUFDakMsVUFBSVAsR0FBRyxDQUFDUSxNQUFKLEtBQWUsR0FBbkIsRUFBd0I7QUFDcEIsWUFBSXZELHFCQUFxQixDQUFDQyxLQUF0QixLQUFnQyxJQUFwQyxFQUEwQztBQUN0Q2lDLGFBQUcsR0FBRyw0QkFBTjtBQUNILFNBRkQsTUFFTztBQUNIQSxhQUFHLEdBQUcsMkJBQU47QUFDSDs7QUFDRGhDLGdCQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NRLFNBQXRDLEdBQWtEdUIsR0FBbEQ7QUFDQWhDLGdCQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsRUFBaUNxRCxLQUFqQyxHQUF5QywwQkFBMEIseUJBQW5FO0FBQ0FwQyx1QkFBZSxDQUFDMEIsUUFBaEIsR0FBeUIsS0FBekI7QUFDSCxPQVRELE1BU08sSUFBR0MsR0FBRyxDQUFDUSxNQUFKLEtBQWUsR0FBbEIsRUFBc0I7QUFDekJkLGVBQU8sQ0FBQzlCLFNBQVIsR0FBb0IscUJBQXBCO0FBQ0E4QyxrQkFBVSxDQUFDO0FBQUEsaUJBQUt2RCxRQUFRLENBQUN3RCxRQUFULENBQWtCQyxJQUFsQixhQUE0QjlKLCtEQUE1QixnQkFBTDtBQUFBLFNBQUQsRUFBaUUsSUFBakUsQ0FBVjtBQUNIO0FBQ0osS0FkRDtBQWVDO0FBQ1I7O0FBRUQsU0FBUzZJLFVBQVQsQ0FBb0JyQixLQUFwQixFQUEyQjtBQUN2QixNQUFJQSxLQUFLLENBQUN1QyxNQUFOLEdBQWUsQ0FBZixJQUFvQixDQUFDQyxLQUFLLENBQUNDLE1BQU0sQ0FBQ3pDLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FBUCxDQUExQixJQUFnREEsS0FBSyxDQUFDMEMsS0FBTixDQUFZLHFCQUFaLENBQWhELElBQXNGMUMsS0FBSyxDQUFDdUMsTUFBTixHQUFlLEVBQXpHLEVBQTZHO0FBQzFHLFFBQUc1RCxxQkFBcUIsQ0FBQ0MsS0FBdEIsS0FBOEIsSUFBakMsRUFBc0M7QUFBQ2lDLFNBQUcsR0FBRyxvQkFBTjtBQUEyQixLQUFsRSxNQUNBO0FBQUNBLFNBQUcsR0FBQyxpQkFBSjtBQUFzQjs7QUFDdEJoQyxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NRLFNBQXRDLEdBQWdEdUIsR0FBaEQ7QUFDQWhDLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixPQUF4QixFQUFpQ3FELEtBQWpDLEdBQXVDLDBCQUF5Qix5QkFBaEU7QUFDQSxXQUFPLEtBQVA7QUFFSDs7QUFDRHRELFVBQVEsQ0FBQ0MsY0FBVCxDQUF3QixZQUF4QixFQUFzQ1EsU0FBdEMsR0FBZ0QsRUFBaEQ7QUFDQVQsVUFBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLEVBQWlDcUQsS0FBakMsR0FBdUMsTUFBdkM7QUFDQSxTQUFPLElBQVA7QUFDSDs7QUFFRCxTQUFTYixTQUFULENBQW1CcUIsS0FBbkIsRUFBMEJDLEtBQTFCLEVBQWlDO0FBQzdCLE1BQUlELEtBQUssQ0FBQ0osTUFBTixHQUFlLENBQWYsSUFBb0JJLEtBQUssQ0FBQ0osTUFBTixHQUFlLEVBQXZDLEVBQTJDO0FBQ3ZDLFFBQUc1RCxxQkFBcUIsQ0FBQ0MsS0FBdEIsS0FBOEIsSUFBakMsRUFBc0M7QUFBQ2lDLFNBQUcsR0FBRyxxQkFBTjtBQUE0QixLQUFuRSxNQUNBO0FBQUNBLFNBQUcsR0FBQyxvQkFBSjtBQUF5Qjs7QUFDMUJoQyxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsZ0JBQXhCLEVBQTBDUSxTQUExQyxHQUFvRHVCLEdBQXBEO0FBQ0FoQyxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsV0FBeEIsRUFBcUNxRCxLQUFyQyxHQUEyQywwQkFBeUIseUJBQXBFO0FBQ0EsV0FBTyxLQUFQO0FBQ0gsR0FORCxNQU1PLElBQUdRLEtBQUssS0FBS0MsS0FBYixFQUFtQjtBQUN0QixRQUFHakUscUJBQXFCLENBQUNDLEtBQXRCLEtBQThCLElBQWpDLEVBQXNDO0FBQUNpQyxTQUFHLEdBQUcscUJBQU47QUFBNEIsS0FBbkUsTUFDQTtBQUFDQSxTQUFHLEdBQUMsd0JBQUo7QUFBNkI7O0FBQzlCaEMsWUFBUSxDQUFDQyxjQUFULENBQXdCLGdCQUF4QixFQUEwQ1EsU0FBMUMsR0FBb0R1QixHQUFwRDtBQUNBaEMsWUFBUSxDQUFDQyxjQUFULENBQXdCLFdBQXhCLEVBQXFDcUQsS0FBckMsR0FBMkMsMEJBQXlCLHlCQUFwRTtBQUNBLFdBQU8sS0FBUDtBQUNIOztBQUNEdEQsVUFBUSxDQUFDQyxjQUFULENBQXdCLFdBQXhCLEVBQXFDcUQsS0FBckMsR0FBMkMsTUFBM0M7QUFDQXRELFVBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ3FELEtBQXJDLEdBQTJDLE1BQTNDO0FBQ0F0RCxVQUFRLENBQUNDLGNBQVQsQ0FBd0IsZ0JBQXhCLEVBQTBDUSxTQUExQyxHQUFzRCxFQUF0RDtBQUNBVCxVQUFRLENBQUNDLGNBQVQsQ0FBd0IsZ0JBQXhCLEVBQTBDUSxTQUExQyxHQUFzRCxFQUF0RDtBQUNBLFNBQU8sSUFBUDtBQUNIOztBQUVELFNBQVNrQyxVQUFULENBQW9CcEIsS0FBcEIsRUFBMkJ5QyxXQUEzQixFQUF3QztBQUNwQyxNQUFJekMsS0FBSyxDQUFDbUMsTUFBTixLQUFpQixFQUFqQixJQUF1Qm5DLEtBQUssQ0FBQzBDLFNBQU4sQ0FBZ0IsQ0FBaEIsRUFBbUIsQ0FBbkIsS0FBeUJELFdBQXBELEVBQWlFO0FBQzdELFFBQUdsRSxxQkFBcUIsQ0FBQ0MsS0FBdEIsS0FBOEIsSUFBakMsRUFBc0M7QUFBQ2lDLFNBQUcsR0FBRyxzQkFBTjtBQUE2QixLQUFwRSxNQUNBO0FBQUNBLFNBQUcsR0FBQyxpQkFBSjtBQUFzQjs7QUFDdkJoQyxZQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NRLFNBQXRDLEdBQWdEdUIsR0FBaEQ7QUFDQWhDLFlBQVEsQ0FBQ0MsY0FBVCxDQUF3QixPQUF4QixFQUFpQ3FELEtBQWpDLEdBQXVDLDBCQUF5Qix5QkFBaEU7QUFDQSxXQUFPLEtBQVA7QUFDSDs7QUFDRHRELFVBQVEsQ0FBQ0MsY0FBVCxDQUF3QixPQUF4QixFQUFpQ3FELEtBQWpDLEdBQXVDLE1BQXZDO0FBQ0F0RCxVQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NRLFNBQXRDLEdBQWdELEVBQWhEO0FBQ0EsU0FBTyxJQUFQO0FBQ0g7O0FBQ0QsU0FBU2lDLFVBQVQsQ0FBb0JwQixLQUFwQixFQUEyQjtBQUN2QixNQUFJQSxLQUFLLENBQUM0QyxRQUFOLENBQWUsR0FBZixDQUFKLEVBQXlCO0FBQ3JCLFFBQUlDLEtBQUssR0FBRzdDLEtBQUssQ0FBQzJDLFNBQU4sQ0FBZ0IzQyxLQUFLLENBQUM4QyxPQUFOLENBQWMsR0FBZCxDQUFoQixFQUFvQzlDLEtBQUssQ0FBQ29DLE1BQTFDLENBQVo7O0FBQ0EsUUFBSVMsS0FBSyxDQUFDVCxNQUFOLEdBQWUsQ0FBbkIsRUFBc0I7QUFDbEIxRCxjQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NRLFNBQXRDLEdBQWdELEVBQWhEO0FBQ0FULGNBQVEsQ0FBQ0MsY0FBVCxDQUF3QixPQUF4QixFQUFpQ3FELEtBQWpDLEdBQXVDLE1BQXZDO0FBQ0EsYUFBTyxJQUFQO0FBQ0g7QUFDSjs7QUFDRCxNQUFHeEQscUJBQXFCLENBQUNDLEtBQXRCLEtBQThCLElBQWpDLEVBQXNDO0FBQUNpQyxPQUFHLEdBQUcscUJBQU47QUFBNEIsR0FBbkUsTUFDQTtBQUFDQSxPQUFHLEdBQUMsaUJBQUo7QUFBc0I7O0FBQ3ZCaEMsVUFBUSxDQUFDQyxjQUFULENBQXdCLFlBQXhCLEVBQXNDUSxTQUF0QyxHQUFnRHVCLEdBQWhEO0FBQ0FoQyxVQUFRLENBQUNDLGNBQVQsQ0FBd0IsT0FBeEIsRUFBaUNxRCxLQUFqQyxHQUF1QywwQkFBeUIseUJBQWhFO0FBQ0EsU0FBTyxLQUFQO0FBQ0g7O0FBQ0QsSUFBSWUsUUFBUSxHQUFHckUsUUFBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLENBQWY7QUFDQW9FLFFBQVEsQ0FBQ3ZDLGdCQUFULENBQTBCLE9BQTFCLEVBQWtDLFlBQU07QUFBQzlCLFVBQVEsQ0FBQ3dELFFBQVQsQ0FBa0JDLElBQWxCLGFBQTRCOUosK0RBQTVCO0FBQTBELENBQW5HO0FBQ0EsSUFBSTJLLFFBQVEsR0FBRXRFLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixVQUF4QixDQUFkO0FBQ0FxRSxRQUFRLENBQUN4QyxnQkFBVCxDQUEwQixPQUExQixFQUFrQyxZQUFNO0FBQUM5QixVQUFRLENBQUN3RCxRQUFULENBQWtCQyxJQUFsQixhQUE0QjlKLCtEQUE1QjtBQUEwRCxDQUFuRzs7Ozs7Ozs7Ozs7O0FDNUhBLHVDOzs7Ozs7Ozs7OztBQ0FBLHVDIiwiZmlsZSI6InJlZ2lzdHJhdGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL3NjcmlwdC9yZWdpc3RyYXRpb25WYWxpZGF0aW9uLmpzXCIpO1xuIiwiLy8gc2hpbSBmb3IgdXNpbmcgcHJvY2VzcyBpbiBicm93c2VyXG52YXIgcHJvY2VzcyA9IG1vZHVsZS5leHBvcnRzID0ge307XG5cbi8vIGNhY2hlZCBmcm9tIHdoYXRldmVyIGdsb2JhbCBpcyBwcmVzZW50IHNvIHRoYXQgdGVzdCBydW5uZXJzIHRoYXQgc3R1YiBpdFxuLy8gZG9uJ3QgYnJlYWsgdGhpbmdzLiAgQnV0IHdlIG5lZWQgdG8gd3JhcCBpdCBpbiBhIHRyeSBjYXRjaCBpbiBjYXNlIGl0IGlzXG4vLyB3cmFwcGVkIGluIHN0cmljdCBtb2RlIGNvZGUgd2hpY2ggZG9lc24ndCBkZWZpbmUgYW55IGdsb2JhbHMuICBJdCdzIGluc2lkZSBhXG4vLyBmdW5jdGlvbiBiZWNhdXNlIHRyeS9jYXRjaGVzIGRlb3B0aW1pemUgaW4gY2VydGFpbiBlbmdpbmVzLlxuXG52YXIgY2FjaGVkU2V0VGltZW91dDtcbnZhciBjYWNoZWRDbGVhclRpbWVvdXQ7XG5cbmZ1bmN0aW9uIGRlZmF1bHRTZXRUaW1vdXQoKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdzZXRUaW1lb3V0IGhhcyBub3QgYmVlbiBkZWZpbmVkJyk7XG59XG5mdW5jdGlvbiBkZWZhdWx0Q2xlYXJUaW1lb3V0ICgpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ2NsZWFyVGltZW91dCBoYXMgbm90IGJlZW4gZGVmaW5lZCcpO1xufVxuKGZ1bmN0aW9uICgpIHtcbiAgICB0cnkge1xuICAgICAgICBpZiAodHlwZW9mIHNldFRpbWVvdXQgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBzZXRUaW1lb3V0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IGRlZmF1bHRTZXRUaW1vdXQ7XG4gICAgICAgIH1cbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBkZWZhdWx0U2V0VGltb3V0O1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICBpZiAodHlwZW9mIGNsZWFyVGltZW91dCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gY2xlYXJUaW1lb3V0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gZGVmYXVsdENsZWFyVGltZW91dDtcbiAgICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gZGVmYXVsdENsZWFyVGltZW91dDtcbiAgICB9XG59ICgpKVxuZnVuY3Rpb24gcnVuVGltZW91dChmdW4pIHtcbiAgICBpZiAoY2FjaGVkU2V0VGltZW91dCA9PT0gc2V0VGltZW91dCkge1xuICAgICAgICAvL25vcm1hbCBlbnZpcm9tZW50cyBpbiBzYW5lIHNpdHVhdGlvbnNcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9XG4gICAgLy8gaWYgc2V0VGltZW91dCB3YXNuJ3QgYXZhaWxhYmxlIGJ1dCB3YXMgbGF0dGVyIGRlZmluZWRcbiAgICBpZiAoKGNhY2hlZFNldFRpbWVvdXQgPT09IGRlZmF1bHRTZXRUaW1vdXQgfHwgIWNhY2hlZFNldFRpbWVvdXQpICYmIHNldFRpbWVvdXQpIHtcbiAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IHNldFRpbWVvdXQ7XG4gICAgICAgIHJldHVybiBzZXRUaW1lb3V0KGZ1biwgMCk7XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIC8vIHdoZW4gd2hlbiBzb21lYm9keSBoYXMgc2NyZXdlZCB3aXRoIHNldFRpbWVvdXQgYnV0IG5vIEkuRS4gbWFkZG5lc3NcbiAgICAgICAgcmV0dXJuIGNhY2hlZFNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9IGNhdGNoKGUpe1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgLy8gV2hlbiB3ZSBhcmUgaW4gSS5FLiBidXQgdGhlIHNjcmlwdCBoYXMgYmVlbiBldmFsZWQgc28gSS5FLiBkb2Vzbid0IHRydXN0IHRoZSBnbG9iYWwgb2JqZWN0IHdoZW4gY2FsbGVkIG5vcm1hbGx5XG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKG51bGwsIGZ1biwgMCk7XG4gICAgICAgIH0gY2F0Y2goZSl7XG4gICAgICAgICAgICAvLyBzYW1lIGFzIGFib3ZlIGJ1dCB3aGVuIGl0J3MgYSB2ZXJzaW9uIG9mIEkuRS4gdGhhdCBtdXN0IGhhdmUgdGhlIGdsb2JhbCBvYmplY3QgZm9yICd0aGlzJywgaG9wZnVsbHkgb3VyIGNvbnRleHQgY29ycmVjdCBvdGhlcndpc2UgaXQgd2lsbCB0aHJvdyBhIGdsb2JhbCBlcnJvclxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZFNldFRpbWVvdXQuY2FsbCh0aGlzLCBmdW4sIDApO1xuICAgICAgICB9XG4gICAgfVxuXG5cbn1cbmZ1bmN0aW9uIHJ1bkNsZWFyVGltZW91dChtYXJrZXIpIHtcbiAgICBpZiAoY2FjaGVkQ2xlYXJUaW1lb3V0ID09PSBjbGVhclRpbWVvdXQpIHtcbiAgICAgICAgLy9ub3JtYWwgZW52aXJvbWVudHMgaW4gc2FuZSBzaXR1YXRpb25zXG4gICAgICAgIHJldHVybiBjbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9XG4gICAgLy8gaWYgY2xlYXJUaW1lb3V0IHdhc24ndCBhdmFpbGFibGUgYnV0IHdhcyBsYXR0ZXIgZGVmaW5lZFxuICAgIGlmICgoY2FjaGVkQ2xlYXJUaW1lb3V0ID09PSBkZWZhdWx0Q2xlYXJUaW1lb3V0IHx8ICFjYWNoZWRDbGVhclRpbWVvdXQpICYmIGNsZWFyVGltZW91dCkge1xuICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBjbGVhclRpbWVvdXQ7XG4gICAgICAgIHJldHVybiBjbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gd2hlbiB3aGVuIHNvbWVib2R5IGhhcyBzY3Jld2VkIHdpdGggc2V0VGltZW91dCBidXQgbm8gSS5FLiBtYWRkbmVzc1xuICAgICAgICByZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0KG1hcmtlcik7XG4gICAgfSBjYXRjaCAoZSl7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICAvLyBXaGVuIHdlIGFyZSBpbiBJLkUuIGJ1dCB0aGUgc2NyaXB0IGhhcyBiZWVuIGV2YWxlZCBzbyBJLkUuIGRvZXNuJ3QgIHRydXN0IHRoZSBnbG9iYWwgb2JqZWN0IHdoZW4gY2FsbGVkIG5vcm1hbGx5XG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0LmNhbGwobnVsbCwgbWFya2VyKTtcbiAgICAgICAgfSBjYXRjaCAoZSl7XG4gICAgICAgICAgICAvLyBzYW1lIGFzIGFib3ZlIGJ1dCB3aGVuIGl0J3MgYSB2ZXJzaW9uIG9mIEkuRS4gdGhhdCBtdXN0IGhhdmUgdGhlIGdsb2JhbCBvYmplY3QgZm9yICd0aGlzJywgaG9wZnVsbHkgb3VyIGNvbnRleHQgY29ycmVjdCBvdGhlcndpc2UgaXQgd2lsbCB0aHJvdyBhIGdsb2JhbCBlcnJvci5cbiAgICAgICAgICAgIC8vIFNvbWUgdmVyc2lvbnMgb2YgSS5FLiBoYXZlIGRpZmZlcmVudCBydWxlcyBmb3IgY2xlYXJUaW1lb3V0IHZzIHNldFRpbWVvdXRcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQuY2FsbCh0aGlzLCBtYXJrZXIpO1xuICAgICAgICB9XG4gICAgfVxuXG5cblxufVxudmFyIHF1ZXVlID0gW107XG52YXIgZHJhaW5pbmcgPSBmYWxzZTtcbnZhciBjdXJyZW50UXVldWU7XG52YXIgcXVldWVJbmRleCA9IC0xO1xuXG5mdW5jdGlvbiBjbGVhblVwTmV4dFRpY2soKSB7XG4gICAgaWYgKCFkcmFpbmluZyB8fCAhY3VycmVudFF1ZXVlKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgZHJhaW5pbmcgPSBmYWxzZTtcbiAgICBpZiAoY3VycmVudFF1ZXVlLmxlbmd0aCkge1xuICAgICAgICBxdWV1ZSA9IGN1cnJlbnRRdWV1ZS5jb25jYXQocXVldWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHF1ZXVlSW5kZXggPSAtMTtcbiAgICB9XG4gICAgaWYgKHF1ZXVlLmxlbmd0aCkge1xuICAgICAgICBkcmFpblF1ZXVlKCk7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBkcmFpblF1ZXVlKCkge1xuICAgIGlmIChkcmFpbmluZykge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIHZhciB0aW1lb3V0ID0gcnVuVGltZW91dChjbGVhblVwTmV4dFRpY2spO1xuICAgIGRyYWluaW5nID0gdHJ1ZTtcblxuICAgIHZhciBsZW4gPSBxdWV1ZS5sZW5ndGg7XG4gICAgd2hpbGUobGVuKSB7XG4gICAgICAgIGN1cnJlbnRRdWV1ZSA9IHF1ZXVlO1xuICAgICAgICBxdWV1ZSA9IFtdO1xuICAgICAgICB3aGlsZSAoKytxdWV1ZUluZGV4IDwgbGVuKSB7XG4gICAgICAgICAgICBpZiAoY3VycmVudFF1ZXVlKSB7XG4gICAgICAgICAgICAgICAgY3VycmVudFF1ZXVlW3F1ZXVlSW5kZXhdLnJ1bigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHF1ZXVlSW5kZXggPSAtMTtcbiAgICAgICAgbGVuID0gcXVldWUubGVuZ3RoO1xuICAgIH1cbiAgICBjdXJyZW50UXVldWUgPSBudWxsO1xuICAgIGRyYWluaW5nID0gZmFsc2U7XG4gICAgcnVuQ2xlYXJUaW1lb3V0KHRpbWVvdXQpO1xufVxuXG5wcm9jZXNzLm5leHRUaWNrID0gZnVuY3Rpb24gKGZ1bikge1xuICAgIHZhciBhcmdzID0gbmV3IEFycmF5KGFyZ3VtZW50cy5sZW5ndGggLSAxKTtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGFyZ3NbaSAtIDFdID0gYXJndW1lbnRzW2ldO1xuICAgICAgICB9XG4gICAgfVxuICAgIHF1ZXVlLnB1c2gobmV3IEl0ZW0oZnVuLCBhcmdzKSk7XG4gICAgaWYgKHF1ZXVlLmxlbmd0aCA9PT0gMSAmJiAhZHJhaW5pbmcpIHtcbiAgICAgICAgcnVuVGltZW91dChkcmFpblF1ZXVlKTtcbiAgICB9XG59O1xuXG4vLyB2OCBsaWtlcyBwcmVkaWN0aWJsZSBvYmplY3RzXG5mdW5jdGlvbiBJdGVtKGZ1biwgYXJyYXkpIHtcbiAgICB0aGlzLmZ1biA9IGZ1bjtcbiAgICB0aGlzLmFycmF5ID0gYXJyYXk7XG59XG5JdGVtLnByb3RvdHlwZS5ydW4gPSBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5mdW4uYXBwbHkobnVsbCwgdGhpcy5hcnJheSk7XG59O1xucHJvY2Vzcy50aXRsZSA9ICdicm93c2VyJztcbnByb2Nlc3MuYnJvd3NlciA9IHRydWU7XG5wcm9jZXNzLmVudiA9IHt9O1xucHJvY2Vzcy5hcmd2ID0gW107XG5wcm9jZXNzLnZlcnNpb24gPSAnJzsgLy8gZW1wdHkgc3RyaW5nIHRvIGF2b2lkIHJlZ2V4cCBpc3N1ZXNcbnByb2Nlc3MudmVyc2lvbnMgPSB7fTtcblxuZnVuY3Rpb24gbm9vcCgpIHt9XG5cbnByb2Nlc3Mub24gPSBub29wO1xucHJvY2Vzcy5hZGRMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLm9uY2UgPSBub29wO1xucHJvY2Vzcy5vZmYgPSBub29wO1xucHJvY2Vzcy5yZW1vdmVMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUFsbExpc3RlbmVycyA9IG5vb3A7XG5wcm9jZXNzLmVtaXQgPSBub29wO1xucHJvY2Vzcy5wcmVwZW5kTGlzdGVuZXIgPSBub29wO1xucHJvY2Vzcy5wcmVwZW5kT25jZUxpc3RlbmVyID0gbm9vcDtcblxucHJvY2Vzcy5saXN0ZW5lcnMgPSBmdW5jdGlvbiAobmFtZSkgeyByZXR1cm4gW10gfVxuXG5wcm9jZXNzLmJpbmRpbmcgPSBmdW5jdGlvbiAobmFtZSkge1xuICAgIHRocm93IG5ldyBFcnJvcigncHJvY2Vzcy5iaW5kaW5nIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5cbnByb2Nlc3MuY3dkID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gJy8nIH07XG5wcm9jZXNzLmNoZGlyID0gZnVuY3Rpb24gKGRpcikge1xuICAgIHRocm93IG5ldyBFcnJvcigncHJvY2Vzcy5jaGRpciBpcyBub3Qgc3VwcG9ydGVkJyk7XG59O1xucHJvY2Vzcy51bWFzayA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gMDsgfTtcbiIsIm1vZHVsZS5leHBvcnRzID0ge1xuICBwb3J0OiBwcm9jZXNzLmVudi5QT1JUIHx8IDMwMDAsXG4gIHJvdXRpbmc6IHtcbiAgICBhdXRob3JpemF0aW9uOiAnL2F1dGhvcml6YXRpb24nLFxuICAgIHJlZ2lzdHJhdGlvbjogJy9yZWdpc3RyYXRpb24nLFxuICAgIGdyb3VwU3R1ZGVudDogJy9ncm91cFN0dWRlbnQnLFxuICAgIGdldEFsbEdyb3VwczogJy9nZXRBbGxHcm91cHMnLFxuICAgIGFjY291bnRTZXR0aW5nOiAnL2FjY291bnRTZXR0aW5nJyxcbiAgICBncm91cHM6ICcvZ3JvdXBzJyxcbiAgICB0YWJsZTogJy8nLFxuICAgIHVwZGF0ZTogJy91cGRhdGUnLFxuICAgIGRlbGV0ZTogJy9kZWxldGUnLFxuICAgIGFjY291bnRVcGRhdGU6ICcvYWNjb3VudC11cGRhdGUnLFxuICAgIGZvcmdvdHRlblBhc3N3b3JkOiAnL2ZvcmdvdHRlbi1wYXNzd29yZCcsXG4gICAgZGVsZXRlR3JvdXA6ICcvZGVsZXRlLWdyb3VwJyxcbiAgICBzdHVkZW50Q2xlYXI6ICcvc3R1ZGVudC1jbGVhcicsXG4gICAgdXBkYXRlR3JvdXA6ICcvdXBkYXRlLWdyb3VwJyxcbiAgICBzZW5kSW1hZ2U6ICcvc2VuZC1pbWFnZScsXG4gICAgcmVzZXRTZXR0aW5nczogJy9yZXNldC1zZXR0aW5ncydcblxuICB9XG59O1xuIiwiXG5cblxuXG5jb25zdCBsb2NhbGhvc3RTZXJ2ID0gJ2h0dHA6Ly8zNS4yMDUuMTQ4LjIzMC9zZXJ2ZXIvJztcbmNvbnN0IHdlYnBhY2tMb2NhbEhvc3QgPSAnaHR0cDovLzM1LjIwNS4xNDguMjMwLyc7XG5cbi8vXG4vLyBjb25zdCBsb2NhbGhvc3RTZXJ2ID0gXCJodHRwOi8vbG9jYWxob3N0OjMwMDBcIjtcbi8vIGNvbnN0IHdlYnBhY2tMb2NhbEhvc3QgPSBcImh0dHA6Ly9sb2NhbGhvc3Q6OTAwMFwiO1xuXG5cblxuXG5leHBvcnQge1xuICAgIGxvY2FsaG9zdFNlcnYsXG4gICAgd2VicGFja0xvY2FsSG9zdCxcblxufTsiLCJleHBvcnQgZGVmYXVsdHtcbiAgICBlbjp7IHRleHQ6e1xuICAgICAgICAgICAgbXlBY2NvdW50OiBcIk15IEFjY291bnRcIixcbiAgICAgICAgICAgIGljb25UaXRsZTogXCJDaGFuZ2UgeW91ciBhdmF0YXJcIixcbiAgICAgICAgICAgIGxvZ2luVGl0bGU6IFwiTG9naW5cIixcbiAgICAgICAgICAgIHBhc3N3b3JkMVRpdGxlOiBcIlBhc3N3b3JkXCIsXG4gICAgICAgICAgICBwYXNzd29yZDJUaXRsZTogXCJDaGVjayBwYXNzd29yZFwiLFxuICAgICAgICAgICAgZW1haWxUaXRsZTogXCJFbWFpbFwiLFxuICAgICAgICAgICAgcGhvbmVUaXRsZTogXCJQaG9uZVwiLFxuICAgICAgICAgICAgYWJvdXRNeXNlbGZUaXRsZTogXCJBYm91dCBNeXNlbGZcIixcbiAgICAgICAgICAgIFNhdmVCdG46IFwiU2F2ZSBjaGFuZ2VzXCIsXG4gICAgICAgICAgICBjbG9zZTogXCJDbG9zZVwiLFxuICAgICAgICAgICAgY2hhdDpcIkNoYXRcIixcbiAgICAgICAgICAgIGNoYXRXcmFwcGVyOlwiQ29taW5nIHNvb25cIixcbiAgICAgICAgICAgIGdhbWVCb3VuY2U6XCJCb3VuY2VcIixcbiAgICAgICAgICAgIHN0YXJ0OlwiU3RhcnQgZ2FtZVwiLFxuICAgICAgICAgICAgY2xlYXI6XCJDbGVhclwiLFxuICAgICAgICAgICAgY2hhbmdlSW5wdXRzOlwi0KFoYW5nZVwiLFxuICAgICAgICB9XG4gICAgICB9LFxuICAgIHJ1OnsgdGV4dDoge1xuICAgICAgICAgICAgbXlBY2NvdW50OiBcItCc0L7QuSDQsNC60LrQsNGD0L3RglwiLFxuICAgICAgICAgICAgaWNvblRpdGxlOiBcItCY0LfQvNC10L3QuNGC0Ywg0YHQstC+0Lkg0LDQstCw0YLQsNGAXCIsXG4gICAgICAgICAgICBsb2dpblRpdGxlOiBcItCb0L7Qs9C40L1cIixcbiAgICAgICAgICAgIHBhc3N3b3JkMVRpdGxlOiBcItCf0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgcGFzc3dvcmQyVGl0bGU6IFwi0J/RgNC+0LLQtdGA0LrQsCDQv9Cw0YDQvtC70Y9cIixcbiAgICAgICAgICAgIGVtYWlsVGl0bGU6IFwi0LXQvNC10LnQu1wiLFxuICAgICAgICAgICAgcGhvbmVUaXRsZTogXCLQotC10LvQtdGE0L7QvVwiLFxuICAgICAgICAgICAgYWJvdXRNeXNlbGZUaXRsZTogXCLQntCx0L4g0LzQvdC1XCIsXG4gICAgICAgICAgICBTYXZlQnRuOiBcItCh0L7RhdGA0LDQvdC40YLRjCDQuNC30LzQtdC90LXQvdC40Y9cIixcbiAgICAgICAgICAgIGNsb3NlOiBcItCX0LDQutGA0YvRgtGMXCIsXG4gICAgICAgICAgICBjaGF0Olwi0KfQsNGCXCIsXG4gICAgICAgICAgICBjaGF0V3JhcHBlcjpcItCSINGA0LDQt9GA0LDQsdC+0YLQutC1KVwiLFxuICAgICAgICAgICAgZ2FtZUJvdW5jZTpcItCY0LPRgNCwINCo0LDRgNC40LrQuFwiLFxuICAgICAgICAgICAgc3RhcnQ6XCLQndCw0YfQsNGC0Ywg0LjQs9GA0YNcIixcbiAgICAgICAgICAgIGNsZWFyOlwi0J7Rh9C40YHRgtC40YLRjFwiLFxuICAgICAgICAgICAgY2hhbmdlSW5wdXRzOlwi0JjQt9C80LXQvdC40YLRjFwiLFxuICAgICAgICB9XG4gICAgfVxufTsiLCJleHBvcnQgZGVmYXVsdCB7XG4gICAgZW46eyB0ZXh0OntcbiAgICAgICAgICAgIHRleHRBdXRob3JpemF0aW9uOlwiQXV0aG9yaXphdGlvblwiLFxuICAgICAgICAgICAgbGFiZWxMb2dpbjpcIkxvZ2luOlwiLFxuICAgICAgICAgICAgbGFiZWxQYXNzd29yZDpcIlBhc3N3b3JkOlwiLFxuICAgICAgICAgICAgcmVnaXN0cmF0aW9uQnRuQXZ0OlwiUmVnaXN0cmF0aW9uXCIsXG4gICAgICAgICAgICBsb2dJbkJ0bjpcIkxvZyBJblwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc3dvcmQ6XCJGb3Jnb3R0ZW4gUGFzc3dvcmRcIixcbiAgICAgICAgICAgIHRleHRGb3Jnb3R0ZW5QYXNzd29yZDpcIlBhc3N3b3JkIHJlY292ZXJ5XCIsXG4gICAgICAgICAgICBmb3Jnb3R0ZW5MYWJlbExvZ2luOlwiTG9naW46XCIsXG4gICAgICAgICAgICBmb3Jnb3R0ZW5MYWJlbFBhc3N3b3JkOlwiS2V5d29yZDpcIixcbiAgICAgICAgICAgIGZvcmdvdHRlblBhc3NMYWJlbDpcIllvdXIgcGFzc3dvcmQ6XCIsXG4gICAgICAgICAgICBnZXRQYXNzd29yZDpcIkdldCBQYXNzd29yZFwiLFxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBsb2dpbkF2dDpcIndyaXRlIHlvdXIgbG9naW5cIixcbiAgICAgICAgICAgIHBhc3N3b3JkQXZ0Olwid3JpdGUgeW91ciBwYXNzd29yZFwiLFxuICAgICAgICAgICAgZm9yZ290dGVuTG9naW5BdnQ6XCJ3cml0ZSB5b3VyIGxvZ2luIFwiLFxuICAgICAgICAgICAga2F5V29yZEF2dDpcIndyaXRlIHlvdXIga2V5d29yZFwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc3dvcmRNZXNzYWdlOlwiWW91ciBwYXNzd29yZFwiXG4gICAgICAgIH1cblxuICAgIH0sXG4gICAgcnU6eyB0ZXh0OiB7XG4gICAgICAgICAgICB0ZXh0QXV0aG9yaXphdGlvbjpcItCQ0LLRgtC+0YDQuNC30LDRhtC40Y9cIixcbiAgICAgICAgICAgIGxhYmVsTG9naW46XCLQm9C+0LPQuNC9OlwiLFxuICAgICAgICAgICAgbGFiZWxQYXNzd29yZDpcItCf0LDRgNC+0LvRjDpcIixcbiAgICAgICAgICAgIHJlZ2lzdHJhdGlvbkJ0bkF2dDpcItCg0LXQs9C40YHRgtGA0LDRhtC40Y9cIixcbiAgICAgICAgICAgIGxvZ0luQnRuOlwi0JLQvtC50YLQuFwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc3dvcmQ6XCLQl9Cw0LHRi9C7INC/0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgdGV4dEZvcmdvdHRlblBhc3N3b3JkOlwi0JLQvtGB0YHRgtCw0L3QvtCy0LvQtdC90LjQtSDQv9Cw0YDQvtC70Y9cIixcbiAgICAgICAgICAgIGZvcmdvdHRlbkxhYmVsTG9naW46XCLQm9C+0LPQuNC9OlwiLFxuICAgICAgICAgICAgZm9yZ290dGVuTGFiZWxQYXNzd29yZDpcItCh0LXQutGA0LXRgtC90L7QtSDRgdC70L7QstC+OlwiLFxuICAgICAgICAgICAgZm9yZ290dGVuUGFzc0xhYmVsOlwi0JLQsNGIINC/0LDRgNC+0LvRjDpcIixcbiAgICAgICAgICAgIGdldFBhc3N3b3JkOlwi0J/QvtC70YPRh9C40YLRjCDQv9Cw0YDQvtC70YxcIixcbiAgICAgICAgfSxcbiAgICAgICAgcGxhY2Vob2xkZXI6e1xuICAgICAgICAgICAgbG9naW5BdnQ6XCLQktCy0LXQtNC40YLQtSDQstCw0Ygg0LvQvtCz0LjQvVwiLFxuICAgICAgICAgICAgcGFzc3dvcmRBdnQ6XCLQktCy0LXQtNC40YLQtSDQstCw0Ygg0L/QsNGA0L7Qu9GMXCIsXG4gICAgICAgICAgICBmb3Jnb3R0ZW5Mb2dpbkF2dDpcItCS0LLQtdC00LjRgtC1INCy0LDRiCDQu9C+0LPQuNC9XCIsXG4gICAgICAgICAgICBrYXlXb3JkQXZ0Olwi0JLQstC10LTQuNGC0LUg0YHQtdC60YDQtdGC0L3QvtC1INGB0LvQvtCy0L5cIixcbiAgICAgICAgICAgIGZvcmdvdHRlblBhc3N3b3JkTWVzc2FnZTpcItCi0YPRgiDQsdGD0LTQtdGCINCy0LDRiCDQv9Cw0YDQvtC70YxcIixcbiAgICAgICAgfVxuICAgIH1cbn07XG4iLCJleHBvcnQgZGVmYXVsdCB7XG4gICAgZW46eyB0ZXh0OntcbiAgICAgICAgICAgIGNvbnRyb2xQYW5lbDpcIkNvbnRyb2wgUGFuZWxcIixcbiAgICAgICAgICAgIGV4aXRDYWJpbmV0OiBcIkV4aXRcIixcbiAgICAgICAgICAgIGFkZHN0dWRlbnQ6IFwiQWRkIHN0dWRlbnRcIixcbiAgICAgICAgICAgIGxhYmVsTmFtZTogXCJOYW1lXCIsXG4gICAgICAgICAgICBsYWJlbExhc3RuYW1lOiBcIlN1cm5hbWVcIixcbiAgICAgICAgICAgIGxhYmVsQWdlOiBcIkFnZVwiLFxuICAgICAgICAgICAgbGFiZWxDaXR5OiBcIkNpdHlcIixcbiAgICAgICAgICAgIENyZWF0ZTogXCJDcmVhdGVcIixcbiAgICAgICAgICAgIHRhYmxlTmFtZTogXCJOYW1lXCIsXG4gICAgICAgICAgICB0YWJsZUxhc3RuYW1lOiBcIkxhc3RuYW1lXCIsXG4gICAgICAgICAgICB0YWJsZUFnZTogXCJBZ2VcIixcbiAgICAgICAgICAgIHRhYmxlQ2l0eTogXCJDaXR5XCIsXG4gICAgICAgICAgICB0YWJsZUJ1dHRvbnM6IFwiQnV0dG9uXCIsXG4gICAgICAgICAgICBjbGVhcklucHV0OlwiQ2xlYXJcIixcbiAgICAgICAgICAgIHByaW50TGFiZWw6XCJFbnRlciB2YWx1ZVwiLFxuICAgICAgICAgICAgcmVzdWx0TGFiZWw6XCJSZXN1bHRcIixcbiAgICAgICAgICAgIGNvbnZlcnRCdXR0b246XCJDb252ZXJ0XCIsXG4gICAgICAgICAgICBwcmludExhYmVsMjpcIkVudGVyIHZhbHVlXCIsXG4gICAgICAgICAgICByZXN1bHRMYWJlbDI6XCJSZXN1bHRcIixcbiAgICAgICAgICAgIGdldEN1cnJlbmN5OlwiQ29udmVydFwiLFxuICAgICAgICAgICAgY29udmVydGVyVGl0bGU6XCJDb252ZXJ0ZXJcIixcbiAgICAgICAgICAgIGJ0bkNoYW5nZUxlbmd0aDpcIkxlbmd0aCBjb252ZXJ0ZXJcIixcbiAgICAgICAgICAgIGJ0bkNoYW5nZU1vbmV5OlwiTW9uZXkgY29udmVydGVyXCIsXG4gICAgICAgICAgICBtZXRlcjE6XCJNZXRlclwiLFxuICAgICAgICAgICAgdmVyc3QxOlwiVmVyc3RcIixcbiAgICAgICAgICAgIG1pbGUxOlwiTWlsZVwiLFxuICAgICAgICAgICAgZm9vdDE6XCJGb290XCIsXG4gICAgICAgICAgICB5YXJkMTpcIllhcmRcIixcbiAgICAgICAgICAgIG1ldGVyMjpcIk1ldGVyXCIsXG4gICAgICAgICAgICB2ZXJzdDI6XCJWZXJzdFwiLFxuICAgICAgICAgICAgbWlsZTI6XCJNaWxlXCIsXG4gICAgICAgICAgICBmb290MjpcIkZvb3RcIixcbiAgICAgICAgICAgIHlhcmQyOlwiWWFyZFwiLFxuICAgICAgICAgICAgbW9kZXMxOlwiTW9kZXNcIixcbiAgICAgICAgICAgIHN0dWRlbnRzMTpcIlN0dWRlbnRzXCIsXG4gICAgICAgICAgICBwYWludDE6XCJQYWludFwiLFxuICAgICAgICAgICAgY29udmVydGVyMTpcIkNvbnZlcnRlclwiLFxuICAgICAgICAgICAgY2FsY3VsYXRvcjE6XCJDYWxjdWxhdG9yXCIsXG4gICAgICAgICAgICBzdHVkZW50c1RpdGxlOlwiU3R1ZGVudHNcIixcbiAgICAgICAgfSxcbnBsYWNlaG9sZGVyOntcbiAgICBOYW1lOlwiV3JpdGUgbmFtZVwiLFxuICAgIExhc3RuYW1lOlwiV3JpdGUgbGFzdG5hbWVcIixcbiAgICBBZ2U6XCJXcml0ZSBhZ2VcIixcbiAgICBDaXR5OlwiV3JpdGUgY2l0eVwiLFxuICAgIGlucHV0Q29udmVydGVyOlwiRW50ZXIgdmFsdWVcIixcbiAgICBvdXRwdXRDb252ZXJ0ZXI6XCJSZXN1bHRcIixcbiAgICBpbnB1dEN1cnJlbmN5OlwiRW50ZXIgdmFsdWVcIixcbiAgICBvdXRwdXRSZXN1bHQ6XCJSZXN1bHRcIixcbiAgICBjb250clBhbmVsOlwiQ29udHJvbCBQYW5lbFwiLFxuICAgIGxpbmVUaGljazpcIkxpbmUgdGhpY2tuZXNzXCIsXG4gICAgbGluZUNvbDpcIkxpbmUgY29sb3JcIixcbiAgICBjbGVhcjpcIkNsZWFyXCIsXG4gICAgbGFiZWxQYWludDpcIlBhaW50XCIsXG4gICAgdGV4dFNldHRpbmdzOlwiU2V0dGluZ3NcIixcbiAgICBub3RvZmljYXRpb25MYWJlbDpcIk5vdGlmaWNhdGlvbnNcIixcbiAgICBsYW5ndWFnZUxhYmVsOlwiQ2hvb3NlIGxhbmd1YWdlXCIsXG4gICAgcmVzZXQ6XCJSZXNldFwiXG5cbn1cblxuICAgIH0sXG4gICAgcnU6eyB0ZXh0OiB7XG4gICAgICAgICAgICBjb250cm9sUGFuZWw6XCLQn9Cw0L3QtdC70Ywg0YPQv9GA0LDQstC70LXQvdC40Y9cIixcbiAgICAgICAgICAgIGV4aXRDYWJpbmV0OiBcItCS0YvQudGC0LhcIixcbiAgICAgICAgICAgIGFkZHN0dWRlbnQ6IFwi0JTQvtCx0LDQstC40YLRjCDQodGC0YPQtNC10L3RgtCwXCIsXG4gICAgICAgICAgICBsYWJlbE5hbWU6IFwi0JjQvNGPXCIsXG4gICAgICAgICAgICBsYWJlbExhc3RuYW1lOiBcItCk0LDQvNC40LvQuNGPXCIsXG4gICAgICAgICAgICBsYWJlbEFnZTogXCLQktC+0LfRgNCw0YHRglwiLFxuICAgICAgICAgICAgbGFiZWxDaXR5OiBcItCT0L7RgNC+0LRcIixcbiAgICAgICAgICAgIENyZWF0ZTogXCLQodC+0LfQtNCw0YLRjFwiLFxuICAgICAgICAgICAgdGFibGVOYW1lOiBcItCY0LzRj1wiLFxuICAgICAgICAgICAgdGFibGVMYXN0bmFtZTogXCLQpNCw0LzQuNC70LjRj1wiLFxuICAgICAgICAgICAgdGFibGVBZ2U6IFwi0JLQvtC30YDQsNGB0YJcIixcbiAgICAgICAgICAgIHRhYmxlQ2l0eTogXCLQk9C+0YDQvtC0XCIsXG4gICAgICAgICAgICB0YWJsZUJ1dHRvbnM6IFwi0JrQvdC+0L/QutC4XCIsXG4gICAgICAgICAgICBjbGVhcklucHV0Olwi0J7Rh9C40YHRgtC40YLRjFwiLFxuICAgICAgICAgICAgcHJpbnRMYWJlbDpcItCS0LLQtdC00LjRgtC1INC30L3QsNGH0LXQvdC40LVcIixcbiAgICAgICAgICAgIHJlc3VsdExhYmVsOlwi0KDQtdC30YPQu9GM0YLQsNGCXCIsXG4gICAgICAgICAgICBjb252ZXJ0QnV0dG9uOlwi0JrQvtC90LLQtdGA0YLQuNGA0L7QstCw0YLRjFwiLFxuICAgICAgICAgICAgcHJpbnRMYWJlbDI6XCLQktCy0LXQtNC40YLQtSDQt9C90LDRh9C10L3QuNC1XCIsXG4gICAgICAgICAgICByZXN1bHRMYWJlbDI6XCLQoNC10LfRg9C70YzRgtCw0YJcIixcbiAgICAgICAgICAgIGdldEN1cnJlbmN5Olwi0JrQvtC90LLQtdGA0YLQuNGA0L7QstCw0YLRjFwiLFxuICAgICAgICAgICAgY29udmVydGVyVGl0bGU6XCLQmtC+0L3QstC10YDRgtC10YBcIixcbiAgICAgICAgICAgIGJ0bkNoYW5nZUxlbmd0aDpcItCa0L7QvdCy0LXRgNGC0LXRgCDRgNCw0YHRgdGC0L7Rj9C90LjRj1wiLFxuICAgICAgICAgICAgYnRuQ2hhbmdlTW9uZXk6XCLQmtC+0L3QstC10YDRgtC10YAg0LLQsNC70Y7RglwiLFxuICAgICAgICAgICAgbWV0ZXIxOlwi0JzQtdGC0YBcIixcbiAgICAgICAgICAgIHZlcnN0MTpcItCS0LXRgNGB0YLQsFwiLFxuICAgICAgICAgICAgbWlsZTE6XCLQnNC40LvRj1wiLFxuICAgICAgICAgICAgZm9vdDE6XCLQpNGD0YJcIixcbiAgICAgICAgICAgIHlhcmQxOlwi0K/RgNC0XCIsXG4gICAgICAgICAgICBtZXRlcjI6XCLQnNC10YLRgFwiLFxuICAgICAgICAgICAgdmVyc3QyOlwi0JLQtdGA0YHRgtCwXCIsXG4gICAgICAgICAgICBtaWxlMjpcItCc0LjQu9GPXCIsXG4gICAgICAgICAgICBmb290MjpcItCk0YPRglwiLFxuICAgICAgICAgICAgeWFyZDI6XCLQr9GA0LRcIixcbiAgICAgICAgICAgIG1vZGVzMTpcItCc0L7QtNGLXCIsXG4gICAgICAgICAgICBzdHVkZW50czE6XCLQodGC0YPQtNC10L3RglwiLFxuICAgICAgICAgICAgcGFpbnQxOlwi0KDQuNGB0L7QstCw0LvQutCwXCIsXG4gICAgICAgICAgICBjb252ZXJ0ZXIxOlwi0JrQvtC90LLQtdGA0YLQtdGAXCIsXG4gICAgICAgICAgICBjYWxjdWxhdG9yMTpcItCa0LDQu9GM0LrRg9C70Y/RgtC+0YBcIixcbiAgICAgICAgICAgIHN0dWRlbnRzVGl0bGU6XCLQodGC0YPQtNC10L3RgtGLXCIsXG4gICAgICAgICAgICBjb250clBhbmVsOlwi0J/QsNC90LXQu9GMINGD0L/RgNCw0LLQu9C10L3QuNGPXCIsXG4gICAgICAgICAgICBsaW5lVGhpY2s6XCLQotC+0LvRidC40L3QsCDQu9C40L3QuNC4XCIsXG4gICAgICAgICAgICBsaW5lQ29sOlwi0KbQstC10YIg0LvQuNC90LjQuFwiLFxuICAgICAgICAgICAgY2xlYXI6XCLQntGH0LjRgdGC0LjRgtGMXCIsXG4gICAgICAgICAgICBsYWJlbFBhaW50Olwi0KDQuNGB0L7QstCw0LvQutCwXCIsXG4gICAgICAgICAgICB0ZXh0U2V0dGluZ3M6XCLQndCw0YHRgtGA0L7QudC60LhcIixcbiAgICAgICAgICAgIG5vdG9maWNhdGlvbkxhYmVsOlwi0KPQstC10LTQvtC80LvQtdC90LjRj1wiLFxuICAgICAgICAgICAgbGFuZ3VhZ2VMYWJlbDpcItCS0YvQsdGA0LDRgtGMINGP0LfRi9C6XCIsXG4gICAgICAgICAgICByZXNldDpcItCh0LHRgNC+0YHQuNGC0YxcIlxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBOYW1lOlwi0JLQstC10LTQuNGC0LUg0LjQvNGPXCIsXG4gICAgICAgICAgICBMYXN0bmFtZTpcItCS0LLQtdC00LjRgtC1INGE0LDQvNC40LvQuNGOXCIsXG4gICAgICAgICAgICBBZ2U6XCLQktCy0LXQtNC40YLQtSDQstC+0LfRgNCw0YHRglwiLFxuICAgICAgICAgICAgQ2l0eTpcItCS0LLQtdC00LjRgtC1INCz0L7RgNC+0LRcIixcbiAgICAgICAgICAgIGlucHV0Q29udmVydGVyOlwi0JLQstC10LTQuNGC0LUg0LfQvdCw0YfQtdC90LjQtVwiLFxuICAgICAgICAgICAgb3V0cHV0Q29udmVydGVyOlwi0KDQtdC30YPQu9GM0YLQsNGCXCIsXG4gICAgICAgICAgICBpbnB1dEN1cnJlbmN5Olwi0JLQstC10LTQuNGC0LUg0LfQvdCw0YfQtdC90LjQtVwiLFxuICAgICAgICAgICAgb3V0cHV0UmVzdWx0Olwi0KDQtdC30YPQu9GM0YLQsNGCXCIsXG4gICAgICAgIH1cbiAgICB9XG59O1xuXG5cbiIsImltcG9ydCBsYW5ndWFnZUJveEluZGV4IGZyb20gXCIuL2luZGV4TGFuZ3VhZ2VcIjtcbmltcG9ydCBhdXRvclBhZ2UgZnJvbSBcIi4vYXV0b3JpemF0aW9uTGFuZ3VhZ2VcIjtcbmltcG9ydCBhY2NTZXRQYWdlIGZyb20gXCIuL2FjY291bnRTZXR0aW5nc0xhbmd1YWdlXCI7XG5pbXBvcnQgcmVnUGFnZSBmcm9tIFwiLi9yZWdpc3RyYXRpb25MYW5ndWFnZVwiXG5leHBvcnQge2NoYW5nZUxhbmd1YWdlfTtcbmZ1bmN0aW9uIGNoYW5nZUxhbmd1YWdlKHBhZ2UpIHtcbiAgICBpZiAocGFnZSA9PT0gXCJhdXRob3JcIil7XG4gICAgICAgIHZhciBwYWdlQ29sPWF1dG9yUGFnZTtcbiAgICB9IGVsc2UgaWYgKHBhZ2UgPT09IFwiYWNjU2V0XCIpe1xuICAgICAgICB2YXIgcGFnZUNvbD1hY2NTZXRQYWdlO1xuICAgIH0gZWxzZSBpZihwYWdlID09PSBcImluZGV4XCIpe1xuICAgICAgICB2YXIgcGFnZUNvbD1sYW5ndWFnZUJveEluZGV4O1xuICAgIH0gZWxzZSBpZihwYWdlPT09XCJyZWdcIil7XG4gICAgICAgIHZhciBwYWdlQ29sPXJlZ1BhZ2U7XG4gICAgfVxuICAgIHZhciBsYW5ndWFnZSA9IHNlbGVjdEVsZW1lbnRMYW5ndWFnZS52YWx1ZTtcbiAgICBpZiAobGFuZ3VhZ2U9PT1cImFyXCIpe1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRpcmVjdGlvblwiKS5zZXRBdHRyaWJ1dGUoXCJkaXJcIixcInJ0bFwiKTtcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnbGFuZ3VhZ2UnLCBsYW5ndWFnZSk7XG4gICAgICAgIGxhbmd1YWdlPVwiZW5cIjtcbiAgICB9IGVsc2Uge1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRpcmVjdGlvblwiKS5zZXRBdHRyaWJ1dGUoXCJkaXJcIixcImx0bFwiKTtcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnbGFuZ3VhZ2UnLCBsYW5ndWFnZSk7XG4gICAgfVxuICAgIGNoYW5nZVRleHQocGFnZUNvbFtsYW5ndWFnZV0udGV4dCk7XG4gICAgY2hhbmdlUGxhY2VIb2xkZXIocGFnZUNvbFtsYW5ndWFnZV0ucGxhY2Vob2xkZXIpXG59XG5mdW5jdGlvbiBjaGFuZ2VUZXh0KG9iaikge1xuICAgIGZvciAoY29uc3Qga2V5IGluIG9iaikge1xuICAgICAgICBpZihkb2N1bWVudC5nZXRFbGVtZW50QnlJZChrZXkpIT09bnVsbCkge1xuICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoa2V5KS5pbm5lckhUTUwgPSBvYmpba2V5XTtcbiAgICAgICAgfVxuICAgIH1cbn07XG5mdW5jdGlvbiBjaGFuZ2VQbGFjZUhvbGRlcihvYmopIHtcbiAgICBmb3IgKGNvbnN0IGtleSBpbiBvYmopIHtcbiAgICAgICAgaWYoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoa2V5KSE9PW51bGwpIHtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGtleSkucGxhY2Vob2xkZXIgPSBvYmpba2V5XTtcbiAgICAgICAgfVxuICAgIH1cbn07XG4iLCJleHBvcnQgZGVmYXVsdCB7XG4gICAgZW46eyB0ZXh0OntcbiAgICAgICAgICAgIGNsb3NlOiBcIlJldHVybiB0byBsb2dpblwiLFxuICAgICAgICAgICAgcmVnTGFiZWw6IFwiUmVnaXN0cmF0aW9uXCIsXG4gICAgICAgICAgICBsb2dpbkxhYmVsOiBcIkxvZ2luKlwiLFxuICAgICAgICAgICAgcGFzc0xhYmVsOiBcIlBhc3N3b3JkKlwiLFxuICAgICAgICAgICAgcGFzczJMYWJlbDogXCJDb25maXJtIHBhc3N3b3JkKlwiLFxuICAgICAgICAgICAgZW1haWxMYWJlbDogXCJFLW1haWwqXCIsXG4gICAgICAgICAgICBwaG9uZUxhYmVsOiBcIlBob25lIG51bWJlcipcIixcbiAgICAgICAgICAgIGtleXdvcmRMYWJlbDogXCJLZXl3b3JkXCIsXG4gICAgICAgICAgICBrZXl3b3JkTm90aWZpY2F0aW9uOlwiVGhlIGtleXdvcmQgbmVlZGVkIGZvciBwYXNzd29yZCByZWNvdmVyeVwiLFxuICAgICAgICAgICAgcmVnaXN0cmF0aW9uQnRuOlwiU2lnbiB1cFwiLFxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBsb2dpbjpcIkxvZ2luXCIsXG4gICAgICAgICAgICBwYXNzd29yZDE6XCJQYXNzd29yZFwiLFxuICAgICAgICAgICAgcGFzc3dvcmQyOlwiQ29uZmlybSBQYXNzd29yZFwiLFxuICAgICAgICAgICAgZW1haWw6XCJFLW1haWxcIixcbiAgICAgICAgICAgIHBob25lOlwiUGhvbmUgbnVtYmVyXCIsXG4gICAgICAgICAgICBrZXl3b3JkOiBcIktleXdvcmRcIixcbiAgICAgICAgfVxuXG4gICAgfSxcbiAgICBydTp7IHRleHQ6IHtcbiAgICAgICAgICAgIGNsb3NlOiBcItCS0LXRgNC90YPRgtGB0Y8g0Log0LDQstGC0L7RgNC40LfQsNGG0LjQuFwiLFxuICAgICAgICAgICAgcmVnTGFiZWw6IFwi0KDQtdCz0LjRgdGC0YDQsNGG0LjRj1wiLFxuICAgICAgICAgICAgbG9naW5MYWJlbDogXCLQm9C+0LPQuNC9KlwiLFxuICAgICAgICAgICAgcGFzc0xhYmVsOiBcItCf0LDRgNC+0LvRjCpcIixcbiAgICAgICAgICAgIHBhc3MyTGFiZWw6IFwi0J/QvtC00YLQstC10YDQtNC40YLQtSDQv9Cw0YDQvtC70YwqXCIsXG4gICAgICAgICAgICBlbWFpbExhYmVsOiBcItCf0L7Rh9GC0LAqXCIsXG4gICAgICAgICAgICBwaG9uZUxhYmVsOiBcItCi0LXQu9C10YTQvtC90L3Ri9C5INC90L7QvNC10YAqXCIsXG4gICAgICAgICAgICBrZXl3b3JkTGFiZWw6IFwi0KHQtdC60YDQtdGC0L3QvtC1INGB0LvQvtCy0L5cIixcbiAgICAgICAgICAgIGtleXdvcmROb3RpZmljYXRpb246XCLQodC10LrRgNC10YLQvdC+0LUg0YHQu9C+0LLQviDQtNC70Y8g0LLQvtGB0YHRgtCw0L3QvtCy0LvQtdC90LjRjyDQv9Cw0YDQvtC70Y9cIixcbiAgICAgICAgICAgIHJlZ2lzdHJhdGlvbkJ0bjpcItCX0LDRgNC10LPQuNGB0YLRgNC40YDQvtCy0LDRgtGM0YHRj1wiLFxuICAgICAgICB9LFxuICAgICAgICBwbGFjZWhvbGRlcjp7XG4gICAgICAgICAgICBsb2dpbjpcItCS0LLQtdC00LjRgtC1INC70L7Qs9C40L1cIixcbiAgICAgICAgICAgIHBhc3N3b3JkMTpcItCS0LLQtdC00LjRgtC1INC/0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgcGFzc3dvcmQyOlwi0J/QvtCy0YLQvtGA0LjRgtC1INC/0LDRgNC+0LvRjFwiLFxuICAgICAgICAgICAgZW1haWw6XCLQktCy0LXQtNC40YLQtSDQv9C+0YfRgtGDXCIsXG4gICAgICAgICAgICBwaG9uZTpcItCS0LLQtdC00LjRgtC1INGC0LXQu9C10YTQvtC90L3Ri9C5INC90L7QvNC10YBcIixcbiAgICAgICAgICAgIGtleXdvcmQ6IFwi0JLQstC10LTQuNGC0LUg0YHQtdC60YDQtdGC0L3QvtC1INGB0LvQvtCy0L5cIixcbiAgICAgICAgfVxuICAgIH1cbn07IiwiaW1wb3J0IHt3ZWJwYWNrTG9jYWxIb3N0fSBmcm9tIFwiLi9oZWxwZXJzL2NvbnN0XCI7XG5pbXBvcnQge2xvY2FsaG9zdFNlcnZ9IGZyb20gXCIuL2hlbHBlcnMvY29uc3RcIjtcbmltcG9ydCB7Y2hhbmdlTGFuZ3VhZ2V9IGZyb20gXCIuL2hlbHBlcnMvbGFuZ3VhZ2UvbGFuZ3VhZ2UuanNcIjtcbmltcG9ydCBcIi4uL3N0eWxlL2Zvb3Rlci5sZXNzXCI7XG52YXIgY29uc3RhbnRzID0gcmVxdWlyZSgnLi4vLi4vc2VydmVyL2hlbHBlcnMvY29uc3RhbnRzJyk7XG5cbnZhciBzZWxlY3RFbGVtZW50TGFuZ3VhZ2UgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNlbGVjdEVsZW1lbnRMYW5ndWFnZVwiKTtcbnNlbGVjdEVsZW1lbnRMYW5ndWFnZS5vbmNoYW5nZSA9ICgpID0+IHtjaGFuZ2VMYW5ndWFnZShcInJlZ1wiKX07XG5tYWluKCk7XG5cbmZ1bmN0aW9uIG1haW4oKSB7XG4gICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJsYW5ndWFnZVwiKSE9bnVsbCl7XG4gICAgICAgIHNlbGVjdEVsZW1lbnRMYW5ndWFnZS52YWx1ZT1zZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFwibGFuZ3VhZ2VcIik7XG4gICAgICAgIGNoYW5nZUxhbmd1YWdlKFwicmVnXCIpO1xuICAgIH1lbHNle1xuICAgICAgICBjaGFuZ2VMYW5ndWFnZShcInJlZ1wiKTt9XG59XG5cbmxldCByZWdpc3RyYXRpb25CdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInJlZ2lzdHJhdGlvbkJ0blwiKTtcbnJlZ2lzdHJhdGlvbkJ0bi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZ2V0VmFsaWRhdGlvbik7XG5sZXQgZXJyID0gXCJcIjtcbmxldCBVc2VyID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMubG9naW4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImxvZ2luXCIpLnZhbHVlLnJlcGxhY2UoL1xccy9nLCAnJyk7XG4gICAgdGhpcy5wYXNzd29yZCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmQxXCIpLnZhbHVlO1xuICAgIHRoaXMucGFzc3dvcmQyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwYXNzd29yZDJcIikudmFsdWU7XG4gICAgdGhpcy5lbWFpbCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZW1haWxcIikudmFsdWU7XG4gICAgdGhpcy5waG9uZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGhvbmVcIikudmFsdWUucmVwbGFjZSgvWysoKS0vXFxzXS9nLCAnJyk7XG4gICAgdGhpcy5rZXl3b3JkID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJrZXl3b3JkXCIpLnZhbHVlO1xuXG59XG5jb25zb2xlLmxvZyhkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImtleXdvcmRcIikudmFsdWUpO1xuZnVuY3Rpb24gZ2V0VmFsaWRhdGlvbigpIHtcbiAgICBsZXQgZWxlbWVudFZhbHVlID0gbmV3IFVzZXIoKTtcbiAgICBsZXQgbWVzc2FnZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWVzc2FnZVwiKTtcbiAgICBpZiAoY2hlY2tMb2dpbihlbGVtZW50VmFsdWUubG9naW4pICYmIGNoZWNrUGFzcyhlbGVtZW50VmFsdWUucGFzc3dvcmQsIGVsZW1lbnRWYWx1ZS5wYXNzd29yZDIpICYmIGNoZWNrRW1haWwoZWxlbWVudFZhbHVlLmVtYWlsKSAmJiBjaGVja1Bob25lKGVsZW1lbnRWYWx1ZS5waG9uZSwgXCIzODBcIikpIHtcbiAgICAgICAgZGVsZXRlIGVsZW1lbnRWYWx1ZS5wYXNzd29yZDI7XG4gICAgICAgIHJlZ2lzdHJhdGlvbkJ0bi5kaXNhYmxlZD10cnVlO1xuICAgICAgICBsZXQgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgICAgIHhoci5vcGVuKFwiUE9TVFwiLCBgJHtsb2NhbGhvc3RTZXJ2fSR7Y29uc3RhbnRzLnJvdXRpbmcucmVnaXN0cmF0aW9ufWApO1xuICAgICAgICB4aHIuc2V0UmVxdWVzdEhlYWRlcihcIkNvbnRlbnQtdHlwZVwiLCBcImFwcGxpY2F0aW9uL2pzb25cIik7XG4gICAgICAgIHhoci5zZW5kKEpTT04uc3RyaW5naWZ5KGVsZW1lbnRWYWx1ZSkpO1xuICAgICAgICB4aHIub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYgKHhoci5zdGF0dXMgPT09IDQwMCkge1xuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RFbGVtZW50TGFuZ3VhZ2UudmFsdWUgPT09IFwicnVcIikge1xuICAgICAgICAgICAgICAgICAgICBlcnIgPSBcItCi0LDQutC+0Lkg0LvQvtCz0LjQvSDRg9C20LUg0YHRg9GJ0LXRgdGC0LLRg9C10YJcIjtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBlcnIgPSBcIlRoaXMgbG9naW4gYWxyZWFkeSBleGlzdHNcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImVycm9yTG9naW5cIikuaW5uZXJIVE1MID0gZXJyO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibG9naW5cIikuc3R5bGUgPSBcImJvcmRlci1jb2xvcjogIzkwMDtcXG5cIiArIFwiIGJhY2tncm91bmQtY29sb3I6ICNGRERcIjtcbiAgICAgICAgICAgICAgICByZWdpc3RyYXRpb25CdG4uZGlzYWJsZWQ9ZmFsc2U7XG4gICAgICAgICAgICB9IGVsc2UgaWYoeGhyLnN0YXR1cyA9PT0gMjAwKXtcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmlubmVySFRNTCA9IFwi0KDQtdCz0LjRgdGC0YDQsNGG0LjRjyDRg9GB0L/QtdGI0L3QsFwiO1xuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCk9PiBkb2N1bWVudC5sb2NhdGlvbi5ocmVmID0gYCR7d2VicGFja0xvY2FsSG9zdH0vaW5kZXguaHRtbGAsIDE1MDApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB9XG59XG5cbmZ1bmN0aW9uIGNoZWNrTG9naW4obG9naW4pIHtcbiAgICBpZiAobG9naW4ubGVuZ3RoIDwgNSB8fCAhaXNOYU4oTnVtYmVyKGxvZ2luWzBdKSkgfHwgbG9naW4ubWF0Y2goL1s/ISwu0LAt0Y/QkC3Qr9GR0IFcXHNdKyQvaSkgfHwgbG9naW4ubGVuZ3RoID4gMzApIHtcbiAgICAgICBpZihzZWxlY3RFbGVtZW50TGFuZ3VhZ2UudmFsdWU9PT1cInJ1XCIpe2VyciA9IFwi0J3QtdC60L7RgNGA0LXQutGC0L3Ri9C5INC70L7Qs9C40L1cIn1lbHNlXG4gICAgICAge2Vycj1cIkluY29ycmVjdCBsb2dpblwifVxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImVycm9yTG9naW5cIikuaW5uZXJIVE1MPWVycjtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJsb2dpblwiKS5zdHlsZT1cImJvcmRlci1jb2xvcjogIzkwMDtcXG5cIiArXCIgYmFja2dyb3VuZC1jb2xvcjogI0ZERFwiO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG5cbiAgICB9XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJlcnJvckxvZ2luXCIpLmlubmVySFRNTD1cIlwiO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibG9naW5cIikuc3R5bGU9XCJub25lXCI7XG4gICAgcmV0dXJuIHRydWU7XG59XG5cbmZ1bmN0aW9uIGNoZWNrUGFzcyhwYXNzMSwgcGFzczIpIHtcbiAgICBpZiAocGFzczEubGVuZ3RoIDwgNSB8fCBwYXNzMS5sZW5ndGggPiAxNSkge1xuICAgICAgICBpZihzZWxlY3RFbGVtZW50TGFuZ3VhZ2UudmFsdWU9PT1cInJ1XCIpe2VyciA9IFwi0J3QtdC60L7RgNGA0LXQutGC0L3Ri9C5INC/0LDRgNC+0LvRjFwifWVsc2VcbiAgICAgICAge2Vycj1cIkluY29ycmVjdCBwYXNzd29yZFwifVxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImVycm9yUGFzc3dvcmQxXCIpLmlubmVySFRNTD1lcnI7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmQxXCIpLnN0eWxlPVwiYm9yZGVyLWNvbG9yOiAjOTAwO1xcblwiICtcIiBiYWNrZ3JvdW5kLWNvbG9yOiAjRkREXCI7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9IGVsc2UgaWYocGFzczEgIT09IHBhc3MyKXtcbiAgICAgICAgaWYoc2VsZWN0RWxlbWVudExhbmd1YWdlLnZhbHVlPT09XCJydVwiKXtlcnIgPSBcItCf0LDRgNC+0LvQuCDQvdC1INGB0L7QstC/0LDQtNCw0Y7RglwifWVsc2VcbiAgICAgICAge2Vycj1cIlBhc3N3b3JkcyBkbyBub3QgbWF0Y2hcIn1cbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJlcnJvclBhc3N3b3JkMlwiKS5pbm5lckhUTUw9ZXJyO1xuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBhc3N3b3JkMlwiKS5zdHlsZT1cImJvcmRlci1jb2xvcjogIzkwMDtcXG5cIiArXCIgYmFja2dyb3VuZC1jb2xvcjogI0ZERFwiO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmQxXCIpLnN0eWxlPVwibm9uZVwiO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGFzc3dvcmQyXCIpLnN0eWxlPVwibm9uZVwiO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZXJyb3JQYXNzd29yZDFcIikuaW5uZXJIVE1MID0gXCJcIjtcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImVycm9yUGFzc3dvcmQyXCIpLmlubmVySFRNTCA9IFwiXCI7XG4gICAgcmV0dXJuIHRydWU7XG59XG5cbmZ1bmN0aW9uIGNoZWNrUGhvbmUocGhvbmUsIGNvdW50cmlDb2RlKSB7XG4gICAgaWYgKHBob25lLmxlbmd0aCAhPT0gMTIgfHwgcGhvbmUuc3Vic3RyaW5nKDAsIDMpICE9IGNvdW50cmlDb2RlKSB7XG4gICAgICAgIGlmKHNlbGVjdEVsZW1lbnRMYW5ndWFnZS52YWx1ZT09PVwicnVcIil7ZXJyID0gXCLQndC10LrQvtGA0YDQtdC60YLRgNGL0Lkg0YLQtdC70LXRhNC+0L1cIn1lbHNlXG4gICAgICAgIHtlcnI9XCJJbmNvcnJlY3QgcGhvbmVcIn1cbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJlcnJvclBob25lXCIpLmlubmVySFRNTD1lcnI7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGhvbmVcIikuc3R5bGU9XCJib3JkZXItY29sb3I6ICM5MDA7XFxuXCIgK1wiIGJhY2tncm91bmQtY29sb3I6ICNGRERcIjtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBob25lXCIpLnN0eWxlPVwibm9uZVwiO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZXJyb3JQaG9uZVwiKS5pbm5lckhUTUw9XCJcIjtcbiAgICByZXR1cm4gdHJ1ZTtcbn1cbmZ1bmN0aW9uIGNoZWNrRW1haWwoZW1haWwpIHtcbiAgICBpZiAoZW1haWwuaW5jbHVkZXMoXCJAXCIpKSB7XG4gICAgICAgIGxldCBkb21lbiA9IGVtYWlsLnN1YnN0cmluZyhlbWFpbC5pbmRleE9mKFwiQFwiKSwgZW1haWwubGVuZ3RoKTtcbiAgICAgICAgaWYgKGRvbWVuLmxlbmd0aCA+IDIpIHtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZXJyb3JFbWFpbFwiKS5pbm5lckhUTUw9XCJcIjtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZW1haWxcIikuc3R5bGU9XCJub25lXCI7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpZihzZWxlY3RFbGVtZW50TGFuZ3VhZ2UudmFsdWU9PT1cInJ1XCIpe2VyciA9IFwi0J3QtdC60L7RgNGA0LXQutGC0L3Ri9C5IGUtbWFpbFwifWVsc2VcbiAgICB7ZXJyPVwiaW5jb3JyZWN0IGVtYWlsXCJ9XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJlcnJvckVtYWlsXCIpLmlubmVySFRNTD1lcnI7XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJlbWFpbFwiKS5zdHlsZT1cImJvcmRlci1jb2xvcjogIzkwMDtcXG5cIiArXCIgYmFja2dyb3VuZC1jb2xvcjogI0ZERFwiO1xuICAgIHJldHVybiBmYWxzZTtcbn1cbmxldCBjbG9zZUJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2xvc2VcIik7XG5jbG9zZUJ0bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsKCkgPT4ge2RvY3VtZW50LmxvY2F0aW9uLmhyZWYgPSBgJHt3ZWJwYWNrTG9jYWxIb3N0fS9pbmRleC5odG1sYH0pO1xubGV0IGxvZ29Ib21lPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImxvZ29Ib21lXCIpO1xubG9nb0hvbWUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCgpID0+IHtkb2N1bWVudC5sb2NhdGlvbi5ocmVmID0gYCR7d2VicGFja0xvY2FsSG9zdH0vaW5kZXguaHRtbGB9KTtcblxuaW1wb3J0IFwiLi4vLi4vc3JjL3N0eWxlL3JlZ2lzdHJhdGlvbi5sZXNzXCJcbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiJdLCJzb3VyY2VSb290IjoiIn0=